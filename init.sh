#!/usr/bin/env bash

NAME="$(basename "$0")"
help()
{
cat << EOF
${NAME}: Functional Unit Testing Framework Helper Script

${NAME} [-h|--help] [-p] [-o] [-d] [-path P] [-l] [-c] [-m [mark]] [-r TEST] [-D DEVICE] [-allure D] [-tr]

Make sure to execute ${NAME} from within the parent directory!
Commands:
    -h|--help : print this help message
    -p        : Enable live logging of log/stdout/stderr to terminal
    -o        : Disable pytest capture of log/stdout/stderr and print to terminal
    -d        : Debug - increase pytest verbosity level to debug
    -path P   : Filepath to pytest testcase definition file(s) or folder. Default folder: "test/"
            ${NAME} -path test/NM_test.py                   -> single pytest file
            ${NAME} -path test/UT_test.py,test/CM_test.py   -> comma separated list of pytest files
    -l        : Run pytest --listtest to print all collected testcases for selected configuration
    -c        : Run pytest --listconfigs to list all available configurations for selected model
    -m [mark] : Run pytest --markers|-m [mark] to list available markers, or use one or more markers with bool expression
            ${NAME} --markers                               -> list possible markers
            ${NAME} -m "dut_only"                           -> Use a single custom marker
            ${NAME} -m "dut_only and not dfs or require_rc" -> Boolean expression using several custom markers (use quotes)
    -r TEST   : Run single test case, or comma-separated list
            ${NAME} -r test_foo
            ${NAME} -r test_foo,test_bar,test_baz
    -D DEVICE : Run pytest --devices DEVICE to specify comma-separated list of devices to establish connection to. Options: dut, ref, rpi_client.
                Example: ${NAME} -D dut,ref
    -allure D : Run pytest --alluredir X to generate Allure results into directory D.
            ${NAME} -allure allure-results -> allure results will be in allure-results
            ${NAME} -allure /tmp/allure-results -> allure results will be in /tmp/allure-results
    -tr       : Run pytest --transferonly=True to only transfer shell folder onto devices without test execution.
    -nodocker : Disables docker execution (not recommended, debug only)
    -dfb      : Rebuilds docker/Dockerfile image even if exists
    -noshunpack : Disables unpacking of shell tarball from resource/shell/*.tar.bz2

Note: for options "-r, -D, -m. -path" list arguments should be comma-separated, without spaces:
    ${NAME} -r test_foo,test_bar -> ok
    ${NAME} -r test_foo test_bar -> NOT OK

Examples:
  Run all collected tests in pytest folder, enable live terminal logging:
    ${NAME} -p
  Run all collected tests in pytest folder, disable capture:
    ${NAME} -o
  Collect tests pytest folder, run only configurations for test_foo, enable live terminal logging:
    ${NAME} -p -r test_foo
  Collect tests pytest folder, run specified tests test_foo and test_bar with test_config_3, enable live terminal logging:
    ${NAME} -p -r test_foo,test_bar[test_config_3]
  Run all collected tests in pytest folder, create Allure results in allure-results/ folder:
    ${NAME} -allure allure-results
  Collect tests pytest folder, run only configurations for test_bar and test_baz, execute only on dut device:
    ${NAME} -D dut -r test_bar,test_baz
EOF
}

# Globals:
DOCKER_HOME="/tmp/fut-base"
DOCKER_NAME="fut.app"
ENV_FILE="env.list"
FUT_PYTEST_PATH="test/"
PYTEST_CMD="python3 -m pytest"

# Options (defaults):
ALLUREDIR=""
CAPTURE="-rsx"
DBG_FLAG=""
DEVICES=""
DOCKER_REBUILD=""
LOGCLI=""
MARKERS=""
PSEUDO_TTY="--tty -i"
RESULTS=""
SH_UNPACK="true"
TC_LIST=""
USE_DOCKER="true"
VERBOSE="-v"
# Switches
TC_COLLECT=""
TRANSFER_ONLY=""

while [[ "${1}" == -* ]]; do
    option="${1}"
    shift
    case "${option}" in
        -h|--help)
            help
            exit 0
            ;;
        -p)
            LOGCLI="-o log_cli=true"
            ;;
        -o)
            CAPTURE="-rsx --capture=no"
            ;;
        -path)
            FUT_PYTEST_PATH="${1//,/ }"
            shift
            ;;
        -d)
            DBG_FLAG="--dbg"
            ;;
        -r)
            # comma separated list
            TC_LIST="--runtest ${1//,/ }"
            shift
            ;;
        -D)
            # comma separated list
            DEVICES="--devices ${1//,/ }"
            shift
            ;;
        -allure)
            ALLUREDIR="${1}"
            RESULTS="--alluredir=${1}"
            shift
            ;;
        -tr)
            TRANSFER_ONLY="--transferonly"
            ;;
        -l)
            TC_COLLECT="--listtests"
            ;;
        -c)
            TC_COLLECT="--listconfigs"
            ;;
        -m)
            MARKERS="--markers"
            if [[ "${1}" == [^-]* ]]; then
                MARKERS="-m \"${1}\""
                shift
            fi
            ;;
        -nodocker)
            USE_DOCKER="false"
            ;;
        -dfb)
            DOCKER_REBUILD="true"
            ;;
        -notty)
            PSEUDO_TTY=""
            ;;
        -noshunpack)
            SH_UNPACK="false"
            ;;
        -*)
            echo "Unknown argument: ${option}"
            help
            exit 1
            ;;
    esac
done

# Checking env var in framework
export USE_DOCKER
FUT_OPTS="${DEVICES} ${TC_LIST} ${RESULTS} ${CAPTURE} ${VERBOSE} ${DBG_FLAG} ${LOGCLI} ${MARKERS}"
FUT_CMD=""

# Special commands that prevent test execution:
if [[ "${TC_COLLECT}" != "" ]]; then
    FUT_CMD="${PYTEST_CMD} ${FUT_PYTEST_PATH} ${TC_COLLECT} ${DEVICES} ${LOGCLI} ${VERBOSE} ${DBG_FLAG}"
elif [[ "${TRANSFER_ONLY}" != "" ]]; then
    FUT_CMD="${PYTEST_CMD} ${FUT_PYTEST_PATH} ${TRANSFER_ONLY} ${DEVICES} ${LOGCLI} ${VERBOSE} ${DBG_FLAG}"
else
    FUT_CMD="${PYTEST_CMD} ${FUT_PYTEST_PATH} ${FUT_OPTS}"
fi

# enable exit on non 0
set -e

# Unpack shell tarball on request. Ensure single file in target dir
SH_TAR_NAME='resource/shell/*.tar.bz2'
[ "${SH_UNPACK}" == "true" -a "$(ls ${SH_TAR_NAME} 2>/dev/null | wc -l)" == "1" ] &&
    tar -xvjf ${SH_TAR_NAME} -C ./ --transform 's/fut\/shell/shell/'

if [ "$USE_DOCKER" == "true" ]; then
    cmd_file="fut_cmd.sh"
    echo "#!/bin/bash" > ${cmd_file}
    echo "/home/plume/scripts/app.sh &>>/tmp/fut.log &" >> ${cmd_file}
    echo "sleep 5" >> ${cmd_file}
    echo "cd ${DOCKER_HOME} && ${FUT_CMD}" >> ${cmd_file}
    chmod 755 ${cmd_file}

    if [[ $ALLUREDIR != "" ]]; then
        MNT_ALLURE="-v ${ALLUREDIR}:/${ALLUREDIR}"
        mkdir -p "${ALLUREDIR}"
    fi
    USER_CMD="--user $(id -u):$(id -g)"
    MNT_SSH=""
    if [[ -n "$SSH_AUTH_SOCK" ]];then
        MNT_SSH="-v ${SSH_AUTH_SOCK}:${SSH_AUTH_SOCK}"
    fi
    MNT_HOME="-v $(pwd):${DOCKER_HOME}"
    MNT_APP="-v $(pwd)/docker/app:/var/www/app"
    MNT_STATIC_FB="-v $(pwd):/var/www/app/static/fut-base/"
    PORT_APP="-p 0.0.0.0:8000:8000"
    ENV_CMD="--env-file ${ENV_FILE}"
    CAP_ADD="--cap-add=NET_ADMIN --cap-add=NET_RAW"
    NET_HOST="--net=host"
    DOCKER_CMD="docker run ${PSEUDO_TTY} ${USER_CMD} ${MNT_SSH} ${MNT_HOME} ${MNT_ALLURE} ${MNT_APP} ${MNT_STATIC_FB} ${PORT_APP} ${CAP_ADD} ${NET_HOST} ${ENV_CMD} ${DOCKER_NAME} ${DOCKER_HOME}/${cmd_file}"

    cd docker/
    if [ -z "$(docker images -q ${DOCKER_NAME}:latest)" -o "${DOCKER_REBUILD}" == true ]; then
        echo "${NAME}: Rebuilding docker image ${DOCKER_NAME}"
        docker build -t ${DOCKER_NAME} .
    else
        echo "${NAME}: Using existing docker image ${DOCKER_NAME}"
    fi

    echo "Using docker to execute: ${FUT_CMD}"
    echo "${DOCKER_CMD}"
    ${DOCKER_CMD}

    rm ../${cmd_file}
else
    echo "${FUT_CMD}"
    eval "${FUT_CMD}"
fi
