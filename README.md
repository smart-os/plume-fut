# Functional Unit Testing

## Overview

Functional Unit Testing (FUT) verifies individual OpenSync manager
functionalities. FUT tests individual functional units on the devices without
the need for cloud management and full-scale end-to-end testing.

FUT operates at a higher level than pure unit testing of C source code, which
tests function calls and APIs of managers, tools, and libraries. FUT also
operates at a lower level than end-to-end system testing, which relies on cloud
management, as well as device OpenSync integration. System testing takes more
effort to decouple the detected faults in either part under test.

---

## Environment setup

FUT test scripts are implemented as individual shell scripts. They are intended
for execution on the Device Under Test (DUT). These scripts are executable
manually, and are "atomic" - without dependencies, other than setup scripts and
common libraries. These are all part of the code. You can execute these scripts
manually, providing only input parameters.

If you wish to execute the tests with the provided **FUT python framework**,
run the tests from the RPI server device within the FUT testbed. By
providing an official RPI server image, the execution environment is predefined
and tested. This means that all the dependent packages and python3 libraries
are installed in advance. You do not need to install anything by yourself.
The framework is based on the python3 test library `pytest`, with additional
extended implementation.

---

## Quick start

The reference model for testing and verification of FUT scripts is
`PP213X-EX`, also known as Design Reference Kit (DRK). You should use
this model as a baseline, along with configuration files and shell library
overrides.

A "template_model" configuration is also provided. This configuration contains
examples of alternative configuration options, so it is advised to compare your
custom model configurations with this template as well.

If you wish to adapt the configuration for another device model, and the
corresponding configuration files are not yet created:

1. Duplicate and rename the reference model configuration folder, or any
provided folder.
2. Modify the content of configuration files to match the selected device
model.

Go to section "Configuration files" for more details.

For easier use of FUT framework, the script `init.sh` wraps the pytest
library invocation, and provides the common FUT run commands.
The script includes an extensive help that details its use:

```bash
./init.sh -h
./init.sh --help
```

### Test execution

Use the helper script without input parameters to perform the test execution of
all available and configured testcases. The tests are selected and configured
based on the DUT configuration folder setting. This is how you tell the
framework which per-device configuration files to read.

```bash
./init.sh
```

Use helper script to only transfer the files to the device(s) without test
execution. This feature is particularly useful if the user wishes to debug
or manually run scripts on the device.

```bash
./init.sh -tr
```

Use helper script to expose the pytest markers facility. If used without
parameters, all available markers are listed - default and custom ones. If used
with input parameters, the markers provided are used for test execution and
filtering.

```bash
# List available markers
./init.sh -m
# Use a single marker or a boolean expression of several markers (use quotes)
./init.sh -m "dut_only"
./init.sh -m "dut_only and not dfs or require_rc"
```

### Collection and execution filtering

Use helper script to specify the path to the pytest testcase definition file(s)
or folder(s), defaulting to `test/`. This way, you can manipulate the
collection of test cases by providing only a select suite of testcases,
determined by the provided folder or file(s). If several files are listed, use
comma separated notation.

```bash
./init.sh -path test/NM_test.py
./init.sh -path test/UT_test.py,test/CM_test.py
```

Use helper script to list all collected testcases for the selected
configuration. This is useful in combination with the `-r` flag for specifying
the list of testcases to execute, instead of all available testcases (by
default).

```bash
./init.sh -l
```

Use helper script to list all available configurations for the selected model.
This is similar to `-l` option, but prints each parametrized testcase
separately, instead of only once. For example, one testcase can be executed
several times with different input parameters. All options are listed
here.

```bash
./init.sh -c
```

Use helper script to define a subset of the default set of testcases. Specify
the testcase selection by names or by names and input parameter configuration.
If specifying several testcases, use comma separated notation.

```bash
./init.sh -r test_foo
./init.sh -r test_bar[config_0]
./init.sh -r test_foo,test_bar,test_baz[config_0],test_baz[config_4]
```

### Specifying devices

Use helper script to define a subset of currently available or specific FUT
testbed devices. This can be useful if you wish to restrict the testcase
execution only to the DUT. If specifying several devices, use comma separated
notation.

```bash
./init.sh -D dut
./init.sh -D dut,ref
```

### Debug

Use helper script to collect and execute all tests configured for the selected
model. You can also enable live logging of log/stdout/stderr to the terminal.
Pytest usually captures everything going to stdout and stderr with the purpose
of report generation and customizable logging. This option also allows you to
see the same captured content on the terminal.

```bash
./init.sh -p
```

Use helper script to enable the debug mode: increase pytest logging verbosity
level to debug and change the logging format to provide more information. You
can combine this flag with the `-p` flag to enable both features
simultaneously.

```bash
./init.sh -d
```

Use helper script to disable pytest pytest capture of stdout and stderr and
print directly to terminal. This can aid manual execution, because all shell
printout is directly visible in the terminal during the test run. This option
can be combined with the `-p` or `-d` flag. The effect of this option is
however, that in case result generation is enabled, no captured content will
be present in the report.

```bash
./init.sh -o
```

Additionally, use helper script to collect WM_test.py, and execute all tests as
configured for the selected model. You can also disable pytest capture and
testbed validation.

### Reporting

Use helper script to collect and execute all available tests configured for the
selected model, and also generate pytest results in the `allure` package
friendly format. The results are stored in the specified folder.

```bash
./init.sh -allure allure-results
```

The `allure` package visualizes the FUT test execution results.
Generating and viewing the report (not to be confused with the results) is out
of scope for this file.

---

## Configuration files

FUT framework allows you to configure testcase execution
without the need to change any test shell scripts. There are several
editable configuration files that serve as inputs for
both - Python framework and shell test scripts. Configure these files as
needed.

The configuration files are grouped into three types:

- **testbed:** General settings, applicable to the specific testbed instance,
for example: username and password, management IP, configuration folder name
- **device:** per-model, device-specific information, for example: regulatory
domain information, radio interface names and bands, model string, timeout
- **testcase:** several per-model files, each containing input parameters for
a suite of shell test scripts

Configuration files are python3 dictionaries. These files are directly sourced
by the Python framework.

This is how the directory structure looks like:

```bash
fut-base/config/
|-- model
|   `-- PP213X_EX_2_0
|       |-- device
|       |   `-- config.py
|       `-- testcase
|           |-- BRV_config.py
|           |-- CM_config.py
|           |-- NM_config.py
|           |-- ONBRD_config.py
|           |-- SM_config.py
|           |-- UT_config.py
|           |-- WM_config.py
|           `-- hello_world_config.py
|
|-- rules
|   `-- regulatory.py
`-- testbed
    `-- config.py
```

### Testbed configuration

FUT framework can not be generalized in advance to work with the cases, in
which the device models and configurations vary widely. Providing information
about the testbed setup is essential. The testbed configuration file location
is `config/testbed/config.py`.

The parameters in the testbed configuration file are applicable to the devices
present in the testbed. They define how to configure and access the testbed
devices. The file also contains code comments. These comments explain what each
configuration option means.

File content example:

```python
testbed_cfg = {
    "default": {
        "fut_dir": "/tmp/fut-base",
        "reconnect_attempts": 10,
        "reconnect_sleep": 10,
    },
    ...
    "server": {
        "host": "192.168.4.1",
        "username": "plume",
        "password": "plume",
        "rsa": {
            "key_path": "/home/plume/.ssh/id_rsa",
            "key_pass": "plume",
        },
        "curl": {
            "host": "http://192.168.4.1/"
        },
        "mgmt_vlan": 1,
        "tb_role": "server",
    },
    "dut": {
        "host": "192.168.4.10",
        "mgmt_vlan": 4,
        "mgmt_ip": "192.168.4.10",
        "wan_ip": "192.168.200.10",
        "CFG_FOLDER": "PP213X_EX_2_0",
        "tb_role": "gw",
    },
    "ref": {
        "host": "192.168.4.11",
        "mgmt_vlan": 4,
        "mgmt_ip": "192.168.4.11",
        "wan_ip": "192.168.200.11",
        "CFG_FOLDER": "PP213X_EX_2_0",
        "tb_role": "leaf1",
    ...
}
```

Customize the device management IP and VLAN:

```python
testbed_cfg = {
    ...
    "dut": {
        "host": "192.168.2.1",
        "mgmt_vlan": 1,
        "mgmt_ip": "192.168.2.1",
        "wan_ip": "192.168.200.10",
        "CFG_FOLDER": "PP213X_EX_2_0",
        "tb_role": "gw",
    },
    ...
}
```

Customize the device configuration folder:

```python
testbed_cfg = {
    ...
    "dut": {
        "host": "192.168.4.10",
        "mgmt_vlan": 4,
        "mgmt_ip": "192.168.4.10",
        "wan_ip": "192.168.200.10",
        "CFG_FOLDER": "my_opensync_device",
        "tb_role": "gw",
    },
    ...
}
```

### Device configuration

The device configuration file contains device-specific per-model information,
that is required for successful test parametrization. Such information includes
radio interface names and bands, regulatory domain information, model string,
specific tool paths, etc.
The file location is: `config/model/<my_model>/device/config.py`.
The file also contains code comments on what each configuration option means.
If some configuration entries are not applicable for your particular device
model, the value should be set to "None" (without quotations).

Device interface configuration examples for dual-band devices (two radios):

```python
device_cfg = {
    'backhaul_ap': {
        '24g': 'wl1.1',
        '5g': 'wl0.1',
        '5gl': None,
        '5gu': None,
    },
    'backhaul_sta': {
        '24g': 'wl1',
        '5g': 'wl0',
        '5gl': None,
        '5gu': None,
    },
    'device_type': 'extender',
    'home_ap': {
        '24g': 'wl1.2',
        '5g': 'wl0.2',
        '5gl': None,
        '5gu': None,
    },
    'interface_name_eth_lan': None,
    'interface_name_eth_wan': 'eth0',
    'interface_name_lan_bridge': 'br-home',
    'interface_name_ppp_wan': 'ppp-wan',
    'interface_name_wan_bridge': 'br-wan',
    'model_string': 'OpenSyncExtender',
    'mtu_backhaul': 1552,
    'mtu_uplink_gre': 1514,
    'mtu_wan': 1500,
    'onboard_ap': {
        '24g': 'wl1.3',
        '5g': 'wl0.3',
        '5gl': None,
        '5gu': None,
    },
    'patch_port_lan-to-wan': 'patch-h2w',
    'patch_port_wan-to-lan': 'patch-w2h',
    'phy_radio_name': {
        '24g': 'wl1',
        '5g': 'wl0',
        '5gl': None,
        '5gu': None,
    },
    'radio_antennas': {
        '24g': '2x2',
        '5g': '4x4',
        '5gl': None,
        '5gu': None,
    },
    'radio_channels': {
        '24g': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        '5g': [36, 40, 44, 48, 52, 56, 60, 64, 100, 104, 108, 112, 116, 120,
               124, 128, 132, 136, 140, 144, 149, 153, 157, 161, 165],
        '5gl': None,
        '5gu': None,
    },
    'regulatory_domain': 'US',
    'um_fw_download_path': '/tmp/pfirmware',
    'uplink_gre': {
        '24g': 'g-wl1',
        '5g': 'g-wl0',
        '5gl': None,
        '5gu': None,
    },
}
```

Device interface configuration examples for tri-band devices (three radios):

```python
device_cfg = {
    'backhaul_ap': {
        '24g': 'bhaul-ap-24',
        '5g': None,
        '5gl': 'bhaul-ap-l50',
        '5gu': 'bhaul-ap-u50',
    },
    'backhaul_sta': {
        '24g': 'bhaul-sta-24',
        '5g': None,
        '5gl': 'bhaul-sta-l50',
        '5gu': 'bhaul-sta-u50',
    },
    'device_type': 'extender',
    'home_ap': {
        '24g': 'home-ap-24',
        '5g': None,
        '5gl': 'home-ap-l50',
        '5gu': 'home-ap-u50',
    },
    'interface_name_eth_lan': 'eth1',
    'interface_name_eth_wan': 'eth0',
    'interface_name_lan_bridge': 'br-home',
    'interface_name_ppp_wan': 'ppp-wan',
    'interface_name_wan_bridge': 'br-wan',
    'model_string': 'OpenSyncExtender',
    'mtu_backhaul': 1600,
    'mtu_uplink_gre': 1562,
    'mtu_wan': 1500,
    'onboard_ap': {
        '24g': 'onboard-ap-24',
        '5g': None,
        '5gl': 'onboard-ap-l50',
        '5gu': 'onboard-ap-u50',
    },
    'patch_port_lan-to-wan': 'patch-h2w',
    'patch_port_wan-to-lan': 'patch-w2h',
    'phy_radio_name': {
        '24g': 'wifi0',
        '5g': None,
        '5gl': 'wifi1',
        '5gu': 'wifi2',
    },
    'radio_antennas': {
        '24g': '2x2',
        '5g': None,
        '5gl': '2x2',
        '5gu': '4x4',
    },
    'radio_channels': {
        '24g': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        '5g': None,
        '5gl': [36, 40, 44, 48, 52, 56, 60, 64],
        '5gu': [100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144,
                149, 153, 157, 161, 165],
    },
    'regulatory_domain': 'EU',
    'um_fw_download_path': '/tmp/pfirmware',
    'uplink_gre': {
        '24g': 'g-bhaul-sta-24',
        '5g': None,
        '5gl': 'g-bhaul-sta-l50',
        '5gu': 'g-bhaul-sta-u50',
    },
}
```

Example of regulatory domain (country code) setting. This is important and must
match the actual configuration of the device, because channel validity is based
on the regulatory information. Currently supported options are: `US` and `EU`.

```python
device_cfg = {
    ...
    'regulatory_domain': 'US'
}
```

Device shell environment configuration, where tool paths are defined among
other things:

```python
shell_cfg = {
    'FUT_TOPDIR': '/tmp/fut-base',
    'OPENSYNC_ROOTDIR': "/usr/opensync",
    'LIB_OVERRIDE_FILE': ('{}/shell/lib/override/opensync_extender_lib_override.sh',
                          'fut_dir'),
    'DEFAULT_WAIT_TIME': ('{}', 'test_script_timeout'),
    'OVSH': ('{}/tools/ovsh --quiet --timeout={}000',
             'opensync_rootdir',
             'test_script_timeout'),
    'LOGREAD': 'cat /var/log/messages',
    'MODEL': 'OpenSyncExtender',
}
```

Alternatively, some values can be hardcoded instead of remaining runtime
configurable:

```python
shell_cfg = {
...
    'LIB_OVERRIDE_FILE': '/usr/opensync/shell/lib/override/opensync_extender_lib_override.sh',
    'DEFAULT_WAIT_TIME': 80,
    'OVSH': '/usr/opensync/tools/ovsh --quiet --timeout=50000',
...
}
```

### Testcase configuration

The folder `config/model/<my_model>/testcase/` contains configuration files
with the filename format `*_config.py`. Each of the configuration files
contains settings specific for one suite of tests, defined in the corresponding
pytest helper script `test/*_test.py`.
For example, here are the configuration and pytest test collection scripts,
both corresponding to NM - network manager:

```bash
./config/model/<my_model>/testcase/NM_config.py
./test/NM_test.py
```

See below a demonstrative example of the content for one of the testcase
suites. Names and values are symbolic.

```python
test_cfg = {
    "simple_testcase_without_config": [
        {},
    ],
    "echo_input_parameter_to_stdout": {
        "input_parameter": "Hello world!",
        "test_script_timeout": 42 # Timeout after 42 seconds
    },
    "test_in_development": {
        "value" : 42,
        "skip": True,
        "skip_msg": "Test implementation pending"
    },
    ...
}
```

---

## Developer notes

FUT test scripts, framework and configuration files are designed to allow the
users of FUT tests (QA engineers or OpenSync integrator) to validate their
work. For example, to execute the existing testcases without changing them.
This means that the **user of FUT tests** is the one who provides the input
parameters and runs the existing tests, while it is not necessary to develop
new scripts or change the source code.

**Developers of FUT test cases** and **writers of FUT shell scripts** on the
other hand are those who create the testcase definitions, and design and
implement the test script. It is ideal for the developers of new OpenSync
functionalities to be the ones who also provide the definitions of test cases
(e.g., bullet points or text paragraph), and also design and implement the
shell script due to their knowledge of the newly developped feature.

### Basic FUT postulates

1. **Shell scripts are atomic.** The user must be able to manually execute the
test shell scripts, and execute or source setup scripts and shared libraries,
provided that input parameters are known. This both separates the testcase
implementation in shell from the python framework assistive facilities, as well
as lower the barrier for entry to developers of new test scripts.
2. **Code is modular.** Use of shared shell libraries and common functions is
encouraged. This makes testcases more flexible, if some functions in the test
script must be overloaded, due to the unique per-model properties.
3. **Code is portable.** Test scripts, common libraries and shell functions
must be implemented in such a way, that correct code execution is possible on
several device models, despite the differences in execution environments. This
can be achieved by making test procedures modular and functions overloadable.
This creates the opportunity to specialize certain function calls in the
`lib/override` folder.
4. **Framework should only assist the user.** There must be no testcase
implementation in Python. Only shell is allowed. This can be tested if the test
shell scripts are executable and fully functional without the Python framework.
The framework should only make it easier to execute the tests en-masse, by
handling the management ssh, copying files, collecting results, and generating
the reports.

### Development process

To reduce the entry barrier for developers who wish to contribute the testcases
and implement the shell scripts, **developers must provide:**

1. _Definition of the testcase, listed dependencies and environment specifics._
2. _Test shell script, including comments and parameter explanation._
3. _Example of use_ like the invocation with example input parameters.

Once the test script is developed and is executable on the device, the
_FUT maintainers_ can help with integrating the shell script into the Python
framework, create the testcase configuration entries, and test the execution.

The following sections detail the individual stages of the development process.

#### Testcase definition

Testcase definition is the first step of developing a new FUT testcase, because
it is the _"source of truth"_ throughout the entire development process. In all
future conflicts, for example discovered bugs in the test script
implementation, the testcase definition is the reference, and others have to
change or adapt.

This makes the testcase definition of paramount importance from the start, and
must be executed flawlessly. Any change in the definition must result in the
restart of the entire process.This ensures proper quality level.

Testcase definition is done in writing, either bullet points or descriptively.
The definition must contain:

- Description of the environment, necessary for testing
- Description of the subject of testing: _what is being tested_
- All the setup steps needed _before_ the test
- The actual test step or several steps
- Definition of expected results after each step
- Any teardown steps needed _after_ the test (e.g., for cleanup)

It is recommended that the available tools or software is used. TestRail for
example.

Let us begin with an arbitrary, easy to understand example. The testcase name
is **"Verify tool ifconfig is present on device"**. The definition for
such a testcase might look like this:

- Prerequisites:
  - Ensure administrative or sudo privilege on device
  - Ensure unrestricted shell access
  - Management SSH connectivity to device
- Test steps:
  - Check if tool `ifconfig` is present on system
  - Check if tool `ifconfig` is in `PATH`
- Expected results:
  - Tool `ifconfig` is present on the device
  - Tool `ifconfig` is in `PATH` on location `/path/to/ifconfig`

#### Shell script design and implementation

Before starting to write code, a _test script design_ should be created. In
this step, the developer converts the bullet points or description of the
testcase definition into more detailed steps that become the shell code. The
design for each individual test script must be committed into the team
collaboration software, like Jira, to be made available to others.

The use of shared shell libraries and common functions is encouraged during the
design phase, as this ensures code sharing and code modularity. Examples
of code already exist for developers to ovserve, explore folder `shell/`.

During implementation, the test shell scripts must be completed with code
comments, input parameter explanations, and examples of use like invocation
with example input parameters. Optionally, setup and teardown step requirements
are listed.

Let us continue with the example from the previous chapter. The test script
design is noted in the team collaboration software tools. Here, we skip to the
script implementation.

For future testcase grouping, let us also define the
suite of tests, named _Entity_ tests. Therefore we prepend our
filenames accordingly, and place them inside the `shell/tests/entity` folder.

There are two steps: the **setup** and the **test** step. The setup script
`shell/tests/entity/entity_setup.sh` ensures that the _prerequisites_ from
the testcase definition are met. The script might look like this:

```bash
#!/bin/sh

# Include environment config from default shell file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
# Include shared libraries and library overrides
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${FUT_TOPDIR}/shell/lib/entity_lib.sh
source ${LIB_OVERRIDE_FILE}

tc_name="entity/$(basename "$0")"

entity_setup_env &&
    log "$tc_name: entity_setup_env - Success " ||
    raise "entity_setup_env - Failed" -l "$tc_name" -ds

exit 0
```

You may have noticed that there is a call to source the `lib/entity_lib.sh`
script. This common script should contain all the functions that several test
scripts might want to share and reuse. You may have also noticed that the only
function call is to `entity_setup_env`, which coincidentally is
implemented inside the `lib/entity_lib.sh` script. Here is how this script
might look like:

```bash
#!/bin/sh

# Include environment config from default shell file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source ${FUT_TOPDIR}/shell/config/default_shell.sh
fi
# Include shared libraries and library overrides
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${LIB_OVERRIDE_FILE}

check_isnot_sudo()
{
    sudo -l -U $USER | egrep -q 'not allowed to run sudo'
    rv=$?
    return $rv
}

entity_setup_env()
{
    fn_name="entity_lib:entity_setup_env"
    log -deb "${fn_name} - Running ENTITY setup"

    # Ensure administrative or sudo privilege on device
    check_isnot_sudo &&
        raise "$USER lacks sudo privilege" -l "$fn_name" -ds

    return 0
}

is_tool_on_system()
{
    command -v $1
    return $?
}
```

The code above is a good example of library functions that are easily
overloadable. The function `entity_setup_env` is called by the setup script,
and inside is a call to another function `check_isnot_sudo`, which may be
implemented differently on another model or platform. In that case, the user
can override any default function from the file
`lib/override/<my_device>_lib_override.sh`.

The test script contains the actual test steps, outlined in the testcase
definition. The filename is for example
`shell/tests/entity/entity_tool_present_on_device.sh` and the script might
look like this:

```bash
#!/bin/sh

# Include environment config from default shell file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source ${FUT_TOPDIR}/shell/config/default_shell.sh
fi
# Include shared libraries and library overrides
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${FUT_TOPDIR}/shell/lib/entity_lib.sh
source ${LIB_OVERRIDE_FILE}

tc_name="$(basename $0)"
usage()
{
cat << EOF
${tc_name} [-h] tool_name
where options are:
    -h  show this help message
input arguments:
    tool_name=$1 -- tool name to check presence on system - (string)(required)
this script is dependent on following:
    - running: entity_setup.sh
example of usage:
   ${tc_name} "ls"
   ${tc_name} "bc"
EOF
exit 1
}

while getopts h option; do
    case "$option" in
        h)
            usage
            ;;
    esac
done

NARGS=1
if [ $# -lt ${NARGS} ]; then
    raise "Requires at least '${NARGS}' input argument(s)" -l "${tc_name}" -arg
fi
tool_name=$1
log_title "${tc_name}: Verify tool ${tool_name} is present on device"

is_tool_on_system ${tool_name} &&
    log -deb "${tc_name}: tool ${tool_name} present on device" ||
    die "${tc_name}: tool ${tool_name} not present on device"

pass
```

After all the shell scripts are created, they should be manually executable and
should "pass". The job of the developer may end here, and the FUT maintainers
may take over the following steps, or the developer may choose to continue
by themselves.

#### Framework integration and configuration creation

The framework is based around the testing library for python3 `pytest`.
Therefore, you must create a _"test suite"_ file. This file uses pytest
fixtures to define the testcases and exposes them to the user.

To create a new suite of tests named `ENTITY`, create `ENTITY_test.py` inside
the folder `test/`. This file is a pytest wrapper that maps shell
scripts to pytest _testcases_. The pytest file should have the following
content:

```python
from framework.tools.functions import get_command_arguments
from .globals import ExpectedShellResult
from .globals import FutMain
import pytest


@pytest.fixture(autouse=True, scope="module")
def brv_initial_setup(dut_handler):
    assert dut_handler.transfer(manager='entity')
    assert dut_handler.run('tests/entity/entity_setup') == ExpectedShellResult
    yield

# Read entire testcase configuration
entity_config = FutMain.get_test_config(cfg_file_prefix='ENTITY')

test_entity_tool_present_on_device_args =
    entity_config.get('entity_tool_present_on_device', [])
@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_entity_tool_present_on_device_args)
def test_entity_tool_present_on_device(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['tool_name'],
    )
    assert dut_handler.run('entity/entity_tool_present_on_device', test_args)
        == ExpectedShellResult
```

There is a lot going on in the example above. The first important aspect is the
so called "module fixture" `entity_initial_setup`. This is the way in which
the FUT framework replaces manually transferring all files to device(s), and
running the setup script as initialization. The setup script is only run once
if the user wishes to run several testcases sequentially, thus saving the
overhead time.

The second important part is the testcase function definition
`test_entity_tool_present_on_device`. If the function name starts with
`test_*`, pytest automatically collects it as an available testcase.
Within the function, there is the input argument collection, and then the
remote execution of test shell script on the device, where the correct result
is asserted.

The third important part is the configuration collection and test function
parametrization. The variable `entity_config` contains all the input parameters
for all test functions, including our example
`test_entity_tool_present_on_device`. These input arguments are collected from
the _testcase configuration file_, that was explained in the previous section.

Here is how you add a testcase configuration file, or testcase configuration
entry to an existing file. Create `ENTITY_config.py` file into the folder
`config/model/<my_model>/testcase/`, or open if already created. The file
content might look like this:

```python
test_cfg = {
    "entity_tool_present_on_device": [
        {
            "tool_name": "ifconfig",
        },
    ],
    ...
}
```

If the testcase definition requires us to test for several tools, not only the
`ifconfig` tool, you can achieve this without changing any testcase or
framework implementation. Pytest parametrization `@pytest.mark.parametrize`
enables the user to quickly add more configurations to the testcase config
file:

```python
test_cfg = {
    "entity_tool_present_on_device": [
        {
            "tool_name": "ifconfig",
        },
        {
            "tool_name": "echo",
        },
        {
            "tool_name": "ls",
        },
    ],
    ...
}
```

---

### Testing your work

This completes the entire integration process from start to finish. The user
can now verify the developed testcase as part of the entire test run,
individually, or as specified by the test configuration:

```bash
./init.sh  -r entity_tool_present_on_device
./init.sh  -r entity_tool_present_on_device[config_0],entity_tool_present_on_device[config_2]
```
