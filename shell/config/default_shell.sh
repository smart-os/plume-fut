#!/bin/sh


if [ -z "$FUT_TOPDIR" ]; then
    export FUT_TOPDIR="/tmp/fut-base"
fi

if [ -z "$OPENSYNC_ROOTDIR" ]; then
    export OPENSYNC_ROOTDIR="/usr/opensync"
fi

if [ -z "$LOGREAD" ]; then
    export LOGREAD="cat /var/log/messages"
fi

if [ -z "$DEFAULT_WAIT_TIME" ]; then
    export DEFAULT_WAIT_TIME=30
fi

if [ -z "$OVSH" ]; then
    export OVSH="${OPENSYNC_ROOTDIR}/tools/ovsh --quiet --timeout=180000"
fi

if [ -z "$OVSH_FAST" ]; then
    export OVSH_FAST="${OPENSYNC_ROOTDIR}/tools/ovsh --quiet --timeout=10000"
fi

if [ -z "$OVSH_SLOW" ]; then
    export OVSH_SLOW="${OPENSYNC_ROOTDIR}/tools/ovsh --quiet --timeout=50000"
fi

if [ -z "$LIB_OVERRIDE_FILE" ]; then
    export LIB_OVERRIDE_FILE="/tmp/fut-base/shell/config/empty.sh"
fi

if [ -z "$PATH" ]; then
    export PATH="/bin:/sbin:/usr/bin:/usr/sbin:${OPENSYNC_ROOTDIR}/tools:${OPENSYNC_ROOTDIR}/bin"
fi
