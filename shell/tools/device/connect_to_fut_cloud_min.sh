#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/lib/cm2_lib.sh
source ${LIB_OVERRIDE_FILE}

log "tools/device/$(basename "$0"): Connect device to FUT Cloud on RPI server"

connect_to_fut_cloud &&
    log "tools/device/$(basename "$0"): Device connected to FUT Cloud" ||
    die "tools/device/$(basename "$0"): Failed to connect to FUT Cloud"

exit 0
