#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${FUT_TOPDIR}/shell/lib/nm2_lib.sh
source ${LIB_OVERRIDE_FILE}

log "tools/device/$(basename "$0"): Creating Inet entry"
create_inet_entry "$@" &&
    log "tools/device/$(basename "$0"): create_inet_entry - Success" ||
    raise "create_inet_entry - Failed" -l "tools/device/$(basename "$0")" -tc

exit 0
