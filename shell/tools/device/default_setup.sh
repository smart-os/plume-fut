#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${LIB_OVERRIDE_FILE}

tc_name="tools/device/$(basename $0)"
help()
{
cat << EOF
${tc_name} [-h]

This script returns the device into a default state, that should be equal to the state right after boot.
EOF
raise "Printed help" -l "$tc_name" -arg
}

while getopts h option; do
    case "$option" in
        h)
            help
            ;;
    esac
done


log "${tc_name}: Device Default Setup"

cm_disable_fatal_state &&
    log -deb "${tc_name} Success: cm_disable_fatal_state" ||
    raise "Failed: cm_disable_fatal_state" -l "${tc_name}" -ds

disable_watchdog &&
    log -deb "${tc_name} Success: disable_watchdog" ||
    raise "Failed: disable_watchdog" -l "${tc_name}" -ds

stop_healthcheck &&
    log -deb "${tc_name} Success: stop_healthcheck" ||
    raise "Failed: stop_healthcheck" -l "${tc_name}" -ds

restart_managers
log -deb "${tc_name}: Executed restart_managers, exit code: $?"

exit 0
