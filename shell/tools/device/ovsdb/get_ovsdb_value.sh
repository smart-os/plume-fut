#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${LIB_OVERRIDE_FILE}


ovsdb_table=${1}
ovsdb_field=${2}
ovsdb_where_field=${3}
ovsdb_where_value=${4}
ovsdb_raw=${5:-false}
ovsdb_opt=''
ovsdb_where=''

if [ "$ovsdb_raw" == 'true' ]; then
    ovsdb_opt='-raw'
fi

if [ -n "$ovsdb_where_field" ] && [ -n "$ovsdb_where_value" ]; then
    ovsdb_where="-w $ovsdb_where_field $ovsdb_where_value"
fi

get_ovsdb_entry_value "$ovsdb_table" "$ovsdb_field" $ovsdb_where ${ovsdb_opt} ||
    raise "Failed to retrive ovsdb value {$ovsdb_table:$ovsdb_field $ovsdb_where}" -l "$(basename "$0")" -oe
