#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/cm2_lib.sh
source ${LIB_OVERRIDE_FILE}


address=$1
type=$2

log "tools/device/$(basename "$0"): Manipulating traffic for $address - $type"

manipulate_iptables_address "$type" "$address"
    log "tools/device/$(basename "$0"): manipulate_iptables_address - Success" ||
    die "tools/device/$(basename "$0"): manipulate_iptables_address - Failed"

exit 0
