#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/cm2_lib.sh
source ${LIB_OVERRIDE_FILE}


type=$1

log "tools/device/$(basename "$0"): Manipulating DNS and SSL traffic - $type"

manipulate_iptables_protocol "$type" DNS
    log "tools/device/$(basename "$0"): manipulate_iptables_protocol - Success" ||
    die "tools/device/$(basename "$0"): manipulate_iptables_protocol - Failed"

manipulate_iptables_protocol "$type" SSL
    log "tools/device/$(basename "$0"): manipulate_iptables_protocol - Success" ||
    die "tools/device/$(basename "$0"): manipulate_iptables_protocol - Failed"

exit 0
