#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/cm2_lib.sh
source ${LIB_OVERRIDE_FILE}


if_name=$1
should_get_address=$2

log "tools/device/$(basename "$0"): Starting udhcpc on interface $if_name"
start_udhcpc $if_name $should_get_address &&
    log "tools/device/$(basename "$0"): start_udhcpc - Success" ||
    raise "start_udhcpc - Failed" -l "tools/device/$(basename "$0")" -tc

exit 0
