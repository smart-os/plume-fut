#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/wm2_lib.sh
source ${LIB_OVERRIDE_FILE}

usage="$(basename "$0") [-h] \$1 \$2

where arguments are:
    if_name=\$1 -- if_name in Wifi_Radio_Config for which interface to change channel (string)(required)
    channel=\$2 -- channel to set for if_name (int)(required)

Script is used to set the channel for interface if_name in Wifi_Radio_Config table

Script does following:
    - update channel column in Wifi_Radio_Config table where if_name
    - wait for changes to reflect into Wifi_Radio_State table

Dependent on:
    - running WM/NM managers - min_wm2_setup.sh - existance of active interfaces

Example of usage:
    $(basename "$0") wifi1 49 - change channel to 49 for interface if_name wifi1
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [[ $# -lt 2 ]]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

trap 'run_setup_if_crashed wm || true' EXIT SIGINT SIGTERM

if_name=$1
channel=$2

change_channel "$if_name" "$channel" ||
    raise "change_channel - Failed" -l "tools/device/$(basename "$0")" -tc

exit 0
