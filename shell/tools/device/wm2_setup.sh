#!/bin/sh

# Setup test environment for WM tests.

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${FUT_TOPDIR}/shell/lib/wm2_lib.sh
source ${LIB_OVERRIDE_FILE}

wm_setup_test_environment "$@" &&
    log "tools/device/$(basename "$0"): wm_setup_test_environment - Success " ||
    raise "wm_setup_test_environment - Failed" -l "tools/device/$(basename "$0")" -tc

exit 0
