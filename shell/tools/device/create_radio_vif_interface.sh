#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${FUT_TOPDIR}/shell/lib/wm2_lib.sh
source ${LIB_OVERRIDE_FILE}

trap 'run_setup_if_crashed wm || true' EXIT SIGINT SIGTERM

log "tools/device/$(basename "$0"): create_radio_vif_interface - Bringing up interface"

create_radio_vif_interface "$@" &&
    log "tools/device/$(basename "$0"): create_radio_vif_interface - Success" ||
    raise "create_radio_vif_interface - Failed" -l "tools/device/$(basename "$0")" -tc

exit 0
