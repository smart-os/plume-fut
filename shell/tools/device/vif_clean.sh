#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/wm2_lib.sh
source ${LIB_OVERRIDE_FILE}


usage="$(basename "$0") [-h]

Script is used to empty Wifi_VIF_Config table

Script does following:
    - delete whole Wifi_VIF_Config table

Example of usage:
    $(basename "$0") - delete/empty Wifi_VIF_Config table
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

vif_clean ||
    raise "vif_clean - Failed" -l "tools/device/$(basename "$0")" -tc

exit 0
