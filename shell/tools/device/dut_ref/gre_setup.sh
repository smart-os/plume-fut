#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/nm2_lib.sh
source ${LIB_OVERRIDE_FILE}

if_name=$1
if_role=$2

log "tools/device/dut_ref/$(basename "$0"): $if_role GRE Setup"

if [ "$if_role" == "gw" ]; then
    wait_for_function_response 'notempty' "cat /tmp/dhcp.leases | awk '{print $3}'"
    gre_remote_inet_addr=$(cat /tmp/dhcp.leases | awk '{print $3}')
    gre_if_name="pgd$(echo ${gre_remote_inet_addr//./-}| cut -d'-' -f3-4)"
    gre_local_inet_addr=$(ovsh s Wifi_Inet_Config -w if_name=="$if_name" inet_addr -r)

    add_bridge_port br-home "$gre_if_name"
elif [ "$if_role" == "leaf" ]; then
    gre_if_name="g-${if_name}"
    gre_local_inet_addr=$(ifconfig "${if_name}" | grep "inet addr:" | awk '{print $2}' | cut -d':' -f2)
    gw_ap_subnet=$(${OVSH} s Wifi_Route_State -w if_name=="$if_name" dest_addr -r)
    gre_remote_inet_addr="$(echo "$gw_ap_subnet" | cut -d'.' -f1-3).$(( $(echo "$gw_ap_subnet" | cut -d'.' -f4) +1 ))"

    add_bridge_port br-wan "$gre_if_name"
else
    die "tools/device/dut_ref/$(basename "$0"): Wrong if_role provided"
fi

create_inet_entry \
    -if_name "$gre_if_name" \
    -network true \
    -enabled true \
    -if_type gre \
    -ip_assign_scheme none \
    -mtu 1500 \
    -gre_ifname "$if_name" \
    -gre_remote_inet_addr "$gre_remote_inet_addr" \
    -gre_local_inet_addr "$gre_local_inet_addr" &&
        log "tools/device/dut_ref/$(basename "$0"): Gre interface $gre_if_name created" ||
        die "tools/device/dut_ref/$(basename "$0"): Failed to create Gre interface $gre_if_name"

add_bridge_port br-wan patch-w2h &&
    log "tools/device/dut_ref/$(basename "$0"): Success - add_bridge_port br-wan patch-w2h" ||
    die "tools/device/dut_ref/$(basename "$0"): Failed - add_bridge_port br-wan patch-w2h"

add_bridge_port br-home patch-h2w &&
    log "tools/device/dut_ref/$(basename "$0"): Success - add_bridge_port br-wan patch-h2w" ||
    die "tools/device/dut_ref/$(basename "$0"): Failed - add_bridge_port br-wan patch-h2w"

set_interface_patch patch-w2h patch-h2w patch-w2h &&
    log "tools/device/dut_ref/$(basename "$0"): Success - set_interface_patch patch-w2h patch-h2w" ||
    die "tools/device/dut_ref/$(basename "$0"): Failed - set_interface_patch patch-w2h patch-h2w"

set_interface_patch patch-h2w patch-w2h patch-h2w &&
    log "tools/device/dut_ref/$(basename "$0"): Success - set_interface_patch patch-h2w patch-w2h" ||
    die "tools/device/dut_ref/$(basename "$0"): Failed - set_interface_patch patch-h2w patch-w2h"

exit 0
