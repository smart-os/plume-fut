#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/cm2_lib.sh
source ${LIB_OVERRIDE_FILE}


br_name=$1
br_port=$2

log "tools/device/dut_ref/$(basename "$0"): Adding bridge $br_name on $br_port"

add_bridge_port $br_name $br_port &&
    log "tools/device/dut_ref/$(basename "$0"): add_bridge_port - Success" ||
    die "tools/device/dut_ref/$(basename "$0"): add_bridge_port - Failed"

exit 0
