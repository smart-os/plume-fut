#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/lib/wm2_lib.sh
source ${LIB_OVERRIDE_FILE}


log "tools/device/dut_ref/$(basename "$0"): GW Setup"

log "tools/device/dut_ref/$(basename "$0"): Running device_init"
device_init &&
    log "tools/device/dut_ref/$(basename "$0"): device_init - Success" ||
    die "tools/device/dut_ref/$(basename "$0"): device_init - Failed"

log "tools/device/dut_ref/$(basename "$0"): Running start_openswitch"
start_openswitch &&
    log "tools/device/dut_ref/$(basename "$0"): start_openswitch - Success" ||
    die "tools/device/dut_ref/$(basename "$0"): start_openswitch - Failed"

log "tools/device/dut_ref/$(basename "$0"): Running start_wireless_driver"
start_wireless_driver &&
    log "tools/device/dut_ref/$(basename "$0"): start_wireless_driver - Success" ||
    die "tools/device/dut_ref/$(basename "$0"): start_wireless_driver - Failed"

log "tools/device/dut_ref/$(basename "$0"): Starting WM manager"
start_specific_manager wm &&
    log "tools/device/dut_ref/$(basename "$0"): WM manager started" ||
    die "tools/device/dut_ref/$(basename "$0") - start_specific_manager wm - Failed"

log "tools/device/dut_ref/$(basename "$0"): Starting NM manager"
start_specific_manager nm &&
    log "tools/device/dut_ref/$(basename "$0"): NM manager started" ||
    die "tools/device/dut_ref/$(basename "$0") - start_specific_manager nm - Failed"

empty_ovsdb_table AW_Debug ||
    die "tools/device/dut_ref/$(basename "$0"): empty_ovsdb_table AW_Debug - Failed"

set_manager_log WM TRACE ||
    die "tools/device/dut_ref/$(basename "$0"): set_manager_log WM TRACE - Failed"

set_manager_log NM TRACE ||
    die "tools/device/dut_ref/$(basename "$0"): set_manager_log NM TRACE - Failed"

exit 0
