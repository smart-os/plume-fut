#!/bin/sh

# Include basic environment config from file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source $LIB_DIR/wm2_lib.sh
source ${LIB_OVERRIDE_FILE}

trap 'run_setup_if_crashed wm || true' EXIT SIGINT SIGTERM

# check_type options:
#  - "Wifi_VIF_State" : default
#  - "Wifi_Radio_State"
#  - "client_connect"
#  - "leaf_connect"

check_type=${1:-Wifi_VIF_State}
enabled=${2:-true}
key=$3
value=$4

log "tools/device/$(basename "$0"): Checking WiFi presence - check type: $check_type"

case $check_type in
    Wifi_VIF_State)
        ${OVSH} s Wifi_VIF_State --where $key==$value enabled:=$enabled
        [[ $? == 0 ]] &&
            log "tools/device/$(basename "$0"): WiFi is present"; exit $? ||
            log "tools/device/$(basename "$0"): WiFi is NOT present"; exit $?
    ;;
    Wifi_Radio_State)
        ${OVSH} s Wifi_Radio_State --where $key==$value enabled:=$enabled
        [[ $? == 0 ]] &&
            log "tools/device/$(basename "$0"): WiFi is present"; exit $? ||
            log "tools/device/$(basename "$0"): WiFi is NOT present"; exit $?
    ;;
    leaf_connect)
        log "tools/device/$(basename "$0"): Not implemented. Pending implementation!"
        exit 0
    ;;
    client_connect)
        log "tools/device/$(basename "$0"): Not implemented. Pending implementation!"
        exit 0
    ;;
    *)
        log "tools/device/$(basename "$0"): Usage: $0 {Wifi_VIF_State|Wifi_Radio_State|client_connect}"
        exit 1
esac
