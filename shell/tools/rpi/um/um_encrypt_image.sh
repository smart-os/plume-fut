#!/usr/bin/env bash

current_dir=$(dirname "$(realpath "$BASH_SOURCE")")
fut_topdir="$(realpath "$current_dir"/../../..)"
source "$fut_topdir/lib/rpi_lib.sh"

usage="$(basename "$0") [-h] \$1 \$2

"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [[ $# -lt 2 ]]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

um_fw_unc_path=$1
um_fw_key_path=$2

um_encrypt_image "$um_fw_unc_path" "$um_fw_key_path"