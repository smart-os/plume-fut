#!/usr/bin/env bash

current_dir=$(dirname "$(realpath "$BASH_SOURCE")")
fut_topdir="$(realpath "$current_dir"/../..)"
source "$fut_topdir/lib/rpi_lib.sh"

usage="$(basename "$0") [-h] \$1 \$2 \$3

where arguments are:
    interface=\$1 --  interface name which to use to connect to network (string)(required)
    network_ssid=\$2 --  network ssid to which rpi client will try and connect (string)(required)
    network_bssid=\$3 --  network bssid (MAC) to which rpi client will try and connect (string)(required)
    network_pass=\$4 -- network password which rpi client will use to try and connect to network (string)(required)
    network_key_mgmt=\$5 -- used as key_mgmt value in wpa_supplicant.conf file
    enable_dhcp=\$6 -- enable or disable DHCPDISCOVER on network !!! PARTIALLY IMPLEMENTED !!!
        - possibilities:
            - on (string) - enable dhcp discover for interface
            - off (string) - disable dhcp discover for interface - (default value)
                    - killing dhclient process after 5 seconds - workaround
    msg_prefix=\$7 -- used as message prefix for log messages (string)(optional)
Script is used to connect RPI client to WPA2 network

Script does following:
    - bring down interface wlan0 -> ifdown wlan0
    - clear wpa_supplicant.conf file
    - create new wpa_supplicant.conf using wpa_passphrase and other configuration
    - bring up interface wlan0 -> ifup wlan0
    - check if RPI client is connected to netowork -> ip link show wlan0

Example of usage:
    $(basename "$0") wlan0 wm_dut_24g_network 72:a7:56:f0:0d:72 WifiPassword123 WPA-PSK on
        - connect to wm_dut_24g_network ssid with bssid 72:a7:56:f0:0d:72 using wlan0 interface key_mgmnt mode of
          WPA-PSK using network password WifiPassword123 and keeping dhcp discover on
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [[ $# -lt 6 ]]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

interface=$1
network_ssid=$2
network_bssid=$3
network_pass=$4
network_key_mgmt=$5
enable_dhcp=$6
msg_prefix=$7

network_connect_to_wpa2 $interface $network_ssid $network_bssid $network_pass $network_key_mgmt $enable_dhcp $msg_prefix