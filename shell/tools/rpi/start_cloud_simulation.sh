#!/usr/bin/env bash

current_dir=$(dirname "$(realpath "$BASH_SOURCE")")
fut_topdir="$(realpath "$current_dir"/../..)"
source "$fut_topdir/lib/rpi_lib.sh"

help()
{
cat << EOF
Usage: ${MY_NAME} [--help|-h] [--stop|-s]

OPTIONS:
  --help|-h : this help message
  --stop|-s : stop the service instead of starting it

Script configures and (re)starts haproxy that acts as simulated cloud.
EOF
}

ARGS=""
# parse command line arguments
while [[ "${1}" == -* ]]; do
    option="${1}"
    shift
    case "${option}" in
        -h|--help)
            help
            exit 0
            ;;
        -s|--stop)
            stop_cloud_simulation
            exit 0
            ;;
        -r|--restart)
            stop_cloud_simulation
            ;;
        -*)
            help
            exit 0
            ;;
    esac
done

start_cloud_simulation
