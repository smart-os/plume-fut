#!/usr/bin/env bash

current_dir=$(dirname "$(realpath "$BASH_SOURCE")")
fut_topdir="$(realpath "$current_dir"/../../..)"
source "$fut_topdir/lib/rpi_lib.sh"

tc_name="tools/rpi/cm/$(basename $0)"
usage()
{
cat << EOF
${tc_name} [-h] ip_address type
Options:
    -h  show this help message
Arguments:
    ip_address=$1 -- IP address to perform action on - (string)(required)
    type=$2 -- type of action to perform: block/unblock - (string)(required)
example of usage:
   ${tc_name} "192.168.200.11" "block"
   ${tc_name} "192.168.200.10" "unblock"
EOF
exit 1
}

while getopts h option; do
    case "$option" in
        h)
            usage
            ;;
    esac
done

NARGS=2
[ $# -ne ${NARGS} ] && raise "Requires exactly '${NARGS}' input argument(s)" -l "$tc_name" -arg
ip_address=${1}
type=${2}

log "${tc_name}: Manipulate internet traffic: ${type} ${ip_address}"
address_internet_manipulation "$ip_address" "$type"
