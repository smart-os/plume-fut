#!/bin/sh

# Include basic environment config
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source ${FUT_TOPDIR}/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/cm2_lib.sh
source ${LIB_OVERRIDE_FILE}

############################################ INFORMATION SECTION - START ###############################################
#
#   Base library of common Upgrade Manager functions (Plume specific)
#
############################################ INFORMATION SECTION - STOP ################################################


############################################ SETUP SECTION - START #####################################################

ut_setup_test_environment()
{
    log -deb "UT SETUP"

    stop_healthcheck ||
        die "lib/um_lib: ut_setup_test_environment - Failed stop_healthcheck"

    cm_disable_fatal_state ||
        die "lib/cm2_lib: ut_setup_test_environment - Failed: cm_disable_fatal_state"

    # Ignoring failures
    /etc/init.d/manager restart || true
}

############################################ SETUP SECTION - STOP ######################################################
