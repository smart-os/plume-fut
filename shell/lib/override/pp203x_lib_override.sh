#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

############################################ INFORMATION SECTION - START ###############################################
#
#   PP203X libraries overrides
#
############################################ INFORMATION SECTION - STOP ################################################


############################################ UNIT OVERRIDE SECTION - START #############################################
# Returns the filename of the script manipulating OpenSync managers
get_managers_script()
{
    echo "/etc/init.d/manager"
}

disable_watchdog()
{
    fn_name="unit_lib:disable_watchdog"
    log -deb "$fn_name - Disabling watchdog"
    ${OPENSYNC_ROOTDIR}/bin/wpd --set-auto
    sleep 1
    PID=$(pidof wpd) || raise "wpd not running" -l "$fn_name" -ds
}

############################################ UNIT OVERRIDE SECTION - STOP ##############################################


############################################ LED OVERRIDE SECTION - START ##############################################

test_ledm_led_config()
{
    fc_name="pp203x_lib_override:test_ledm_led_config"
    log -deb "$fc_name - Turning on RED LED"
    led_config_ovsdb red || raise "Failed" -l "$fc_name" -tc
    check_led_gpio_state red || raise "Failed" -l "$fc_name" -tc
    sleep 1

    log -deb "$fc_name - Turning on GREEN LED"
    led_config_ovsdb green || raise "Failed" -l "$fc_name" -tc
    check_led_gpio_state green || raise "Failed" -l "$fc_name" -tc
    sleep 1

    log -deb "$fc_name - Turning on BLUE LED"
    led_config_ovsdb blue || raise "Failed" -l "$fc_name" -tc
    check_led_gpio_state blue || raise "Failed" -l "$fc_name" -tc
    sleep 1

    log -deb "$fc_name - Turning on WHITE LED"
    led_config_ovsdb on || raise "Failed" -l "$fc_name" -tc
    check_led_gpio_state on || raise "Failed" -l "$fc_name" -tc
    sleep 1

    log -deb "$fc_name - Turning LED off"
    led_config_ovsdb off || raise "Failed" -l "$fc_name" -tc
    check_led_gpio_state off || raise "Failed" -l "$fc_name" -tc
    sleep 1

    log -deb "$fc_name - LED mode blink"
    led_config_ovsdb blink || raise "Failed" -l "$fc_name" -tc
    sleep 5

    log -deb "$fc_name - LED mode breathe"
    led_config_ovsdb breathe || raise "Failed" -l "$fc_name" -tc
    sleep 5

    log -deb "$fc_name - LED mode pattern"
    led_config_ovsdb pattern || raise "Failed" -l "$fc_name" -tc
    sleep 5

    led_config_ovsdb off || raise "Failed" -l "$fc_name" -tc
    check_led_gpio_state off || raise "Failed" -l "$fc_name" -tc
}

led_config_ovsdb()
{
    mode=${2:-"off"}

    r=0
    g=0
    b=0
    case "$mode" in
    "red")
        r=255
        mode=on
        ;;
    "green")
        g=255
        mode=on
        ;;
    "blue")
        b=255
        mode=on
        ;;
    "on")
        r=255
        g=255
        b=255
        ;;
    esac

    ${OVSH} u AWLAN_Node led_config:='["map",[["intensity_blue","'$b'"],["intensity_green","'$g'"],["intensity_red","'$r'"],["mode","'$mode'"]]]' || die "Failed"
}

check_led_gpio_state()
{
    fc_name="pp203x_lib_override:check_led_gpio_state"
    mode=${2:-"off"}

    r=$(cat /sys/class/leds/pp203x:red:system/brightness) || raise "Error reading LED system file" -l "$fc_name" -tc
    g=$(cat /sys/class/leds/pp203x:green:system/brightness) || raise "Error reading LED system file" -l "$fc_name" -tc
    b=$(cat /sys/class/leds/pp203x:blue:system/brightness) || raise "Error reading LED system file" -l "$fc_name" -tc
    expr=0
    expg=0
    expb=0
    case "$mode" in
    "red")
        expr=255
        ;;
    "green")
        expg=255
        ;;
    "blue")
        expb=255
        ;;
    "on")
        expr=255
        expg=255
        expb=255
        ;;
    "off")
        ;;
    *)
        log -deb "$fc_name - This mode is not supported!"
        return 1
    esac

    if [ $r -ne $expr -a $g -ne $expg -a $b -ne $expb ]; then
        log -deb "$fc_name - $mode LED GPIO incorrect"
        return 1
    fi

    return 0
}

############################################ LED OVERRIDE SECTION - STOP ###############################################


############################################ CM OVERRIDE SECTION - START ###############################################

############################################ CM OVERRIDE SECTION - STOP ################################################
