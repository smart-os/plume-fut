#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

############################################ INFORMATION SECTION - START ###############################################
#
#   PP213X-EX libraries overrides
#
############################################ INFORMATION SECTION - STOP ################################################


############################################ UNIT OVERRIDE SECTION - START #############################################

stop_healthcheck()
{
    log -deb "Healthcheck service not present on system"
    return 0
}

############################################ UNIT OVERRIDE SECTION - STOP ##############################################
