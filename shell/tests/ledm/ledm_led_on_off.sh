#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${FUT_TOPDIR}/shell/lib/ledm_lib.sh
source ${LIB_OVERRIDE_FILE}


log_title "LEDM subtest: LED on off"

log "Positive test cases - assert result is true"
log "  Turning LED on, test if on"
led_config_ovsdb on || die "Failed"
check_led_gpio_state on || die "Failed"

log "  Turning LED off, test if off"
led_config_ovsdb off || die "Failed"
check_led_gpio_state off || die "Failed"

log "Negative test cases - assert result is false"
log "  Turning LED on, test if off"
led_config_ovsdb on || die "Failed"
check_led_gpio_state off || die "Unexpected pass"

log "  Turning LED off, test if on"
led_config_ovsdb off || die "Failed"
check_led_gpio_state on || die "Failed"

pass

