#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/nm2_lib.sh"
source "${LIB_OVERRIDE_FILE}"

trap '
    reset_inet_entry $if_name || true
    run_setup_if_crashed nm || true
    check_restore_management_access || true
' EXIT SIGINT SIGTERM

usage="
$(basename "$0") [-h] \$1 \$2 \$3

where options are:
    -h  show this help message

where arguments are:
    if_name=\$1 -- used as if_name in Wifi_Inet_Config table - (string)(required)
    if_type=\$2 -- used as if_type in Wifi_Inet_Config table - default 'vif'- (string)(optional)
    broadcast=\$3 -- used as broadcast in Wifi_Inet_Config table - default '10.10.10.255' - (string)(optional)

this script is dependent on following:
    - running NM manager
    - running WM manager

example of usage:
   /tmp/fut-base/shell/nm2/nm2_set_broadcast.sh wifi0
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

if_name=$1
if_type=$2
broadcast=$3

tc_name="nm2/$(basename "$0")"

log "$tc_name: Creating Wifi_Inet_Config entries for $if_name"
create_inet_entry \
    -if_name "$if_name" \
    -enabled true \
    -network true \
    -ip_assign_scheme static \
    -inet_addr 10.10.10.30 \
    -netmask "255.255.255.0" \
    -if_type "$if_type" &&
        log "$tc_name: Interface successfully created" ||
        raise "Failed to create interface" -l "$tc_name" -tc

log "$tc_name: Setting BROADCAST for $if_name to $broadcast"
update_ovsdb_entry Wifi_Inet_Config -w if_name "$if_name" -u broadcast "$broadcast" &&
    log "$tc_name: update_ovsdb_entry - Wifi_Inet_Config table updated - broadcast $broadcast" ||
    raise "update_ovsdb_entry - Failed to update Wifi_Inet_Config - broadcast $broadcast" -l "$tc_name" -tc

wait_ovsdb_entry Wifi_Inet_State -w if_name "$if_name" -is broadcast "$broadcast" &&
    log "$tc_name: wait_ovsdb_entry - Wifi_Inet_Config reflected to Wifi_Inet_State - broadcast $broadcast" ||
    raise "wait_ovsdb_entry - Failed to reflect Wifi_Inet_Config to Wifi_Inet_State - broadcast $broadcast" -l "$tc_name" -tc

log "$tc_name: LEVEL 2 - Check if BROADCAST was properly applied to $if_name"
wait_for_function_response 0 "interface_broadcast $if_name | grep -q \"$broadcast\"" &&
    log "$tc_name: LEVEL2: BROADCAST applied to ifconfig - broadcast $broadcast" ||
    raise "LEVEL2: Failed to apply BROADCAST to ifconfig - broadcast $broadcast" -l "$tc_name" -tc

log "$tc_name: Removing broadcast from Wifi_Inet_Config"
update_ovsdb_entry Wifi_Inet_Config -w if_name "$if_name" \
    -u broadcast 0.0.0.0 \
    -u ip_assign_scheme none &&
        log "$tc_name: update_ovsdb_entry - Wifi_Inet_Config table updated - broadcast 0.0.0.0" ||
        raise "update_ovsdb_entry - Failed to update Wifi_Inet_Config - broadcast 0.0.0.0" -l "$tc_name" -tc

wait_ovsdb_entry Wifi_Inet_State -w if_name "$if_name" \
    -is broadcast 0.0.0.0 \
    -is ip_assign_scheme none &&
        log "$tc_name: wait_ovsdb_entry - Wifi_Inet_Config reflected to Wifi_Inet_State - broadcast 0.0.0.0" ||
        raise "wait_ovsdb_entry - Failed to reflect Wifi_Inet_Config to Wifi_Inet_State - broadcast 0.0.0.0" -l "$tc_name" -tc

log "$tc_name: LEVEL 2 - Check if BROADCAST was properly removed from $if_name"
wait_for_function_response 1 "interface_broadcast $if_name | grep -q \"$broadcast\"" &&
    log "$tc_name: LEVEL2: BROADCAST removed from ifconfig - interface $if_name" ||
    raise "LEVEL2: Failed to remove BROADCAST to ifconfig - interface $if_name" -l "$tc_name" -tc

pass
