#!/bin/sh

# TEST DESCRIPTION
# Try to configure MTU to existing interface.
#
# TEST PROCEDURE
# - Configure interface with selected parameters
#
# EXPECTED RESULTS
# Test is passed:
# - if interface is properly configured AND
# - if mtu is applied to State table
# Test fails:
# - if inteface cannot be configured OR
# - if mtu is not applied to State table

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/nm2_lib.sh"
source "${LIB_OVERRIDE_FILE}"

trap '
    reset_inet_entry $if_name || true
    run_setup_if_crashed nm || true
    check_restore_management_access || true
' EXIT SIGINT SIGTERM

usage="
$(basename "$0") [-h] \$1 \$2 \$3

where options are:
    -h  show this help message

where arguments are:
    if_name=\$1 -- used as if_name in Wifi_Inet_Config table - (string)(required)
    if_type=\$2 -- used as if_type in Wifi_Inet_Config table - default 'vif' - (string)(optional)
    mtu=\$3 -- used as mtu in Wifi_Inet_Config table - default '1600' - (integer)(optional)

this script is dependent on following:
    - running NM manager
    - running WM manager

example of usage:
   /tmp/fut-base/shell/nm2/nm2_set_mtu.sh eth0 eth 1500
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

# Provide at least 2 argument(s).
if [ $# -lt 2 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

# Fill variables with provided arguments or defaults.
if_name=$1
if_type=$2
mtu=$3

tc_name="nm2/$(basename "$0")"

log "$tc_name: Creating Wifi_Inet_Config entries for $if_name (enabled=true, network=true, ip_assign_scheme=static)"
create_inet_entry \
    -if_name "$if_name" \
    -enabled true \
    -network true \
    -ip_assign_scheme static \
    -if_type "$if_type" &&
        log "$tc_name: Interface successfully created" ||
        raise "Failed to create interface" -l "$tc_name" -tc

log "$tc_name: Setting MTU to $mtu"
update_ovsdb_entry Wifi_Inet_Config -w if_name "$if_name" -u mtu "$mtu" &&
    log "$tc_name: update_ovsdb_entry - Wifi_Inet_Config table updated - mtu $mtu" ||
    raise "update_ovsdb_entry - Failed to update Wifi_Inet_Config - mtu $mtu" -l "$tc_name" -tc

wait_ovsdb_entry Wifi_Inet_State -w if_name "$if_name" -is mtu "$mtu" &&
    log "$tc_name: wait_ovsdb_entry - Wifi_Inet_Config reflected to Wifi_Inet_State - mtu $mtu" ||
    raise "wait_ovsdb_entry - Failed to reflect Wifi_Inet_Config to Wifi_Inet_State - mtu $mtu" -l "$tc_name" -tc

log "$tc_name: LEVEL 2 - Checking if MTU was properly applied to $if_name"
wait_for_function_response 0 "interface_mtu $if_name | grep -q \"$mtu\"" &&
    log "$tc_name: MTU applied to ifconfig - interface $if_name" ||
    raise "Failed to apply MTU to ifconfig - interface $if_name" -l "$tc_name" -tc

pass
