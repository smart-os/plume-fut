#!/bin/sh
# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/nm2_lib.sh"
source "${LIB_OVERRIDE_FILE}"

trap '
    delete_inet_interface $vif_name
    run_setup_if_crashed nm || true
    check_restore_management_access || true
' EXIT SIGINT SIGTERM

usage="
$(basename "$0") [-h] \$1 \$2 \$3

where options are:
    -h  show this help message

where arguments are:
    if_name=\$1 -- used as if_name in Wifi_Inet_Config table - (string)(required)
    vlan_no=\$2 -- used as vlan identifier - example, vlan_no 100 will result in vif_name eth0.100 - (int)(required)
    if_type=\$3 -- used as if_type in Wifi_Inet_Config table - default 'vlan'- (string)(optional)

this script is dependent on following:
    - running NM manager

example of usage:
   /tmp/fut-base/shell/nm2/nm2_rapid_multiple_insert_delete_iface.sh eth0
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

if_name=$1
vlan_id=$2
if_type=${3:-vlan}
vif_name="$if_name.$vlan_id"
v_ip_address="10.10.10.$vlan_id"

tc_name="nm2/$(basename "$0")"

log "$tc_name: Creating interface $vif_name"
create_inet_entry \
    -if_name "$vif_name" \
    -enabled true \
    -network true \
    -ip_assign_scheme static \
    -inet_addr "$v_ip_address" \
    -netmask "255.255.255.0" \
    -if_type "$if_type" \
    -vlan_id "$vlan_id" \
    -parent_ifname "$if_name" &&
        log "$tc_name: create_vlan_inet_entry - Success" ||
        raise "create_vlan_inet_entry - Failed" -l "$tc_name" -tc

log "$tc_name: LEVEL 2 - Check if IP ADDRESS: $v_ip_address was properly applied to $vif_name"
wait_for_function_response 0 "interface_ip_address $vif_name | grep -q \"$v_ip_address\"" &&
    log "$tc_name: LEVEL2: Ip address applied to $vif_name" ||
    raise "Failed to apply settings to ifconfig (ENABLED) - interface $vif_name" -l "$tc_name" -tc

log "$tc_name: Setting ENABLED to false - interface $vif_name"
update_ovsdb_entry Wifi_Inet_Config -w if_name "$vif_name" -u enabled false &&
    log "$tc_name: update_ovsdb_entry - Wifi_Inet_Config table updated - enabled=false" ||
    raise "update_ovsdb_entry - Failed to update Wifi_Inet_Config - enabled=false" -l "$tc_name" -tc

wait_ovsdb_entry Wifi_Inet_State -w if_name "$vif_name" -is enabled false &&
    log "$tc_name: wait_ovsdb_entry - Wifi_Inet_Config reflected to Wifi_Inet_State - enabled=false" ||
    raise "wait_ovsdb_entry - Failed to reflect Wifi_Inet_Config to Wifi_Inet_State - enabled=false" -l "$tc_name" -tc

log "$tc_name: LEVEL 2 - Check if IP ADDRESS: $v_ip_address was properly removed from ifconfig $vif_name"
wait_for_function_response 1 "interface_ip_address $if_name | grep -q \"$v_ip_address\"" &&
    log "$tc_name: Settings removed from ifconfig (DISABLE #2) - interface $if_name" ||
    raise "Failed to remove settings from ifconfig (DISABLE #2) - interface $if_name" -l "$tc_name" -tc

pass
