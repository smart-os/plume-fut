#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/sm_lib.sh"
source "${LIB_OVERRIDE_FILE}"

trap 'run_setup_if_crashed sm' EXIT SIGINT SIGTERM

usage="$(basename "$0") [-h] \$1 \$2 \$3 \$4 \$5

where arguments are:
    sm_report_radio=\$1 -- freq_band that is reported inside SM manager in logs (string)(required)
        - example:
            Caesar:
                50U freq_band is reported as 5GU
                50L freq_band is reported as 5GL
            Tyrion:
                50L freq_band is reported as 5G
    sm_reporting_interval=\$2 -- used as reporting_interval column in Wifi_Stats_Config (int)(required)
    sm_sampling_interval=\$3 -- used as sampling_interval column in Wifi_Stats_Config (int)(required)
    sm_report_type=\$4 -- used as report_type column in Wifi_Stats_Config (string)(required)
    sm_leaf_mac=\$5 -- inspect logs for given Leaf MAC address reports (string)(required)

Script does following:
    - insert into Wifi_Stats_Config appropriate leaf (client) reporting configuration
    - tail logs (/tmp/logs/messages - logread -f) for matching patterns for SM leaf reporting
        - log messages are device/platform dependent

Dependent on:
    - running WM/NM managers - min_wm2_setup.sh - existance of active interfaces
    - sm_setup.sh
    - sm_reporting_env_setup.sh
    - existence of connected Leaf device - if not, test will timeout

Example of usage:
    $(basename "$0")
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -lt 5 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

sm_radio_type=$1
sm_reporting_interval=$2
sm_sampling_interval=$3
sm_report_type=$4
sm_leaf_mac=$5

tc_name="sm/$(basename "$0")"

log "$tc_name: Inspecting leaf report on $sm_radio_type for leaf $sm_leaf_mac"

inspect_leaf_report \
    "$sm_radio_type" \
    "$sm_reporting_interval" \
    "$sm_sampling_interval" \
    "$sm_report_type" \
    "$sm_leaf_mac" || die "sm/$(basename "$0"): inspect_leaf_report - Failed"

pass
