#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/cm2_lib.sh"
source "${LIB_OVERRIDE_FILE}"

trap '
    check_restore_management_access || true
    run_setup_if_crashed cm || true
' EXIT SIGINT SIGTERM

usage="
$(basename "$0") [-h] \$1

where options are:
    -h  show this help message

where arguments are:
    if_name=\$@ -- used as connection interface - (string)

this script is dependent on following:
    - running CM manager
    - running NM manager

example of usage:
    /tmp/fut-base/shell/cm2/$(basename "$0") eth0
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if_name=${1:-eth0}
bit_process="01 05 35 75"

tc_name="cm2/$(basename "$0")"
log "$tc_name: CM2 test - CM OBSERVE BLE STATUS  - interface down/up"
log "$tc_name: Simulating CM full reconnection by dropping interface"

log "$tc_name: Dropping interface $if_name"
ifconfig "$if_name" down &&
    log "$tc_name: Interface $if_name is down" ||
    raise "Couldn't bring down interface $if_name" -l "$tc_name" -ds

log "$tc_name: Sleeping for 5 seconds"
sleep 5

log "$tc_name: Bringing back interface $if_name"
ifconfig "$if_name" up &&
    log "$tc_name: Interface $if_name is up" ||
    raise "Couldn't bring up interface $if_name" -l "$tc_name" -ds

for bit in $bit_process; do
    log "$tc_name: Checking AW_Bluetooth_Config payload for $bit:00:00:00:00:00"
    wait_ovsdb_entry AW_Bluetooth_Config -is payload "$bit:00:00:00:00:00" &&
        log "$tc_name: wait_ovsdb_entry - AW_Bluetooth_Config payload changed to $bit:00:00:00:00:00" ||
        raise "AW_Bluetooth_Config payload failed to change $bit:00:00:00:00:00" -l "$tc_name" -tc
done

pass
