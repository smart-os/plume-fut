#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/cm2_lib.sh"
source "${LIB_OVERRIDE_FILE}"

trap '
    check_restore_management_access || true
    run_setup_if_crashed cm || true
' EXIT SIGINT SIGTERM

usage="
$(basename "$0") [-h] \$1

where options are:
    -h  show this help message

where arguments are:
    test_type=\$@ -- used as test step - (string)(required)

this script is dependent on following:
    - running CM manager
    - running NM manager

example of usage:
   /tmp/fut-base/shell/cm2/$(basename "$0") internet_blocked
   /tmp/fut-base/shell/cm2/$(basename "$0") internet_recovered
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

test_type=$1

tc_name="cm2/$(basename "$0")"
log "$tc_name: CM2 test - CM OBSERVE BLE STATUS - internet blocked"

case $test_type in
    internet_blocked)
        bit_process="75 35 15 05"
    ;;
    internet_recovered)
        bit_process="75"
    ;;
    *)
        raise "Incorrect test_type provided" -l "$tc_name" -arg
esac

for bit in $bit_process; do
    log "$tc_name: Checking AW_Bluetooth_Config payload for $bit:00:00:00:00:00"
    wait_ovsdb_entry AW_Bluetooth_Config -is payload "$bit:00:00:00:00:00" &&
        log "$tc_name: wait_ovsdb_entry - AW_Bluetooth_Config payload changed to $bit:00:00:00:00:00" ||
        raise "AW_Bluetooth_Config payload failed to change $bit:00:00:00:00:00" -l "$tc_name" -tc
done

pass
