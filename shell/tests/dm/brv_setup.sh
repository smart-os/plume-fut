#!/bin/sh

# Include environment config from default shell file
source /tmp/fut-base/shell/config/default_shell.sh
[ -e "/tmp/fut_set_env.sh" ] && source /tmp/fut_set_env.sh
# Include shared libraries and library overrides
source ${FUT_TOPDIR}/shell/lib/brv_lib.sh

brv_setup_env &&
    log "brv/$(basename "$0"): brv_setup_env - Success " ||
    die "brv/$(basename "$0"): brv_setup_env - Failed"

exit 0
