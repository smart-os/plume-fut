#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/dm_lib.sh"
source "${LIB_OVERRIDE_FILE}"

# fetching managers
MANAGER_NR=`ps w | grep -i plume | wc -l`
if [ "$MANAGER_NR" -lt 15 ]; then
    die "Not enough managers started."
fi
