#!/bin/sh

# Include basic environment config
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source ${FUT_TOPDIR}/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/brv_lib.sh

tc_name="brv/$(basename $0)"
usage()
{
cat << EOF
${tc_name} [-h] tool_path
where options are:
    -h  show this help message
input arguments:
    tool_path=$1 -- name or path of the required tool - (string)(required)
                    If only the tool name is provided, PATH is searched.
                    If the absolute path is provided, that is used to confirm the presence.
this script is dependent on following:
    - running brv_setup.sh
example of usage:
   ${tc_name} "ls"
   ${tc_name} "bc"
EOF
}

while getopts h option; do
    case "$option" in
        h)
            usage
            exit 1
            ;;
    esac
done

NARGS=1
[ $# -lt ${NARGS} ] && raise "Requires at least '${NARGS}' input argument(s)" -l "${tc_name}" -arg
tool_path=$1
log_title "${tc_name}: Verify tool '${tool_path}' is present on device"

is_tool_on_system ${tool_path}
rc=$?
if [ $rc == 0 ]; then
    log -deb "${tc_name}: tool '${tool_path}' found on device"
elif [ $rc == 126 ]; then
    raise "Tool '${tool_path}' found on device but could not be invoked" -l "${tc_name}" -ec ${rc} -tc
else
    raise "Tool '${tool_path}' could not be found on device" -l "${tc_name}" -ec ${rc} -tc
fi

pass
