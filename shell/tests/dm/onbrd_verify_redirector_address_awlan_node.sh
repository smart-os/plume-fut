#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$1

where options are:
    -h  show this help message

where arguments are:
    redirector_address=\$1 -- used as FW version to verify running correct FW - (string)(required)

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0") ssl:wildfire.plume.tech:443
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

redirector_addr=$1
tc_name="onbrd/$(basename "$0")"

log "$tc_name: ONBRD Verify redirector address, waiting for '$redirector_addr'"

wait_ovsdb_entry AWLAN_Node -is redirector_addr "$redirector_addr" &&
    log "$tc_name: wait_ovsdb_entry - AWLAN_Node redirector_addr equal to '$redirector_addr'" ||
    raise "wait_ovsdb_entry - AWLAN_Node redirector_addr NOT equal to '$redirector_addr'" -l "$tc_name" -tc

pass
