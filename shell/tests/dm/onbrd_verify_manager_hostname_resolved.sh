#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$1 \$2

where options are:
    -h  show this help message

where arguments are:
    manager_addr=\$1 -- used as manager address - (string)(required)
    target=\$2 -- used to check manager address resolved - (string)(required)

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0") ssl:ec2-54-200-0-59.us-west-2.compute.amazonaws.com:443 ssl:54.200.0.59:443
   /tmp/fut-base/shell/onbrd/$(basename "$0") ssl:54.200.0.59:443 ssl:54.200.0.59:443
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -lt 2 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

manager_addr=$1

# Restart managers to start every config resolution from the begining
restart_managers

# Give time to managers to bring up tables
sleep 30

tc_name="onbrd/$(basename "$0")"

log "$tc_name: Setting AWLAN_Node manager_addr to '$manager_addr'"
update_ovsdb_entry AWLAN_Node -u manager_addr "$manager_addr" &&
    log "$tc_name: update_ovsdb_entry - AWLAN_Node table updated - manager_addr '$manager_addr'" ||
    raise "update_ovsdb_entry - Failed to update AWLAN_Node table - manager_addr '$manager_addr'" -l "$tc_name" -tc

log "$tc_name: ONBRD Verify manager hostname resolved, waiting for Manager is_connected true"
wait_ovsdb_entry Manager -is is_connected true &&
    log "$tc_name: wait_ovsdb_entry - Manager is_connected is true" ||
    raise "wait_ovsdb_entry - Manager is_connected is NOT true" -l "$tc_name" -tc

shift
targets=$@

# shellcheck disable=SC2034
for i in $(seq 1 $#); do

    target=$1
    shift

    wait_ovsdb_entry Manager -is target "$target" &&
        { log "$tc_name: wait_ovsdb_entry - Manager target is '$target'"; pass; } ||
        log "$tc_name: wait_ovsdb_entry - Manager target is NOT '$target'"

done

die "onbrd/$(basename "$0"): wait_ovsdb_entry - Manager target NOT resolved"
