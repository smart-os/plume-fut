#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$@

where options are:
    -h  show this help message

where arguments are:
    model=\$@ -- used as model to verify correct model entry - (string)(required)

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0") PP213X_EX
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

expected_model=$1

tc_name="onbrd/$(basename "$0")"

log "$tc_name: ONBRD Verify model, waiting for '$expected_model'"
wait_for_function_response 0 "check_ovsdb_entry AWLAN_Node -w model \"'$expected_model'\"" &&
    log "$tc_name: SUCCESS: check_model '$expected_model'" ||
    raise "FAIL: check_model '$expected_model'" -l "$tc_name" -tc

pass
