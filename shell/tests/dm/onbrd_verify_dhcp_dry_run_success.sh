#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$1

where options are:
    -h  show this help message

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0") eth0
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -ne 1 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

if_name=$1

tc_name="onbrd/$(basename "$0")"

log "$tc_name: ONBRD Verify DHCP dry run success"

update_ovsdb_entry Connection_Manager_Uplink -w if_name "$if_name" -u has_L2 false &&
    log "$tc_name: update_ovsdb_entry - Connection_Manager_Uplink table updated - has_L2 false" ||
    raise "update_ovsdb_entry - Failed to update WifConnection_Manager_Uplinki_Inet_Config - has_L2 false" -l "$tc_name" -tc

wait_ovsdb_entry Connection_Manager_Uplink -w if_name "$if_name" -is has_L2 false &&
    log "$tc_name: wait_ovsdb_entry - Connection_Manager_Uplink - has_L2 is false" ||
    raise "wait_ovsdb_entry - Failed to reflect has_L2 to Connection_Manager_Uplink has_L2 is not false" -l "$tc_name" -tc

wait_ovsdb_entry Connection_Manager_Uplink -w if_name "$if_name" -is has_L3 false &&
    log "$tc_name: wait_ovsdb_entry - Connection_Manager_Uplink - has_L3 is false" ||
    raise "wait_ovsdb_entry - Failed to reflect has_L3 to Connection_Manager_Uplink has_L3 is not false" -l "$tc_name" -tc


update_ovsdb_entry Connection_Manager_Uplink -w if_name "$if_name" -u has_L2 true &&
    log "$tc_name: update_ovsdb_entry - Connection_Manager_Uplink table updated - has_L2 false" ||
    raise "update_ovsdb_entry - Failed to update WifConnection_Manager_Uplinki_Inet_Config - has_L2 false" -l "$tc_name" -tc

wait_ovsdb_entry Connection_Manager_Uplink -w if_name "$if_name" -is has_L2 true &&
    log "$tc_name: wait_ovsdb_entry - Connection_Manager_Uplink - has_L2 is true" ||
    raise "wait_ovsdb_entry - Failed to reflect has_L2 to Connection_Manager_Uplink has_L2 is not true" -l "$tc_name" -tc

wait_ovsdb_entry Connection_Manager_Uplink -w if_name "$if_name" -is has_L3 true &&
    log "$tc_name: wait_ovsdb_entry - Connection_Manager_Uplink - has_L3 is true" ||
    raise "wait_ovsdb_entry - Failed to reflect has_L3 to Connection_Manager_Uplink has_L3 is not true" -l "$tc_name" -tc

pass
