#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$@

where options are:
    -h  show this help message

where arguments are:
    num_of_radios=\$@ -- used as number of radios to verify correct number of radios configured - (string)(required)

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0") 2
   /tmp/fut-base/shell/onbrd/$(basename "$0") 3
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

num_of_radios=$1

tc_name="onbrd/$(basename "$0")"
log "$tc_name: ONBRD Verify number of radios, waiting for '$num_of_radios'"

wait_for_function_response 0 "check_number_of_radios $num_of_radios" &&
    log "$tc_name: SUCCESS: number of radios $num_of_radios" ||
    raise "FAIL: number of radios $num_of_radios" -l "$tc_name" -tc

pass
