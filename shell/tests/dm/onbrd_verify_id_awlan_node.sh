#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$1

where options are:
    -h  show this help message

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0")
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

tc_name="onbrd/$(basename "$0")"

log "$tc_name: ONBRD Verify id, waiting for non-empty string"

wait_for_function_response 'notempty' "get_ovsdb_entry_value AWLAN_Node id" &&
    log "$tc_name: ID populated" ||
    raise "ID un-populated" -l "$tc_name" -tc

pass
