#!/bin/sh

# Setup test environment for DM tests.

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/dm_lib.sh"
source "${LIB_OVERRIDE_FILE}"

tc_name="dm/$(basename "$0")"

log_title "$tc_name: DM test environment script"

log "$tc_name: Stopping managers"
disable_managers

log "$tc_name: Starting only DM"
start_specific_manager dm

sleep 5
log "$tc_name: Checking managers"
ps w | grep -i plume
