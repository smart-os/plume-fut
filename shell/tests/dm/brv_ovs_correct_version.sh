#!/bin/sh

# Include environment config from default shell file
source /tmp/fut-base/shell/config/default_shell.sh
[ -e "/tmp/fut_set_env.sh" ] && source /tmp/fut_set_env.sh
# Include shared libraries and library overrides
source ${FUT_TOPDIR}/shell/lib/brv_lib.sh

tc_name="brv/$(basename $0)"
usage()
{
cat << EOF
${tc_name} [-h] expected_version
where options are:
    -h  show this help message
input arguments:
    expected_version=$1 -- expected version of the Open vSwitch - (string)(required)
this script is dependent on following:
    - running brv_setup.sh
example of usage:
   ${tc_name} "2.8.7"
EOF
}

while getopts h option; do
    case "$option" in
        h)
            usage
            exit 1
            ;;
    esac
done

NARGS=1
[ $# -ne ${NARGS} ] && raise "Requires ${NARGS} input arguments" -l "${tc_name}" -arg
expected_ovs_ver=$1
log_title "${tc_name}: Verify OVS version is ${expected_ovs_ver}"

check_ovs_version ${expected_ovs_ver} &&
    log -deb "${tc_name}: OVS version on the device is as expected: ${expected_ovs_ver}" ||
    raise "OVS version on the device: ${actual_ovs_ver} is NOT as expected: ${expected_ovs_ver}" -l "${tc_name}" -tc

pass
