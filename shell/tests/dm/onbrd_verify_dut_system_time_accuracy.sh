#!/bin/sh

# It is important for this particular testcase, to capture current time ASAP
time_now=$(date -u +"%s")

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$1

where options are:
    -h  show this help message
arguments:
    time_ref=\$1 -- format: seconds since epoch. Used to compare system time. (int)(required)
    time_accuracy=\$2 -- format: seconds. Allowed time deviation from reference time. (int)(required)
It is important to compare timestamps to the same time zone: UTC is used internally!

example of usage:
    accuracy=2
    reference_time=\$(date --utc +\"%s\")
   /tmp/fut-base/shell/onbrd/$(basename "$0") \$reference_time \$time_accuracy
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

# Input parameters
if [ $# -ne 2 ]; then
    echo 1>&2 "$0: incorrect number of input arguments"
    echo "$usage"
    exit 2
fi

time_ref=$1
time_accuracy=$2
tc_name="onbrd/$(basename "$0")"

# Timestamps in human readable format
time_ref_str=$(date -d @"${time_ref}")
time_now_str=$(date -d @"${time_now}")

# Calculate time difference and ensure absolute value
time_diff=$(( time_ref - time_now ))
if [ $time_diff -lt 0 ]; then
    time_diff=$(( -time_diff ))
fi

log "$tc_name: Checking time ${time_now_str} against reference ${time_ref_str}"
if [ $time_diff -le "$time_accuracy" ]; then
    log "$tc_name: Time difference ${time_diff}s is within ${time_accuracy}s"
else
    raise "Time difference ${time_diff}s is NOT within ${time_accuracy}s" -l "$tc_name" -tc
fi

pass
