#!/bin/sh

# Setup test environment for ONBRD tests.

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

tc_name="onbrd/$(basename "$0")"

onbrd_setup_test_environment "$@" &&
    log "$tc_name: onbrd_setup_test_environment - Success " ||
    raise "onbrd_setup_test_environment - Failed" -l "$tc_name" -ds

exit 0
