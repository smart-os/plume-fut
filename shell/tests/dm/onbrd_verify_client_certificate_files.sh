#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$1

where options are:
    -h  show this help message

where arguments are:
    cert_file=\$1 -- used as file to check - (string)(required)

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0") ca_cert
   /tmp/fut-base/shell/onbrd/$(basename "$0") certificate
   /tmp/fut-base/shell/onbrd/$(basename "$0") private_key
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -ne 1 ]; then
    echo 1>&2 "$0: incorrect number of input arguments"
    echo "$usage"
    exit 2
fi

cert_file=$1
tc_name="onbrd/$(basename "$0")"
cert_file_path=$(get_ovsdb_entry_value SSL "$cert_file")

log "$tc_name: ONBRD Verify file exists"
[ -e "$cert_file_path" ] &&
    log "$tc_name: SUCCESS: file is valid - $cert_file_path" ||
    raise "FAIL: file is missing - $cert_file_path" -l "$tc_name" -tc

log "$tc_name: ONBRD Verify file is not empty"
[ -s "$cert_file_path" ] &&
    log "$tc_name: SUCCESS: file is not empty - $cert_file_path" ||
    raise "FAIL: file is empty - $cert_file_path" -l "$tc_name" -tc

pass
