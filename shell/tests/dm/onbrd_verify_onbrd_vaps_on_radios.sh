#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${FUT_TOPDIR}/shell/lib/wm2_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$1

where options are:
    -h  show this help message

where arguments are:
    if_name=\$1 -- used as interface name to check - (string)(required)

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0") onboard-ap-24
   /tmp/fut-base/shell/onbrd/$(basename "$0") onboard-ap-l50
   /tmp/fut-base/shell/onbrd/$(basename "$0") onboard-ap-u50
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -lt 1 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

interface_name=$1

tc_name="onbrd/$(basename "$0")"

log "$tc_name: ONBRD Verify onboarding VAPs on all radios, check interface $interface_name"

wait_for_function_response 0 "check_ovsdb_entry Wifi_VIF_State -w if_name $interface_name" &&
    log "$tc_name: SUCCESS: interface $interface_name exists" ||
    raise "FAIL: interface $interface_name does not exist" -l "$tc_name" -tc

log "$tc_name: Clean created interfaces after test"
vif_clean

pass
