#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/unit_lib.sh"
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$1 \$2

where options are:
    -h  show this help message

where arguments are:
    firmware_version=\$1 -- used as FW version to verify running correct FW - (string)(required)
    only_check_nonempty=\$2 -- used to override exact FW version match and only check if firmware_version is populated - (string)(optional)

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0") 2.4.3-72-g65b961c-dev-debug
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [ $# -ne 1 ]; then
    echo 1>&2 "$0: incorrect number of input arguments"
    echo "$usage"
    exit 2
fi

search_rule=${1:-"non_empty"}

tc_name="onbrd/$(basename "$0")"

if [ "$search_rule" = "non_empty" ]; then
    log "$tc_name: ONBRD Verify FW version, waiting for $search_rule string"
    wait_for_function_response 'notempty' "get_ovsdb_entry_value AWLAN_Node firmware_version" &&
        log "$tc_name: check_firmware_version_populated - firmware_version populated" ||
        raise "check_firmware_version_populated - firmware_version un-populated" -l "$tc_name" -tc
elif [ "$search_rule" = "pattern_match" ]; then
    log "$tc_name: ONBRD Verify FW version, waiting for $search_rule"
    wait_for_function_response 0 "onbrd_check_fw_pattern_match" &&
        log "$tc_name: check_fw_pattern_match - firmware_version patter match" ||
        raise "check_fw_pattern_match - firmware_version patter didn't match" -l "$tc_name" -tc
else
    log "$tc_name: ONBRD Verify FW version, waiting for exactly '$search_rule'"
    wait_ovsdb_entry AWLAN_Node -is firmware_version "$search_rule" &&
        log "$tc_name: wait_ovsdb_entry - AWLAN_Node firmware_version equal to '$1'" ||
        raise "wait_ovsdb_entry - AWLAN_Node firmware_version NOT equal to '$1'" -l "$tc_name" -tc
fi

pass
