#!/bin/sh

# Include environment config from default shell file
source /tmp/fut-base/shell/config/default_shell.sh
[ -e "/tmp/fut_set_env.sh" ] && source /tmp/fut_set_env.sh
# Include shared libraries and library overrides
source ${FUT_TOPDIR}/shell/lib/brv_lib.sh

tc_name="brv/$(basename $0)"
usage()
{
cat << EOF
${tc_name} [-h] builtin_tool
where options are:
    -h  show this help message
input arguments:
    builtin_tool=$1 -- name of the required busybox built-in tool - (string)(required)
this script is dependent on following:
    - running brv_setup.sh
example of usage:
   ${tc_name} "tail"
EOF
}

while getopts h option; do
    case "$option" in
        h)
            usage
            exit 1
            ;;
    esac
done

NARGS=1
[ $# -lt ${NARGS} ] && raise "Requires at least '${NARGS}' input argument(s)" -l "${tc_name}" -arg
builtin_tool=$1
log_title "${tc_name}: Verify '${builtin_tool}' is built into busybox"

is_tool_on_system "busybox"
rc=$?
if [ $rc != 0 ]; then
    raise "Refusing tool search, busybox is not present on system" -l "${tc_name}" -nf
fi

is_busybox_builtin ${builtin_tool}
rc=$?
if [ $rc == 0 ]; then
    log -deb "${tc_name}: '${builtin_tool}' is built into busybox"
else
    raise "'${builtin_tool}' is not built into busybox" -l "${tc_name}" -tc
fi

pass
