#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source "${FUT_TOPDIR}/shell/lib/onbrd_lib.sh"
source "${LIB_OVERRIDE_FILE}"

usage="
$(basename "$0") [-h] \$1

where options are:
    -h  show this help message

this script is dependent on following:
    - running DM manager

example of usage:
   /tmp/fut-base/shell/onbrd/$(basename "$0") not_set
   /tmp/fut-base/shell/onbrd/$(basename "$0") cloud
   /tmp/fut-base/shell/onbrd/$(basename "$0") monitor
   /tmp/fut-base/shell/onbrd/$(basename "$0") battery
"

while getopts h option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

device_mode=${1:-"not_set"}

tc_name="onbrd/$(basename "$0")"

log "$tc_name: ONBRD Verify device mode in AWLAN_Node"

if [ "$device_mode" = "not_set" ]; then
    wait_ovsdb_entry AWLAN_Node -is device_mode "[\"set\",[]]" &&
        log "$tc_name: wait_ovsdb_entry - AWLAN_Node device_mode equal to '[\"set\",[]]'" ||
        raise "wait_ovsdb_entry - AWLAN_Node device_mode NOT equal to '$device_mode'" -l "$tc_name" -tc
else
    wait_ovsdb_entry AWLAN_Node -is device_mode "$device_mode" &&
        log "$tc_name: wait_ovsdb_entry - AWLAN_Node device_mode equal to '$device_mode'" ||
        raise "wait_ovsdb_entry - AWLAN_Node device_mode NOT equal to '$device_mode'" -l "$tc_name" -tc
fi

pass
