#!/bin/sh

if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${FUT_TOPDIR}/shell/lib/um_lib.sh
source ${LIB_OVERRIDE_FILE}

usage="
$(basename "$0") [-h] \$1 \$2 \$3 \$4

where options are:
    -h  show this help message

where arguments are:
    fw_path=\$1 -- download path of UM - used to clear the folder on UM setup - (string)(required)
    fw_url=\$2 -- used as firmware_url in AWLAN_Node table - (string)(required)
    fw_name=\$3 -- used as FW name to validate file and md5 sum files - (string)(required)

this script is dependent on following:
    - running UM manager
    - udhcpc on interface

example of usage:
   /tmp/fut-base/shell/nm2/$(basename "$0").sh /tmp/pfirmware http://url_to_image image_name.eim
"

while getopts hcs:fs: option; do
    case "$option" in
        h)
            echo "$usage"
            exit 1
            ;;
    esac
done

if [[ $# -lt 2 ]]; then
    echo 1>&2 "$0: not enough arguments"
    echo "$usage"
    exit 2
fi

fw_path=$1
fw_url=$2
fw_name=${fw_url##*/}
tc_name="um/$(basename "$0")"

trap '
  reset_um_triggers $fw_path || true
  run_setup_if_crashed um || true
' EXIT SIGINT SIGTERM

log "$tc_name: UM Download FW - firmware_url"

log "$tc_name: Setting firmware_url to $fw_url"
update_ovsdb_entry AWLAN_Node -u firmware_url "$fw_url" &&
    log "$tc_name: update_ovsdb_entry - AWLAN_Node firmware_url set to $fw_url" ||
    raise "update_ovsdb_entry - Failed to set firmware_url to $fw_url in AWLAN_Node" -l "$tc_name" -tc

log "$tc_name: Waiting for FW download start"
wait_ovsdb_entry AWLAN_Node -is upgrade_status $(get_um_code "UPG_STS_FW_DL_START") &&
    log "$tc_name: wait_ovsdb_entry - Success to wait" ||
    raise "wait_ovsdb_entry - Failed to wait" -l "$tc_name" -tc

log "$tc_name: Waiting for FW download finish"
wait_ovsdb_entry AWLAN_Node -is upgrade_status $(get_um_code "UPG_STS_FW_DL_END") &&
    log "$tc_name: wait_ovsdb_entry - Success to wait" ||
    raise "wait_ovsdb_entry - Failed to wait" -l "$tc_name" -tc

log "$tc_name: Checking for image in /tmp/pfirmware"
wait_for_function_response 0 "ls $fw_path/$fw_name" &&
    log "$tc_name: Image exists in $fw_path" ||
    raise "Image doesn't exist in $fw_path" -l "$tc_name" -tc

log "$tc_name: Checking for image md5 sum in $fw_path"
wait_for_function_response 0 "ls $fw_path/$fw_name.md5" &&
    log "$tc_name: Image exists in $fw_path" ||
    raise "Image doesn't exist in $fw_path" -l "$tc_name" -tc

pass
