#!/bin/sh

# Setup test environment for UM tests.

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${FUT_TOPDIR}/shell/lib/um_lib.sh
source ${LIB_OVERRIDE_FILE}

tc_name="um/$(basename "$0")"

um_setup_test_environment "$@" &&
    log "$tc_name: um_setup_test_environment - Success " ||
    raise "$tc_name: um_setup_test_environment - Failed" -l "$tc_name" -ds

exit 0
