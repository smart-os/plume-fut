#!/bin/sh

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${LIB_OVERRIDE_FILE}

DEMO_MANAGER_BIN_NAME="hello_world"

log_title "$DEMO_MANAGER_BIN_NAME: SETUP"
device_init

start_openswitch

log "Ensure single instance of $DEMO_MANAGER_BIN_NAME"
killall_process_by_name "${DEMO_MANAGER_BIN_NAME}"

log "Starting $DEMO_MANAGER_BIN_NAME"
start_specific_manager ${DEMO_MANAGER_BIN_NAME} ||
    raise "start_specific_manager $DEMO_MANAGER_BIN_NAME" -l "$fn_name" -fc

log "Changing $DEMO_MANAGER_BIN_NAME log level to TRACE"
${OVSH} delete AW_Debug -w name==$DEMO_MANAGER_BIN_NAME
${OVSH} insert AW_Debug name:=$DEMO_MANAGER_BIN_NAME log_severity:=TRACE

pass "Setup completed!"
