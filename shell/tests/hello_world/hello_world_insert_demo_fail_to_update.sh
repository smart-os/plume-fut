#!/bin/sh

# This test script will simulate an issue with the manager target layer
# implementation, which will look like failing to update OVSDB - layer 1 test.
# This test is expected to fail for demonstration purposes, to show what
# happens when tests fail due to incorrect manager implementation - in this
# case, manager failing to updatte Node_State table, yet still writing to file.

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh

DEMO_MODULE_NAME="hello-world"
DEMO_OUTPUT_FILE=${DEMO_OUTPUT_FILE:-"/tmp/$DEMO_MODULE_NAME-demo"}
DEMO_TEST_TITLE="Fail to update ovsdb table"
# Input arguments:
DEMO_TEST_KEY=${1:-"demo"}
DEMO_TEST_VALUE=${2:-"fail-to-update"}

log_title "$DEMO_MODULE_NAME: $DEMO_TEST_TITLE"

log "Test preconditions: Clean ovsdb table if not empty"
${OVSH} delete Node_Config || die "Failed to empty table"

log "Start test: Write to Node_Config"
${OVSH} insert Node_Config module:=$DEMO_MODULE_NAME key:=$DEMO_TEST_KEY value:=$DEMO_TEST_VALUE || die "Failed"

# Level 1 test - checking correct OVSDB behaviour
log "Waiting for Node_State table to reflect entry in Node_Config table"
${OVSH} wait Node_State --where module==$DEMO_MODULE_NAME key:=$DEMO_TEST_KEY module:=$DEMO_MODULE_NAME value:=$DEMO_TEST_VALUE || die_with_code 11 "Failed"

# Level 2 test - checking the expected actions were applied to the system
#   Note: This test is redundant, due to above test failing, but is kept for clarity
log "Checking correct system action was performed"
log "Verifying existence of file $DEMO_OUTPUT_FILE."
[ -f $DEMO_OUTPUT_FILE ] || die_with_code 21 "File not present on system!"
file_content="$(cat $DEMO_OUTPUT_FILE)"
log "Expecting file content: $DEMO_TEST_KEY=$DEMO_TEST_VALUE"
[ "$file_content" = "$DEMO_TEST_KEY=$DEMO_TEST_VALUE" ] || die_with_code 22 "File content not correct: $file_content"

pass "$DEMO_TEST_TITLE - TEST PASSED"
