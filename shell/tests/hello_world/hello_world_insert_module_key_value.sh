#!/bin/sh

# This is a basic test script, verifying what happens when writing to ovsdb.
# It verifies the manager target layer implementation in two layers of tests:
#   Layer 1: verify that whatever was written into Node_Config table
#            is reflected in Node_State table, after supposed.
#   Layer 2: verify that the expected action resulted in changes on the system
#            is reflected in Node_State table, after supposed expected action.

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi
source ${FUT_TOPDIR}/shell/lib/unit_lib.sh

DEMO_MODULE_NAME="hello-world"
DEMO_OUTPUT_FILE=${DEMO_OUTPUT_FILE:-"/tmp/$DEMO_MODULE_NAME-demo"}
DEMO_TEST_TITLE="Insert module-key-value into OVSDB"
# Input arguments:
DEMO_TEST_KEY=${1:-"fut-variable"}
DEMO_TEST_VALUE=${2:-"test-value"}

log_title "$DEMO_MODULE_NAME: $DEMO_TEST_TITLE"

log "Test preconditions: Clean ovsdb table if not empty"
${OVSH} delete Node_Config || die "Failed to empty table"

log "Start test: Write to Node_Config"
${OVSH} insert Node_Config module:=$DEMO_MODULE_NAME key:=$DEMO_TEST_KEY value:=$DEMO_TEST_VALUE || die "Failed"

# Level 1 test - checking correct OVSDB behaviour
log "Waiting for Node_State table to reflect entry in Node_Config table"
${OVSH} wait Node_State --where module==$DEMO_MODULE_NAME key:=$DEMO_TEST_KEY module:=$DEMO_MODULE_NAME value:=$DEMO_TEST_VALUE || die_with_code 11 "Failed"

# Level 2 test - checking the expected actions were applied to the system
log "Checking correct system action was performed"
log "Verifying existence of file $DEMO_OUTPUT_FILE."
[ -f $DEMO_OUTPUT_FILE ] || die_with_code 21 "File not present on system!"
file_content="$(cat $DEMO_OUTPUT_FILE)"
log "Expecting file content: $DEMO_TEST_KEY=$DEMO_TEST_VALUE"
[ "$file_content" = "$DEMO_TEST_KEY=$DEMO_TEST_VALUE" ] || die_with_code 22 "File content not correct: $file_content"

pass "$DEMO_TEST_TITLE - TEST PASSED"
