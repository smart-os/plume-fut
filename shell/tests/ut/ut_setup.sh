#!/bin/sh

# Setup test environment for Unit Tests.

# Include basic environment config from default shell file and if any from FUT framework generated /tmp/fut_set_env.sh file
if [ -e "/tmp/fut_set_env.sh" ]; then
    source /tmp/fut_set_env.sh
else
    source /tmp/fut-base/shell/config/default_shell.sh
fi

source ${FUT_TOPDIR}/shell/lib/unit_lib.sh
source ${FUT_TOPDIR}/shell/lib/ut_lib.sh
source ${LIB_OVERRIDE_FILE}

tc_name="ut/$(basename "$0")"

ut_setup_test_environment "$@" &&
    log "$tc_name: ut_setup_test_environment - Success " ||
    raise "ut_setup_test_environment - Failed" -l "$tc_name" -ds

exit 0
