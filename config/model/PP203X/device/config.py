"""
    Configuration 'framework_cfg' determines variables used within the framework.

    'py_test_timeout' - Timeout in seconds for each pytest testcase (integer)(mandatory)
    'test_script_timeout' - Timeout in seconds, used to determine shell timeouts (integer)(mandatory)
    'log_messages_path' - Path relative to FUT sources on testbed server, where system logs are placed (string)(legacy)
    'log_dump_path' - Path relative to FUT sources on testbed server, where log tarballs are placed (string)(legacy)
    'username' - Username for device management SSH access (string)(mandatory)
    'password' - Password for device management SSH access  (string)(optional, if using ssh key)
    'transfer_method' - File transfer method to and from the device (string)(mandatory, options: 'curl', 'scp'=default)
"""

framework_cfg = {
    'py_test_timeout': 60,
    'test_script_timeout': 30,
    'log_messages_path': 'logread/',
    'log_dump_path': 'logpull/',
    'username': 'plume',
    'password': 'Plume1234Development',
    'transfer_method': 'scp',
    'pod_api_model': 'pp203x',
    'device_version_cmd': "sed 's/ .*//' /.version",
}

"""
    Configuration 'shell_cfg' determines the environment variables exported on the device for shell script execution.

    Configuration values of type tuple are constructed during runtime, by replacing curly braces in the first item with
    values of the variables in subsequent items. For example, the 'OVSH' key will evaluate into the value:
        'OVSH': '/usr/opensync/tools/ovsh --quiet --timeout=30000'
    if the variables have values: opensync_rootdir = '/usr/opensync' and test_script_timeout = 30

    'FUT_TOPDIR' - Top directory of FUT on the device
    'OPENSYNC_ROOTDIR' - Root directory of OpenSync on the device
    'LIB_OVERRIDE_FILE' - path to the library override file on the device. This file is used to override default
        implementation of functions in FUT libraries, to match device execution environment and capabilities.
    'DEFAULT_WAIT_TIME' - Timeout in milliseconds used for ovsh commands
    'OVSH' - 'ovsh' tool invocation command, containing absolute path to the tool and all desired flags
    'LOGREAD' - Command for reading system logs
    'LOGDUMP' (optional) command which outputs a tarball containing system logs into /tmp/manual_logpull.tar.gz
    'PATH' - Export a custom system PATH on the device and override the default system PATH
    'MODEL' - Model string as reported by OpenSync
"""

shell_cfg = {
    'FUT_TOPDIR': '/tmp/fut-base',
    'OPENSYNC_ROOTDIR': '/usr/plume',
    'LIB_OVERRIDE_FILE': ('{}/shell/lib/override/pp203x_lib_override.sh', 'fut_dir'),
    'DEFAULT_WAIT_TIME': ('{}', 'test_script_timeout'),
    'OVSH': ('{}/tools/ovsh --quiet --timeout={}000', 'opensync_rootdir', 'test_script_timeout'),
    'LOGREAD': ('{}/tools/logread', 'opensync_rootdir'),
    'LOGDUMP': ('{}/bin/lm_logs_collector.sh --stdout', 'opensync_rootdir'),
    'PATH': ('/bin:/sbin:/usr/bin:/usr/sbin:{}/tools', 'opensync_rootdir'),
    'MODEL': 'PP203X',
    'ifconfig': '/sbin/ifconfig',
}

"""
    Configuration 'device_cfg' determines model specific properties of a device.
        Entries that are not applicable should have the value None.
"""

device_cfg = {
    'backhaul_ap': {
        '24g': 'bhaul-ap-24',
        '5g': None,
        '5gl': 'bhaul-ap-l50',
        '5gu': 'bhaul-ap-u50',
    },
    'backhaul_sta': {
        '24g': 'bhaul-sta-24',
        '5g': None,
        '5gl': 'bhaul-sta-l50',
        '5gu': 'bhaul-sta-u50',
    },
    'device_type': 'extender',
    'home_ap': {
        '24g': 'home-ap-24',
        '5g': None,
        '5gl': 'home-ap-l50',
        '5gu': 'home-ap-u50',
    },
    'interface_name_eth_lan': 'eth1',
    'interface_name_eth_wan': 'eth0',
    'interface_name_lan_bridge': 'br-home',
    'interface_name_ppp_wan': 'ppp-wan',
    'interface_name_wan_bridge': 'br-wan',
    'model_string': 'PP203X',
    'mtu_backhaul': 1600,
    'mtu_uplink_gre': 1562,
    'mtu_wan': 1500,
    'onboard_ap': {
        '24g': 'onboard-ap-24',
        '5g': None,
        '5gl': 'onboard-ap-l50',
        '5gu': 'onboard-ap-u50',
    },
    'patch_port_lan-to-wan': 'patch-h2w',
    'patch_port_wan-to-lan': 'patch-w2h',
    'phy_radio_name': {
        '24g': 'wifi0',
        '5g': None,
        '5gl': 'wifi1',
        '5gu': 'wifi2',
    },
    'radio_antennas': {
        '24g': '2x2',
        '5g': None,
        '5gl': '2x2',
        '5gu': '4x4',
    },
    'radio_channels': {
        '24g': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        '5g': None,
        '5gl': [36, 40, 44, 48, 52, 56, 60, 64],
        '5gu': [100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144, 149, 153, 157, 161, 165],
    },
    'regulatory_domain': 'US',
    'um_fw_download_path': '/tmp/pfirmware',
    'uplink_gre': {
        '24g': 'g-bhaul-sta-24',
        '5g': None,
        '5gl': 'g-bhaul-sta-l50',
        '5gu': 'g-bhaul-sta-u50',
    },
    'vif_radio_idx': {
        'backhaul_ap': 1,
        'backhaul_sta': 0,
        'home_ap': 2,
        'onboard_ap': 3,
    },
}
