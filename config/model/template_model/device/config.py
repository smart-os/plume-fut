"""
    Configuration 'framework_cfg' determines variables used within the framework.

    'py_test_timeout' - Timeout in seconds for each pytest testcase (integer)(mandatory)
    'test_script_timeout' - Timeout in seconds, used to determine shell timeouts (integer)(mandatory)
    'log_messages_path' - Path relative to FUT sources on testbed server, where system logs are placed (string)(legacy)
    'log_dump_path' - Path relative to FUT sources on testbed server, where log tarballs are placed (string)(legacy)
    'username' - Username for device management SSH access (string)(mandatory)
    'password' - Password for device management SSH access  (string)(optional, if using ssh key)
    'transfer_method' - File transfer method to and from the device (string)(mandatory, options: 'curl', 'scp'=default)
"""

framework_cfg = {
    'py_test_timeout': 60,
    'test_script_timeout': 30,
    'log_messages_path': 'logread/',
    'log_dump_path': 'logpull/',
    'username': 'osync',
    'password': 'osync123',
    'transfer_method': 'scp',
    'pod_api_model': 'pp213x'
}

"""
    Configuration 'shell_cfg' determines the environment variables exported on the device for shell script execution.

    Configuration values of type tuple are constructed during runtime, by replacing curly braces in the first item with
    values of the variables in subsequent items. For example, the 'OVSH' key will evaluate into the value:
        'OVSH': '/usr/opensync/tools/ovsh --quiet --timeout=30000'
    if the variables have values: opensync_rootdir = '/usr/opensync' and test_script_timeout = 30

    'FUT_TOPDIR' - Top directory of FUT on the device
    'OPENSYNC_ROOTDIR' - Root directory of OpenSync on the device
    'LIB_OVERRIDE_FILE' - path to the library override file on the device. This file is used to override default
        implementation of functions in FUT libraries, to match device execution environment and capabilities.
    'DEFAULT_WAIT_TIME' - Timeout in milliseconds used for ovsh commands
    'OVSH' - 'ovsh' tool invocation command, containing absolute path to the tool and all desired flags
    'LOGREAD' - Command for reading system logs
    'LOGDUMP' (optional) command which outputs a tarball containing system logs into /tmp/manual_logpull.tar.gz
    'PATH' - Export a custom system PATH on the device and override the default system PATH
    'MODEL' - Model string as reported by OpenSync
"""

shell_cfg = {
    'FUT_TOPDIR': '/tmp/fut-base',
    'OPENSYNC_ROOTDIR': '/usr/opensync',
    'LIB_OVERRIDE_FILE': ('{}/shell/lib/override/osync_ex_lib_override.sh', 'fut_dir'),
    'DEFAULT_WAIT_TIME': ('{}', 'test_script_timeout'),
    'OVSH': ('{}/tools/ovsh --quiet --timeout={}000', 'opensync_rootdir', 'test_script_timeout'),
    'LOGREAD': 'cat /var/log/messages',
    'PATH': ('/bin:/sbin:/usr/bin:/usr/sbin:{}/tools:/opt/bin:/opt/sbin', 'opensync_rootdir'),
    'MODEL': 'OpenSyncExtender',
}

"""
    Configuration 'device_cfg' determines model specific properties of a device.
        Entries that are not applicable should have the value None.
"""

device_cfg = {
    'backhaul_ap': {
        '24g': 'backhaul_ap_24g',
        '5g': 'backhaul_ap_5g',
        '5gl': 'backhaul_ap_5gl',
        '5gu': 'backhaul_ap_5gu',
    },
    'backhaul_sta': {
        '24g': 'backhaul_sta_24g',
        '5g': 'backhaul_sta_5g',
        '5gl': 'backhaul_sta_5gl',
        '5gu': 'backhaul_sta_5gu',
    },
    'device_type': 'extender',
    'home_ap': {
        '24g': 'home_ap_24g',
        '5g': 'home_ap_5g',
        '5gl': 'home_ap_5gl',
        '5gu': 'home_ap_5gu',
    },
    'interface_name_eth_lan': 'eth1',
    'interface_name_eth_wan': 'eth0',
    'interface_name_lan_bridge': 'br-home',
    'interface_name_ppp_wan': 'ppp-wan',
    'interface_name_wan_bridge': 'br-wan',
    'model_string': 'OpenSyncExtender',
    'mtu_backhaul': 1600,
    'mtu_uplink_gre': 1562,
    'mtu_wan': 1500,
    'onboard_ap': {
        '24g': 'onboard_ap_24g',
        '5g': 'onboard_ap_5g',
        '5gl': 'onboard_ap_5gl',
        '5gu': 'onboard_ap_5gu',
    },
    'patch_port_lan-to-wan': 'patch-h2w',
    'patch_port_wan-to-lan': 'patch-w2h',
    'phy_radio_name': {
        '24g': 'phy_radio_name_24g',
        '5g': 'phy_radio_name_5g',
        '5gl': 'phy_radio_name_5gl',
        '5gu': 'phy_radio_name_5gu',
    },
    'radio_antennas': {
        '24g': '2x2',
        '5g': '2x2',
        '5gl': '3x3',
        '5gu': '4x4',
    },
    'radio_channels': {
        '24g': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
        '5g': [36, 40, 44, 48,
               52, 56, 60, 64,
               100, 104, 108, 112,
               116, 120, 124, 128, 132,
               136, 140, 144,
               149, 153, 157, 161, 165],
        '5gl': [36, 40, 44, 48,
                52, 56, 60, 64,
                100, 104, 108, 112,
                116, 120, 124, 128, 132,
                136, 140, 144,
                149, 153, 157, 161, 165],
        '5gu': [36, 40, 44, 48,
                52, 56, 60, 64,
                100, 104, 108, 112,
                116, 120, 124, 128, 132,
                136, 140, 144,
                149, 153, 157, 161, 165],
    },
    'regulatory_domain': 'US',
    'um_fw_download_path': '/tmp/pfirmware',
    'uplink_gre': {
        '24g': 'uplink_gre_24g',
        '5g': 'uplink_gre_5g',
        '5gl': 'uplink_gre_5gl',
        '5gu': 'uplink_gre_5gu',
    },
    'vif_radio_idx': {
        'backhaul_ap': 1,
        'backhaul_sta': 0,
        'home_ap': 2,
        'onboard_ap': 3,
    },
}
