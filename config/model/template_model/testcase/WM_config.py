test_cfg = {
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name, default channel
    # Test checks if cac aborting is working properly
    "wm2_dfs_cac_aborted": [
        {
            "channel1": 120,
            "channel2": 100,
            "channel_type": "DFS",
            "comment": "Testing DFS channels",
            "default_channel": 120,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters: password, hw_mode, mode, if_name, radio_band, radio_idx,
    # ssid, vif_if_name
    "wm2_ht_mode_and_channel_iteration": [
        {
            "channel_mode": "manual",
            "channels": [
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
            ],
            "enabled": "true",
            "ht_modes": [
                "HT20",
                "HT40",
            ],
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channels": [
                36,
                40,
                44,
                48,
                52,
                56,
                60,
                64,
            ],
            "comment": "HT20/40/80 5GL",
            "enabled": "true",
            "ht_modes": [
                "HT20",
                "HT40",
                "HT80",
            ],
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channels": [
                100,
                104,
                108,
                112,
                116,
                120,
                124,
                128,
                132,
                136,
                140,
                144,
                149,
                153,
                157,
                161,
            ],
            "comment": "HT20/40/80 5GU",
            "enabled": "true",
            "ht_modes": [
                "HT20",
                "HT40",
                "HT80",
            ],
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
        {
            "channels": [
                165,
            ],
            "comment": "HT20 c165 5GU",
            "enabled": "true",
            "ht_modes": [
                "HT20",
            ],
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, freq_band,
    # radio_idx, ssid, vif_if_name
    # freq_band is intentionally WRONG to test will immutable field be changed or not
    "wm2_immutable_radio_freq_band": [
        {
            "channel": 2,
            "comment": "Testing IMMUTABLE field FREQ_BAND 2.4G",
            "enabled": "true",
            "freq_band": "5GL",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing IMMUTABLE field FREQ_BAND 5GL",
            "enabled": "true",
            "freq_band": "5GU",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing IMMUTABLE field FREQ_BAND 5GU",
            "enabled": "true",
            "freq_band": "5GL",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # custom_hw_mode is intentionally WRONG to test will immutable field be changed or not
    "wm2_immutable_radio_hw_mode": [
        {
            "channel": 2,
            "comment": "Testing IMMUTABLE field HW_MODE 2.4G",
            "custom_hw_mode": "11b",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing IMMUTABLE field HW_MODE 5GL",
            "custom_hw_mode": "11n",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing IMMUTABLE field HW_MODE 5GU",
            "custom_hw_mode": "11g",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # hw_type is intentionally WRONG to test will immutable field be changed or not
    "wm2_immutable_radio_hw_type": [
        {
            "channel": 2,
            "comment": "Testing IMMUTABLE field HW_TYPE 2.4G",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "hw_type": "qca4219",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing IMMUTABLE field HW_TYPE 5GL",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "hw_type": "qca4220",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing IMMUTABLE field HW_TYPE 5GU",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "hw_type": "qca4221",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # custom_if_name is intentionally WRONG to test will immutable field be changed or not
    "wm2_immutable_radio_if_name": [
        {
            "channel": 2,
            "comment": "Testing IMMUTABLE field IF_NAME 2.4G",
            "custom_if_name": "wifi10",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing IMMUTABLE field IF_NAME 5GL",
            "custom_if_name": "wifi20",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing IMMUTABLE field IF_NAME 5GU",
            "custom_if_name": "wifi30",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # Test tries to set custom bcn_int (beacon interval)
    "wm2_set_bcn_int": [
        {
            "bcn_int": 200,
            "channel": 2,
            "comment": "Testing BEACON INTERVAL 2.4G",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "bcn_int": 400,
            "channel": 36,
            "comment": "Testing BEACON INTERVAL 5GL",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "bcn_int": 600,
            "channel": 149,
            "comment": "Testing BEACON INTERVAL 5GU",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # Test tries to use DFS channel. If channel is not ready for use test will be skipped
    "wm2_set_channel_dfs": [
        {
            "channel_type": "DFS",
            "channels": [
                52,
                56,
                60,
                64,
            ],
            "comment": "Testing DFS channels",
            "default_channel": 36,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel_type": "DFS",
            "channels": [
                100,
                104,
                108,
                112,
                116,
                120,
                124,
                128,
                132,
                136,
                140,
            ],
            "comment": "Testing DFS channels",
            "default_channel": 149,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # Test tries to use NON DFS channel. If channel is not valid it will be skipped
    "wm2_set_channel_non_dfs": [
        {
            "channel_type": "NON_DFS",
            "channels": [
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10,
                11,
            ],
            "default_channel": 2,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel_type": "NON_DFS",
            "channels": [
                36,
                40,
                44,
                48,
            ],
            "comment": "Testing NON DFS channels",
            "default_channel": 36,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel_type": "NON_DFS",
            "channels": [
                149,
                153,
                157,
                161,
                165,
            ],
            "comment": "Testing NON DFS channels",
            "default_channel": 149,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # Test tries to use different HT_MODES on different channels. If channels are not read (DFS) or wrong it will be
    # skipped
    "wm2_set_ht_mode": [
        {
            "channels": [
                2,
                6,
                10,
            ],
            "comment": "Testing HT MODE at several channels 2.4G",
            "enabled": "true",
            "ht_modes": [
                "HT20",
                "HT40",
            ],
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channels": [
                36,
                40,
                44,
                48,
            ],
            "comment": "Testing HT MODE at several channels 5GL",
            "enabled": "true",
            "ht_modes": [
                "HT20",
                "HT40",
                "HT80",
            ],
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channels": [
                149,
                153,
                157,
                161,
            ],
            "comment": "Testing HT MODE at several channels 5GU",
            "enabled": "true",
            "ht_modes": [
                "HT20",
                "HT40",
                "HT80",
            ],
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    "wm2_set_radio_country": [
        {
            "channel": 2,
            "comment": "Testing COUNTRY field 2.4G - LEVEL1 ONLY",
            "country": "DE",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing COUNTRY field 5GL - LEVEL1 ONLY",
            "country": "DE",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing COUNTRY field 5GU - LEVEL1 ONLY",
            "country": "DE",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
        {
            "channel": 2,
            "comment": "Testing COUNTRY field 2.4G - LEVEL1 ONLY",
            "country": "US",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing COUNTRY field 5GL - LEVEL1 ONLY",
            "country": "US",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing COUNTRY field 5GU - LEVEL1 ONLY",
            "country": "US",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # Test tries to disable and re-enable radios/interfaces
    "wm2_set_radio_enabled": [
        {
            "channel": 2,
            "comment": "Testing ENABLED field 2.4G",
            "enabled": "false",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing ENABLED field 5GL",
            "enabled": "false",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing ENABLED field 5GU",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT:
    #   vif_radio_idx, if_name, ssid, security, channel, ht_mode, hw_mode
    # mode, country, vif_if_name, fallback_bssid, fallback_channel
    # Test checks if FALLBACK_PARENTS are activated when DFS radar event is triggered
    "wm2_set_radio_fallback_parents": [
        {
            "channel": 56,
            "comment": "Testing FALLBACK_PARENTS field 5G",
            "enabled": "true",
            "fallback_channel": 36,
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5g_network",
            "vif_if_name": "home-ap-50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 60,
            "comment": "Testing FALLBACK_PARENTS field 5G",
            "enabled": "true",
            "fallback_channel": 40,
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5g_network",
            "vif_if_name": "home-ap-50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 64,
            "comment": "Testing FALLBACK_PARENTS field 5G",
            "enabled": "true",
            "fallback_channel": 48,
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5g_network",
            "vif_if_name": "home-ap-50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 100,
            "comment": "Testing FALLBACK_PARENTS field 5G",
            "enabled": "true",
            "fallback_channel": 149,
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5g_network",
            "vif_if_name": "home-ap-50",
            "vif_radio_idx": 4,
        },
        {
            "channel": 104,
            "comment": "Testing FALLBACK_PARENTS field 5G",
            "enabled": "true",
            "fallback_channel": 153,
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5g_network",
            "vif_if_name": "home-ap-50",
            "vif_radio_idx": 4,
        },
        {
            "channel": 108,
            "comment": "Testing FALLBACK_PARENTS field 5G",
            "enabled": "true",
            "fallback_channel": 157,
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5g_network",
            "vif_if_name": "home-ap-50",
            "vif_radio_idx": 4,
        },
    ],
    "wm2_set_radio_thermal_tx_chainmask": [
        {
            "channel": 2,
            "comment": "Testing TX CHAINMASK 2.4G",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "thermal_tx_chainmask": 2,
            "tx_chainmask": 3,
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing TX CHAINMASK 5GL",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "thermal_tx_chainmask": 1,
            "tx_chainmask": 3,
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing TX CHAINMASK 5GU",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "thermal_tx_chainmask": 12,
            "tx_chainmask": 15,
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # Test tries to set tx_chainmask to a custom value. If value is not valid test will fail
    "wm2_set_radio_tx_chainmask": [
        {
            "channel": 2,
            "comment": "Testing TX CHAINMASK 2.4G",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "tx_chainmask": 3,
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing TX CHAINMASK 5GL",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "tx_chainmask": 3,
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing TX CHAINMASK 5GU",
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "tx_chainmask": 15,
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
    "wm2_set_radio_tx_power": [
        {
            "channel": 2,
            "enabled": "true",
            "ht_mode": "HT40",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "tx_powers": [
                1,
                3,
                10,
                17,
                30,
            ],
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 1,
        },
        {
            "channel": 36,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "tx_powers": [
                1,
                3,
                10,
                17,
                30,
            ],
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 2,
        },
        {
            "channel": 136,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "tx_powers": [
                1,
                3,
                10,
                17,
                30,
            ],
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 3,
        },
    ],
    # Creates wireless interfaces with parameters on DUT: password, hw_mode, mode, if_name, radio_band,
    # radio_idx, ssid, vif_if_name
    # Test tries to delete and re-insert vif_config ID's
    "wm2_set_radio_vif_configs": [
        {
            "channel": 2,
            "comment": "Testing VIF_CONFIGS field 2.4G",
            "custom_channel": 6,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11n",
            "if_name": "wifi0",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_24g_network",
            "vif_if_name": "home-ap-24",
            "vif_radio_idx": 2,
        },
        {
            "channel": 36,
            "comment": "Testing VIF_CONFIGS field 5GL",
            "custom_channel": 48,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi1",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gl_network",
            "vif_if_name": "home-ap-L50",
            "vif_radio_idx": 3,
        },
        {
            "channel": 149,
            "comment": "Testing VIF_CONFIGS field 5GU",
            "custom_channel": 157,
            "enabled": "true",
            "ht_mode": "HT20",
            "hw_mode": "11ac",
            "if_name": "wifi2",
            "mode": "ap",
            "security": {
                "encryption": "WPA-PSK",
                "key": "WifiPassword123",
            },
            "ssid": "wm_dut_5gu_network",
            "vif_if_name": "home-ap-U50",
            "vif_radio_idx": 4,
        },
    ],
}
