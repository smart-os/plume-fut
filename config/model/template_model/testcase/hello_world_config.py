test_cfg = {
    "hello_world_insert_demo_fail_to_update": [
        {
            "expect_to_pass": False,
            "kv_key": "demo",
            "kv_value": "fail-to-update",
        },
        {
            "expect_to_pass": True,
            "kv_key": "fut-variable",
            "kv_value": "fail-to-update",
        },
    ],
    "hello_world_insert_demo_fail_to_write": [
        {
            "expect_to_pass": False,
            "kv_key": "demo",
            "kv_value": "fail-to-write",
        },
        {
            "expect_to_pass": True,
            "kv_key": "fut-variable",
            "kv_value": "fail-to-write",
        },
    ],
    "hello_world_insert_foreign_module": [
        {
            "kv_key": "fut-variable",
            "kv_value": "test-value",
            "module_name": "bye-bye",
        },
    ],
    "hello_world_insert_module_key_value": [
        {
            "kv_key": "fut-variable",
            "kv_value": "test-value",
        },
    ],
    "hello_world_update_module_key_value": [
        {
            "kv_changed_value": "changed-value",
            "kv_key": "fut-variable",
            "kv_value": "test-value",
        },
    ],
}
