test_cfg = {
    # Use unit test name with its original name, note test_ prefix to define if that unit test should be skipped or not
    "test_network_metadata": {
        "skip": True,
        "skip_msg": "Unit test broken",
    },
    # Collection of UT is made from resource/ut/ folder. There are 2 ways of collection:
    #   1. Case -> test_cfg['ut']['ut_name'] -> DEFINED
    #       - example ut_name = 'debugs_unit_tests' <- No suffix
    #       - if folder 'debugs_unit_tests' exists, collect
    #       - if no folder - search for 'debugs_unit_tests.tar', if exists extract and collect
    #       - if no 'debugs_unit_tests.tar' - search for 'debugs_unit_tests.tar.bz2', if exists extract and collect
    #       - else skip UT_test
    #   2. Case -> test_cfg['ut']['ut_name'] -> UNDEFINED/EMPTY
    #       - search for any '*.tar.bz2', if exists extract and collect
    #   3. Otherwise skip UT_test
    "ut": {
        "ut_name": "debugs_unit_tests",
    },
}
