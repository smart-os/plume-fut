test_cfg = {
    "sm_leaf_report": [
        {
            "dut_interfaces": [
                {
                    "channel": 1,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT20",
                    "hw_mode": "11n",
                    "if_name": "wifi0",
                    "mode": "ap",
                    "radio_band": 24,
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "sm_gw_24g_network",
                    "vif_if_name": "home-ap-24",
                    "vif_radio_idx": 2,
                },
                {
                    "channel": 48,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT20",
                    "hw_mode": "11ac",
                    "if_name": "wifi1",
                    "mode": "ap",
                    "radio_band": "50L",
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "sm_gw_5gl_network",
                    "vif_if_name": "home-ap-L50",
                    "vif_radio_idx": 3,
                },
                {
                    "channel": 149,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT40",
                    "hw_mode": "11ac",
                    "if_name": "wifi2",
                    "mode": "ap",
                    "radio_band": "50U",
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "sm_gw_5gu_network",
                    "vif_if_name": "home-ap-U50",
                    "vif_radio_idx": 4,
                },
            ],
            "leaf_interfaces": [
                {
                    "channel": 1,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT20",
                    "hw_mode": "11n",
                    "if_name": "wifi0",
                    "mode": "sta",
                    "radio_band": 24,
                    "ref_ssid": "sm_gw_24g_network",
                    "report_cfg": {
                        "radio_type": "2.4G",
                        "report_type": "raw",
                        "reporting_interval": 10,
                        "sampling_interval": 5,
                    },
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "vif_if_name": "home-sta-24",
                    "vif_radio_idx": 2,
                },
                {
                    "channel": 48,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT20",
                    "hw_mode": "11ac",
                    "if_name": "wifi1",
                    "mode": "sta",
                    "radio_band": "50L",
                    "ref_ssid": "sm_gw_5gl_network",
                    "report_cfg": {
                        "radio_type": "5GL",
                        "report_type": "raw",
                        "reporting_interval": 10,
                        "sampling_interval": 5,
                    },
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "vif_if_name": "home-sta-L50",
                    "vif_radio_idx": 3,
                },
                {
                    "channel": 149,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT40",
                    "hw_mode": "11ac",
                    "if_name": "wifi2",
                    "mode": "sta",
                    "radio_band": "50U",
                    "ref_ssid": "sm_gw_5gu_network",
                    "report_cfg": {
                        "radio_type": "5GU",
                        "report_type": "raw",
                        "reporting_interval": 10,
                        "sampling_interval": 5,
                    },
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "vif_if_name": "home-sta-U50",
                    "vif_radio_idx": 4,
                },
            ],
        },
    ],
    "sm_neighbor_report": [
        {
            "dut_interfaces": [
                {
                    "channel": 6,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT20",
                    "hw_mode": "11n",
                    "if_name": "wifi0",
                    "mode": "ap",
                    "radio_band": 24,
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "sm_dut_24g_network",
                    "vif_if_name": "home-ap-24",
                    "vif_radio_idx": 2,
                },
                {
                    "channel": 48,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT20",
                    "hw_mode": "11ac",
                    "if_name": "wifi1",
                    "mode": "ap",
                    "radio_band": "50L",
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "sm_dut_5gl_network",
                    "vif_if_name": "home-ap-L50",
                    "vif_radio_idx": 3,
                },
                {
                    "channel": 149,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT40",
                    "hw_mode": "11ac",
                    "if_name": "wifi2",
                    "mode": "ap",
                    "radio_band": "50U",
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "sm_dut_5gu_network",
                    "vif_if_name": "home-ap-U50",
                    "vif_radio_idx": 4,
                },
            ],
            "neighbor_interfaces": [
                {
                    "channel": 6,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT40",
                    "hw_mode": "11n",
                    "if_name": "wifi0",
                    "mode": "ap",
                    "radio_band": 24,
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "neighbor_24g_network",
                    "vif_if_name": "home-ap-24",
                    "vif_radio_idx": 2,
                },
                {
                    "channel": 48,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT20",
                    "hw_mode": "11ac",
                    "if_name": "wifi1",
                    "mode": "ap",
                    "radio_band": "50L",
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "neighbor_50l_network",
                    "vif_if_name": "home-ap-L50",
                    "vif_radio_idx": 3,
                },
                {
                    "channel": 149,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT40",
                    "hw_mode": "11ac",
                    "if_name": "wifi2",
                    "mode": "ap",
                    "radio_band": "50U",
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "neighbor_50u_network",
                    "vif_if_name": "home-ap-U50",
                    "vif_radio_idx": 4,
                },
            ],
            "report_configurations": [
                {
                    "channel": 6,
                    "neighbor_ssid": "neighbor_24g_network",
                    "radio_type": "2.4G",
                    "report_type": "raw",
                    "reporting_interval": 10,
                    "sampling_interval": 5,
                    "survey_type": "on-chan",
                },
                {
                    "channel": 48,
                    "neighbor_ssid": "neighbor_50l_network",
                    "radio_type": "5GL",
                    "report_type": "raw",
                    "reporting_interval": 10,
                    "sampling_interval": 5,
                    "survey_type": "on-chan",
                },
                {
                    "channel": 149,
                    "neighbor_ssid": "neighbor_50u_network",
                    "radio_type": "5GU",
                    "report_type": "raw",
                    "reporting_interval": 10,
                    "sampling_interval": 5,
                    "survey_type": "on-chan",
                },
            ],
        },
    ],
    "sm_survey_report": [
        {
            "dut_interfaces": [
                {
                    "channel": 6,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT20",
                    "hw_mode": "11n",
                    "if_name": "wifi0",
                    "mode": "ap",
                    "radio_band": 24,
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "sm_gw_24g_network",
                    "vif_if_name": "home-ap-24",
                    "vif_radio_idx": 2,
                },
                {
                    "channel": 48,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT20",
                    "hw_mode": "11ac",
                    "if_name": "wifi1",
                    "mode": "ap",
                    "radio_band": "50L",
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "sm_gw_5gl_network",
                    "vif_if_name": "home-ap-L50",
                    "vif_radio_idx": 3,
                },
                {
                    "channel": 149,
                    "channel_mode": "manual",
                    "enabled": "true",
                    "ht_mode": "HT40",
                    "hw_mode": "11ac",
                    "if_name": "wifi2",
                    "mode": "ap",
                    "radio_band": "50U",
                    "security": {
                        "encryption": "WPA-PSK",
                        "key": "WifiPassword123",
                    },
                    "ssid": "sm_gw_5gu_network",
                    "vif_if_name": "home-ap-U50",
                    "vif_radio_idx": 4,
                },
            ],
            "reports_cfg": [
                {
                    "channels": [
                        6,
                    ],
                    "radio_type": "2.4G",
                    "report_type": "raw",
                    "reporting_interval": 10,
                    "sampling_interval": 5,
                    "survey_type": "on-chan",
                },
                {
                    "channels": [
                        1,
                        2,
                        3,
                        4,
                        5,
                        7,
                        8,
                        9,
                        10,
                        11,
                    ],
                    "radio_type": "2.4G",
                    "report_type": "raw",
                    "reporting_interval": 10,
                    "sampling_interval": 5,
                    "survey_type": "off-chan",
                },
                {
                    "channels": [
                        48,
                    ],
                    "radio_type": "5GL",
                    "survey_type": "on-chan",
                },
                {
                    "channels": [
                        36,
                        40,
                        44,
                        52,
                        56,
                        60,
                        64,
                    ],
                    "radio_type": "5GL",
                    "report_type": "raw",
                    "reporting_interval": 10,
                    "sampling_interval": 5,
                    "survey_type": "off-chan",
                },
                {
                    "channels": [
                        149,
                    ],
                    "radio_type": "5GU",
                    "report_type": "raw",
                    "reporting_interval": 10,
                    "sampling_interval": 5,
                    "survey_type": "on-chan",
                },
                {
                    "channels": [
                        100,
                        104,
                        108,
                        112,
                        116,
                        120,
                        124,
                        128,
                        132,
                        136,
                        140,
                        153,
                        157,
                        161,
                        165,
                    ],
                    "radio_type": "5GU",
                    "report_type": "raw",
                    "reporting_interval": 10,
                    "sampling_interval": 5,
                    "survey_type": "off-chan",
                },
            ],
        },
    ],
}
