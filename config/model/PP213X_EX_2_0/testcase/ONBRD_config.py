test_cfg = {
    "onbrd_verify_bridge_mode": [
        {
            "home_interface": "br-home",
            "patch_h2w": "patch-h2w",
            "patch_w2h": "patch-w2h",
            "uplink_interface": "eth0",
            "wan_interface": "br-wan",
            "wan_ip": "192.168.200.10",
        },
    ],
    "onbrd_verify_client_certificate_files": [
        {
            "cert_file": "ca_cert",
        },
        {
            "cert_file": "certificate",
        },
        {
            "cert_file": "private_key",
        },
    ],
    "onbrd_verify_client_tls_connection": [
        {
            "test_script_timeout": 120,
            "tls_ver": "1.0",
        },
        {
            "test_script_timeout": 120,
            "tls_ver": "1.1",
        },
        {
            "test_script_timeout": 120,
            "tls_ver": "1.2",
        },
    ],
    "onbrd_verify_device_mode_awlan_node": [
        {
            "device_mode": "not_set",
        },
    ],
    "onbrd_verify_dhcp_dry_run_success": [
        {
            "if_name": "eth0",
        },
    ],
    "onbrd_verify_dut_system_time_accuracy": [
        {
            "time_accuracy": "2",
        },
    ],
    "onbrd_verify_fw_version_awlan_node": [
        {
            "search_rule": "pattern_match",
        },
    ],
    "onbrd_verify_home_vaps_on_home_bridge": [
        {
            "home_interface": {
                "name": "br-home",
            },
            "interface": {
                "channel": 2,
                "enabled": "true",
                "ht_mode": "HT20",
                "hw_mode": "11n",
                "if_name": "wifi0",
                "mode": "ap",
                "security": {
                    "encryption": "WPA-PSK",
                    "key": "WifiPassword123",
                },
                "ssid": "wm_dut_24g_network",
                "vif_if_name": "home-ap-24",
                "vif_radio_idx": 2,
            },
        },
        {
            "home_interface": {
                "name": "br-home",
            },
            "interface": {
                "channel": 36,
                "enabled": "true",
                "ht_mode": "HT20",
                "hw_mode": "11ac",
                "if_name": "wifi1",
                "mode": "ap",
                "security": {
                    "encryption": "WPA-PSK",
                    "key": "WifiPassword123",
                },
                "ssid": "wm_dut_5gl_network",
                "vif_if_name": "home-ap-l50",
                "vif_radio_idx": 3,
            },
        },
        {
            "home_interface": {
                "name": "br-home",
            },
            "interface": {
                "channel": 136,
                "enabled": "true",
                "ht_mode": "HT20",
                "hw_mode": "11ac",
                "if_name": "wifi2",
                "mode": "ap",
                "security": {
                    "encryption": "WPA-PSK",
                    "key": "WifiPassword123",
                },
                "ssid": "wm_dut_5gu_network",
                "vif_if_name": "home-ap-u50",
                "vif_radio_idx": 4,
            },
        },
    ],
    "onbrd_verify_home_vaps_on_radios": [
        {
            "interface": {
                "channel": 2,
                "enabled": "true",
                "ht_mode": "HT20",
                "hw_mode": "11n",
                "if_name": "wifi0",
                "mode": "ap",
                "security": {
                    "encryption": "WPA-PSK",
                    "key": "WifiPassword123",
                },
                "ssid": "wm_dut_24g_network",
                "vif_if_name": "home-ap-24",
                "vif_radio_idx": 2,
            },
        },
        {
            "interface": {
                "channel": 36,
                "enabled": "true",
                "ht_mode": "HT20",
                "hw_mode": "11ac",
                "if_name": "wifi1",
                "mode": "ap",
                "security": {
                    "encryption": "WPA-PSK",
                    "key": "WifiPassword123",
                },
                "ssid": "wm_dut_5gl_network",
                "vif_if_name": "home-ap-l50",
                "vif_radio_idx": 3,
            },
        },
        {
            "interface": {
                "channel": 136,
                "enabled": "true",
                "ht_mode": "HT20",
                "hw_mode": "11ac",
                "if_name": "wifi2",
                "mode": "ap",
                "security": {
                    "encryption": "WPA-PSK",
                    "key": "WifiPassword123",
                },
                "ssid": "wm_dut_5gu_network",
                "vif_if_name": "home-ap-u50",
                "vif_radio_idx": 4,
            },
        },
    ],
    "onbrd_verify_id_awlan_node": [
        {}
    ],
    "onbrd_verify_manager_hostname_resolved": [
        {
            "manager_addr": "ssl:54.200.0.59:443",
            "target": [
                "ssl:54.200.0.59:443",
            ],
        },
        {
            "manager_addr": "ssl:ec2-54-200-0-59.us-west-2.compute.amazonaws.com:443",
            "target": [
                "ssl:54.200.0.59:443",
                "ssl:52.36.197.250:443",
            ],
        },
    ],
    "onbrd_verify_model_awlan_node": [
        {
            "model": "PP213X-EX",
        },
    ],
    "onbrd_verify_number_of_radios": [
        {
            "num_of_radios": "3",
        },
    ],
    "onbrd_verify_onbrd_vaps_on_radios": [
        {
            "interface": {
                "channel": 2,
                "enabled": "true",
                "ht_mode": "HT20",
                "hw_mode": "11n",
                "if_name": "wifi0",
                "mode": "ap",
                "security": {
                    "encryption": "WPA-PSK",
                    "key": "WifiPassword123",
                },
                "ssid": "wm_dut_24g_network",
                "vif_if_name": "onboard-ap-24",
                "vif_radio_idx": 2,
            },
        },
        {
            "interface": {
                "channel": 36,
                "enabled": "true",
                "ht_mode": "HT20",
                "hw_mode": "11ac",
                "if_name": "wifi1",
                "mode": "ap",
                "security": {
                    "encryption": "WPA-PSK",
                    "key": "WifiPassword123",
                },
                "ssid": "wm_dut_5gl_network",
                "vif_if_name": "onboard-ap-l50",
                "vif_radio_idx": 3,
            },
        },
        {
            "interface": {
                "channel": 136,
                "enabled": "true",
                "ht_mode": "HT20",
                "hw_mode": "11ac",
                "if_name": "wifi2",
                "mode": "ap",
                "security": {
                    "encryption": "WPA-PSK",
                    "key": "WifiPassword123",
                },
                "ssid": "wm_dut_5gu_network",
                "vif_if_name": "onboard-ap-u50",
                "vif_radio_idx": 4,
            },
        },
    ],
    "onbrd_verify_redirector_address_awlan_node": [
        {
            "redirector_addr": "ssl:wildfire.plume.tech:443",
        },
    ],
    "onbrd_verify_router_mode": [
        {
            "wan_interface": "br-wan",
            "home_interface": "br-home",
            "start_pool": "10.10.10.20",
            "end_pool": "10.10.10.50",
        },
    ],
    "onbrd_verify_wan_ip_address": [
        {
            "wan_interface": "br-wan",
        },
    ],
}
