"""
testbed_cfg = {                         FUT testbed configuration. Python dictionary for testbed specific settings
    "default": {
        "fut_dir": "/tmp/fut-base/",    Directory on the device (DUT, REF) into which the FUT framework copies files
        "reconnect_attempts": 10,       The number of reconnect attempts by the framework for a lost ssh connection
        "reconnect_sleep": 10,          Configures the duration between ssh reconnect attempts in seconds
    },
    "server": {                         Configuration for the RPI server
        "host": "192.168.4.1",          RPI server IP on management subnet. The example here is for Plume devices.
        "username": "plume",
        "password": "plume",
        "rsa": {
            "key_path": "/home/plume/.ssh/id_rsa",
            "key_pass": "plume",
        },
        "curl": {                       Configuration for web hosted file transfer
            "host": "http://192.168.4.1/"
        },
        "mgmt_vlan": 1,                 VLAN 1 is the system VLAN for the FUT testbed
        "tb_role": "server",            RPI server role in the FUT testbed.
                                          The role is linked to the network switch configuration.
    },
    "dut": {                            Device under test. These entries are modified for each specific device model.
        "host": "192.168.4.10",         DUT management IP
        "mgmt_vlan": 4,                 VLAN of DUT management IP. Synced with "host".
        "mgmt_ip": "192.168.4.10",      DUT management IP. Synced with "host".
        "wan_ip": "192.168.200.10",     DUT WAN IP
        "CFG_FOLDER": "PP213X_EX_2_0",  DUT configuration folder name. The name depends on each specific device model.
        "tb_role": "gw",                DUT role in the FUT testbed. The role is linked to the network switch
                                          configuration. "gw" indicates the device uplink is wired.
    },
    "ref": {                            Reference device. Model "PP213X_EX_2_0" is used as default.
        "host": "192.168.4.11",         REF management IP
        "mgmt_vlan": 4,                 VLAN of REF management IP. Synced with "host".
        "mgmt_ip": "192.168.4.11",      REF management IP. Synced with "host".
        "wan_ip": "192.168.200.11",     REF WAN IP
        "CFG_FOLDER": "PP213X_EX_2_0",  REF configuration folder name. This model is supported by default.
        "tb_role": "leaf1",             REF role in the FUT testbed. The role is linked to the network switch
                                          configuration. "leaf" indicates the device uplink is wireless.
    },
    "client": {                         RPI client
        "host": "192.168.4.13",         RPI client management IP
        "mgmt_vlan": 4,                 VLAN of RPI client management IP. Synced with "host".
        "mgmt_ip": "192.168.4.13",      RPI client management IP. Synced with "host".
        "wan_ip": "192.168.200.13",     RPI client WAN IP. If the entry is removed, a lease from the DHCP pool is used.
        "tb_role": "rpi1",              RPI client role in the FUT testbed.
                                          The role is linked to the network switch configuration.
    },
}
"""

testbed_cfg = {
    "default": {
        "fut_dir": "/tmp/fut-base",
        "reconnect_attempts": 10,
        "reconnect_sleep": 10,
    },
    "network_switch": {
        "host": "192.168.5.254",
        "username": "admin",
        "password": "12testtest",
        "name": "stb-switch",
        "port": 23,
        "management_vlan": 5,
        "promptchar": ">",
        "adminpromptchar": "#",
        "ipaddr_alloc": "dhcp",
        "type": "tplink",
        "alias": [
            {"port": 1, "name": "uplink", "backhaul": 1},
            {"port": 2, "name": "server", "backhaul": 1},
            {"port": 3, "name": "gw", "backhaul": 1},
            {"port": 4, "name": "leaf1", "backhaul": 1},
            {"port": 5, "name": "leaf2", "backhaul": 1},
            {"port": 6, "name": "rpi1", "backhaul": 1},
            {"port": 7, "name": "rpi2", "backhaul": 1},
            {"port": 8, "name": "rpower", "backhaul": 1},
        ],
        "vlans": [
            {"vlan": 4, "name": "Managment"},
            {"vlan": 5, "name": "Switch_mn"},
            {"vlan": 6, "name": "Power_Switch_mn"},
            {"vlan": 24, "name": "PPPoE_CPE1"},
            {"vlan": 35, "name": "PPPoE_CPE2"},
            {"vlan": 200, "name": "Fake_internet"},
            {"vlan": 201, "name": "Stateful_IPv6"},
            {"vlan": 202, "name": "Stateless_IPv6"},
            {"vlan": 203, "name": "Slaack_IPv6"},
            {"vlan": 303, "name": "GW_eth0"},
            {"vlan": 304, "name": "Leaf1_eth0"},
            {"vlan": 305, "name": "Leaf2_eth0"},
            {"vlan": 309, "name": "GW_eth1"},
            {"vlan": 310, "name": "Leaf1_eth1"},
            {"vlan": 311, "name": "Leaf2_eth1"},
            {"vlan": 312, "name": "GW_eth2"},
            {"vlan": 313, "name": "GW_eth3"},
            {"vlan": 314, "name": "GW_eth4"},
            {"vlan": 351, "name": "Client_vlan1"},
            {"vlan": 352, "name": "Client_vlan2"},
            {"vlan": 4000, "name": "Mikrotik-IPv6"},
        ],
        "interface": [
            {"port": 1, "name": "uplink", "vlan_tagged": None, "vlan_untagged": 1, "pvid": 1, },
            {"port": 2, "name": "server", "vlan_tagged": [4, 5, 6, 24, 35, 200, 201, 202, 203],
                "vlan_untagged": None, "pvid": 1, },
            {"port": 3, "name": "gw", "vlan_tagged": [4, 24, 35], "vlan_untagged": 200, "pvid": 200, },
            {"port": 4, "name": "leaf1", "vlan_tagged": 4, "vlan_untagged": 304, "pvid": 304, },
            {"port": 5, "name": "leaf2", "vlan_tagged": 4, "vlan_untagged": 305, "pvid": 305, },
            {"port": 6, "name": "rpi1", "vlan_tagged": 305, "vlan_untagged": 4, "pvid": 4, },
            {"port": 7, "name": "rpi2", "vlan_tagged": [351, 352], "vlan_untagged": 4, "pvid": 4, },
            {"port": 8, "name": "rpower", "vlan_tagged": None, "vlan_untagged": 6, "pvid": 6, },
            # Extended configuration for 16-port switch, first 8 ports are the same
            {"port": 9, "name": "gw_lan_1", "vlan_tagged": None, "vlan_untagged": 309, "pvid": 309, },
            {"port": 10, "name": "leaf1_lan", "vlan_tagged": None, "vlan_untagged": 310, "pvid": 310, },
            {"port": 11, "name": "leaf2_lan", "vlan_tagged": None, "vlan_untagged": 311, "pvid": 311, },
            {"port": 12, "name": "gw_lan_2", "vlan_tagged": None, "vlan_untagged": 312, "pvid": 312, },
            {"port": 13, "name": "gw_lan_3", "vlan_tagged": None, "vlan_untagged": 313, "pvid": 313, },
            {"port": 14, "name": "gw_lan_4", "vlan_tagged": None, "vlan_untagged": 314, "pvid": 314, },
            {"port": 15, "name": "server_lan", "vlan_tagged": None, "vlan_untagged": 309, "pvid": 309, },
            {"port": 16, "name": "nc", "vlan_tagged": 4, "vlan_untagged": 200, "pvid": 200, },
        ],
    },
    "server": {
        "host": "192.168.4.1",
        "username": "plume",
        "password": "plume",
        "rsa": {
            "key_path": "/home/plume/.ssh/id_rsa",
            "key_pass": "plume",
        },
        "curl": {
            "host": "http://192.168.4.1:8000/"
        },
        "mgmt_vlan": 1,
        "tb_role": "server",
    },
    "dut": {
        "host": "192.168.4.10",
        "mgmt_vlan": 4,
        "mgmt_ip": "192.168.4.10",
        "wan_ip": "192.168.200.10",
        "CFG_FOLDER": "PP213X_EX_2_0",
        "tb_role": "gw",
    },
    "ref": {
        "host": "192.168.4.11",
        "mgmt_vlan": 4,
        "mgmt_ip": "192.168.4.11",
        "wan_ip": "192.168.200.11",
        "CFG_FOLDER": "PP213X_EX_2_0",
        "tb_role": "leaf1",
    },
    "ref2": {
        "host": "192.168.4.12",
        "mgmt_vlan": 4,
        "mgmt_ip": "192.168.4.12",
        "wan_ip": "192.168.200.12",
        "CFG_FOLDER": "PP213X_EX_2_0",
        "tb_role": "leaf2",
    },
    "client": {
        "host": "192.168.4.13",
        "mgmt_vlan": 4,
        "mgmt_ip": "192.168.4.13",
        "wan_ip": "192.168.200.13",
        "tb_role": "rpi1",
    },
    "client2": {
        "host": "192.168.4.14",
        "mgmt_vlan": 4,
        "mgmt_ip": "192.168.4.14",
        "wan_ip": "192.168.200.14",
        "tb_role": "rpi2",
    },
    "rpower": {
        "host": "192.168.6.1",
        "mgmt_vlan": 6,
        "username": "admin",
        "password": "1234",
        "tb_role": "rpower",
    },
}
