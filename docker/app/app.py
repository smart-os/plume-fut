#!/usr/bin/python3

from flask import Flask
from flask import render_template
from flask import jsonify
from flask import request
from flask_cors import CORS
from flask_cors import cross_origin
import subprocess
import os


STATIC_DIR = '/var/www/app/static/'
BUILDS_DIR = '/tmp/fut-base/builds/'
REPORTS_DIR = f'{STATIC_DIR}/reports/'
ALLURE_PATH = '/usr/local/lib/allure/bin/allure'
FUT_ENV_NAMES = ['DUT_CFG_FOLDER', 'REF_CFG_FOLDER']


app = Flask(__name__, static_folder='static')
fut_cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route("/")
@cross_origin()
def home():
    return render_template('home.html')


@app.route("/get_builds")
def get_builds():
    return jsonify({'success': True, 'data': os.listdir(BUILDS_DIR)})


@app.route("/get_allure")
def get_allure():
    build_name = request.args.get('build_name')
    report_url = f'/static/reports/{build_name}/index.html'
    if os.path.isdir(REPORTS_DIR + build_name):
        return jsonify({'success': True, 'url': report_url})
    cmd = f'{ALLURE_PATH} generate -o {REPORTS_DIR}{build_name} {BUILDS_DIR}{build_name}'
    cmd = cmd.split()
    cmd_pass = subprocess.Popen(
        ['echo', 'plume'],
        stdout=subprocess.PIPE
    )
    cmd_process = subprocess.Popen(
        ['sudo', '-S'] + cmd,
        stdin=cmd_pass.stdout,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        universal_newlines=True
    )
    cmd_process.communicate()
    exit_code = cmd_process.returncode
    return jsonify(
        {
            'success': exit_code == 0,
            'msg': 'Error while generating' if exit_code != 0 else build_name,
            'url': report_url,
            'cmd': cmd
        }
    )


@app.route('/fut-base/<path:path>')
def get_fut_file(path):
    return app.send_static_file('fut-base/' + path)


if __name__ == "__main__":
    # app.run(host='0.0.0.0', port=8000)
    app.run(host='0.0.0.0', port=5000, debug=False)
