#!/bin/bash

if [[ -z "$SSH_AUTH_SOCK" ]];then
    export SSH_AUTH_SOCK="/dev/null"
fi

docker-compose $@
