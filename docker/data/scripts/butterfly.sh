#!/bin/bash

while true
do
  echo "Starting butterfly.server.py"
  echo "$SSH_AUTH_SOCK" > /tmp/.auth_sock
  chmod 755 /tmp/butterfly_env.sh

  butterfly.server.py --host=0.0.0.0 --port=57575 --unsecure
  echo "Crashed butterfly.server.py"
  sleep 1
done