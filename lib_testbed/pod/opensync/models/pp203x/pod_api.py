from typing import Union

from lib_testbed.pod.opensync.models.generic.pod_api import PodApi as PodApiGeneric


class PodApi(PodApiGeneric):
    def get_radio_temperatures(self, radio: Union[int, str, list] = None, **kwargs):
        """
        Function for retrieving radio temperatures from caesar pods
        :param radio: accepted arguments: radio_id(e.g.: 0, 1, 2), band frequency(e.g.: '2.4G', '5GL', '5GU'),
                      list of radio_ids or band frequencies, or use None for all radios
        :return: radio temperature as int or list(temperatures are ordered the same as in radio argument if provided,
                 else they are ordered by radio id)
        """
        return self.lib.get_radio_temperatures(radio, **kwargs)
