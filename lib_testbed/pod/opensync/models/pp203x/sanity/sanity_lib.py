#!/usr/bin/env python3

import os
from subprocess import getoutput as getout
from subprocess import getstatusoutput as getstatusout

from lib_testbed.util.sanity.ovsdb import ovsdb_find_row
from lib_testbed.pod.opensync.models.generic.sanity.sanity_lib import SanityLib as SanityLibGeneric


class SanityLib(SanityLibGeneric):

    def __init__(self, tables, gw_tables, logs_dir=None, outfile=None, outstyle="full", sys_log_file_name='messages'):
        super().__init__(tables, gw_tables, logs_dir, outfile, outstyle, sys_log_file_name)
        self.id_length = 10
        self.br_home = ['br-home']
        self.br_wan = ['br-wan']
        self.home_ap = ['home-ap-24']
        self.bands = ['2.4G', '5GL', '5GU']
        self.wan_device = ['eth0', 'eth1', 'eth0.835', 'eth1.835']
        self.bhaul_ap = ['bhaul-ap-24', 'bhaul-ap-l50', 'bhaul-ap-u50']
        self.bhaul_sta = ['bhaul-sta-24', 'bhaul-sta-l50', 'bhaul-sta-u50']
        self.table_list = [['AWLAN_Node table'],
                           ['Manager table'],
                           ['Bridge table'],
                           ['DHCP_leased_IP table'],
                           ['Interface table'],
                           ['Wifi_Master_State table'],
                           ['Wifi_Inet_Config table', 'Wifi_Inet_State table'],
                           ['Wifi_Radio_Config table', 'Wifi_Radio_State table'],
                           ['Wifi_VIF_Config table', 'Wifi_VIF_State table'],
                           ['Wifi_Associated_Clients table']]

    def ovsdb_sanity_check(self, force_leaf=False, **kwargs):
        self.home_ap + self._get_home_ap_5_list()
        super().ovsdb_sanity_check(force_leaf, **kwargs)
        self.validate_tables()
        return self.retval

    def _get_home_ap_5_list(self):
        # SP can have both home-5G up, or based on optimizer decision just one
        home_ap_5 = []
        if ovsdb_find_row(self.tables['Interface table'], 'name', 'home-ap-l50'):
            home_ap_5.append('home-ap-l50')
        if ovsdb_find_row(self.tables['Interface table'], 'name', 'home-ap-u50'):
            home_ap_5.append('home-ap-u50')
        if not home_ap_5:
            self._create_output('Interface table', 'ERROR', 'No home-ap on 5G interface')
        return home_ap_5

    def check_is_gateway(self, force_leaf):
        if not force_leaf and not self.gw_node:
            for wan in self.wan_device:
                prt = ovsdb_find_row(self.tables['Port table'], 'name', wan)
                if prt:
                    br = ovsdb_find_row(self.tables['Bridge table'], 'ports', prt['_uuid'])
                    if br['name'] == "br-wan":
                        self.gw_node = True
                        self.wan_device = wan
                        break

    # ##########################################################################
    # sys log related methods
    # ##########################################################################
    def sys_log_sanity_check(self):
        self._check_dhcp_idx()
        if any(x.startswith(self.sys_log_file_name) for x in os.listdir(self.logs_dir)):
            self._check_time()
            self._check_kernel_crash()
            self._check_app_crash()
            self._check_vap(('bhaul-ap-24', 'bhaul-ap-l50', 'bhaul-ap-u50', 'home-ap-24', 'home-ap-l50', 'home-ap-u50'))
            self._check_eth()
            self._check_wm(wifi=('wifi0', 'wifi1', 'wifi2'), bhaul_sta=('bhaul-sta-24', 'bhaul-sta-l50', 'bhaul-sta-u50'),
                           home_ap=('home-ap-24', 'home-ap-l50', 'home-ap-u50'))
            self._check_cm()
            self._check_single_dhcp_server()
        else:
            self._create_output('syslog', 'ERROR', 'System log file {} not found in logs'
                                .format(self.sys_log_file))

    def _check_kernel_crash(self):
        boot_cnt = getout("cat {}* | egrep 'Calibrating delay loop' | wc -l"
                          .format(self.sys_log_file))
        lvl = 'Warning' if int(boot_cnt) > 3 else 'INFO'

        crash_status, crash_log = \
            getstatusout("ls {}/*crash*tar.gz > /dev/null 2>&1".format(self.logs_dir))
        if not crash_status:
            lvl = 'ERROR'
            extra = "   #### Contains kernel crash log ####"
        else:
            extra = ''
        up_time, up_time_str = self._get_up_time()
        if up_time < 600:
            lvl = 'Warning'
        self._create_output('KERNEL', lvl, "Uptime {0} sec [{1}], Boot Count={2}{3}"
                            .format(up_time, up_time_str, boot_cnt, extra))
