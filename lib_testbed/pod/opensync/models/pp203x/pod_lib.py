import time
from typing import Union
from lib_testbed.util.logger import log
from lib_testbed.pod.opensync.models.generic.pod_lib import PodLib as PodLibGeneric
from lib_testbed.pod.opensync.models.generic.pod_lib import DFS_REGION_MAP


class PodLib(PodLibGeneric):
    def get_radio_temperatures(self, radio: Union[int, str, list] = None, **kwargs):
        """
        Function for retrieving radio temperatures from caesar pods
        :param radio: accepted arguments: radio_id(e.g.: 0, 1, 2), band frequency(e.g.: '2.4G', '5GL', '5GU),
                      list of radio_ids or band frequencies, or use None for all radios
        :return: radio temperature as int or list(temperatures are ordered the same as in radio argument if provided,
                 else they are ordered by radio id)
        """
        radio_band_mapping = {
            '2.4G': 0,
            '5GL': 1,
            '5GU': 2
        }
        if radio is None:
            return [self.get_radio_temperatures(value, **kwargs) for _, value in radio_band_mapping.items()]
        elif isinstance(radio, list):
            return [self.get_radio_temperatures(item, **kwargs) for item in radio]
        elif isinstance(radio, str):
            return self.get_radio_temperatures(radio_band_mapping[radio], **kwargs)
        elif isinstance(radio, int):
            temp = -1
            retries = 3
            while temp <= 0 and retries > 0:
                retries -= 1
                output = self.strip_stdout_result(self.run_command(f'iwpriv wifi{radio} get_therm', **kwargs))
                output[1] = output[1].split(':')
                if output[0] != 0 or len(output[1]) != 2:
                    continue
                temp = int(output[1][1])

            return temp

        raise ValueError

    def trigger_radar_detected_event(self, **kwargs):
        """
        Trigger radar detected event
        Args:
            **kwargs:

        Returns: list(retval, stdout, stderr)

        """
        response = self.run_command('radartool -i wifi1 bangradar; radartool -i wifi2 bangradar', **kwargs)
        return response

    def get_boot_partition(self, **kwargs):
        """
        Get boot partition name
        Args:
            **kwargs:

        Returns: list(retval, stdout, stderr)

        """
        response = self.run_command('cat /proc/cmdline | sed -r "s/ubi.mtd=([a-zA-Z0-9]+).*/\\1/g"', **kwargs)
        return response

    def get_partition_dump(self, partition, **kwargs):
        """
        Get partition hex dump
        Args:
            partition: (str) partition name
            **kwargs:

        Returns: list(retval, stdout, stderr)

        """
        dump = ''
        part = 'BOOTCFG2' if 'rootfs2' in partition else 'BOOTCFG1'
        mtd = self.get_stdout(self.strip_stdout_result(
            self.run_command(f'cat /proc/mtd | grep {part} | sed "s/:.*//"')), **kwargs)
        hexdump = self.run_command(f'hexdump /dev/{mtd}')
        for line in hexdump[1].splitlines():
            if line.startswith('*'):
                continue
            # big/little indian convert
            for part in line.split():
                if len(part) > 4:
                    continue
                dump += part[2:] + part[:2] + ' '
        hexdump[1] = dump
        return hexdump

    def is_fw_fuse_burned(self, **kwargs):
        response = self.run_command('pmf -fuse verify | grep "Authentication enabled" | grep "ERROR NOT SET" | wc -l',
                                    **kwargs)
        return self.get_stdout(response).strip() == '0'

    def set_fan_mode(self, status, **kwargs):
        """
        Enable or disable fan on the device
        Args:
            status: (bool)
            **kwargs:

        Returns: list(retval, stdout, stderr)

        """
        log.info(f'{"Enabling" if status else "Disabling"} fan')
        fan_value = '9000' if status else '0'
        results = None
        for state in range(1, 9):
            response = self.ovsdb.set_value(table='Node_Config',
                                            value={'key': f'SPFAN_state{state}_fanrpm', 'value': fan_value,
                                                   'module': 'tm'}, **kwargs)
            results = self.merge_result(response, response)
        return results

    def set_region(self, region, **kwargs):
        assert region in DFS_REGION_MAP, f"Region {region} is not supported!"
        log.info(f"Set {region} region for node {self.get_nickname()}")
        res = self.run_command("fw_setenv plume_development 1")
        for radio in ['rgdmn0', 'rgdmn1', 'rgdmn2']:
            res = self.merge_result(res, self.run_command(f"pmf -fw {DFS_REGION_MAP[region]} -{radio} {region}"))

        log.info("Rebooting pod")
        self.reboot()
        time.sleep(10)
        self.wait_available(timeout=2 * 60)
        timeout = time.time() + 120
        while time.time() < timeout:
            if self.run_command(f"pmf -r -rgdmn0")[1]:
                break
            time.sleep(2)
        else:
            assert False, f"Cannot get country code information after reboot"

        for radio in ['rgdmn0', 'rgdmn1', 'rgdmn2']:
            assert DFS_REGION_MAP[region].lower() in self.run_command(f"pmf -r -{radio}")[1].lower()
        return res
