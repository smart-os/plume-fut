from lib_testbed.util.logger import log


class PodTool:
    def __init__(self, lib):
        self.lib = lib
        # Dead code, workaround for resolving references by pycharm
        if hasattr(self, "INVALID_STATEMENT"):
            from lib_testbed.pod.opensync.models.generic.pod_lib import PodLib
            self.lib = PodLib()

    def get_name(self):
        return self.lib.get_name()

    def list(self, **kwargs):
        """List all configured pods"""
        return self.lib.shell_list(**kwargs)

    def run(self, command, *args, **kwargs):
        """Run a command

        :return: list of stdout"""
        result = self.lib.run_command(command, *args, **kwargs)
        return self.lib.strip_stdout_result(result)

    def ssh(self, params="", **kwargs):
        """Start interactive ssh session"""
        return self.lib.ssh(params, **kwargs)

    def version(self, **kwargs):
        """Display firmware version of node(s)"""
        return self.lib.version(**kwargs)

    def reboot(self, **kwargs):
        """Reboot node(s)"""
        return self.lib.reboot(**kwargs)

    def ping(self, host=None, **kwargs):
        """Ping"""
        return self.lib.ping(host, **kwargs)

    def uptime(self, **kwargs):
        """Uptime"""
        return self.lib.uptime(**kwargs)

    def get_file(self, remote_file, location, **kwargs):
        """Copy a file from node(s)"""
        return self.lib.get_file(remote_file, location, **kwargs)

    def put_file(self, file_name, location, **kwargs):
        """Copy a file onto device(s)"""
        return self.lib.put_file(file_name, location, **kwargs)

    def deploy(self, **kwargs):
        """Deploy files to node(s)"""
        return self.lib.deploy(**kwargs)

    def restart(self, **kwargs):
        """Restart managers on node(s)"""
        return self.lib.restart(**kwargs)

    def check(self, **kwargs):
        """Pod health check"""
        return self.lib.check(**kwargs)

    def enable(self, **kwargs):
        """Enable agent and wifi radios on node(s)"""
        return self.lib.enable(**kwargs)

    def disable(self, **kwargs):
        """Disable agent and wifi radios on node(s)"""
        return self.lib.disable(**kwargs)

    def info(self):
        """Node connection information"""
        return self.lib.info()

    def get_model(self, **kwargs):
        """Display type of node(s)"""
        return self.lib.get_model(**kwargs)

    def bssid(self, bridge='', **kwargs):
        """Display BSSID of node bridge = <br-wan|br-home>-, default both"""
        result = self.lib.bssid(bridge, **kwargs)
        if not result[0]:
            all_macs = result[1].split('\n')
            ifname_mac = ''
            for mac in all_macs:
                ifname = self.lib.ovsdb.get_str(table='Wifi_VIF_State', select='if_name', where=[f'mac=={mac}'])
                ifname_mac += f'{ifname}: {mac}\n'
            result[1] = ifname_mac
        return result

    def get_serial_number(self, **kwargs):
        """Get node(s) serial number"""
        return self.lib.get_serial_number(**kwargs)

    def connected(self, **kwargs):
        """Returns cloud connection state of each node"""
        return self.lib.connected(**kwargs)

    def get_ovsh_table_tool(self, table, **kwargs):
        """Get ovsh table from pods locally tool format"""
        return self.lib.get_ovsh_table_tool(table, **kwargs)

    def scp(self, *args, **kwargs):
        """SCP: "{DEST}" replaced with provided pod"""
        return self.lib.scp(*args, **kwargs)

    def start_wps_session(self, if_name=None, psk=None, *args, **kwargs):
        """Start wps session"""
        if if_name is None:
            if_name = [iface for iface in self.lib.iface.get_all_home_bhaul_ifaces() if 'home' in iface][0]

        key = None
        wps_keys = self.lib.get_wps_keys(if_name)
        if psk is None:
            key = list(wps_keys.items())[0][0]
        else:
            for k, v in wps_keys.items():
                if psk == v:
                    key = k
                    break

        if psk is not None and key is None:
            for k, v in wps_keys.items():
                if psk == k:
                    key = k
                    break

        if key is None:
            raise Exception(f'Psk "{psk}" is not set on the pod.')

        result = self.lib.start_wps_session(if_name=if_name, key_id=key)
        return [0 if result else 1, str(result), '']

    def wait_available(self, timeout=5, **kwargs):
        """Wait for device(s) to become available"""
        return self.lib.wait_available(timeout, **kwargs)

    def role(self):
        """Node role: return gw or leaf"""
        return self.lib.role()

    def get_logs(self, directory=None):
        """Get logs from pods locally"""
        return self.lib.get_logs(directory)

    def upgrade(self, image, *args, **kwargs):
        """Upgrade node firmware, Optional: -p=<encyp_key>, -e-> erase certificates, -n->skip version check"""
        return self.lib.upgrade(image, *args, **kwargs)

    def recover(self):
        """Recover pod to allow management access"""
        prev_log_level = log.getEffectiveLevel()
        log.setLevel(log.DEBUG)
        result = self.lib.recover()
        log.setLevel(prev_log_level)
        return result

    def sanity(self, *args):
        """Run sanity on selected pods, add arg --nocolor for simple output"""
        def print_sanity_output(sanity_out):
            """
            User friendly sanity output print
            Args:
                sanity_out: dict from node_sanity
            """
            from termcolor import colored
            if sanity_out['gw_pod']:
                print(f"GW pod: {sanity_out['gw_pod']}")
            for i in range(len(sanity_out['serial'])):
                print(f"Sanity check for: {sanity_out['serial'][i]}")
                for line in sanity_out['out'][i]:
                    if line[1] == 'INFO':
                        level = colored(line[1], 'green')
                    elif line[1] == 'Warning':
                        level = colored(line[1], 'yellow')
                    elif line[1] == 'ERROR':
                        level = colored(line[1], 'red', attrs=['bold', 'blink'])
                    else:
                        level = colored(line[1], 'white')
                    tname = colored(line[0], 'magenta')
                    message = colored(line[2], 'white')
                    print(f'{tname: >25}- {level: <12}: {message}')
                print(' ')

        resposne = self.lib.sanity('--lib', *args)
        print_sanity_output(resposne)
        status = 1 if not resposne['ret'] else 0
        return [status, resposne['health'], '']

    def get_crash(self):
        """Get crash log file from node"""
        return self.lib.get_crash()

    def get_ips(self, iface):
        """Get ipv4 and ipv6 address for desired interface"""
        return self.lib.get_ips(iface)

    def set_region(self, region, **kwargs):
        """Set DFS region"""
        return self.lib.set_region(region=region, **kwargs)
