import distutils
import tempfile
import shutil
import datetime
import re
import os
import json
import time
import subprocess
from typing import Union
from uuid import UUID

from lib_testbed.pod.opensync.models.generic.pod_tool import PodTool
from lib_testbed.util.base_lib import Iface
from lib_testbed.util.opensyncexception import OpenSyncException
from lib_testbed.util.logger import log
from lib_testbed.util.common import BASE_DIR
from lib_testbed.pod.pod_base import PodBase
from lib_testbed.util.common import DeviceCommon

DFS_REGION_MAP = {'EU': '0x0037', 'US': '0x003a', 'JP': '0x8FAF'}


class PodLib(PodBase):
    def __init__(self, **kwargs):
        pods = kwargs["config"].get("Nodes")
        if pods:
            # Assume that first Node configured in Nodes is a gateway and the rest are leafs
            # Extend config with roles: gw or leaf
            # TODO: consider to fetch this information from cloud
            for i, pod in enumerate(pods):
                if pod.get("role"):
                    continue
                if i == 0:
                    role = "gw"
                else:
                    role = "leaf"
                pod["role"] = role
        super().__init__(**kwargs)
        self.ext_path = '/usr/plume/tools:/usr/plume/bin:/sbin:/usr/opensync/tools:/usr/opensync/bin'
        self.iface = PodIface(lib=self)
        self.tool = PodTool(lib=self)
        self.pod_info = []
        self.ovsdb = Ovsdb(self)
        self.msg = None
        self.storage = {}

    def get_opensync_path(self, **kwargs):
        """Get path to opensync"""
        res = self.get_stdout(self.run_command('ps | grep "bin/dm" | head -n 1', **kwargs), skip_exception=True)
        if not res:
            log.warning("Cannot get path to OpenSync, using default: '/usr/opensync'")
            return [0, '/usr/opensync', '']
        return self.strip_stdout_result([0, res.split(' ')[-1].replace('/bin/dm', ''), ''])

    def ping(self, host=None, v6=False, **kwargs):
        """Ping"""
        ping = 'ping6' if v6 else 'ping'
        if host:
            result = self.run_command(f"{ping} -c1 -w5 {host}")
        else:
            cmd = self.device.get_last_hop_cmd(f"{ping} -c1 -w5 {self.device.get_ip()}")
            result = self.run_command(cmd, self.device, skip_remote=True, **kwargs)
        result[1] = result[1] if result[0] == 0 else ''
        return self.strip_stdout_result(result)

    def reboot(self, **kwargs):
        """Reboot node(s)"""
        return self.run_command('/sbin/reboot', **kwargs)

    def version(self, **kwargs):
        """Display firmware version of node(s)"""
        return self.strip_stdout_result(self.ovsdb.get_raw(table='AWLAN_Node',
                                                           select='firmware_version'))

    def platform_version(self, **kwargs):
        """Display platform version of node(s)"""
        return self.strip_stdout_result(self.ovsdb.get_raw(table='AWLAN_Node',
                                                           select='platform_version'))

    def uptime(self, timeout=20, **kwargs):
        """Display uptime of node(s)"""
        out_format = 'user'
        if kwargs.get('out_format'):
            out_format = kwargs.get('out_format')
            del kwargs['out_format']
        assert out_format in ['user', 'timestamp'], "Unsupported format {}".format(out_format)
        if out_format == 'user':
            result = self.run_command("uptime", timeout=timeout, **kwargs)
            return self.strip_stdout_result(result)
        elif out_format == 'timestamp':
            result = self.run_command("cat /proc/uptime", timeout=timeout, **kwargs)
            result = self.strip_stdout_result(result)
            if result[0] == 0:
                result[1] = result[1].split()[0].strip()
            return result

    def get_file(self, remote_file, location, **kwargs):
        """Copy a file from node(s)"""
        location = "{0}/{1}".format(location, self.get_name())
        # Check if location exists
        if not os.path.isdir(location):
            try:
                os.makedirs(location)
            except Exception as e:
                return 'Could not create dir {}: {}.'.format(location, str(e))
        command = self.device.scp_cmd("{{DEST}}:{}".format(remote_file), location)
        return self.run_command(command, skip_remote=True, **kwargs)

    def restart(self, **kwargs):
        """Restart managers on node(s)"""
        init_cmd = '/etc/init.d/manager {0}'
        stop_cmd = self.device.get_remote_cmd(init_cmd.format('stop'))
        start_cmd = self.device.get_remote_cmd(init_cmd.format('start'))
        stop_result = self.run_command(stop_cmd, self.device, skip_remote=True, **kwargs)
        start_result = self.run_command(start_cmd, self.device, skip_remote=True, **kwargs)
        # start always returns 1 even if succeeded, so workaround that
        ret_val = self.merge_result(stop_result, [0, start_result[1], start_result[2]])
        return ret_val

    def healthcheck_stop(self, **kwargs):
        """Stop healthcheck on pod"""
        out = self.run_command('/etc/init.d/healthcheck stop')
        # stopping always returns 1 even if successful, to change it to 0
        if out[0] == 1:
            out[0] = 0
        return out

    def healthcheck_start(self, **kwargs):
        """Start healthcheck on pod"""
        return self.run_command('/etc/init.d/healthcheck start')

    def deploy(self, **kwargs):
        """Deploy files to node(s)"""
        remote_dir = self.get_node_deploy_path()
        resp = self.run_command(f"mkdir -p {remote_dir}", **kwargs)
        if not self.result_ok(resp):
            raise Exception(f"Can\'t create directory: {remote_dir}\n{resp}")
        # TODO: so far there is just one file, but what in case we would like to mix files from generic and model
        # hint: copy first parent class files, then from model
        model_path = DeviceCommon.get_model_path(self.device.config['model'], parent_model=None, dev_type='pod')
        deploy_dir = DeviceCommon.resolve_model_path_file(model_path, 'deploy') + '/*'
        command = self.device.scp_cmd(deploy_dir, f"{{DEST}}:{remote_dir}")
        return self.run_command(command, **kwargs, timeout=10 * 60, skip_remote=True)

    def check(self, **kwargs):
        """Pod health check"""
        result = self.run_command(f'{self.get_node_deploy_path()}/node-check', **kwargs)
        if result[0] and ('node-check: not found' in result[2] or 'node-check: No such file' in result[2]):
            log.info("Deploying tools...")
            deploy_status = self.deploy()
            if deploy_status[0] != 0:
                err_msg = f"Deploy to POD has failed! {deploy_status[2]}"
                # 0, since we do not want to fail GW sanity check
                return [0, err_msg, err_msg]
            # make sure file is executable
            self.run_command(f'chmod +x {self.get_node_deploy_path()}/node-check', **kwargs)
            # try again
            result = self.run_command(f'{self.get_node_deploy_path()}/node-check', **kwargs)
        return result

    def enable(self, **kwargs):
        """Enable agent and wifi radios on node(s)"""
        return self.run_command('pidof dm || /etc/init.d/manager start', **kwargs)

    def disable(self, **kwargs):
        """Disable agent and wifi radios on node(s)"""
        return self.run_command('killall dm wm nm cm; /etc/init.d/manager stop', **kwargs)

    def info(self):
        """Node connection information"""
        result = self.device._parse_host_info()
        if not result:
            return [1, '', 'Error during getting connection information']
        return [0, result, '']

    def get_model(self, **kwargs):
        """Display type of node(s)"""
        return self.strip_stdout_result(self.ovsdb.get_raw(table='AWLAN_Node',
                                        select='model'))

    def add_to_acl(self, bridge, mac):
        self.ovsdb.set_value(table='Wifi_VIF_Config',
                             where=[f'if_name=={bridge}'],
                             value={'mac_list_type': 'whitelist'})
        self.ovsdb.set_value(table='Wifi_VIF_Config',
                             where=[f'if_name=={bridge}'],
                             value={'mac_list': mac})

    def bssid(self, bridge='', **kwargs):
        """Display BSSID of node bridge = <br-wan|br-home>-, default both"""
        where = ['mode==ap']
        if bridge == 'br-wan':
            where.append(f'bridge!={self.iface.get_native_br_home()}')
        elif bridge == 'br-home':
            # For home interfaces vif_radio_idx==2
            where.extend([f'bridge=={self.iface.get_native_br_home()}', 'vif_radio_idx==2'])
        result = self.ovsdb.get_raw(table='Wifi_VIF_State',
                                    select='mac',
                                    where=where)
        return self.strip_stdout_result(result)

    def get_serial_number(self, **kwargs):
        """Get node(s) serial number"""
        return self.strip_stdout_result(self.ovsdb.get_raw(table='AWLAN_Node',
                                                           select='id'))

    def connected(self, **kwargs):
        """returns cloud connection state of each node"""
        result = self.strip_stdout_result(self.ovsdb.get_raw(table='Manager',
                                                             select='status'))

        if 'ACTIVE' not in result[1]:
            return [0, False, '']
        return [0, True, '']

    def get_ovsh_table(self, table, **kwargs):
        """get ovsh table from pods locally json format"""
        result = self.run_command(f'ovsh -j s {table}', **kwargs)
        return self.strip_stdout_result(result)

    def get_ovsh_table_tool(self, table, **kwargs):
        """get ovsh table from pods locally tool format"""
        result = self.run_command(f'ovsh s {table}', **kwargs)
        return self.strip_stdout_result(result)

    def role(self, **kwargs):
        """Node role: return gw or leaf"""
        if not self.iface.is_ovs():
            # Assume that only residential gateways doesn't support ovs
            return [0, 'GW', '']
        # stations exists only on leafs
        result = self.ovsdb.get_str(table='Wifi_VIF_State',
                                    select='if_name',
                                    where=['mode==sta'])

        return [0, 'Leaf' if len(result) != 0 else 'GW', '']

    def get_ips(self, iface, **kwargs):
        """get ipv4 and ipv6 address for desired interface"""
        ip_adds = {'ipv4': False, 'ipv6': False}
        get_int = "ip addr show {}".format(iface)
        result = self.run_command(get_int, **kwargs)
        for res in result[1].splitlines():
            if "inet " in res:
                ip_adds['ipv4'] = res.strip().split(" ")[1].split("/")[0].strip()
            if "inet6 " in res:
                ip_adds['ipv6'] = res.strip().split(" ")[1].split("/")[0].strip()
        return [0, ip_adds, '']

    def get_logs(self, directory=None, **kwargs):
        """get logs from pods locally"""
        if directory and not os.path.isdir(directory):
            try:
                os.makedirs(directory)
            except Exception as e:
                return [1, '', 'Could not create dir {}: {}.'.format(directory, str(e))]

        if kwargs.get('timeout'):
            log_pull_timeout = kwargs['timeout']
        else:
            log_pull_timeout = 60
        kwargs['timeout'] = 20
        path = self.get_stdout(self.get_opensync_path())
        if self.result_ok(self.run_command(f'ls {path}/bin/lm_logs_collector.sh', **kwargs)):
            command = f'{path}/bin/lm_logs_collector.sh --stdout'
        elif self.result_ok(self.run_command(f'ls {path}/bin/logpull.sh', **kwargs)):
            command = f'{path}/bin/logpull.sh --stdout'
        elif self.result_ok(self.run_command(f'ls {path}/scripts/lm_log_pull.sh', **kwargs)):
            # TODO: Remove once all devices has support for creating log-pull tarball file
            command = f"sh -c 'set -- 1 log-pull.tgz; curl() {{ cp log-pull.tgz /tmp/lm/log-pull-copy.tgz; true; }}; " \
                      f". {path}scripts/lm_log_pull.sh' 1>&2 && cat /tmp/lm/log-pull-copy.tgz" \
                      f" && rm /tmp/lm/log-pull-copy.tgz"
        else:
            return [1, '', 'Log-pull script not found in /usr/opensync/bin']
        kwargs['timeout'] = log_pull_timeout
        result = self.run_command(command, **kwargs)
        if result[0] == 0:
            fn = 'logs-%s-%s.tgz' % (self.get_name(), datetime.datetime.strftime(datetime.datetime.utcnow(),
                                     "%Y%m%d_%H%M%S"))
            if directory:
                fn = os.path.join(directory, fn)
            if not result[1]:
                return [1, '', 'Log-pull script returned empty response']
            with open(fn, 'wb') as fh:
                fh.write(result[1])
            log_file = [0, fn, '']
        else:
            log_file = [result[0], '', result[2]]
        return log_file

    def erase_certificates(self):
        """Erase partition and install DEVELOPMENT certificates"""
        # get certs from lab-server
        dev_null = open(os.devnull, 'w')
        # 10.100.1.2 -> lab-server.sf.wildfire.exchange
        subprocess.check_call(['scp', 'plume@10.100.1.2:/home/plume/piranha_unencrypted_certs/*',
                               '/tmp/'], stdout=dev_null)
        # copy them to nodes
        for cert in ['/tmp/ca.pem', '/tmp/client.pem', '/tmp/client_dec.key']:
            put_output = self.put_file(cert, '/tmp')

        # Check output from def put_file()
        self.get_stdout(put_output)

        # erase partition
        self.get_stdout(self.run_command('mtd erase /dev/mtd14'))

        # install certs
        install_crets = f'cd /tmp; certwrite ca.pem; certwrite client.pem; certwrite client_dec.key'

        # Raise exception if failed
        self.get_stdout(self.run_command(install_crets))

    def upgrade(self, image, *args, **kwargs):
        """Upgrade node firmware, Optional: -p=<encyp_key>, -e-> erase certificates, -n->skip version check"""
        if kwargs.get('skip_if_the_same'):
            cur_ver = self.version()[1]
            if cur_ver in image:
                return [0, cur_ver, '']
        kwargs.pop('skip_if_the_same', '')

        skip_version_check = False
        erase_certs = False
        image_file = os.path.basename(image)
        target_file_name = "/tmp/pfirmware/{0}".format(image_file)
        dec_passwd = None
        for arg in args:
            if arg == '-n' or arg == 'skip_version_check':
                skip_version_check = True
            if '-p=' in arg:
                dec_passwd = arg[3:]
            if '-e' in args:
                erase_certs = True

        if dec_passwd and image[-3:] != 'eim':
            raise Exception('Use eim file for encrypted image')
        if not dec_passwd and image[-3:] != 'img':
            raise Exception('Use img file for unencrypted image')

        self.run_command("mkdir -p /tmp/pfirmware", **kwargs)
        self.put_file(image, "/tmp/pfirmware")
        remote_md5sum = self.run_command('md5sum /tmp/pfirmware/{0} | cut -d" " -f1'.format(image_file), **kwargs)
        remote_md5sum = self.get_stdout(remote_md5sum)
        local_md5sum = os.popen('md5sum {0} | cut -d" " -f1'.format(image)).read().strip()

        md5sum = remote_md5sum.strip()
        if md5sum != local_md5sum:
            res_output = [1, "", "Failed MD5sum image: {0} node: {1} ".format(local_md5sum, md5sum)]
        else:
            res_output = [0, "", ""]

        if res_output[0] != 0:
            return res_output

        # determine which command should be used for upgrade
        if dec_passwd:
            upg_comm = f"safeupdate  -u {target_file_name} -P {dec_passwd}"
        else:
            upg_comm = f"safeupdate  -u {target_file_name}"

        if erase_certs:
            self.erase_certificates()

        result = self.run_command(upg_comm, timeout=5 * 60, **kwargs)

        # wait for nodes to reboot
        time.sleep(120)

        # don't rely on update return value
        result = self.merge_result(result, self.wait_available(60, **kwargs))

        if skip_version_check:
            return result

        log.info("Checking version")
        check_version = self.version(**kwargs)
        return self.merge_result([result[0], '', result[2]], check_version)

    def sanity(self, *args):
        """run sanity on selected pods, add arg --nocolor for simple output"""
        out = 'full'
        for arg in args:
            if arg == '--nocolor':
                out = 'simple'
            elif arg == '--noout':
                out = 'none'
            elif arg == '--lib':
                out = 'lib'

        dump = self.run_command('ovsdb-client dump -f json $(cat /proc/$(pidof ovsdb-server)/cmdline | '
                                'grep -o unix:.*)')
        dump = self.get_stdout(dump, skip_exception=True)
        tmpd = os.path.join(tempfile.gettempdir(), 'podsanity-{}'.format(str(time.time())))

        poddir = os.path.join(tmpd, self.get_name())
        os.makedirs(poddir)
        dmpfile = os.path.join(poddir, 'ovsdb-client_-f_json_dump')
        with open(dmpfile, 'w') as dumpfile:
            print(dump, file=dumpfile)

        ret_status = PodLib._sanity_tool(tmpd, out)

        # cleanup
        shutil.rmtree(tmpd)

        # health check
        health_result = self.check()
        ret_status['health'] = health_result[1]
        restart_managers = False
        if 'Failure' in health_result[1]:
            if out in ['full', 'simple']:
                log.error(f'Health check for {self.get_nickname()} failed:')
                log.info(health_result[1], indent=1)
            if 'Kernel/managers dumps:    Failure' in health_result[1]:
                self.get_crash()
                restart_managers = True
            if 'was restarted' in health_result[1] or 'is not running' in health_result[1] or restart_managers:
                self.restart()
        if health_result[0]:
            ret_status['ret'] = False
        return ret_status

    def poll_pod_sanity(self, timeout=300, expect=True, *args):
        """
        Loops sanity on until pass
        Args:
            timeout: (int) time to wait for sanity pass,
            expect: (bool) What we expecting, True for pass sanity, False for fail sanity.
            *args:

        Returns: 1 if sanity failed 0 otherwise
        """
        status = {'ret': False}
        log.info(f"[{self.get_name()}] Waiting for sanity to {'pass' if expect else 'fail'} {int(timeout/60)} min")
        timeout = time.time() + timeout
        while time.time() < timeout:
            status = self.sanity('--lib', *args)
            if status['ret'] is expect:
                break
            if 'Kernel/managers dumps:    Failure' in status.get('health'):
                log.error(f'Sanity failed due to crash:\n{status.get("health")}')
                break
            time.sleep(5)
        self.print_sanity_output(status)
        return 1 if status['ret'] is False else 0

    def print_sanity_output(self, sanity_out):
        """
        User friendly sanity output printer
        Args:
            sanity_out: (dict) output from node_sanity

        """
        msg = '[{}:{}] Sanity {}\n'.format(self.get_name(),
                                           sanity_out['serial'][0] if sanity_out['serial'] else 'unknown',
                                           'successful' if sanity_out["ret"] else 'failed')

        if sanity_out["gw_pod"]:
            msg += f'GW pod: {sanity_out["gw_pod"]}\n'
        for i in range(len(sanity_out['serial'])):
            msg += 'Sanity check for: {}\n'.format(sanity_out['serial'][i])
            for line in sanity_out['out'][i]:
                msg += '{0: >17}- {1: <10}:{2}\n'.format(line[0], line[1], line[2])
            msg += '\n'
        if sanity_out.get('health'):
            msg += sanity_out['health']
        log.info(msg, show_file=False)

    def get_crash(self):
        """get crash log file from node"""
        serial = self.get_serial_number()[1].strip()
        name = self.get_nickname()
        # /usr/plume/log_archive/crash/ < 3.0.0
        # /sys/fs/pstore/ > 3.0.0
        crash_places = ['/usr/plume/log_archive/crash/', '/sys/fs/pstore/']
        crash_saved = False
        crash_response = ''
        crash_no_crash = f'Crash not found for {name}'
        lpdir = os.path.join('/tmp', 'automation', 'crash_{}_{}'.format(serial, str(int(time.time()))))

        # core.gz are stored in /tmp
        tmp_file = self.get_stdout(self.run_command('ls /tmp | grep core.gz'), skip_exception=True)
        for item in tmp_file.split('\n'):
            if not item:
                continue
            self.get_file('/tmp/' + item, lpdir)
            self.run_command('rm ' + '/tmp/' + item)

        for remote_path in crash_places:
            out = self.get_stdout(self.run_command(f'ls {remote_path}'), skip_exception=True)
            if not out:
                continue
            tar_name = f"/tmp/{remote_path.split('/')[-2]}_log.tar"
            tar = self.run_command(f"tar -cvpf {tar_name} {remote_path}*")
            if tar[0]:
                log.error(f"Cannot tar {remote_path}: {tar[2]}")
                continue
            response = self.get_file(tar_name, lpdir)
            if response[0] == 0:
                crash_response += f'{serial} crash saved on {name} to: {lpdir}'
                log.error(f'{serial} crash saved on {name} to: {lpdir}')
            else:
                log.error(f"Cannot download crash file: {response[2]}")
            self.run_command(f'rm {remote_path}*')
            self.run_command(f'rm {tar_name}')
            crash_saved = True
            crash_no_crash = ''
        if crash_saved:
            self.restart()
            time.sleep(30)
        return [int(not crash_saved), crash_response, crash_no_crash]

    def get_log_level(self, manager_name):
        """
        Get logging level for OpenSync manager
        Args:
            manager_name: (str) OpenSync manager name: SM, BM, etc
            level: (str) logging level: trace, debug, info, notice, warning, err, crit, alert, emerg

        Returns: level: (str) logging level: trace, debug, info, notice, warning, err, crit, alert, emerg
        """
        # special case for BM
        if manager_name == 'BM':
            level = self.ovsdb.get_int(table='Band_Steering_Config',
                                       select='debug_level')
            if level == 1:
                level = 'debug'
            elif level == 2:
                level = 'trace'
            else:
                level = 'err'

        else:
            level = self.ovsdb.get_str(table='AW_Debug',
                                       select='log_severity',
                                       where=[f'name=={manager_name}'])
        return level

    def set_log_level(self, manager_name, level):
        """
        Set logging level for OpenSync manager
        Args:
            manager_name: (str) OpenSync manager name: SM, BM, etc
            level: (str) logging level: trace, debug, info, notice, warning, err, crit, alert, emerg

        Returns: ([[0,0,0],]) nodes output
        """
        if level not in ['trace', 'debug', 'info', 'notice', 'warning', 'err', 'crit', 'alert', 'emerg']:
            raise OpenSyncException(f'Unknown logging level: {level}',
                                    "Possible levels: ['trace', 'debug', 'info', 'notice', 'warning', 'err', 'crit',"
                                    " 'alert', 'emerg']")
        # special case for BM
        if manager_name == 'BM':
            if level == 'debug':
                level = 1
            elif level == 'trace':
                level = 2
            else:
                level = 0
            self.ovsdb.set_value(table='Band_Steering_Config',
                                 value={'debug_level': level})
        else:
            self.ovsdb.set_value(table='AW_Debug',
                                 value={'log_severity': f'{level}'},
                                 where=[f'name=={manager_name}'])

    @staticmethod
    def _sanity_tool(tmpd, out):
        try:
            from lib_testbed.util.sanity.sanity import Sanity
        except ModuleNotFoundError:
            log.warning('Sanity module is not available, skipping')
            return {'gw_pod': None, 'serial': [], 'ret': True, 'out': [], 'wifi_stats': []}

        with open(os.path.join(tmpd, 'sanity.log'), 'w') as out_file:
            san_tool = Sanity(outfile=out_file, outstyle=out)
            if out != 'lib':
                log.info('running sanity on {}'.format(tmpd))
            return san_tool.sanity_location(tmpd)

    @staticmethod
    def sanity_message(ret_status):
        """Method to print the output"""
        # TODO: wifi_stats
        # TODO: Add attributes to filter printing
        node_out = ret_status['out']
        serial = ret_status['serial']
        health = ret_status['health']
        log.info("{} sanity check for: {}".format(ret_status['gw_pod'], serial))
        for row in node_out:
            tname = row[0]
            level = row[1]
            regex = re.compile(r"\033\[[0-9;]+m")
            message = regex.sub('', row[2])

            log_method = log.info
            if "ERROR" in level:
                log_method = log.error
            elif "Warning" in level:
                log_method = log.warning
            log_method('{0: >17}- {1: <25}:{2}'.format(level, tname, message))
        log_method = log.info
        if 'Failure' in health:
            log_method = log.error
        log_method('Health check for {}:'.format(serial))
        for line in health.split('\n'):
            log_method('\t' + line)

    def get_userbase(self):
        if not hasattr(self, 'cloud'):
            return None
        from lib.cloud.userbase import UserBase
        for _obj_name, obj in self.cloud.__dict__.items():
            if isinstance(obj, UserBase):
                return obj
        return None

    def get_custbase(self):
        if not hasattr(self, 'cloud'):
            return None
        from lib.cloud.custbase import CustBase
        for _obj_name, obj in self.cloud.__dict__.items():
            if isinstance(obj, CustBase):
                if not obj.is_initialized():
                    obj.initialize()
                return obj
        return None

    # TODO: Might be broken after refactor
    def cache_pods_info(self):
        ub = self.get_userbase()
        if not ub:
            raise Exception("No userbase object found. Ensure that a test includes opensync_cloud mark")
        log.console("Discovering nodes: ", show_file=False, end='')
        serial = self.get_stdout(self.get_serial_number(), skip_exception=True)
        serial = serial.strip()
        name = self.get_nickname()
        cb_nodes_info = ub.get_pods_info()
        pods_info = []
        for gw, cb_nodes in cb_nodes_info.items():
            for cb_node_name, cb_node_info in cb_nodes.items():
                info_serial = cb_node_info['serial']
                info_nickname = cb_node_name
                info_connection = cb_node_info['connectionState']
                info_gw = True if gw == "gw" else False
                info_model = cb_node_info['model']
                pods_info.append({'serial': info_serial,
                                   'id': info_serial,
                                   'name': None,
                                   'access': False,
                                   'connectionState': info_connection,
                                   'gw': info_gw,
                                   'model': info_model,
                                   'nickname': info_nickname
                                  })
            if not serial:
                log.warning("Cannot get serial for node: {} through management access".format(name))
                continue
            found = False
            for node in pods_info:
                if node['serial'] == serial:
                    node['access'] = True
                    node['name'] = name
                    found = True
                    break
            if not found:
                out = self.get_model()
                if not out[0]:
                    model = out[1].strip()
                else:
                    log.warning('Unable to get model for pod {}'.format(name))
                    model = 'unknown'
                pods_info.append({'serial': serial,
                                  'id': serial,
                                  'name': name,
                                  'access': True,
                                  'connectionState': 'Disconnected',
                                  'gw': False,
                                  'model': model,
                                  'nickname': name
                                  })
        nodes_log = []
        for node in pods_info:
            if node['gw']:
                # Add gateway as a first item in the list
                idx = 0
            else:
                idx = len(nodes_log)
            access = ", access:False" if not node['access'] else ""
            disconnected = ", status:'{}'".format(node['connectionState']) \
                if node['connectionState'] != "connected" else ""
            nodes_log.insert(idx, "{}: {{name:{}, model:{}{}{}}}".format(
                node['serial'], node['name'], node['model'], access, disconnected))
        log.console(",  ".join(nodes_log), show_file=False)
        self.pod_info = [pod_info for pod_info in pods_info if pod_info['name'] == name]

    def get_radio_temperatures(self, radio: Union[int, str, list] = None, **kwargs):
        """
        Function for retrieving radio temperatures from piranha pods
        :param radio: accepted arguments: radio_id(e.g.: 0, 1, ...), band frequency(e.g.: '2.4G', '5G'),
                      list of radio_ids or band frequencies, or use None for all radios
        :return: radio temperature as int or list(temperatures are ordered the same as in radio argument if provided,
                 else they are ordered by radio id)
        """
        radio_band_mapping = {
            '2.4G': 0,
            '5G': 1
        }
        if radio is None:
            return [self.get_radio_temperatures(value, **kwargs) for _, value in radio_band_mapping.items()]
        elif isinstance(radio, list):
            return [self.get_radio_temperatures(item, **kwargs) for item in radio]
        elif isinstance(radio, str):
            return self.get_radio_temperatures(radio_band_mapping[radio], **kwargs)
        elif isinstance(radio, int):
            temp = -1
            retries = 3
            while temp <= 0 and retries > 0:
                retries -= 1
                output = self.strip_stdout_result(self.run_command(f'iwpriv wifi{radio} get_therm', **kwargs))
                output[1] = output[1].split(':')
                if output[0] != 0 or len(output[1]) != 2:
                    continue
                temp = int(output[1][1])

            return temp

        raise ValueError

    # TODO: Remove as this method is already implemented in Iface class
    def get_mac(self, ifname="", **kwargs):
        """
        Get mac address of interface from device
        Args:
            ifname: (str) Device interface name
            **kwargs:

        Returns: (list) [[(int) ret, (dict) stdout, (str) stderr]]

        """
        result = self.run_command(f"cat /sys/class/net/{ifname}/address", **kwargs)
        return self.strip_stdout_result(result)

    def get_macs(self, **kwargs):
        return self.ovsdb.get_str(table='Wifi_VIF_State',
                                  select='mac',
                                  where=['ssid!=we.backhaul', 'ssid!=we.piranha'])

    def recover(self, **kwargs):
        """
        Recover pod by using dedicated recover class for a pod model.
        This method might use serial access to the pod instead of ssh management access.
        :return: list including ret_value, stdout, stderr
        """
        failed = [255, '', 'Failed to recover']
        success = [0, 'Succesfully recovered', '']
        model = self.device.config['model']
        response = None
        try:
            recover_class = DeviceCommon.resolve_model_class('recover.py', model, gw_model=None, dev_type='pod',
                                                             default_family='opensync')
            response = recover_class(self, **kwargs)
        except KeyError as e:
            if 'Could not resolve path' in str(e):
                log.info(f'{e}')
            else:
                log.exception('Failed to recover')
        except Exception:
            log.exception('Failed to recover')
        if not response:
            return failed
        return success

    def wait_eth_connection_ready(self, timeout, **kwargs):
        # TODO: change ovsdb call with wait operation when implemented
        """
        Wait for disable loop status so that to be ready for connect eth client to device
        :param timeout: (int) timeout in seconds
        :return: (bool) True if eth connection is ready, False if not ready after timeout and skip_exception is True
        """
        skip_exception = kwargs.get('skip_exception', False)
        start_time = time.time()
        while start_time + timeout > time.time():
            loop = self.ovsdb.get_bool(table='Connection_Manager_Uplink',
                                       select='loop',
                                       where=['if_type==eth'], skip_exception=True)

            if isinstance(loop, list) and True not in loop:
                break
            elif isinstance(loop, bool) and not loop:
                break
            elif loop is None:
                break

            time.sleep(5)
        else:
            if skip_exception:
                return False
            raise TimeoutError(f'Ethernet connection is not ready after {timeout} seconds.')

        time.sleep(40)
        return True

    def get_datetime(self, **kwargs):
        """
        Get current datetime
        :return: date as python datetime object
        """
        epoch = self.get_stdout(self.strip_stdout_result(self.run_command('date +%s')), **kwargs)
        if not epoch:
            return None
        epoch = int(epoch)
        return datetime.datetime.fromtimestamp(epoch)

    def set_datetime(self, date_time, **kwargs):
        """
        Set date
        :param date_time: python datetime object
        """
        return self.run_command(f'date -s @{int(date_time.timestamp())}', **kwargs)

    def wait_bs_table_ready(self, timeout=60, **kwargs):
        # TODO: change ovsdb call with wait operation when implemented
        """
        Wait for ovsdb Band_Steering_Clients table to be populated
        :param timeout: Timeout
        :return: (bool) True if bs table is ready, False if not ready after timeout and skip_exception is True
        """
        skip_exception = kwargs.get('skip_exception', False)
        start_time = time.time()
        while time.time() - start_time < timeout:
            bs_uuid = self.ovsdb.get_uuid(table='Band_Steering_Clients',
                                          select='_uuid',
                                          skip_exception=skip_exception)

            if bs_uuid is None or (isinstance(bs_uuid, list) and len(bs_uuid) == 0):
                time.sleep(5)
                continue

            return True
        else:
            if skip_exception:
                return False
            raise TimeoutError(f'Band_Steering_Clients table is not available after {timeout} seconds.')

    def get_tx_power(self, interface, **kwargs):
        """
        Get current tx power in dBm
        Args:
            interface: (str) Wireless interface

        Returns: raw output [(int) ret, (std) std_out, (str) str_err]

        """
        cmd = "iwconfig %s | grep Tx-Power | awk '{print $4}'" % interface
        response = self.strip_stdout_result(self.run_command(cmd, **kwargs))
        if not response[0]:
            response[1] = response[1].split(':')[-1] if ':' in response[1] else response[1].split('=')[-1]
        return response

    def decrease_tx_power_on_all_ifaces(self, percent_ratio, **kwargs):
        """
        Decrease value of tx power on the all home_ap, bhaul interfaces
        Args:
            percent_ratio: (int) Percent ratio from 0 to 100

        Returns:

        """
        percent_ratio = percent_ratio / 100
        all_interfaces = self.iface.get_all_home_bhaul_ifaces()
        cmd = ''
        for interface in all_interfaces:
            current_tx_power = int(self.get_stdout(self.get_tx_power(interface)))
            expect_tx_power = int(current_tx_power - (current_tx_power * percent_ratio))
            if expect_tx_power < 1:
                expect_tx_power = 1
            cmd += f'iwconfig {interface} txpower {expect_tx_power}dbm; '
        response = self.run_command(cmd, **kwargs)
        return response

    def increase_tx_power_on_all_ifaces(self, percent_ratio, **kwargs):
        """
        Increase value of tx power on the all home_ap, bhaul interfaces
        Args:
            percent_ratio: (int) Percent ratio from 0 to 100

        Returns:

        """
        percent_ratio = percent_ratio / 100
        all_interfaces = self.iface.get_all_home_bhaul_ifaces()
        cmd = ''
        for interface in all_interfaces:
            current_tx_power = int(self.get_stdout(self.get_tx_power(interface)))
            expect_tx_power = int(current_tx_power + (current_tx_power * percent_ratio))
            cmd += f'iwconfig {interface} txpower {expect_tx_power}dbm; '
        response = self.run_command(cmd, **kwargs)
        return response

    def set_tx_power(self, tx_power, interfaces=None, **kwargs):
        """
        Set current tx power in dBm
        Args:
            interfaces: (str) or (list) Name of wireless interfaces
            tx_power: (int) Tx power in dBm.

        Returns:

        """
        if not interfaces:
            interfaces = self.iface.get_all_home_bhaul_ifaces()
        if isinstance(interfaces, str):
            interfaces = [interfaces]
        cmd = ''
        for interface in interfaces:
            cmd += f'iwconfig {interface} txpower {tx_power}dbm; '
        response = self.run_command(cmd, **kwargs)
        return response

    def get_driver_data_rate(self, ifname, mac_address, **kwargs):
        """
        Get current data rate from driver
        Args:
            ifname: (str) Name of interface
            mac_address: (str) Mac address of connected client interface
        Returns: list (ret, stdout, stderr) stdout as (dict) {'data_rate': (str), 'bytes': (int), 'rssi': (list)}

        """
        data_rate_table = list()
        drate = ''
        result = self.run_command(f'plume {ifname} peer_rx_stats {mac_address}', **kwargs)
        if result[0]:
            return result

        for line in result[1].splitlines():
            if 'CCK' not in line and 'OFDM' not in line and 'MHz |' not in line:
                continue
            rssi = []
            if 'MHz |' in line:
                splitline = line.split('|')
                bw = splitline[0].strip()
                nss = splitline[1].strip()
                mcs = splitline[2].strip()
                if bw == '20MHz':
                    bw = 0
                elif bw == '40MHz':
                    bw = 1
                elif bw == '80MHz':
                    bw = 2
                else:
                    bw = 3
                drate = '[{0} {1} {2}]'.format(bw, nss, mcs)
                dbytes = int(splitline[4].strip())
                rssi.append(splitline[10].strip())

                for index in range(4):
                    p20 = splitline[11 + index * 4].strip()
                    e20 = splitline[11 + index * 4].strip()
                    e40 = splitline[11 + index * 4].strip()
                    e80 = splitline[11 + index * 4].strip()
                    rssi.append(dict(p2=p20, e20=e20, e40=e40, e80=e80))
            else:
                splitline = line.split('|')
                if 'CCK' in splitline[0]:
                    drate = ''.join(splitline[0].strip().split()[-2:])
                elif 'OFDM' in splitline[0]:
                    drate = splitline[0].strip().split()[-1]
                dbytes = int(splitline[2].strip())
                rssi.append(splitline[7].strip())

                for index in range(4):
                    p20 = splitline[8 + index * 4].strip()
                    e20 = splitline[8 + index * 4].strip()
                    e40 = splitline[8 + index * 4].strip()
                    e80 = splitline[8 + index * 4].strip()
                    rssi.append(dict(p2=p20, e20=e20, e40=e40, e80=e80))

            data_rate_table.append({'data_rate': drate, 'bytes': dbytes, 'rssi': rssi})
        result[1] = data_rate_table
        return result

    def trigger_radar_detected_event(self, **kwargs):
        """
        Trigger radar detected event
        Args:
            **kwargs:

        Returns: list(retval, stdout, stderr)

        """
        response = self.run_command('radartool -i wifi1 bangradar', **kwargs)
        return response

    def get_boot_partition(self, **kwargs):
        """
        Get boot partition name
        Args:
            **kwargs:

        Returns: list(retval, stdout, stderr)

        """
        response = self.run_command('cat /proc/mtd | grep bootconfig | sed "s/:.*//"', **kwargs)
        return response

    def get_partition_dump(self, partition, **kwargs):
        """
        Get partition hex dump
        Args:
            partition: (str) partition name
            **kwargs:

        Returns: list(retval, stdout, stderr)

        """
        log.warning('Method not implemented, as model is not supporting it')
        return [0, 'Not implemented', '']

    def clear_log_messages(self, **kwargs):
        """
        Clear log messages
        Args:
            **kwargs:

        Returns: list(retval, stdout, stderr)

        """
        return self.run_command('echo "" > /var/log/messages || echo "" > /tmp/log/messages || true', **kwargs)

    def is_fw_fuse_burned(self, **kwargs):
        return False

    def get_cpu_memory_usage(self, **kwargs):
        """
        Get cpu memory usage
        Returns: dict('memory': {'used': int, 'free': int}, 'cpu': {'usr': int, 'sys': int, 'nic': int, 'idle': int',
                                 'io': int, 'irq': int, 'sirq': int}
        """
        top = self.run_command('top -n1', **kwargs)
        if top[0]:
            return top
        out = {}
        top = top[1]
        for line in top.splitlines():
            if 'Mem:' in line:
                line = line[line.find('Mem:') + 4:]
                out['memory'] = {}
                for element in line.split(','):
                    try:
                        if 'used' in element:
                            out['memory']['used'] = int(element[:element.find('K')])
                        if 'free' in element:
                            out['memory']['free'] = int(element[:element.find('K')])
                    except ValueError:
                        log.warning(f'Unable to parse {element}')
            if 'CPU:' in line:
                line = line.replace('CPU:', '')
                line = line.replace('%', '')
                out['cpu'] = {}
                keys = ['usr', 'sys', 'nic', 'idle', 'io', 'irq', 'sirq']
                idx = 0
                for element in line.split():
                    try:
                        out['cpu'][keys[idx]] = int(element)
                        idx += 1
                    except ValueError:
                        continue
        return out

    def get_sfe_dump(self, ip_address='', timeout=240, **kwargs):
        """
        Get sfe dump from device
        Args:
            ip_address: (str) optional, get entries only for target ip address
            timeout: (int) timeout for ssh request
            **kwargs:

        Returns: str(sfe dump in xml)

        """
        sfe_dump = self.run_command('sfe_dump', timeout=timeout, **kwargs)
        if sfe_dump[0]:
            return sfe_dump
        parse_sfe_dump = [line.strip() for line in sfe_dump[1].splitlines() if 'connection protocol'
                          in line and ip_address in line]
        parse_sfe_dump = '\n'.join(parse_sfe_dump)
        sfe_dump[1] = parse_sfe_dump
        return sfe_dump

    def start_wps_session(self, if_name, key_id):
        value = {'wps': True,
                 'wps_pbc': True}

        if key_id is not None:
            value['wps_pbc_key_id'] = f'"{key_id}"'

        result = self.ovsdb.set_value(value=value,
                                      table='Wifi_VIF_Config',
                                      where=f'if_name=={if_name}')

        return result[0] == 0

    def get_wps_keys(self, if_name):
        keys_str = self.run_command(f'cat /var/run/hostapd-{if_name}.pskfile')[1]
        key_passphrase_list = re.findall(r'keyid=(\S*)\s\S*\s(\S*)', keys_str)
        return dict(key_passphrase_list) if key_passphrase_list is not None else None

    def set_fan_mode(self, status, **kwargs):
        log.info('There is no fan to set, skipping')
        return [0, '', '']

    def remove_crash(self, **kwargs):
        """
        Remove crashes from device
        Args:
            **kwargs:

        Returns: list(retval, stdout, stderr)

        """
        return self.run_command('rm /usr/plume/log_archive/crash/*; rm /sys/fs/pstore/*', **kwargs)

    def set_region(self, region, **kwargs):
        log.error("Model does not support changing region")
        return [1, '', 'Model does not support changing region']

    def get_connection_flows(self, **kwargs):
        return self.run_command("sfe_dump", **kwargs)

    def redirect_stats_to_local_mqtt_broker(self, skip_storage=False, **kwargs):
        def update_pod_ca_file():
            ca_cert = os.path.join(BASE_DIR, 'lib', 'cloud', 'mqtt', 'certs', 'ca-rpi.pem')
            with open(ca_cert, 'r') as f:
                ca_cert = f.read().strip()
                certs = self.run_command('cat /var/certs/ca.pem', **kwargs)[1].strip()
                if ca_cert not in certs:
                    log.info(f'Updating CA cert on the {self.get_nickname()}')
                    self.run_command(f'echo "{ca_cert}" >> /var/certs/ca.pem')
                    certs = self.run_command('cat /var/certs/ca.pem', **kwargs)[1].strip()
                    assert ca_cert in certs

        # redirect mqtt from the test_node to local mqtt broker
        if not skip_storage:
            self.storage['mqtt_settings'] = self.ovsdb.get_map('AWLAN_Node', 'mqtt_settings', **kwargs)
            if not self.storage['mqtt_settings'].get('broker'):
                log.warning("Mqtt setting are gone, restarting managers...")
                self.restart()
                time.sleep(120)
                self.wait_available(timeout=120)
                timeout = time.time() + 5 * 60
                while time.time() < timeout:
                    log.info("Waiting till node connect with controller and gets mqtt settings")
                    cur_map = self.ovsdb.get_map('AWLAN_Node', 'mqtt_settings')
                    if cur_map.get('topics', ''):
                        break
                    time.sleep(5)
                else:
                    return [2, '', 'Pod does not have any mqtt settings']
                self.storage['mqtt_settings'] = self.ovsdb.get_map('AWLAN_Node', 'mqtt_settings', **kwargs)
                if not self.storage['mqtt_settings'].get('broker'):
                    return [1, '', f"Cannot get mqtt setting from the {self.get_nickname()}"]
        # append ca.crt to /var/certs/ca.pem if needed
        update_pod_ca_file()

        self.ovsdb.mutate('AWLAN_Node', 'mqtt_settings', 'del', '["port", "compress", "broker"]', **kwargs)
        self.ovsdb.mutate('AWLAN_Node', 'mqtt_settings', 'ins',
                          '["broker","192.168.200.1"], ["compress",""],["port","8883"]', **kwargs)
        cur_sett = self.ovsdb.get_map('AWLAN_Node', 'mqtt_settings', **kwargs)
        if cur_sett.get('broker', '') == "192.168.200.1":
            return [0, 'Mqtt settings updated successfully', '']
        else:
            return [1, '', f"Mqtt settings not updated properly, expected broker:"
                           f" 192.168.200.1 but got: {cur_sett.get('broker', '')}"]

    def restore_stats_mqtt_settings(self, **kwargs):
        if 'mqtt_settings' not in self.storage:
            return [10, '', 'There is no cached information about mqtt settings']
        self.ovsdb.mutate('AWLAN_Node', 'mqtt_settings', 'del',
                          '["port", "compress", "broker"]', **kwargs)
        self.ovsdb.mutate('AWLAN_Node', 'mqtt_settings', 'ins',
                          f'["broker","{self.storage["mqtt_settings"]["broker"]}"],'
                          f'["compress","{self.storage["mqtt_settings"]["compress"]}"],'
                          f'["port","{self.storage["mqtt_settings"]["port"]}"]', **kwargs)
        cur_sett = self.ovsdb.get_map('AWLAN_Node', 'mqtt_settings', **kwargs)
        if cur_sett.get('broker', '') == self.storage["mqtt_settings"]["broker"]:
            return [0, 'Mqtt settings restored successfully', '']
        else:
            return [1, '', f"Mqtt settings not updated properly, expected broker:"
                           f" {self.storage['mqtt_settings']['broker']} but got: {cur_sett.get('broker', '')}"]

    def enter_factory_mode(self, **kwargs):
        log.info('Entering factory mode')
        assert self.run_command("pmf -e", **kwargs)[0] == 0
        time.sleep(10)
        self.wait_available(timeout=2 * 60, **kwargs)

    def exit_factory_mode(self, **kwargs):
        log.info('Exiting factory mode')
        assert self.run_command("pmf -q", **kwargs)[0] == 0
        time.sleep(10)
        self.wait_available(timeout=2 * 60, **kwargs)


class PodIface(Iface):
    # TODO: find better solution for getting interface informations
    @staticmethod
    def get_br_wan_ifname():
        return 'br-wan'

    def get_br_home_ifname(self):
        br_ifaces = self.lib.ovsdb.get_str(table='Wifi_Inet_State',
                                           select='if_name',
                                           where=['if_type==bridge'],
                                           return_list=True)
        br_home_iface = list(set(br_ifaces) - set(['br-wan']))[0]
        return br_home_iface

    def get_vif_mac(self, ovs_bridge):
        """Get mac of vif bridge"""
        mac_list = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                          select='mac',
                                          where=[f'if_name=={ovs_bridge}'],
                                          return_list=True)
        return mac_list

    def get_inet_mac(self, ovs_bridge):
        """Get mac of inet bridge"""
        mac_list = self.lib.ovsdb.get_str(table='Wifi_Inet_State',
                                          select='mac',
                                          where=[f'if_name=={ovs_bridge}'],
                                          return_list=True)
        return mac_list

    def get_native_ifname(self, ovs_bridge):
        # if not self.lib.run_command('ls /usr/opensync/lib/libaw.so'):
        #     return ovs_bridge
        path = self.lib.get_stdout(self.lib.get_opensync_path())
        response = self.lib.run_command(f'strings {path}/lib/libaw.so | grep -A1 {ovs_bridge}')
        assert not response[0]
        return response[1].splitlines()[1]

    def get_br_wan_mac(self):
        br_wan = self.get_br_wan_ifname()
        br_wan_mac = self.lib.ovsdb.get_str(table='Wifi_Inet_State',
                                            select='hwaddr',
                                            where=[f'if_name=={br_wan}'])
        return br_wan_mac

    def get_br_home_ip(self):
        br_home_iface = self.get_br_home_ifname()
        br_home_ip = self.lib.ovsdb.get_str(table='Wifi_Inet_State',
                                            select='inet_addr',
                                            where=[f'if_name=={br_home_iface}'])
        return br_home_ip

    def get_br_wan_ip(self):
        br_wan_iface = self.get_br_wan_ifname()
        br_wan_ip = self.lib.ovsdb.get_str(table='Wifi_Inet_State',
                                           select='inet_addr',
                                           where=[f'if_name=={br_wan_iface}'])
        return br_wan_ip

    def get_native_br_home(self):
        br_home_iface = self.get_br_home_ifname()
        br_home_mac = self.lib.ovsdb.get_str(table='Wifi_Inet_State',
                                             select='hwaddr',
                                             where=[f'if_name=={br_home_iface}'])
        try:
            br_home_name = self.get_iface_by_mac(br_home_mac)
        except ValueError:
            br_home_ip = self.get_br_home_ip()
            br_home_name = self.get_iface_by_mask(br_home_ip)
        return br_home_name

    def get_bhal_50_ifname(self):
        ssid = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                      select='ssid',
                                      where=['min_hw_mode!=11b', 'min_hw_mode!=11g'],
                                      return_list=True)[0]
        bhal_50_ifname = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                select='if_name',
                                                where=[f'ssid=={ssid}', 'min_hw_mode!=11b', 'min_hw_mode!=11g'],
                                                return_list=True)[0]
        return bhal_50_ifname

    def get_bhal_50_ifnames(self, ssid=None):
        if ssid is None:
            ssid = self.get_bhal_50_ssid()
        bhal_50_ifnames = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                 select='if_name',
                                                 where=[f'ssid=={ssid}', 'min_hw_mode!=11b', 'min_hw_mode!=11g'],
                                                 return_list=True)
        return bhal_50_ifnames

    def get_bhal_50_ssid(self):
        ssid = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                      select='ssid',
                                      where=['min_hw_mode!=11b', 'min_hw_mode!=11g',
                                             'vif_radio_idx==1', 'ssid_broadcast==disabled'])
        return ssid

    def get_bhal_24_ifnames(self, ssid=None):
        if ssid is None:
            ssid = self.get_bhal_24_ssid()
        bhal_24_ifnames_11b = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                     select='if_name',
                                                     where=[f'ssid=={ssid}', 'min_hw_mode==11b'],
                                                     return_list=True)
        bhal_24_ifnames_11g = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                     select='if_name',
                                                     where=[f'ssid=={ssid}', 'min_hw_mode==11g'],
                                                     return_list=True)
        return bhal_24_ifnames_11b + bhal_24_ifnames_11g

    def get_bhal_24_ifname(self, ssid=None):
        if ssid is None:
            ssid = self.get_bhal_24_ssid()
        bhal_24_ifnames_11b = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                     select='if_name',
                                                     where=[f'ssid=={ssid}', 'min_hw_mode==11b'],
                                                     return_list=True)
        bhal_24_ifnames_11g = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                     select='if_name',
                                                     where=[f'ssid=={ssid}', 'min_hw_mode==11g'],
                                                     return_list=True)
        return (bhal_24_ifnames_11b + bhal_24_ifnames_11g)[0]

    def get_bhal_24_ssid(self):
        ssid_11b = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                          select='ssid',
                                          where=['vif_radio_idx==1', 'ssid_broadcast==disabled', 'min_hw_mode==11b'],
                                          return_list=True)
        ssid_11g = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                          select='ssid',
                                          where=['vif_radio_idx==1', 'ssid_broadcast==disabled', 'min_hw_mode==11g'],
                                          return_list=True)
        return (ssid_11b + ssid_11g)[0]

    def get_home_50_ifname(self):
        home_50_ifname = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                select='if_name',
                                                where=[f'bridge==br-home', 'min_hw_mode!=11b', 'min_hw_mode!=11g'])
        return home_50_ifname

    def get_home_24_ifname(self):
        home_24_ifname_11b = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                    select='if_name',
                                                    where=[f'bridge==br-home', 'min_hw_mode==11b'],
                                                    return_list=True)
        home_24_ifname_11g = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                    select='if_name',
                                                    where=[f'bridge==br-home', 'min_hw_mode==11g'],
                                                    return_list=True)
        return (home_24_ifname_11b + home_24_ifname_11g)[0]

    def get_home_50_ifnames(self):
        home_50_ifnames = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                 select='if_name',
                                                 where=[f'bridge==br-home', 'min_hw_mode!=11b', 'min_hw_mode!=11g'],
                                                 return_list=True)
        return home_50_ifnames

    def get_home_50_macs(self):
        home_50_ifnames = self.get_home_50_ifnames()
        home_50_macs = [self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                               select='mac',
                                               where=[f'if_name=={home_50_ifname}'])
                        for home_50_ifname in home_50_ifnames]
        return home_50_macs

    def get_home_50_mac(self):
        home_50_ifname = self.get_home_50_ifname()
        home_50_mac = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                             select='mac',
                                             where=[f'if_name=={home_50_ifname}'])
        return home_50_mac

    def get_home_24_mac(self):
        home_24_ifname = self.get_home_24_ifname()
        home_24_mac = self.lib.ovsdb.get_str(
            table='Wifi_VIF_State', select='mac', where=[f'if_name=={home_24_ifname}'])
        return home_24_mac

    def get_bhal_50_mac(self):
        ovs_bhal_50 = self.get_bhal_50_ifname()
        bhal_50_mac = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                             select='mac',
                                             where=[f'if_name=={ovs_bhal_50}'],
                                             return_list=True)[0]
        return bhal_50_mac

    def get_bhal_50_mac_list(self, bhal_50_iface=None):
        if bhal_50_iface is None:
            bhal_50_iface = self.get_bhal_50_ifname()
        bhal_50_mac_list = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                  select='mac_list',
                                                  where=[f'if_name=={bhal_50_iface}'],
                                                  return_list=True)
        return bhal_50_mac_list

    def get_bhal_50_macs_with_interfaces(self):
        bhal_50_ifaces = self.get_bhal_50_ifnames()
        bhal_50_mac_list = [
            self.lib.ovsdb.get_str(table='Wifi_VIF_State', select='mac', where=[f'if_name=={bhal_50_iface}'])
            for bhal_50_iface in bhal_50_ifaces]
        return bhal_50_mac_list, bhal_50_ifaces

    def get_bhal_24_mac_list(self, bhal_24_iface=None):
        if bhal_24_iface is None:
            bhal_24_iface = self.get_bhal_24_ifname()
        bhal_24_mac_list = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                  select='mac_list',
                                                  where=[f'if_name=={bhal_24_iface}'],
                                                  return_list=True)
        return bhal_24_mac_list

    def get_bhal_24_mac_with_interfaces(self):
        bhal_24_iface = self.get_bhal_24_ifname()
        bhal_24_mac_list = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                  select='mac',
                                                  where=[f'if_name=={bhal_24_iface}'])
        return bhal_24_mac_list, bhal_24_iface

    def get_gre_bhal_50(self):
        ovs_bhal_50 = self.get_bhal_50_ifname()
        try:
            gre_iface = self.get_native_ifname(ovs_bhal_50)
        except AssertionError:
            # TODO: Fix for gw with ovs
            return ovs_bhal_50
        response = self.lib.run_command(f'brctl show | grep {gre_iface}')
        if response[0] == 0:
            gre_bhal_50 = response[1].split()[0]
        else:
            gre_bhal_50 = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                                 select='bridge',
                                                 where=[f'if_name=={ovs_bhal_50}'])
        if not gre_bhal_50:
            gre_bhal_50 = gre_iface
        return gre_bhal_50

    def get_ifname_ssid(self, ifname):
        ssid = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                      select='ssid',
                                      where=[f'if_name=={ifname}'])
        return ssid

    def get_ifname_psk(self, ifname):
        psk_dict = self.lib.ovsdb.get_str(table='Wifi_VIF_State',
                                          select='security',
                                          where=[f'if_name=={ifname}'])
        return psk_dict['key']

    def get_bands_types(self):
        bands_types = self.lib.ovsdb.get_str(table='Wifi_Radio_State',
                                             select='freq_band')
        return bands_types

    def get_allowed_channels(self, band_type):
        allowed_channels = self.lib.ovsdb.get_map(table='Wifi_Radio_State',
                                                  select='channels',
                                                  where=[f'freq_band=={band_type}'])
        allowed_channels = [int(allowed_channel) for allowed_channel, state in allowed_channels.items()
                            if 'allowed' in state]
        allowed_channels.sort()
        # remove channel 165
        if 165 in allowed_channels:
            allowed_channels.remove(165)
        # Type conversion to string due to topology lib supports only string channel values
        allowed_channels = [str(allowed_channel) for allowed_channel in allowed_channels]
        return allowed_channels

    def get_dfs_channels(self, band_type):
        allowed_channels = self.lib.ovsdb.get_map(table='Wifi_Radio_State',
                                                  select='channels',
                                                  where=[f'freq_band=={band_type}'])
        dfs_channels = [int(allowed_channel) for allowed_channel, state in allowed_channels.items()
                        if 52 <= int(allowed_channel) <= 144]
        dfs_channels.sort()
        dfs_channels = [str(dfs_channel) for dfs_channel in dfs_channels]
        return dfs_channels

    def get_physical_wifi_ifname(self, band_type):
        band_types = ['2.4G', '5G', '5GU', '5GL']
        assert band_type in ['2.4G', '5G', '5GU', '5GL'], f'Not supported band type: {band_type}. ' \
            f'Supported bands types: {band_types}'
        ifname = self.lib.ovsdb.get_str(table='Wifi_Radio_State', select='if_name',
                                        where=[f'freq_band=={band_type}'])
        return ifname

    def get_wifi_index(self, band_type):
        ifname = self.get_physical_wifi_ifname(band_type)
        return int(re.search(r'\d+', ifname).group())

    def get_physical_wifi_mac(self, ifname):
        mac_address = self.lib.ovsdb.get_str(table='Wifi_Radio_State', select='mac',
                                             where=[f'if_name=={ifname}'])
        return mac_address

    def get_ip(self, external=True):
        """
        Get pod IP
        Args:
            external: (bool) in case of router mode, indicate, which br-wan or br-home IP return

        Returns: (str) IP address

        """
        if self.is_router_mode():
            br_wan_ip = None
            try:
                br_wan_ip = self.get_iface_ip(self.get_br_wan_ifname())
            except ValueError:
                pass
            if external and br_wan_ip:
                return br_wan_ip
            return self.get_iface_ip(self.get_native_br_home())
        else:
            return self.get_iface_ip(self.get_br_wan_ifname())

    def is_router_mode(self):
        try:
            self.get_iface_ip(self.get_native_br_home())
            return True
        except ValueError:
            return False

    def is_ovs(self):
        response = self.lib.run_command('ovs-vsctl show')
        if response[0] == 0 and len(response[1].splitlines()) > 10:
            return True
        if response[0] and "not found" not in response[2]:
            # Raise exception
            self.lib.get_stdout(response)
        return False

    def get_backhauls(self):
        """
        Get information about all backhaul interfaces
        Returns: (dict) {'dev_if_name': {str), 'ssid': (str), 'mac_list': (list), 'associated_clients': (list)}

        """
        bhal_dict = {}
        bhal_24_ssid = self.get_bhal_24_ssid()
        bhal_50_ssid = self.get_bhal_50_ssid()

        # get info about bhal24 interfaces
        bhal_24_ifaces = self.get_bhal_24_ifnames(bhal_24_ssid)
        for bhal_24_iface in bhal_24_ifaces:
            mac_list = self.get_bhal_24_mac_list(bhal_24_iface)
            associated_clients = self.lib.ovsdb.get_uuid(table='Wifi_VIF_State',
                                                         select='associated_clients',
                                                         where=[f'if_name=={bhal_24_iface}'],
                                                         return_list=True)
            if associated_clients is not None:
                associated_clients = [str(assoc_client) for assoc_client in associated_clients
                                      if assoc_client is not None]
            bhal_dict[bhal_24_iface] = {'dev_if_name': bhal_24_iface, 'ssid': bhal_24_ssid, 'mac_list': mac_list,
                                        'associated_clients': associated_clients}

        # get info about bhal50 interfaces
        bhal_50_ifaces = self.get_bhal_50_ifnames(bhal_50_ssid)
        for bhal_50_iface in bhal_50_ifaces:
            mac_list = self.get_bhal_50_mac_list(bhal_50_iface)
            associated_clients = self.lib.ovsdb.get_uuid(table='Wifi_VIF_State',
                                                         select='associated_clients',
                                                         where=[f'if_name=={bhal_50_iface}'],
                                                         return_list=True)
            if associated_clients is not None:
                associated_clients = [str(assoc_client) for assoc_client in associated_clients
                                      if assoc_client is not None]
            bhal_dict[bhal_50_iface] = {'dev_if_name': bhal_50_iface, 'ssid': bhal_50_ssid, 'mac_list': mac_list,
                                        'associated_clients': associated_clients}

        return bhal_dict

    def get_all_home_bhaul_ifaces(self):
        """
        Get all home_ap, bhaul interfaces
        Returns: (list)

        """
        bhal_24_ssid = self.get_bhal_24_ssid()
        bhal_50_ssid = self.get_bhal_50_ssid()
        bhal_24_ifaces = self.get_bhal_24_ifnames(bhal_24_ssid)
        bhal_50_ifaces = self.get_bhal_50_ifnames(bhal_50_ssid)
        home_50_ifaces = self.get_home_50_ifnames()
        home_24_iface = self.get_home_24_ifname()
        all_interfaces = bhal_24_ifaces + bhal_50_ifaces + home_50_ifaces
        all_interfaces.append(home_24_iface)
        return all_interfaces

    def get_all_assoc_clients_mac(self):
        return self.lib.ovsdb.get_str(table='Wifi_Associated_Clients', select='mac', return_list=True)


class Ovsdb:
    def __init__(self, lib):
        self.lib = lib

    def _generate_get_cmd(self, table, select: str, where: Union[str, list] = None, option: str = ''):
        return f'ovsh s {table} ' \
               f'{self.generate_select(select)} ' \
               f'{self.generate_where(where)} ' \
               f'{option}'

    def get_raw(self, table: str, select: str, where: Union[str, list] = None, skip_exception=False):
        cmd = self._generate_get_cmd(table, select, where, '-r')
        return self.lib.run_command(cmd, skip_exception=skip_exception)

    def get(self, table, select: str, where: Union[str, list] = None, skip_exception=False):
        return self.lib.get_stdout(self.get_raw(table, select, where, skip_exception))

    @staticmethod
    def generate_where(value):
        if value is None or not value:
            return ''
        elif isinstance(value, list):
            return f'-w {" -w ".join(value)}'
        elif isinstance(value, str):
            return f'-w {value}'
        else:
            raise ValueError

    @staticmethod
    def generate_select(value):
        if value is None or not value:
            return ''
        elif isinstance(value, list):
            return " ".join(value)
        elif isinstance(value, str):
            return value
        else:
            raise ValueError

    def parse_raw(self, value_type, output, skip_exception=False,  # noqa C901
                  return_list=False) -> Union[int, bool, str, dict, list, UUID]:
        result = list()

        lines = output.splitlines()

        for line in lines:
            line = line.strip()
            if line == '["set",[]]':
                result.append([])
                continue
            elif line == '':
                continue

            try:
                if value_type == str:
                    if re.match(r'^\[\"set\",\[.*?\]\]$', line) is not None:
                        result.append(self.parse_raw(list, line))
                        continue
                    else:
                        value = line
                else:
                    try:
                        value = json.loads(line)
                    except json.decoder.JSONDecodeError:
                        value = json.loads(f'["set",["{line}"]]')

                if value_type is None:
                    result.append(value)
                elif value_type == dict and isinstance(value, list) and len(value) > 1:
                    if value[0] != 'map':
                        raise ValueError(f'Type missmatch. Expected map; got {value[0]}')
                    result.append(self.ovsdb_map_to_python_dict(value))
                elif value_type == list and isinstance(value, list) and len(value) > 1:
                    if value[0] != 'set':
                        raise ValueError(f'Type missmatch. Expected set; got {value[0]}')
                    result.append(value[1])
                elif value_type == UUID and isinstance(value, list) and len(value) > 1:
                    if re.match(r'^\[\"set\",\[.*?\]\]$', line) is not None:
                        values = self.parse_raw(list, line, skip_exception=skip_exception, return_list=return_list)
                        result += [UUID(val[1]) for val in values]
                        continue
                    if value[0] != 'uuid':
                        raise ValueError(f'Type missmatch. Expected uuid; got {value[0]}')
                    result.append(self.ovsdb_uuid_to_python_uuid(value[1]))
                elif value_type == bool and isinstance(value, str):
                    result.append(bool(distutils.util.strtobool(value)))
                elif value_type == type(value):
                    result.append(value)
                else:
                    raise ValueError(f'Can not convert {type(value).__name__} to {value_type.__name__}')
            except Exception as e:
                result.append(None)
                if not skip_exception:
                    raise e

        if len(result) == 1 and (not return_list or return_list and isinstance(result[0], list)):
            result = result[0]

        return result

    def get_int(self, table: str, select: str, where: Union[str, list] = None,
                skip_exception=False, return_list=False) -> Union[int, list]:
        raw_data = self.get(table, select, where, skip_exception=skip_exception)
        return self.parse_raw(int, raw_data, skip_exception=skip_exception, return_list=return_list)

    def get_bool(self, table: str, select: str, where: Union[str, list] = None,
                 skip_exception=False, return_list=False) -> Union[bool, list]:
        raw_data = self.get(table, select, where, skip_exception=skip_exception)
        return self.parse_raw(bool, raw_data, skip_exception=skip_exception, return_list=return_list)

    def get_str(self, table: str, select: str, where: Union[str, list] = None,
                skip_exception=False, return_list=False) -> Union[str, list]:
        raw_data = self.get(table, select, where, skip_exception=skip_exception)
        return self.parse_raw(str, raw_data, skip_exception=skip_exception, return_list=return_list)

    def get_map(self, table: str, select: str, where: Union[str, list] = None,
                skip_exception=False, return_list=False) -> Union[dict, list]:
        raw_data = self.get(table, select, where, skip_exception=skip_exception)
        return self.parse_raw(dict, raw_data, skip_exception=skip_exception, return_list=return_list)

    def get_set(self, table: str, select: str, where: Union[str, list] = None,
                skip_exception=False, return_list=False) -> list:
        raw_data = self.get(table, select, where, skip_exception=skip_exception)
        return self.parse_raw(list, raw_data, skip_exception=skip_exception, return_list=return_list)

    def get_uuid(self, table: str, select: str, where: Union[str, list] = None,
                 skip_exception=False, return_list=False) -> Union[UUID, list]:
        raw_data = self.get(table, select, where, skip_exception=skip_exception)
        return self.parse_raw(UUID, raw_data, skip_exception=skip_exception, return_list=return_list)

    def set_value(self, value: dict, table: str, where: Union[str, list] = None, skip_exception=False):
        cmd = f'ovsh {"i" if where is None or len(where) == 0 else "U"} {table} ' \
              f'{self.generate_values_str(value, skip_exception=skip_exception)} ' \
              f'{self.generate_where(where)}'
        result = self.lib.run_command(cmd, skip_exception=skip_exception)
        if result[0] == 1 and 'ERROR: Upsert: more than one row matched' in result[2]:
            cmd = f'ovsh u {table} ' \
                  f'{self.generate_values_str(value, skip_exception=skip_exception)} ' \
                  f'{self.generate_where(where)}'
            result = self.lib.run_command(cmd, skip_exception=skip_exception)

        return result

    def delete_row(self, table: str, where: Union[str, list] = None, skip_exception=False):
        cmd = f'ovsh d {table} ' \
              f'{self.generate_where(where)}'
        return self.lib.run_command(cmd, skip_exception=skip_exception)

    def mutate(self, table: str, select: str, action: str, value: str, skip_exception=False):
        assert action in ['ins', 'del']
        if action == 'del':
            cmd = f"ovsh u {table} {select}:{action}:'[\"set\",{value}]'"
        else:
            cmd = f"ovsh u {table} {select}:{action}:'[\"map\",[{value}]]'"
        return self.lib.run_command(cmd, skip_exception=skip_exception)

    def generate_values_str(self, values, skip_exception=False):
        return " ".join([f'{k}{"~" if isinstance(v, str) else ":"}'
                         f'={self.python_value_to_ovsdb_value(v, skip_exception)}' for k, v in values.items()])

    @staticmethod
    def python_list_to_ovsdb_set(value):
        if not isinstance(value, list):
            raise ValueError

        return f'\'["set",[{",".join(value)}]]\''

    @staticmethod
    def ovsdb_map_to_python_dict(value):
        if value[0] == 'map':
            value = value[1]

        res = dict()
        for item in value:
            res[item[0]] = item[1]

        return res

    @staticmethod
    def python_dict_to_ovsdb_map(value):
        if not isinstance(value, dict):
            raise ValueError

        str_list = list()
        for k, v in value.items():
            if isinstance(v, bool):
                v = str(v).lower()

            str_list.append(f'["{k}","{v}"]')

        return f'\'["map",[{",".join(str_list)}]\''

    @staticmethod
    def ovsdb_uuid_to_python_uuid(value):
        if isinstance(value, list) and len(value) > 1:
            value = value[1]

        return UUID(value)

    @staticmethod
    def python_uuid_to_ovsdb_uuid(value):
        if not isinstance(value, UUID):
            raise ValueError

        return f'\'["uuid","{value}"]\''

    def python_value_to_ovsdb_value(self, value, skip_exception=False):
        if isinstance(value, dict):
            value = self.python_dict_to_ovsdb_map(value)
        elif isinstance(value, list):
            value = self.python_list_to_ovsdb_set(value)
        elif isinstance(value, UUID):
            value = self.python_uuid_to_ovsdb_uuid(value)
        elif isinstance(value, bool):
            value = str(value).lower()
        elif isinstance(value, int):
            value = str(value)
        elif isinstance(value, str):
            pass
        else:
            if skip_exception:
                return None
            raise ValueError
        return value

    def get_name(self):
        return "ovsdb_lib"
