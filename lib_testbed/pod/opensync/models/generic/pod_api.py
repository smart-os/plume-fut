import time
import os
import xmltodict
from typing import Union
from uuid import UUID
from urllib import request

from lib_testbed.util.ssh.device_api import DeviceApi
from lib_testbed.util.allure_util import AllureUtil
from lib_testbed.util import rpowerlib
from lib_testbed.util.logger import log
from lib_testbed.util.msg.msg import Msg
from lib_testbed.util.ssh.sshexception import SshException
from lib_testbed.switch.switchlib import SwitchLib


class PodApi(DeviceApi):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.lib = self.initialize_device_lib(**kwargs)
        self.ovsdb = self.lib.ovsdb
        self.msg = self.lib.msg
        # self.ovsdb_helper = self.lib.ovsdb_helper
        self.iface = self.lib.iface
        self.log_catcher = self.lib.log_catcher

        # Dead code, workaround for resolving references by pycharm
        if hasattr(self, "INVALID_STATEMENT"):
            from lib_testbed.pod.opensync.models.generic.pod_lib import PodLib
            self.lib = PodLib()

    def setup_class_handler(self):
        if not self.lib.device:
            return
        if hasattr(self, "session_config") and self.session_config.option.skip_init:
            self.msg = Msg(self.lib)
            return

        if not self.lib.get_stdout(self.lib.uptime(timeout=20), skip_exception=True):
            self.pod_recovery()

        name = self.get_nickname()
        log.info(f"Pod{'s' if self.lib.multi_devices else ''}: {name} has management access")

        # initialize msg lib after recovery, since it do ssh call to check logger type
        self.msg = Msg(self.lib)

        # save FW versions
        if hasattr(self, "session_config"):
            version = self.version()
            serial = self.get_serial_by_name("Nodes", name)
            AllureUtil(self.session_config).add_environment(f'pod_{serial}_version', version, '_error')

    def setup_method_handler(self, method):
        super().setup_method_handler(method)

    def teardown_class_handler(self):
        if not self.lib.device or (hasattr(self, "session_config") and self.session_config.option.skip_init):
            super().teardown_class_handler()
            return
        # check if FW is correct
        timeout = 2 * 60
        start_time = time.time()
        cur_version = None
        first_loop = True
        # it's quite important to check it, so do it in loop in case pod reboots
        while time.time() - start_time < timeout:
            try:
                cur_version = self.version(skip_exception=True)
            except SshException:
                if first_loop:
                    log.warning(f'[{self.get_nickname()}] teardown. Waiting for management access {timeout}s..')
                    first_loop = False
            if cur_version:
                if not first_loop:
                    log.info(f'[{self.get_nickname()}] Management access ready')
                break
            time.sleep(5)
        serial = self.get_serial_by_name("Nodes", self.get_nickname())
        if hasattr(self, "session_config"):
            exp_ver = AllureUtil(self.session_config).get_environment(f'pod_{serial}_version')
        else:
            exp_ver = None
        if cur_version and exp_ver and cur_version != exp_ver:
            log.warning(f"Test ended with incorrect FW version: {cur_version}, upgrading through ssh to: {exp_ver}")
            cloud_obj = self.lib.get_custbase() if self.lib.get_custbase() else None
            if cloud_obj:
                try:
                    vm = cloud_obj.find_version_matrix({self.lib.device.config['model']: exp_ver})
                    self.upgrade_fw_through_ssh(vm)
                    # make sure that version matrix is set
                    cloud_obj.trigger_location_upgrade(vm.get('versionMatrix'))
                except Exception as e:
                    log.error(f'Restoring FW failed: {e}')
            else:
                log.warning('Can not restore FW to correct version without cloud admin object')
        super().teardown_class_handler()

    def pod_recovery(self):
        name = self.get_nickname()
        pod_prefix = 'Pod{}'.format('s' if self.lib.multi_devices else '')

        resp = self.lib.recover()
        if not resp[0] and self.lib.get_stdout(self.lib.uptime(timeout=10), skip_exception=True):
            log.info(f'Pod recover has been finished successfully on {name}')
            return

        # Before run rpower action check switch configuration
        if self.lib.config.get('Switch'):
            switch = SwitchLib(**{'config': self.lib.config})
            log.info(f'Running switch recovery on {name} node with switch configuration')
            switch.recovery_switch_configuration(name)
            log.info('Check again management access after switch recovery')
            if self.lib.get_stdout(self.lib.uptime(timeout=10), skip_exception=True):
                log.info(f'Recovery has been finished successfully on {name} node')
                return

        if not self.lib.config.get('rpower'):
            assert False, f'{pod_prefix}: {name} has no management access and rpower section is ' \
                f'missing in config'

        # get all rpower aliases for getting pods which are connected to rpower
        rpower_aliases = [pod['alias'] for pod in self.lib.config['rpower']]
        rpower_pods = list()
        for rpower_alias in rpower_aliases:
            for rpower in rpower_alias:
                rpower_pods.append(rpower.get('name'))

        assert name in rpower_pods, f'{pod_prefix}: {name} without management access has no configured rpower'
        rpowerlib.rpower_init_config(self.lib.config)

        log.info(f'{pod_prefix} {name} has no management access')
        log.info(f'Check last operation on rpower for: {name}')
        last_request_time = rpowerlib.get_last_request_time([name])
        rpower_status = rpowerlib.rpower_status([name])[name]
        if last_request_time[name] and last_request_time[name] < 60 and rpower_status[0] is not True:
            log.warning(f'Skip changing rpower status due to that last operation has been done'
                        f' {last_request_time[name]} seconds ago')
        else:
            log.info(f'Power cycling not accessible pod: {name}')
            rpowerlib.rpower_off([name])
            # Some models need more time to power off
            time.sleep(15)
            rpowerlib.rpower_on(rpowerlib.rpower_get_all_devices())
            # recover once again
            resp = self.lib.recover()
            if not resp[0]:
                uptime_result = self.lib.get_stdout(self.lib.uptime(timeout=10), skip_exception=True)
                if uptime_result:
                    log.info(f'Pod recover has been finished successfully on {name}')
        log.info('Check again if pod has management access')
        timeout = time.time() + (60 * 3)
        while time.time() < timeout:
            uptime_result = self.lib.get_stdout(self.lib.uptime(timeout=3), skip_exception=True)
            if uptime_result:
                break
            time.sleep(5)
        else:
            assert False, f'Pod name: {name} has no management access. ' \
                f'Can\'t recover {name} pod by power cycle'
        log.info(f'{pod_prefix}: {name} successfully recovered')

    def get_stdout(self, result, skip_exception=False, **_kwargs):
        """Returns the second element of result which is stdout. Validate return value"""
        return self.lib.get_stdout(result, skip_exception=skip_exception)

    def run(self, cmd, **kwargs):
        return super().run(cmd, **kwargs)

    def get_opensync_path(self, **kwargs):
        """Get path to opensync"""
        response = self.lib.get_opensync_path(**kwargs)
        return self.get_stdout(response, **kwargs)

    def ping(self, host=None, v6=False, **kwargs):
        """Ping"""
        response = self.lib.ping(host, v6=v6, **kwargs)
        skip_exception = kwargs.pop('skip_exception') if 'skip_exception' in kwargs else True
        return self.get_stdout(response, skip_exception=skip_exception, **kwargs)

    def reboot(self, **kwargs):
        """Reboot node(s)"""
        response = self.lib.reboot(**kwargs)
        return self.get_stdout(response, **kwargs)

    def version(self, **kwargs):
        """Display firmware version of node(s)"""
        return self.get_stdout(self.lib.version(**kwargs), **kwargs)

    def platform_version(self, **kwargs):
        """Display platform version of node(s)"""
        return self.get_stdout(self.lib.platform_version(**kwargs), **kwargs)

    def uptime(self, timeout=20, **kwargs):
        """Display uptime of node(s)"""
        response = self.lib.uptime(timeout, **kwargs)
        if kwargs.get('out_format'):
            del kwargs['out_format']
        return self.get_stdout(response, **kwargs)

    def get_datetime(self, **kwargs):
        """
        Get current datetime
        :return: date as python datetime object
        """
        return self.lib.get_datetime(**kwargs)

    def set_datetime(self, date_time, **kwargs):
        """
        Set date
        :param date_time: python datetime object
        """
        return self.lib.set_datetime(date_time, **kwargs)

    def get_file(self, remote_file, location, **kwargs):
        """Copy a file from node(s)"""
        response = self.lib.get_file(remote_file, location, **kwargs)
        return self.get_stdout(response, **kwargs)

    def put_file(self, file_name, location, **kwargs):
        """Copy a file onto device(s)"""
        response = self.lib.put_file(file_name, location, **kwargs)
        return self.get_stdout(response, **kwargs)

    def restart(self, **kwargs):
        """Restart managers on node(s)"""
        response = self.lib.restart(**kwargs)
        return self.get_stdout(response, **kwargs)

    def healthcheck_stop(self, **kwargs):
        """Stop healthcheck on pod"""
        response = self.lib.healthcheck_stop(**kwargs)
        return self.get_stdout(response, **kwargs)

    def healthcheck_start(self, **kwargs):
        """Start healthcheck on pod"""
        response = self.lib.healthcheck_start(**kwargs)
        return self.get_stdout(response, **kwargs)

    def deploy(self, **kwargs):
        """Deploy files to node(s)"""
        response = self.lib.deploy(**kwargs)
        return self.get_stdout(response, **kwargs)

    def check(self, **kwargs):
        """Pod health check"""
        response = self.lib.check(**kwargs)
        return self.get_stdout(response, **kwargs)

    def enable(self, **kwargs):
        """Enable agent and wifi radios on node(s)"""
        response = self.lib.enable(**kwargs)
        return self.get_stdout(response, **kwargs)

    def disable(self, **kwargs):
        """Disable agent and wifi radios on node(s)"""
        response = self.lib.disable(**kwargs)
        return self.get_stdout(response, **kwargs)

    def get_model(self, **kwargs):
        """Display type of node(s)"""
        return self.get_stdout(self.lib.get_model(**kwargs), **kwargs)

    def bssid(self, bridge='', **kwargs):
        """Display BSSID of node bridge = <br-wan|br-home>-, default both"""
        return [val for val in self.get_stdout(self.lib.bssid(bridge, **kwargs)).split('\n') if val != '']

    def get_serial_number(self, **kwargs):
        """Get node(s) serial number"""
        return self.get_stdout(self.lib.get_serial_number(**kwargs))

    def connected(self, **kwargs):
        """returns cloud connection state of each node"""
        return self.get_stdout(self.lib.connected(**kwargs))

    def get_ovsh_table(self, table, **kwargs):
        """get ovsh table from pods locally json format"""
        response = self.lib.get_ovsh_table(table, **kwargs)
        return self.get_stdout(response, **kwargs)

    def get_ovsh_table_tool(self, table, **kwargs):
        """get ovsh table from pods locally tool format"""
        resposne = self.lib.get_ovsh_table_tool(table, **kwargs)
        return self.get_stdout(resposne, **kwargs)

    def role(self, **kwargs):
        """Node role: return gw or leaf"""
        return self.get_stdout(self.lib.role(**kwargs))

    def get_ips(self, iface, **kwargs):
        """get ipv4 and ipv6 address for desired interface"""
        response = self.lib.get_ips(iface, **kwargs)
        return self.get_stdout(response, **kwargs)

    def get_macs(self, **kwargs):
        """get all macs from pod using Wifi_VIF_State table"""
        return self.lib.get_macs(**kwargs)

    def get_logs(self, directory=None, **kwargs):
        """get logs from pods locally"""
        response = self.lib.get_logs(directory, **kwargs)
        return self.get_stdout(response, **kwargs)

    def upgrade(self, image, *args, **kwargs):
        """Upgrade node firmware, Optional: -p=<encyp_key>, -e-> erase certificates, -n->skip version check"""
        response = self.lib.upgrade(image, *args, **kwargs)
        return self.get_stdout(response, **kwargs)

    def sanity(self, *args):
        """run sanity on selected pods, add arg --nocolor for simple output"""
        return self.lib.sanity(*args)

    def poll_pod_sanity(self, timeout=500, expect=True, *args):
        """Loops sanity on until pass"""
        return self.lib.poll_pod_sanity(timeout, expect, *args)

    def get_crash(self):
        """get crash log file from node"""
        return self.lib.get_crash()

    def get_log_level(self, manager_name, **kwargs):
        """Get logging level for OpenSync manager"""
        return self.lib.get_log_level(manager_name)

    def set_log_level(self, manager_name, level, **kwargs):
        """Set logging level for OpenSync manager"""
        return self.lib.set_log_level(manager_name, level)

    def get_backhauls(self):
        """
        Get information about all backhaul interfaces
        Returns: (dict) {'dev_if_name': {str), 'ssid': (str), 'mac_list': (list), 'associated_clients': (list)}

        """
        return self.lib.iface.get_backhauls()

    def get_radio_temperatures(self, radio: Union[int, str, list] = None, **kwargs):
        """
        Function for retrieving radio temperatures from piranha pods
        :param radio: accepted arguments: radio_id(e.g.: 0, 1), band frequency(e.g.: '2.4G', '5G'),
                      list of radio_ids or band frequencies, or use None for all radios
        :return: radio temperature as int or list(temperatures are ordered the same as in radio argument if provided,
                 else they are ordered by radio id)
        """
        return self.lib.get_radio_temperatures(radio, **kwargs)

    def upgrade_fw_through_ssh(self, fw_matrix_obj, force=False, reboot_wait=True, **kwargs):
        """
        Upgrade all supports models through ssh
        Args:
            fw_matrix_obj: (list) Firmware matrix objects from cloud which contains models, url, keys etc...
            force: (bool): Upgrade firmware even though the same firmware is already on device
            reboot_wait: (bool): Wait for reboot after upgarde
            **kwargs:

        Returns: (list) Current firmware version on devices.

        """
        model = self.get_model()
        current_version = self.version(skip_exception=True)
        # Get firmware url and key from fw_matrix_obj
        fw_desc = {}
        for fw_model in fw_matrix_obj['models']:
            fw_ver = fw_model.get('firmwareVersion')
            fw_url = fw_model.get('encryptedDownloadUrl')
            fw_key = fw_model.get('firmwareEncryptionKey')
            _model = fw_model.get('model')
            if not fw_url:
                fw_url = fw_model.get('downloadUrl')
            fw_desc[_model] = {'url': fw_url, 'key': fw_key, 'fw': fw_ver}

        fw_data = fw_desc.get(model)
        assert fw_data, f'Not found firmware url for {model} from {fw_matrix_obj}'
        if current_version and current_version == fw_data.get('fw') and not force:
            log.info("Skip upgrading, current version is the same as requested")
            return
        fw_url = fw_data.get('url')
        fw_key = fw_data.get('key')
        log.info(f'Upgrading {model} device with {fw_url}')
        if model == 'Plume Pod v1.0':
            cfw = fw_data['fw'].split('.')
            if cfw[0] == '1':
                if '-' in cfw[1]:
                    cfw[1] = cfw[1][0]
                if cfw[1] < '8':
                    log.info("Downgrade requires erasing certificates...")
                    self.lib.erase_certificates()
        try:
            self.healthcheck_stop()
            self.run(f'safeupdate -d {fw_url}', timeout=400, **kwargs)
            update_cmd = f'safeupdate -u {fw_url}'
        except SshException:
            log.warning('Seems that pod is offline, downloading FW')
            v1_fw_file_name = fw_url.split('/')[-1]
            if not os.path.exists('/tmp/automation/'):
                os.makedirs('/tmp/automation/')
            request.urlretrieve(fw_url, f"/tmp/automation/{v1_fw_file_name}")
            self.put_file(f"/tmp/automation/{v1_fw_file_name}", '/tmp/')
            update_cmd = f'safeupdate -u /tmp/{v1_fw_file_name}'

        if fw_key:
            update_cmd += f' -P "{fw_key}"'
        self.run(update_cmd, timeout=600)

        # Wait for a reboot
        if reboot_wait:
            time.sleep(60)
            self.lib.wait_available(timeout=120)
            return self.version(timeout=20, skip_exception=True)
        return True

    def wait_eth_connection_ready(self, timeout=600, **kwargs):
        """
        Wait for disable loop status so that to be ready for connect eth client to device
        :param timeout: (int) timeout in seconds
        :return: (bool) True if eth connection is ready, False if not ready after timeout and skip_exception is True
        """
        return self.lib.wait_eth_connection_ready(timeout, **kwargs)

    def wait_bs_table_ready(self, timeout=60, **kwargs):
        """
        Wait for ovsdb Band_Steering_Clients table to be populated
        :param timeout: Timeout
        :return: (bool) True if bs table is ready, False if not ready after timeout and skip_exception is True
        """
        return self.lib.wait_bs_table_ready(timeout, **kwargs)

    def decrease_tx_power_on_all_ifaces(self, percent_ratio, **kwargs):
        """
        Decrease value of tx power on the all home_ap, bhaul interfaces
        Args:
            percent_ratio: (int) Percent ratio from 0 to 100

        Returns:

        """
        response = self.lib.decrease_tx_power_on_all_ifaces(percent_ratio, **kwargs)
        return self.get_stdout(response, **kwargs)

    def increase_tx_power_on_all_ifaces(self, percent_ratio, **kwargs):
        """
        Increase value of tx power on the all home_ap, bhaul interfaces
        Args:
            percent_ratio: (int) Percent ratio from 0 to 100

        Returns:

        """
        response = self.lib.increase_tx_power_on_all_ifaces(percent_ratio, **kwargs)
        return self.get_stdout(response, **kwargs)

    def get_tx_power(self, interface, **kwargs):
        """
        Get current tx power in dBm
        Args:
            interface: (str) Wireless interface

        Returns: (int) tx power

        """
        response = self.lib.get_tx_power(interface, **kwargs)
        return self.get_stdout(response, **kwargs)

    def set_tx_power(self, tx_power=-1, interfaces=None, **kwargs):
        """
        Set current tx power in dBm
        Args:
            interfaces: (str) or (list) Name of wireless interfaces
            tx_power: (int) Tx power in dBm. -1 value reset tx power to default settings

        Returns:

        """
        response = self.lib.set_tx_power(tx_power, interfaces, **kwargs)
        return self.get_stdout(response, **kwargs)

    def get_driver_data_rate(self, ifname, mac_address, **kwargs):
        """
        Get current data rate from driver
        Args:
            ifname: (str) Name of interface
            mac_address: (str) Mac address of connected client interface
        Returns:

        """
        response = self.lib.get_driver_data_rate(ifname, mac_address, **kwargs)
        return self.get_stdout(response, **kwargs)

    def trigger_radar_detected_event(self, **kwargs):
        """
        Trigger radar detected event
        Returns: stdout

        """
        response = self.lib.trigger_radar_detected_event(**kwargs)
        return self.get_stdout(response, **kwargs)

    def get_boot_partition(self, **kwargs):
        """
        Get boot partition name
        Args:
            **kwargs:

        Returns: stdout

        """
        response = self.lib.get_boot_partition(**kwargs)
        return self.get_stdout(response, **kwargs)

    def get_partition_dump(self, partition, **kwargs):
        """
        Get hex dump of the partition
        Args:
            partition: (str) partition name
            **kwargs:

        Returns: stdout

        """
        response = self.lib.get_partition_dump(partition, **kwargs)
        return self.get_stdout(response, **kwargs)

    def clear_log_messages(self, **kwargs):
        """
        Clear log messages
        Args:
            **kwargs:

        Returns: stdout

        """
        response = self.lib.clear_log_messages(**kwargs)
        return self.get_stdout(response, **kwargs)

    def is_fw_fuse_burned(self, **kwargs):
        return self.lib.is_fw_fuse_burned(**kwargs)

    def get_cpu_memory_usage(self, **kwargs):
        return self.lib.get_cpu_memory_usage(**kwargs)

    def get_sfe_dump(self, ip_address='', timeout=240, **kwargs):
        response = self.lib.get_sfe_dump(ip_address, timeout, **kwargs)
        return self.get_stdout(response, **kwargs)

    def set_fan_mode(self, status, **kwargs):
        """
        Enable or disable fan on the device
        Args:
            status: (bool)
            **kwargs:

        Returns: stdout

        """
        response = self.lib.set_fan_mode(status, **kwargs)
        return self.get_stdout(response, **kwargs)

    def remove_crash(self, **kwargs):
        """
        Remove crashes from device
        Args:
            **kwargs:

        Returns: stdout

        """
        response = self.lib.remove_crash(**kwargs)
        return self.get_stdout(response, **kwargs)

    def set_region(self, region, **kwargs):
        """
        Set DFS region
        Args:
            region: (str) EU/US/JP

        Returns: stdout

        """
        response = self.lib.set_region(region=region, **kwargs)
        return self.get_stdout(response, **kwargs)

    def get_connection_flows(self, ip, **kwargs):
        """
        Get flows for specific IP address
        Args:
            ip: (str) IP address
            **kwargs:

        Returns: (list) flow list

        """
        response = self.lib.get_connection_flows(**kwargs)
        dump = self.get_stdout(response, **kwargs)
        if not dump:
            return []
        sfe_dump = xmltodict.parse('<sfe_dump>\n' + dump + '\n</sfe_dump>')['sfe_dump']
        flows = []
        if sfe_dump['sfe_ipv4'].get('connections'):
            # in case of single element direct dict is returned
            if type(sfe_dump['sfe_ipv4']['connections'].get('connection')) == list:
                out = sfe_dump['sfe_ipv4']['connections'].get('connection', [])
            else:
                out = [sfe_dump['sfe_ipv4']['connections']['connection']]
            for connection in out:
                if connection['@src_ip'] != ip:
                    continue
                flows.append(connection['@src_dev'])
        log.info(f"{self.get_nickname()} flows for {ip}: {flows}")
        return flows

    def redirect_stats_to_local_mqtt_broker(self, skip_storage=False, **kwargs):
        """
        Updates AWLAN_Node table and redirects stats to mqtt broker started on rpi-server
        Returns: stdout

        No parameters, since rpi-server IP 192.168.200.1 has to match rpi-server certificates used for TLS.
        """
        response = self.lib.redirect_stats_to_local_mqtt_broker(skip_storage=skip_storage, **kwargs)
        return self.get_stdout(response, **kwargs)

    def restore_stats_mqtt_settings(self, **kwargs):
        """
        Restores original AWLAN_Node settings for mqtt
        Returns: stdout
                """
        response = self.lib.restore_stats_mqtt_settings(**kwargs)
        return self.get_stdout(response, **kwargs)

    class OvsdbApi:
        def __init__(self, pod_api):
            self.pod_api = pod_api

        def get_int(self, table: str, select: str, where: Union[str, list] = None, skip_exception=False,
                    return_list=False) -> Union[int, list]:
            return self.pod_api.lib.ovsdb.get_int(table, select, where, skip_exception, return_list)

        def get_bool(self, table: str, select: str, where: Union[str, list] = None, skip_exception=False,
                     return_list=False) -> Union[bool, list]:
            return self.pod_api.lib.ovsdb.get_bool(table, select, where, skip_exception, return_list)

        def get_str(self, table: str, select: str, where: Union[str, list] = None, skip_exception=False,
                    return_list=False) -> Union[str, list]:
            return self.pod_api.lib.ovsdb.get_str(table, select, where, skip_exception, return_list)

        def get_map(self, table: str, select: str, where: Union[str, list] = None, skip_exception=False,
                    return_list=False) -> Union[dict, list]:
            return self.pod_api.lib.ovsdb.get_map(table, select, where, skip_exception, return_list)

        def get_set(self, table: str, select: str, where: Union[str, list] = None, skip_exception=False,
                    return_list=False) -> list:
            return self.pod_api.lib.ovsdb.get_set(table, select, where, skip_exception, return_list)

        def get_uuid(self, table: str, select: str, where: Union[str, list] = None, skip_exception=False,
                     return_list=False) -> Union[UUID, list]:
            return self.pod_api.lib.ovsdb.get_uuid(table, select, where, skip_exception, return_list)

        def set_value(self, value: dict, table: str, where: Union[str, list] = None, skip_exception=False):
            return self.pod_api.lib.ovsdb.set_value(value, table, where, skip_exception)

        def delete_row(self, table: str, where: Union[str, list] = None, skip_exception=False):
            return self.pod_api.lib.ovsdb.delete_row(table, where, skip_exception)

        def get_name(self):
            return "ovsdb_api"
