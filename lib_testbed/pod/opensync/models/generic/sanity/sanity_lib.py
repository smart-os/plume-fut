#!/usr/bin/env python3

import os
import time
import json
import re
from termcolor import colored
from subprocess import getoutput as getout
from subprocess import getstatusoutput as getstatusout
from multiprocessing import Lock
from lib_testbed.util.sanity import ovsdb
from lib_testbed.util.logger import log

lock = Lock()

# Function names map for generic validate_tables() method
func_name_map = {'AWLAN_Node table': ['_ovsdb_sanity_check_awlan_node_table'],
                 'Manager table': ['_ovsdb_sanity_check_manager_table'],
                 'Bridge table': ['_ovsdb_sanity_check_bridge_table'],
                 'DHCP_leased_IP table': ['_ovsdb_sanity_check_dhcp_leased_ip_table'],
                 'Interface table': ['_ovsdb_sanity_check_interface_table'],
                 'Wifi_Master_State table': ['_ovsdb_sanity_check_wifi_master_state_table'],
                 'Wifi_Inet_Config table': ['_ovsdb_sanity_check_wifi_inet_config_table'],
                 'Wifi_Radio_Config table': ['_ovsdb_sanity_check_wifi_radio_config_table'],
                 'Wifi_Associated_Clients table': ['_ovsdb_sanity_check_wifi_associate_clients_table'],
                 'Wifi_VIF_Config table': ['_ovsdb_sanity_check_wifi_vif_config_table',
                                           '_ovsdb_sanity_check_wifi_check_leaf']}


class SanityLib(object):

    def __init__(self, tables, gw_tables, logs_dir=None, outfile=None, outstyle='full', sys_log_file_name='messages'):
        self.func_name_map = func_name_map
        self.tables = tables
        self.gw_tables = gw_tables
        self.output = []
        self.logs_dir = logs_dir
        self._id = None
        self._outfile = outfile
        self.wan_device = "eth0"
        self.sys_log_file_name = sys_log_file_name
        self.outstyle = outstyle
        self.sanity_logs = ''

        if logs_dir and sys_log_file_name:
            if self.logs_dir[-1] != '/':
                self.logs_dir += '/'
            self.sys_log_file = self.logs_dir + sys_log_file_name
        else:
            self.sys_log_file = None

        # self arguments for methods
        self.bands = ['2.4G', '5G']
        self.retval = True
        self.gw_node, self.router_mode, self.id_length = None, None, None
        self.table_list, self.br_wan, self.br_home, self.home_ap, self.bhaul_ap, self.bhaul_sta, self.eth_lan = \
            [], [], [], [], [], [], []

    @staticmethod
    def get_model(awlan_table):
        return awlan_table[0]['model']

    def validate_tables(self, skip_tables=()):
        for table_name in self.table_list:
            if table_name[0] in skip_tables:
                continue
            func_names = func_name_map.get(table_name[0], None)
            if not func_names:
                continue
            for func_name in func_names:
                self.retval &= getattr(self, func_name)()

    def sanity_message(self):
        """
        Method to print the output
        """
        for row in self.output:
            if self.outstyle == 'simple':
                regex = re.compile("\033\[[0-9;]+m")  # noqa W605
                self.print_line('{0: >17}- {1: <25}:{2}'.format(row[1], row[0], regex.sub('', row[2])))
            elif self.outstyle == 'full':
                if row[1] == 'INFO':
                    level = colored(row[1], 'green')
                elif row[1] == 'Warning':
                    level = colored(row[1], 'yellow')
                elif row[1] == 'ERROR':
                    level = colored(row[1], 'red', attrs=['bold', 'blink'])
                else:
                    level = colored(row[1], 'white')
                tname = colored(row[0], 'magenta')
                message = colored(row[2], 'white')

                self.print_line('{0: >17}- {1: <25}:{2}'.format(level, tname, message))
            elif self.outstyle in ['none', 'lib']:
                log.info('outstyle in {}, skipping'.format(self.outstyle))
                # we don't want output at all
                pass
            else:
                # ?? unknown output style
                log.warning('Unknown output style: {}'.format(self.outstyle))
        self.flush_logs()
        log.console("", show_file=False)

    def print_line(self, line):
        self.sanity_logs += line + "\n"
        # log.console(line, show_file=False)
        if self._outfile:
            print(line, file=self._outfile)

    def flush_logs(self):
        with lock:
            print(self.sanity_logs)

    def get_id(self):
        return self._id if self._id else 'UNKNOWN_ID'

    def _create_output(self, table, level, msg):
        table = table.replace(' table', '')
        self.output.append([table, level, msg])

    # ##########################################################################
    # ovsdb tables related methods
    # ##########################################################################
    def _ovsdb_sanity_check_awlan_node_table(self):
        tname = 'AWLAN_Node table'
        table = self.tables[tname]
        errs = 0

        if len(table) == 0:
            self._create_output(table, 'ERROR', 'No rows')
            errs = errs + 1
        else:
            if len(table) > 1:
                self._create_output(tname, 'Warning', 'More than one row')

            if len(table[0]['id']) == 0:
                self._create_output(tname, 'ERROR', 'ID length is 0')
                errs = errs + 1
            elif len(table[0]['id']) != self.id_length:
                self._create_output(tname, 'Warning', 'ID length is not {}'.format(self.id_length))

            if len(table[0]['serial_number']) == 0:
                self._create_output(tname, 'ERROR', 'Serial_number length is 0')
                errs = errs + 1

            if len(table[0]['firmware_version']) == 0:
                self._create_output(tname, 'ERROR', 'Firmware version length is 0')
                errs = errs + 1
            else:
                self._create_output(tname, 'INFO',
                                    colored(table[0]['model'], 'white', attrs=['blink']) + ', ' + colored(
                                        table[0]['id'], 'cyan', attrs=['bold', 'blink']) + ', ' + colored(
                                        table[0]['firmware_version'], 'green', attrs=['dark']))
                if self.gw_node:
                    gwstr = 'Gateway '
                    if self.router_mode:
                        gwstr += '- Router Mode'
                    else:
                        gwstr += '- Bridge Mode'
                    self._create_output(tname, 'INFO', colored(gwstr, 'yellow', 'on_blue',
                                                               attrs=['bold', 'blink', 'underline', 'reverse']))
            if 'mqtt_settings' in table[0]:
                mqtt = table[0]['mqtt_settings']
                if 'broker' in mqtt:
                    if mqtt['broker'].find('amazonaws') >= 0:
                        self._create_output(tname, 'Warning', 'MQTT broker is pointing at Amazon AWS IOT')
                else:
                    self._create_output(tname, 'ERROR', 'MQTT broker not configured')
                    errs = errs + 1

                # ########## [ start ]
                # Comment check pod id at mqtt_settings, mqtt setting contains shrd id not pod id anymore
                # @date: 04/10/2018 nevena
                #if 'topics' in mqtt:
                #    if mqtt['topics'].find(table[0]['id']) < 0:
                #        self._create_output(tname, 'Warning', 'MQTT topic does not contain node id')
                #else:
                #    self._create_output(tname, 'ERROR', 'MQTT topic not configured')
                #    errs = errs + 1
                # ########## [ end ]

            else:
                self._create_output(tname, 'ERROR', 'No MQTT settings')
                errs = errs + 1

        return True if errs else False

    def _ovsdb_sanity_check_manager_table(self):
        tname = 'Manager table'
        table = self.tables[tname]
        errs = 0

        if len(table) == 0:
            self._create_output(tname, 'ERROR', 'No rows')
            errs = errs + 1
        else:
            if len(table) > 1:
                self._create_output(tname, 'Warning', 'More than one row')
            if len(table[0]['target']) == 0:
                self._create_output(tname, 'ERROR', 'Target length is 0')
            if table[0]['is_connected'] != 1:
                self._create_output(tname, 'ERROR', 'Not connected to manager: ' + str(table[0]['target']))
                errs = errs + 1
            else:
                self._create_output(tname, 'INFO', 'Connected to manager: ' + colored(str(table[0]['target']), 'white',
                                                                                      attrs=['bold', 'blink']))
        return True if errs else False

    def _ovsdb_sanity_check_bridge_table(self):
        tname = 'Bridge table'
        table = self.tables[tname]
        errs = 0

        iflist = self.br_wan + self.br_home

        if len(table) == 0:
            self._create_output(tname, 'ERROR', 'No rows')
            errs = errs + 1
        else:
            for n in iflist:
                row = ovsdb.ovsdb_find_row(table, 'name', n)
                if not row:
                    self._create_output(tname, 'ERROR', str(n) + ' row is missing')
                    errs = errs + 1
                elif len(row['ports']) == 0:
                    self._create_output(tname, 'Warning', str(n) + 'has no ports associated with it')

        return True if errs else False

    def _ovsdb_sanity_check_dhcp_leased_ip_table(self):
        tname = 'DHCP_leased_IP table'
        table = self.tables[tname]
        errs = 0

        bhome_cnt = 0
        if len(table) == 0:
            if self.gw_node:
                self._create_output(tname, 'Warning', 'No rows')
        else:
            # find out DHCP home subnet (exists only for router mode)
            try:
                hsub = ovsdb.ovsdb_find_row(self.tables['Wifi_Route_State table'], 'if_name', 'br-home')
                hsub = '.'.join(hsub['dest_addr'].split('.')[:-1])
            except (KeyError, TypeError):
                hsub = '192.168.0'
            for row in table:
                val = '.'.join(row['inet_addr'].split('.')[:-1])
                if val == hsub:
                    bhome_cnt = bhome_cnt + 1
                elif '169.254' in val:
                    is_gre = ovsdb.ovsdb_find_row(self.tables['Wifi_Associated_Clients table'], 'mac',
                                                  row['hwaddr'].lower())
                    if not is_gre:
                        # not valid GRE entry
                        continue

                    gre = ovsdb.ovsdb_find_row(self.tables['Wifi_Inet_Config table'], 'gre_remote_inet_addr',
                                               row['inet_addr'])
                    if not gre:
                        self._create_output(tname, 'ERROR', str(row['inet_addr']) + ' is missing downlink GRE interface'
                                                                                    ' in Wifi_Inet_Config')
                        errs = errs + 1
                    else:
                        prt = ovsdb.ovsdb_find_row(self.tables['Port table'], 'name', gre['if_name'])
                        if not prt:
                            self._create_output(tname, 'ERROR', str(gre['if_name']) + ' is missing from the Port table')
                        else:
                            br = ovsdb.ovsdb_find_row(self.tables['Bridge table'], 'ports', prt['_uuid'])
                            if not br:
                                self._create_output(tname, 'ERROR',
                                                    str(gre['if_name']) + 'port does not belong to a bridge')
                            # device vox3.0 has br-home named br0
                            elif br['name'] != 'br-home':
                                self._create_output(tname, 'ERROR', str(gre['if_name']) + 'port belongs to bridge')

            if self.gw_node and self.router_mode and bhome_cnt == 0:
                self._create_output(tname, 'Warning', 'No home {}* addresses found'.format(hsub))
        return True if errs else False

    def _ovsdb_sanity_check_interface_table(self):
        tname = 'Interface table'
        table = self.tables[tname]
        errs = 0
        iflist = self.br_wan + self.br_home + self.home_ap + self.eth_lan

        if len(table) == 0:
            self._create_output(tname, 'ERROR', 'No rows')
            errs = errs + 1
        else:
            if_list = iflist if self.router_mode else iflist + ['patch-w2h', 'patch-h2w']
            # if_list = iflist if router_mode else iflist + ('patch-w2h', 'patch-h2w', 'g-wl0')
            for n in if_list:
                row = ovsdb.ovsdb_find_row(table, 'name', n)
                if not row:
                    self._create_output(tname, 'Warning', str(n) + ' row is missing')
                    errs = errs + 1
                elif len(row['error']) > 0:
                    self._create_output(tname, 'ERROR', str(row['error']))
                elif row['admin_state'] != 'up':
                    self._create_output(tname, 'Warning', str(n) + ' admin_state ' + str(row['admin_state']) + '!=up')
                else:
                    if n.startswith('bhaul-ap-'):
                        if row['mtu'] != 1600:
                            self._create_output(tname, 'Warning', str(n) + 'MTU ' + str(row['mtu']) + ' !=1600')
                    elif n.startswith('patch-'):
                        if not row['options'] or not row['options']['peer']:
                            self._create_output(tname, 'ERROR', str(n) + ' is missing peer option')
                            errs = errs + 1
                        else:
                            peer = 'patch-h2w' if n == 'patch-w2h' else 'patch-w2h'
                            if row['options']['peer'] != peer:
                                self._create_output(tname, 'ERROR',
                                                    str(n) + ' peer ' + str(row['options']['peer']) + ' !=' + peer)
                                errs = errs + 1
                    elif n == 'br-wan':
                        continue
                    elif n.startswith('wl'):
                        continue
                    elif row['mtu'] != 1500:
                        self._create_output(tname, 'Warning', 'MTU ' + str(row['mtu']) + ' != 1500')
        return True if errs else False

    def _ovsdb_sanity_check_wifi_master_state_table(self):  # noqa
        """
        Check interface type, network state and port state
        Returns: (bool)

        """
        tname = 'Wifi_Master_State table'
        table = self.tables[tname]
        errs = 0
        iflist = self.br_home + self.br_wan + self.bhaul_ap + self.home_ap + self.bhaul_sta + self.eth_lan

        if len(table) == 0:
            self._create_output(tname, 'ERROR', 'No rows')
            errs = errs + 1
        else:
            sta_ok = 0
            for n in iflist:
                row = ovsdb.ovsdb_find_row(table, 'if_name', n)
                if not row:
                    if n.startswith('bhaul-sta-'):
                        break
                    else:
                        self._create_output(tname, 'ERROR', str(n) + ' row is missing')
                        errs = errs + 1
                else:
                    if n.startswith('bhaul-sta-'):
                        if row['if_type'] != 'vif':
                            self._create_output(tname, 'Warning', str(n) + ' if_type ' + row['if_type'] + '!=vif')
                        if self.gw_node:
                            if row['network_state'] != 'down':
                                self._create_output(tname, 'ERROR', str(n) + ' network_state ' + row[
                                    'network_state'] + ' != down on gateway node')
                                errs = errs + 1
                        else:
                            if row['port_state'] == 'active' and row['network_state'] == 'up':
                                sta_ok = sta_ok + 1
                    elif n in ['wl0', 'wl1', 'wifi0', 'wifi1']:
                        if row['if_type'] != 'vif':
                            self._create_output(tname, 'Warning', str(n) + ' if_type ' + row['if_type'] + '!=vif')
                        if self.gw_node:
                            # gw does not have STA interface up and active
                            if row['network_state'] != 'down':
                                self._create_output(tname, 'ERROR', str(n) + ' network_state ' + row[
                                    'network_state'] + ' != down on gateway node')
                                errs = errs + 1
                            elif row['port_state'] != 'inactive':
                                self._create_output(tname, 'ERROR',
                                                    str(n) + ' port_state ' + row['port_state'] + ' !=inactive')
                                errs = errs + 1
                        else:
                            if row['port_state'] == 'active' and row['network_state'] == 'up':
                                sta_ok = sta_ok + 1
                    elif n in ['br-wan', 'br-home']:
                        if row['if_type'] != 'bridge':
                            self._create_output(tname, 'Warning', str(n) + ' if_type' + row['if_type'] + ' != bridge')
                        if row['port_state'] != 'active':
                            self._create_output(tname, 'ERROR',
                                                str(n) + ' port_state ' + row['port_state'] + ' != active')
                            errs = errs + 1
                        if row['network_state'] != 'up':
                            self._create_output(tname, 'ERROR',
                                                str(n) + ' network_state ' + row['network_state'] + ' != up')
                            errs = errs + 1
                        if n == 'br-wan' and row['inet_addr'] == "0.0.0.0":
                            self._create_output(tname, 'Warning', str(n) + ' doesnt have an IP Address')
                    elif n == 'eth0':
                        if row['if_type'] != 'eth':
                            self._create_output(tname, 'Warning', str(n) + ' if_type' + row['if_type'] + ' != eth')
                        if row['port_state'] != 'active':
                            self._create_output(tname, 'ERROR',
                                                str(n) + ' port_state ' + row['port_state'] + ' != active')
                            errs = errs + 1
                        if row['network_state'] != 'up':
                            self._create_output(tname, 'ERROR',
                                                str(n) + ' network_state ' + row['network_state'] + ' != up')
                    elif n.startswith('wl0.') or n.startswith('wl1.'):
                        if row['if_type'] != 'vif':
                            self._create_output(tname, 'Warning', str(n) + ' if_type ' + row['if_type'] + ' != vif')
                        if row['network_state'] != 'up':
                            self._create_output(tname, 'ERROR', str(n) + ' network_state ' + row[
                                'network_state'] + ' != up on gateway node')
            if not self.gw_node and sta_ok == 0:
                self._create_output(tname, 'ERROR', 'No active/up STA connection found')
                errs = errs + 1
        return True if errs else False

    def _ovsdb_sanity_check_wifi_associate_clients_table(self):
        tname = 'Wifi_Associated_Clients table'
        table = self.tables[tname]
        errs = 0

        vif_state = self.tables['Wifi_VIF_State table']
        assoc_cnt = 0
        for row in vif_state:
            if isinstance(row['associated_clients'], list):
                assoc_cnt = assoc_cnt + len(row['associated_clients'])
            elif len(row['associated_clients']) > 4:
                assoc_cnt = assoc_cnt + 1
        if len(table) != assoc_cnt:
            self._create_output(tname, 'ERROR',
                                str(len(table)) + ' != Wifi_VIF_State associated_clients total ' + str(assoc_cnt))
            errs = errs + 1
        return True if errs else False

    def _ovsdb_sanity_check_wifi_inet_config_table(self):  # noqa
        """
        Check Wifi_Inet_State with Wifi_Inet_Config
        Returns: (bool)

        """
        config_tname = 'Wifi_Inet_Config table'
        state_tname = 'Wifi_Inet_State table'
        config_table = self.tables[config_tname]
        state_table = self.tables[state_tname]
        errs = 0

        iflist = self.br_home + self.br_wan + self.bhaul_ap + self.home_ap + self.bhaul_sta

        if self.gw_node:
            iflist.append(self.wan_device) if self.wan_device else iflist

        for r in iflist:
            for pair in [[config_tname, config_table], [state_tname, state_table]]:
                tname = pair[0]
                table = pair[1]
                row = ovsdb.ovsdb_find_row(table, 'if_name', r)
                if not row:
                    self._create_output(tname, 'ERROR', r + ' row is missing')
                    errs = errs + 1
                else:
                    if r == 'br-wan':
                        if self.router_mode and not row['NAT']:
                            self._create_output(tname, 'Warning', r + ' doesnt have NAT enabled')
                            errs = errs + 1
                    elif r == 'br-home':
                        if row['if_type'] != 'bridge':
                            self._create_output(tname, 'Warning', r + ' != bridge')
                        if tname == state_tname:
                            inet_cnt = 0
                            if isinstance(row['inet_addr'], list):
                                inet_cnt = inet_cnt + len(row['inet_addr'])
                            elif len(row['inet_addr']) > 0 and row['inet_addr'] != '0.0.0.0':
                                inet_cnt = inet_cnt + 1
                            if r == 'br-wan' and inet_cnt == 0:
                                self._create_output(tname, 'ERROR', r + ' is missing inet_addr')
                                errs = errs + 1
                            elif r == 'br-home' and inet_cnt != 0 and not self.router_mode:
                                self._create_output(tname, 'ERROR', r + ' has an inet_addr')
                                errs = errs + 1
                            if r == 'br-home':
                                if self.router_mode and row['ip_assign_scheme'] != 'static':
                                    self._create_output(tname, 'ERROR', r + ' ip_assign_scheme is'
                                                                            ' not set to static')
                                elif not self.router_mode and row['ip_assign_scheme'] != 'none':
                                    self._create_output(tname, 'ERROR', r + ' ip_assign_scheme is'
                                                                            ' not set to dhcp')
                                    errs = errs + 1
                    elif r == self.wan_device:
                        # special case for tagged eth ifaces eth0/1.835
                        if_type = 'vlan' if '835' in r else 'eth'
                        if row['if_type'] != if_type:
                            self._create_output(tname, 'Warning', f"{r} if_type {row['if_type']} != {if_type}")
                    else:
                        if row['if_type'] != 'vif':
                            self._create_output(tname, 'Warning', f"{r} if_type {row['if_type']} != vif")
        # Make sure required config entries present in state table
        for crow in config_table:
            if crow['if_name'] in iflist:
                continue
            srow = ovsdb.ovsdb_find_row(state_table, 'if_name', crow['if_name'])
            if not srow:
                self._create_output(state_tname, 'ERROR', str(crow['if_name']) + ' not found but exists ')
                errs = errs + 1
        return True if errs else False

    def _ovsdb_sanity_check_wifi_radio_config_table(self):
        config_tname = 'Wifi_Radio_Config table'
        state_tname = 'Wifi_Radio_State table'
        config_table = self.tables[config_tname]
        state_table = self.tables[state_tname]
        errs = 0

        for r in self.bands:
            for pair in [[config_tname, config_table], [state_tname, state_table]]:
                tname = pair[0]
                table = pair[1]
                row = ovsdb.ovsdb_find_row(table, 'freq_band', r)
                if not row:
                    self._create_output(tname, 'ERROR', str(r) + ' row is missing')
                    errs = errs + 1
                elif tname == 'Wifi_Radio_State table':
                    if len(row['mac']) == 0:
                        self._create_output(tname, 'ERROR', str(r) + ' does not have a MAC Address')
                        errs = errs + 1  # print out channel mode for Radio State table
        for row in state_table:
            if row['channel_mode']:
                self._create_output(state_tname, 'INFO',
                                    'Channel_mode ' + row['freq_band'] + ': ' + str(row['channel_mode']))
        return True if errs else False

    def _ovsdb_sanity_check_wifi_vif_config_table(self):  # noqa
        config_tname = 'Wifi_VIF_Config table'
        state_tname = 'Wifi_VIF_State table'
        config_table = self.tables[config_tname]
        state_table = self.tables[state_tname]
        errs = 0
        home_ap_cnt = 0
        bhaul_ap_cnt = 0
        bhaul_sta_cnt = 0
        home_ap_list = self.home_ap
        bhaul_ap_list = self.bhaul_ap
        bhaul_sta_list = self.bhaul_sta
        req_list = self.home_ap + self.bhaul_ap + self.bhaul_sta

        for r in req_list:
            for pair in [[config_tname, config_table], [state_tname, state_table]]:
                tname = pair[0]
                table = pair[1]
                row = ovsdb.ovsdb_find_row(table, 'if_name', r)
                if not row:
                    if not r.startswith('bhaul-sta-'):
                        self._create_output(tname, 'ERROR', str(r) + ' row is missing')
                        errs = errs + 1
                else:
                    if tname == config_tname:
                        if row['encryption_key']:
                            enc_str = ' Key=' + str(row['encryption_key'])
                        else:
                            enc_str = 'Enc='
                            if row['security'] and row['security']['encryption']:
                                enc_str = ' ' + row['security']['encryption']
                                if row['security']['encryption'] == 'WPA-PSK':
                                    if 'key' in row['security']:
                                        enc_str = enc_str + ' Key=' + str(row['security']['key'])
                                    else:
                                        enc_str = enc_str + ' NO KEY'
                                        self._create_output(tname, 'ERROR', 'NO PASSWORD FOR WPA!')
                            else:
                                enc_str = enc_str + 'UNKNOWN'
                        if r.startswith('bhaul-sta-'):
                            if bhaul_sta_cnt == 0 and bhaul_ap_cnt == 0:
                                self._create_output(tname, 'INFO',
                                                    '{0: >16}{1: <20} {2}'.format('Backhaul SSID=', str(row['ssid']),
                                                                                  enc_str))
                            bhaul_sta_cnt = bhaul_sta_cnt + 1
                        elif r.startswith('bhaul-ap-'):
                            if bhaul_sta_cnt == 0 and bhaul_ap_cnt == 0:
                                self._create_output(tname, 'INFO',
                                                    '{0: >16}{1: <20} {2}'.format('Backhaul SSID=', str(row['ssid']),
                                                                                  enc_str))
                            bhaul_ap_cnt = bhaul_ap_cnt + 1
                        elif r.startswith('home-ap-'):
                            if home_ap_cnt == 0:
                                ssid = row['ssid'].encode('ascii', 'replace')
                                self._create_output(tname, 'INFO',
                                                    '{0: >16}{1: <20} {2}'.format('Home SSID=', str(ssid), enc_str))
                            home_ap_cnt = home_ap_cnt + 1
                    if r in bhaul_sta_list:
                        if row['mode'] != 'sta':
                            self._create_output(tname, 'ERROR', r + ' mode' + str(row['mode']) + ' !=sta')
                            errs = errs + 1
                        if tname == state_tname and isinstance(row['parent'], list) or len(row['parent']) == 0:
                            self._create_output(tname, 'Warning', r + ' parent not set')
                    if r in bhaul_ap_list:
                        if row['mode'] != 'ap':
                            self._create_output(tname, 'ERROR', r + ' mode' + str(row['mode']) + '!=ap')
                            errs = errs + 1
                    if r in home_ap_list:
                        if row['mode'] != 'ap':
                            self._create_output(tname, 'ERROR', r + 'mode ' + str(row['mode']) + '!=ap')
                            errs = errs + 1
                        if row['bridge'] not in self.br_home:
                            self._create_output(tname, 'ERROR', r + 'bridge ' + str(row['bridge']) + '!=br-home')
                            errs = errs + 1
                    else:
                        break

        home_ap_list_cnt = len(home_ap_list)
        if home_ap_cnt != home_ap_list_cnt:
            self._create_output(state_tname, 'ERROR',
                                'Number of home-ap-* {} != {}'.format(home_ap_cnt, home_ap_list_cnt))
            errs = errs + 1

        bhaul_ap_list_cnt = len(bhaul_ap_list)
        if 'bhaul-ap-24' in req_list and bhaul_ap_cnt != bhaul_ap_list_cnt:
            self._create_output(state_tname, 'ERROR',
                                'Number of bhaul-ap-* {} != {}'.format(bhaul_ap_cnt, bhaul_ap_list_cnt))
            errs = errs + 1
        if self.gw_node:
            if bhaul_sta_cnt != 0:
                self._create_output(state_tname, 'ERROR',
                                    'Number of bhaul-ap-* {} != 0 on gateway'.format(bhaul_ap_cnt))
                errs = errs + 1
        elif bhaul_sta_cnt > 1:
            self._create_output(state_tname, 'Warning', 'More than one bhaul-sta-* VIFs found ' + str(bhaul_ap_cnt))
        # Make sure non-required config entries are present in state table
        for crow in config_table:
            if crow['if_name'] in req_list:
                continue
            srow = ovsdb.ovsdb_find_row(state_table, 'if_name', crow['if_name'])
            if not srow:
                self._create_output(state_tname, 'ERROR', str(crow['if_name']) + ' not found but exists in VIF config')
                errs = errs + 1
        # Check for every vlan found in Vif_config confirm that for every pgd
        # interface there exists ifname with that vlan
        for crow in config_table:
            if crow['vlan_id']:
                srow = ovsdb.ovsdb_find_row(self.tables['Wifi_Inet_Config table'], 'vlan_id', crow['vlan_id'])
                if not srow:
                    self._create_output(state_tname, 'ERROR', 'Pdg interface for vlan id:' + str(
                        crow['vlan_id']) + ' not found in Wifi_Inet_State table')
                    errs = errs + 1

        # Check svc-d, security, ssid_broadcast is same in config and state table
        for crow in config_table:
            if crow['if_name'][0:5] == 'svc-d':
                srow = ovsdb.ovsdb_find_row(self.tables['Wifi_VIF_State table'], 'if_name', crow['if_name'])
                if not srow:
                    self._create_output(state_tname, 'ERROR',
                                        'If_name:' + str(crow['if_name']) + ' not found in Wifi_VIF_State table')
                    errs = errs + 1
                secrow = ovsdb.ovsdb_find_row(self.tables['Wifi_VIF_State table'], 'security', crow['security'])
                if not secrow:
                    self._create_output(state_tname, 'ERROR',
                                        'Security' + str(crow['security']) + ' not found in Wifi_VIF_State table')
                    errs = errs + 1
                ssidrow = ovsdb.ovsdb_find_row(self.tables['Wifi_VIF_State table'], 'ssid_broadcast',
                                               crow['ssid_broadcast'])
                if not ssidrow:
                    self._create_output(state_tname, 'ERROR', 'SSID Broadcast:' + str(
                        crow['ssid_broadcast']) + ' not found in Wifi_VIF_State table')
                    errs = errs + 1

        # compare channel column of VIF state and Radio state
        for crow in state_table:
            if crow['channel']:
                srow = ovsdb.ovsdb_find_row(self.tables['Wifi_Radio_State table'], 'channel', crow['channel'])
                if not srow:
                    self._create_output(state_tname, 'ERROR',
                                        'Channel:' + str(crow['channel']) + ' not found in Wifi_Radio_State table')
                    errs = errs + 1

        return True if errs else False

    def _ovsdb_sanity_check_wifi_vif_config_table_bcm(self):  # noqa: C901
        config_tname = 'Wifi_VIF_Config table'
        state_tname = 'Wifi_VIF_State table'
        config_table = self.tables[config_tname]
        state_table = self.tables[state_tname]
        errs = 0
        home_ap_cnt = 0
        bhaul_ap_cnt = 0
        bhaul_sta_cnt = 0
        home_ap_endswith = '.2'
        bhaul_ap_endswith = '.1'
        sta_interfaces = ['wl0', 'wl1']
        req_list = self.bhaul_ap + self.home_ap

        # Check active sta interface and add it to the req_list
        if not self.gw_node:
            tname = 'Connection_Manager_Uplink table'
            row = 'is_used'
            uplink_row = ovsdb.ovsdb_find_row(self.tables[tname], row, True)
            if uplink_row:
                if_name = uplink_row.get('if_name').lstrip('g-')
                req_list.append(if_name)
            else:
                self._create_output(tname, 'ERROR', f'{row} row is missing')
                errs = errs + 1

        home_ap_list = [ap for ap in req_list if ap.endswith(home_ap_endswith)]
        bhaul_ap_list = [ap for ap in req_list if ap.endswith(bhaul_ap_endswith)]
        bhaul_sta_list = [sta for sta in req_list if sta in sta_interfaces]

        for r in req_list:
            for pair in [[config_tname, config_table], [state_tname, state_table]]:
                tname = pair[0]
                table = pair[1]
                row = ovsdb.ovsdb_find_row(table, 'if_name', r)
                if not row:
                    if not r not in sta_interfaces:
                        self._create_output(tname, 'ERROR', str(r) + ' row is missing')
                        errs = errs + 1
                else:
                    if tname == config_tname:
                        if row['encryption_key']:
                            enc_str = ' Key=' + str(row['encryption_key'])
                        else:
                            enc_str = 'Enc='
                            if row['security'] and row['security']['encryption']:
                                enc_str = ' ' + row['security']['encryption']
                                if row['security']['encryption'] == 'WPA-PSK':
                                    if 'key' in row['security']:
                                        enc_str = enc_str + ' Key=' + str(row['security']['key'])
                                    else:
                                        enc_str = enc_str + ' NO KEY'
                                        self._create_output(tname, 'ERROR', 'NO PASSWORD FOR WPA!')
                            else:
                                enc_str = enc_str + 'UNKNOWN'
                        if r.endswith(bhaul_ap_endswith):
                            if bhaul_sta_cnt == 0 and bhaul_ap_cnt == 0:
                                self._create_output(tname, 'INFO',
                                                    '{0: >16}{1: <20} {2}'.format('Backhaul SSID=', str(row['ssid']),
                                                                                  enc_str))
                            bhaul_ap_cnt = bhaul_ap_cnt + 1
                        elif r.endswith(home_ap_endswith):
                            if home_ap_cnt == 0:
                                ssid = row['ssid'].encode('ascii', 'replace')
                                self._create_output(tname, 'INFO',
                                                    '{0: >16}{1: <20} {2}'.format('Home SSID=', str(ssid), enc_str))
                            home_ap_cnt = home_ap_cnt + 1
                        elif r not in sta_interfaces:
                            if bhaul_sta_cnt == 0 and bhaul_ap_cnt == 0:
                                self._create_output(tname, 'INFO',
                                                    '{0: >16}{1: <20} {2}'.format('Backhaul SSID=', str(row['ssid']),
                                                                                  enc_str))
                            bhaul_sta_cnt = bhaul_sta_cnt + 1
                    if r in bhaul_sta_list:
                        if row['mode'] != 'sta':
                            self._create_output(tname, 'ERROR', r + ' mode' + str(row['mode']) + ' !=sta')
                            errs = errs + 1
                        if tname == state_tname and isinstance(row['parent'], list) or len(row['parent']) == 0:
                            self._create_output(tname, 'Warning', r + ' parent not set')
                    if r in bhaul_ap_list:
                        if row['mode'] != 'ap':
                            self._create_output(tname, 'ERROR', r + ' mode' + str(row['mode']) + '!=ap')
                            errs = errs + 1
                    if r in home_ap_list:
                        if row['mode'] != 'ap':
                            self._create_output(tname, 'ERROR', r + 'mode ' + str(row['mode']) + '!=ap')
                            errs = errs + 1
                        if row['bridge'] != 'br-home':
                            self._create_output(tname, 'ERROR', r + 'bridge ' + str(row['bridge']) + '!=br-home')
                            errs = errs + 1
                    else:
                        break

        home_ap_list_cnt = len(home_ap_list)
        if home_ap_cnt != home_ap_list_cnt:
            self._create_output(state_tname, 'ERROR',
                                'Number of home-ap-* {} != {}'.format(home_ap_cnt, home_ap_list_cnt))
            errs = errs + 1

        bhaul_ap_list_cnt = len(bhaul_ap_list)
        if 'wl1.1' in req_list and bhaul_ap_cnt != bhaul_ap_list_cnt:
            self._create_output(state_tname, 'ERROR',
                                'Number of bhaul-ap-* {} != {}'.format(bhaul_ap_cnt, bhaul_ap_list_cnt))
            errs = errs + 1
        if self.gw_node:
            if bhaul_sta_cnt != 0:
                self._create_output(state_tname, 'ERROR',
                                    'Number of bhaul-ap-* {} != 0 on gateway'.format(bhaul_ap_cnt))
                errs = errs + 1
        elif bhaul_sta_cnt > 1:
            self._create_output(state_tname, 'Warning', 'More than one bhaul-sta-* VIFs found ' + str(bhaul_ap_cnt))
        # Make sure non-required config entries are present in state table
        for crow in config_table:
            if crow['if_name'] in req_list:
                continue
            srow = ovsdb.ovsdb_find_row(state_table, 'if_name', crow['if_name'])
            if not srow:
                self._create_output(state_tname, 'ERROR', str(crow['if_name']) + ' not found but exists in VIF config')
                errs = errs + 1
        # Check for every vlan found in Vif_config confirm that for every pgd
        # interface there exists ifname with that vlan
        for crow in config_table:
            if crow['vlan_id']:
                srow = ovsdb.ovsdb_find_row(self.tables['Wifi_Inet_Config table'], 'vlan_id', crow['vlan_id'])
                if not srow:
                    self._create_output(state_tname, 'ERROR', 'Pdg interface for vlan id:' + str(
                        crow['vlan_id']) + ' not found in Wifi_Inet_State table')
                    errs = errs + 1

        # Check svc-d, security, ssid_broadcast is same in config and state table
        for crow in config_table:
            if crow['if_name'][0:5] == 'svc-d':
                srow = ovsdb.ovsdb_find_row(self.tables['Wifi_VIF_State table'], 'if_name', crow['if_name'])
                if not srow:
                    self._create_output(state_tname, 'ERROR',
                                        'If_name:' + str(crow['if_name']) + ' not found in Wifi_VIF_State table')
                    errs = errs + 1
                secrow = ovsdb.ovsdb_find_row(self.tables['Wifi_VIF_State table'], 'security', crow['security'])
                if not secrow:
                    self._create_output(state_tname, 'ERROR',
                                        'Security' + str(crow['security']) + ' not found in Wifi_VIF_State table')
                    errs = errs + 1
                ssidrow = ovsdb.ovsdb_find_row(self.tables['Wifi_VIF_State table'], 'ssid_broadcast',
                                               crow['ssid_broadcast'])
                if not ssidrow:
                    self._create_output(state_tname, 'ERROR', 'SSID Broadcast:' + str(
                        crow['ssid_broadcast']) + ' not found in Wifi_VIF_State table')
                    errs = errs + 1

        # compare channel column of VIF state and Radio state
        for crow in state_table:
            if crow['channel']:
                srow = ovsdb.ovsdb_find_row(self.tables['Wifi_Radio_State table'], 'channel', crow['channel'])
                if not srow:
                    self._create_output(state_tname, 'ERROR',
                                        'Channel:' + str(crow['channel']) + ' not found in Wifi_Radio_State table')
                    errs = errs + 1

        return True if errs else False

    def ovsdb_sanity_check(self, force_leaf=False, **kwargs):
        self._id = self.tables['AWLAN_Node table'][0]['serial_number']
        model = self.get_model(self.tables['AWLAN_Node table'])
        if self.outstyle not in ['none', 'lib']:
            self.print_line(f'{model} sanity check for: {self._id}')

        # check if all tables exist
        for table in self.table_list:
            for i in range(len(table)):
                if self.tables.get(table[i]) is None:
                    self.print_line('Table {} does not exist'.format(table[i]))
                    return False

        if kwargs:
            if kwargs.get('createOutput'):
                self._create_output(kwargs['createOutput']['tableName'],
                                    kwargs['createOutput']['logLevel'], kwargs['createOutput']['msg'])
                del kwargs['createOutput']

        # check if gateway
        self.check_is_gateway(force_leaf)

        # check network mode
        if not self.router_mode:
            self.router_mode = self.check_network_mode()

        if self.router_mode and not self.gw_node:
            self.print_line('Non-Gateway node is not in bridge mode')

    def _ovsdb_sanity_check_wifi_check_leaf(self):
        """
        Check ssid, security, ssid_broadcast, mac list type, mode
        Returns: (bool)

        """
        if not self.gw_tables:
            return False
        config_tname = 'Wifi_VIF_Config table'
        state_tname = 'Wifi_VIF_State table'
        config_table = self.tables[config_tname]
        state_table = self.tables[state_tname]
        errs = 0
        req_list = self.home_ap + self.bhaul_ap

        # Check ssid, security, ssid_broadcast, mac_list_type, mode
        for r in req_list:

            crow = ovsdb.ovsdb_find_row(config_table, 'if_name', r)
            srow = ovsdb.ovsdb_find_row(state_table, 'if_name', r)

            if not srow:
                self._create_output(state_tname, 'ERROR', 'if name:' + str(r) + ' not found in Wifi_VIF_State table')
                errs = errs + 1
                continue
            if not crow:
                self._create_output(state_tname, 'ERROR', 'if name:' + str(r) + ' not found in Wifi_VIF_Config table')
                errs = errs + 1
                continue
            # check ssid
            if crow['ssid'] != srow['ssid']:
                self._create_output(config_tname, 'ERROR',
                                    "ssid does not match: Config {0} State [{1}]".format(crow['ssid'], srow['ssid']))
                errs = errs + 1
            # check security
            if crow['security'] != srow['security']:
                self._create_output(config_tname, 'ERROR',
                                    "security does not match: Config {0} State [{1}]".format(crow['security'],
                                                                                             srow['security']))
                errs = errs + 1
            # check ssid_broadcast
            if 'ssid_broadcast' in crow and crow['ssid_broadcast'] is None or crow['ssid_broadcast'] == '':
                crow['ssid_broadcast'] = 'enabled'
            elif str(crow['ssid_broadcast']) != str(srow['ssid_broadcast']):
                self._create_output(config_tname, 'ERROR',
                                    "ssid_broadcast does not match: Config {0} State [{1}]".format(
                                        crow['ssid_broadcast'], srow['ssid_broadcast']))
                errs = errs + 1
            # check mac_list_type
            if crow['mac_list_type'] is None or crow['mac_list_type'] == '':
                crow['mac_list_type'] = "none"

            elif str(crow['mac_list_type']) != str(srow['mac_list_type']):
                self._create_output(config_tname, 'ERROR',
                                    "mac_list_type does not match: Config {0} State [{1}]".format(crow['mac_list_type'],
                                                                                                  srow[
                                                                                                      'mac_list_type']))
                errs = errs + 1
            # check mode
            if crow['mode'] != srow['mode']:
                self._create_output(config_tname, 'ERROR',
                                    "mode does not match: Config {0} State [{1}]".format(crow['mode'], srow['mode']))
                errs = errs + 1

        return True if errs else False

    def check_is_gateway(self, force_leaf):
        if not force_leaf and not self.gw_node:
            prt = ovsdb.ovsdb_find_row(self.tables['Port table'], 'name', self.wan_device)
            if prt:
                br = ovsdb.ovsdb_find_row(self.tables['Bridge table'], 'ports', prt['_uuid'])
                if br['name'] == "br-wan":
                    self.gw_node = True

    def check_network_mode(self):
        router_mode = True
        prt_w2h = ovsdb.ovsdb_find_row(self.tables['Port table'], 'name', 'patch-w2h')
        prt_h2w = ovsdb.ovsdb_find_row(self.tables['Port table'], 'name', 'patch-h2w')

        if prt_w2h and prt_h2w:
            br_w2h = ovsdb.ovsdb_find_row(self.tables['Bridge table'], 'ports', prt_w2h['_uuid'])
            br_h2w = ovsdb.ovsdb_find_row(self.tables['Bridge table'], 'ports', prt_h2w['_uuid'])
            if br_w2h and br_w2h['name'] == 'br-wan' and br_h2w \
                    and br_h2w['name'] == 'br-home':
                router_mode = False
            else:
                self.retval = False

        return router_mode

    # ##########################################################################
    # sys log related methods
    # ##########################################################################
    def _check_kernel_crash(self):
        '''
        :brief: Check kernel crash
        :return: None
        '''
        boot_cnt = getout("grep 'Calibrating delay loop' {}* | wc -l".format(self.sys_log_file))
        lvl = 'Warning' if int(boot_cnt) > 3 else 'INFO'

        crash_status, crash_log = getstatusout("ls {}/*crashlog* > /dev/null 2>&1".format(self.logs_dir))
        if not crash_status:
            lvl = 'ERROR'
            extra = "   #### Contains kernel crash log ####"
        else:
            extra = ''
        up_time, up_time_str = self._get_up_time()
        if up_time < 600:
            lvl = 'Warning'
        self._create_output('KERNEL', lvl,
                            "Uptime {0} sec [{1}], Boot Count={2}{3}".format(up_time, up_time_str, boot_cnt, extra))

    def _get_up_time(self):
        '''
        :brief: get uptime
        :return: up time in sec and in str format
        '''
        if os.path.isfile(self.logs_dir + 'uptime'):
            up_time = getout("cat {}uptime".format(self.logs_dir)).strip()
            try:
                if 'day' in up_time:
                    up_days = int(up_time.split()[2])
                    up_hours = int(up_time.split()[4].split(':')[0])
                    up_min = int(up_time.split()[4].split(':')[1][:-1])
                elif 'min' in up_time:
                    up_days, up_hours = 0, 0
                    up_min = int(up_time.split()[2])
                else:
                    up_days = 0
                    up_hours = int(up_time.split()[2].split(':')[0])
                    up_min = int(up_time.split()[2].split(':')[1][:-1])
                uptime_sec = (up_min + up_hours * 60 + up_days * 60 * 24) * 60
            except ValueError:
                uptime_sec = -1
            except IndexError:
                uptime_sec = -1
        else:
            uptime_sec = getout("grep '.' {}dmesg | tail -1".format(self.logs_dir))
            try:
                uptime_sec = int(uptime_sec.split('.')[0][1:])
            except (TypeError, KeyError, ValueError):
                log.warning('Cannot get uptime from: {}'.format(uptime_sec))
                uptime_sec = -1
        if uptime_sec > 0:
            up_time_str = time.strftime('%H hrs, %M min, %S sec', time.gmtime(uptime_sec))
            days = int(uptime_sec / 60 / 60 / 24)
            up_time_str = str(days) + ' days, ' + up_time_str
        else:
            up_time_str = 'UNKNOWN'
        return uptime_sec, up_time_str

    def _check_app_crash(self):
        app_crashes = int(getout("ls {}crashed_* 2>&- | wc -l".format(self.logs_dir)))
        core_dumps = int(getout("ls {}*.core.gz 2>&- | wc -l".format(self.logs_dir)))
        if app_crashes or core_dumps:
            self._create_output('USERSPACE', 'Warning', '#### There are {} app crash'
                                                        ' logs, and {} core dumps ####'.format(app_crashes, core_dumps))

    def _check_vap(self, vaps=('bhaul-ap-24', 'bhaul-ap-50', 'home-ap-24', 'home-ap-50')):
        # Make sure AP VAPs are beaconing
        for x in vaps:
            state, msg = getstatusout("cat {}iwconfig | grep -A 1 {}[^0-9] |"
                                      " egrep 'Not-Associated' > /dev/null 2>&1".format(self.logs_dir, x))
            if not state:
                self._create_output('VAP Check', 'ERROR', x + ' is not beaconing!')

    def _check_eth(self):
        up_cnt = int(getout("cat {}* | egrep -i '{}: link up' | wc -l".format(self.sys_log_file, self.wan_device)))
        down_cnt = int(getout("cat {}* | egrep -i '{}: link down' | wc -l".format(self.sys_log_file, self.wan_device)))
        lvl = "Warning" if down_cnt >= 2 else 'INFO'
        if up_cnt or down_cnt:
            up_1000 = int(getout("cat {}* | egrep -i '{}: link up' | egrep '1000Mbps'"
                                 " | wc -l".format(self.sys_log_file, self.wan_device)))
            up_100 = int(getout("cat {}* | egrep -i '{}: link up' | egrep '100Mbps'"
                                " | wc -l".format(self.sys_log_file, self.wan_device)))
            up_10 = int(getout("cat {}* | egrep -i '{}: link up' | egrep '10Mbps'"
                               " | wc -l".format(self.sys_log_file, self.wan_device)))
            self._create_output('{} STATS'.format(self.wan_device), lvl,
                                'DOWN={} times, UP={} times ({}=1000Mbps, {}=100Mbps, {}=10Mbps)'.format(down_cnt,
                                                                                                         up_cnt,
                                                                                                         up_1000,
                                                                                                         up_100, up_10))

    def _check_cm(self):
        disconn_cnt = int(
            getout("egrep 'OVS connection changed from TRUE to FALSE' {}* | wc -l".format(self.sys_log_file)))
        conn_cnt = int(
            getout("egrep 'OVS connection changed from FALSE to TRUE' {}* | wc -l".format(self.sys_log_file)))
        link_errs = int(getout("egrep 'erroring out due to previous state' {}* | wc -l".format(self.sys_log_file)))
        reinit_cnt = int(getout("egrep 'State RE-INIT starting' {}* | wc -l".format(self.sys_log_file)))
        fatal_cnt = int(getout("egrep 'FATAL condition triggered' {}* | wc -l".format(self.sys_log_file)))
        lvl = 'Warning' if fatal_cnt >= 5 or conn_cnt >= 10 else 'INFO'
        self._create_output('CM STATS', lvl, 'Connect={}, Disconnect={},'
                                             ' LinkErrs={}, ReInit={}, Recoveries={}'.format(conn_cnt, disconn_cnt,
                                                                                             link_errs, reinit_cnt,
                                                                                             fatal_cnt))

    def _check_wm(self, wifi=('wifi0', 'wifi1'), bhaul_sta=('bhaul-sta-24', 'bhaul-sta-50'),
                  home_ap=('home-ap-24', 'home-ap-50')):

        csa = {}
        for iface in wifi:
            csa[iface] = int(getout("egrep 'using CSA' {}* | egrep '{}' | wc -l".format(self.sys_log_file, iface)))

        pchange = {}
        configuring = {}
        assoc = {}
        disassoc = {}
        for sta in bhaul_sta:
            pchange[sta] = int(getout("egrep '{}: Parent change' {}* | wc -l".format(sta, self.sys_log_file)))
            configuring[sta] = int(getout("egrep '{}: Configuring' {}* | wc -l".format(sta, self.sys_log_file)))
            assoc[sta] = int(getout("egrep '{}: STA now associated' {}* | wc -l".format(sta, self.sys_log_file)))
            disassoc[sta] = int(
                getout("egrep '{}: STA still disassociated' {}* | wc -l".format(sta, self.sys_log_file)))

        cassoc = {}
        cdisassoc = {}
        for ap in home_ap:
            cassoc[ap] = int(getout("egrep '{}: Client associated' {}* | wc -l".format(ap, self.sys_log_file)))
            cdisassoc[ap] = int(getout("egrep '{}: Client disassociated' {}* | wc -l".format(ap, self.sys_log_file)))

        total_csa = sum(csa.values())
        total_pchange = sum(pchange.values())
        total_conf = sum(configuring.values())
        total_disc = sum(disassoc.values())
        total_assoc = sum(assoc.values())
        total_ap_assoc = sum(cassoc.values())
        total_ap_disassoc = sum(cdisassoc.values())

        total_lvl = 'Warning' if total_disc > 5 or total_assoc - total_conf > 5 else 'INFO'

        # sfmt = 'CSA={} P.Change={} Conf={} Assoc={} Disc={} C.Conn={} C.Disc={}'
        sfmt = '{0: >8} {1: <7} {2:<11} {3:<8} {4:<9} {5:<8} {6:<10} {7:<9}'
        self._create_output('WM STATS', total_lvl,
                            sfmt.format('Totals:', 'CSA={}'.format(total_csa), 'P.Change={}'.format(total_pchange),
                                        'Conf={}'.format(total_conf), 'Assoc={}'.format(total_assoc),
                                        'Disc={}'.format(total_disc), 'C.Conn={}'.format(total_ap_assoc),
                                        'C.Disc={}'.format(total_ap_disassoc)))
        for i in range(len(bhaul_sta)):
            lvl = 'Warning' if disassoc[bhaul_sta[i]] > 5 or assoc[bhaul_sta[i]] - configuring[
                bhaul_sta[i]] > 5 else 'INFO'
            if i == 0:
                radio = '2.4G'
            elif i == 1:
                radio = '5G'
            else:
                radio = '5GU'
            self._create_output('', lvl, sfmt.format('{}:'.format(radio), 'CSA={}'.format(csa[wifi[i]]),
                                                     'P.Change={}'.format(pchange[bhaul_sta[i]]),
                                                     'Conf={}'.format(configuring[bhaul_sta[i]]),
                                                     'Assoc={}'.format(assoc[bhaul_sta[i]]),
                                                     'Disc={}'.format(disassoc[bhaul_sta[i]]),
                                                     'C.Conn={}'.format(cassoc[home_ap[i]]),
                                                     'C.Disc={}'.format(cdisassoc[home_ap[i]])))

    def _check_time(self):
        """
        :brief: Check if Date and Time is set properly after boot
        """
        state, msg = getstatusout("tail -1 $(ls -1t {}* | head -1) | egrep '^Jan  1 '"
                                  " > /dev/null 2>&1".format(self.sys_log_file))

        if not state:
            self._create_output('DateTime', 'Warning', 'Detected date and time was not set')

    def _check_dhcp_idx(self):
        link = self.logs_dir.split('/')
        link = '/'.join(link[:-2])
        ids = []
        try:
            [ids.append(node['backhaulDhcpPoolIdx']) for node in json.load(open(link + '/log-pull.json', 'r'))['nodes']]
        except IOError:
            self._create_output('DHCP Idx', 'Warning', 'log-pull.json file not found!')
            return
        dups = []
        [dups.append(str(i)) for i in ids if ids.count(i) > 1]
        dup_ids = ','.join(set(dups))
        if dup_ids:
            self._create_output('DHCP Idx', 'ERROR',
                                'DHCP pool index collision in {}, duplicate Id(s): {}'.format(link, dup_ids))

    def _check_single_dhcp_server(self):
        """
        :brief: Find gateway from log messages
                if there is
        """
        tname = 'Wifi_Inet_State table'
        table = self.tables[tname]
        errs = 0

        try:
            row = ovsdb.ovsdb_find_row(table, 'if_name', 'br-wan')
            dhcpc = row['dhcpc']['gateway']
        except (KeyError, TypeError):
            # use first found in sys log
            dhcpc = getout(
                "cat {}* | egrep -i 'udhcpc.user: gateway=' | tail -n +3 | head -1".format(self.sys_log_file))
            try:
                dhcpc = dhcpc.split('gateway=')[1]
            except IndexError:
                # nothing to do here...
                return

        dhcpc_gw = getout("cat {}* | egrep -i 'udhcpc.user: gateway='".format(self.sys_log_file))

        # Check for multiple dhcp servers
        for line in dhcpc_gw.split('\n'):
            # if log time is <Jan  1> reboot was detected
            if 'Jan  1' in line:
                errs = 0
                continue
            if 'gateway=' in line and dhcpc not in line:
                errs += 1

        if errs:
            self._create_output('DHCP', 'ERROR', 'Multiple dhcp servers')

    def sys_log_sanity_check(self):
        """
        :brief: sistem log sanity check
        """
        self._check_dhcp_idx()
        if any(x.startswith(self.sys_log_file_name) for x in os.listdir(self.logs_dir)):
            self._check_time()
            self._check_kernel_crash()
            self._check_app_crash()
            self._check_vap()
            self._check_eth()
            self._check_wm()
            self._check_cm()
            self._check_single_dhcp_server()
        else:
            self._create_output('syslog', 'ERROR', 'System log file {} not found in logs'.format(self.sys_log_file))

    # ##########################################################################
    # wireless stats
    # ##########################################################################
    def get_wireless_stats(self):
        stats = {'XRETRY': 'WAL_DBGID_STA_VDEV_XRETRY', 'TX_TMOUT': 'WAL_DBGID_DEV_TX_TIMEOUT',
                 'DEV_RESET': 'WAL_DBGID_DEV_RESET', 'BB_WDOG': 'WAL_DBGID_BB_WDOG_TRIGGERED',
                 'CHANNF1': 'WHAL_ERROR_RESET_CHANNF1', 'BMISS': 'VDEV_MGR_BEACON_MISS_DETECTED',
                 'BMISS_TMOUT': 'VDEV_MGR_BEACON_MISS_TIMER_TIMEOUT',
                 'MC_FMS_Q': 'ath_tx_start_dma: Adding multicast frames to FMS queue',
                 'OUT_OF_SYNC': 'detected out-of-sync'}
        for stat in stats:
            stats[stat] = int(getout("egrep '{}' {}* | wc -l".format(stats[stat], self.sys_log_file)))
        stats['UPTIME'], up_str = self._get_up_time()
        stats['POD'] = self.get_id()
        return stats
