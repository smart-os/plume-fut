#!/usr/bin/env python3
import os
import sys
import logging
_basedir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(_basedir)
from lib_testbed.util.logger import log, LOGGER_NAME, update_logger_with_stream_handler  # noqa: E402
from lib_testbed.pod.pod import Pod  # noqa: E402

GW_NAME = 'gw'


class PodExt:
    def __init__(self, ssh_gateway, gw_id, model, host, host_recover):
        config = self.create_config(ssh_gateway=ssh_gateway,
                                    gw_id=gw_id,
                                    model=model,
                                    host=host,
                                    host_recover=host_recover)
        self.config = config
        # self.recover()

    def recover(self):
        update_logger_with_stream_handler(log)
        pod_api = self.get_pod_api()
        self.set_log_level(logging.DEBUG)
        return pod_api.lib.recover()

    def get_pod_api(self):
        kwargs = {'config': self.config, 'multi_obj': False, 'nickname': GW_NAME}
        pod_obj = Pod(**kwargs)
        pod_api = pod_obj.resolve_obj(**kwargs)
        return pod_api

    @staticmethod
    def set_log_level(level):
        # Set logging level
        logger = logging.getLogger(LOGGER_NAME)
        logger.setLevel(level)

    @staticmethod
    def create_config(ssh_gateway, gw_id, model, host, host_recover):
        config = {
            "ssh_gateway": ssh_gateway,
            "Nodes": [
                {
                    "name": GW_NAME,
                    "id": gw_id,
                    "model": model,
                    "host": host,
                    "host_recover": host_recover
                }
            ]
        }
        return config


if __name__ == '__main__':
    pod = PodExt(ssh_gateway='plume@192.168.40.200',
                 gw_id='',
                 model='pp213x',
                 host={
                     "user": "osync",
                     "name": "192.168.4.10",
                     "pass": "osync123",
                     "port": "22",
                 })
    pod.recover()
