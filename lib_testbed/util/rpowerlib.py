import requests
import xmltodict
import time
from lib_testbed.util.opensyncexception import OpenSyncException
from lib_testbed.util.logger import log

RPOWER_CONFIG = 'RemotePowerConfig'
RPOWER_DEVICE = 'rpower'
RPOWER_DEVICE_IPDADDR = 'ipaddr'
RPOWER_DEVICE_PORT = 'port'
RPOWER_DEVICE_USER = 'user'
RPOWER_DEVICE_PASS = 'pass'
RPOWER_DEVICE_TYPE = 'type'
RPOWER_DEVICE_ALIAS = 'alias'
RPOWER_DEVICE_ALIAS_NAME = 'name'
RPOWER_DEVICE_ALIAS_PORT = 'port'
TBCFG_NODES = 'Nodes'

ENV_RPOWER_CONFIG = 'RPOWER_CONFIG'

RPOWER_DEVICE_ENERGENIE = 'energenie'
ENERGENIE_URL = 'http://{0}:{1}@{2}:{3}/'

RPOWER_DEVICE_WPS = 'wps'
WPS_URL = 'http://{0}:{1}@{2}:{3}/'

RPOWER_DEVICE_FAKE = 'fake'


_rpower_aliases = {}
__rpower_config = None
_rpower_all_device_names = []
_rpower_node_names = []
_rpower_client_names = []


def select_nodes(return_values, sel_fcn=lambda y: y[0] == 0):
    return [node for node, value in list(return_values.items()) if sel_fcn(value)]


def to_list(obj):
    if not hasattr(obj, "__iter__"):
        obj = [obj]
    return obj


class PowerController:
    def on(self, port_no):
        raise NotImplementedError()

    def off(self, port_no):
        raise NotImplementedError()

    def status(self, port_no):
        raise NotImplementedError()

    def get_request_timestamp(self, device_name):
        raise NotImplementedError()

    def get_last_request_time(self, device_name):
        last_request_time = self.get_request_timestamp(device_name)
        if not last_request_time:
            return None
        return time.time() - last_request_time


class EnergeniePowerController(PowerController):
    request_timestamp = {}

    def __init__(self, address, port, user, passwd, url=ENERGENIE_URL, ports=8):
        self.address = address
        self.port = port
        self.url = url
        self.user = user
        self.passwd = passwd
        self.ports = ports

    def execute_request(self, request):
        url = self.url.format(self.user, self.passwd, self.address, self.port) + request
        ret_val = 1
        err_str = ''
        std_out = ''
        try:
            response = requests.get(url, timeout=10)
            if response.status_code != 200:
                ret_val = response.status_code
            else:
                std_out = response.content.decode()
                ret_val = 0

        except Exception as e:
            err_str = str(e)

        return [ret_val, std_out, err_str]

    def on(self, ports):
        ports = to_list(ports)
        mask = ['0'] * self.ports
        for port in ports:
            mask[port-1] = '1'
        req = 'ons.cgi?led={0}'.format("".join(mask))
        ret_val = self.execute_request(req)
        return [ret_val for port in ports]

    def off(self, ports):
        ports = to_list(ports)
        mask = ['0'] * self.ports
        for port in ports:
            mask[port-1] = '1'
        req = 'offs.cgi?led={0}'.format("".join(mask))
        ret_val = self.execute_request(req)
        return [ret_val for port in ports]

    def status(self, ports):
        req = 'status.xml'
        resp = self.execute_request(req)
        respxml = xmltodict.parse(resp[1])
        status = respxml['response']['pot0'].split(',')[10:18]
        retval = [[int(status[port-1]) != 1, "", ""] for port in ports]
        return retval

    @staticmethod
    def set_last_request_time(device_name):
        WPSPowerController.request_timestamp[device_name] = time.time()

    def get_request_timestamp(self, device_name):
        return WPSPowerController.request_timestamp.get(device_name)


class WPSPowerController(PowerController):
    request_timestamp = {}

    def __init__(self, address, port, user, passwd, url=WPS_URL, ports=8):
        self.address = address
        self.port = port
        self.url = url
        self.user = user
        self.passwd = passwd
        self.ports = ports

    def execute_request(self, request):
        url = self.url.format(self.user, self.passwd, self.address, self.port) + request
        ret_val = 1
        err_str = ''
        std_out = ''
        retry = 3
        while retry > 0:
            try:
                response = requests.get(url, timeout=10)
                if response.status_code != 200:
                    ret_val = response.status_code
                else:
                    std_out = response.content.decode()
                    err_str = ''
                    ret_val = 0
                break
            except Exception as e:
                log.warn('Unable to execute request: {}'.format(url))
                err_str = str(e)
            time.sleep(0.5)
            retry -= 1

        return [ret_val, std_out, err_str]

    def on(self, ports):
        resp = []
        for port in ports:
            req = 'outlet?{0}=ON'.format(port)
            out = self.execute_request(req)
            out[1] = 'Success!' if not out[0] else out[1]
            resp.append(out)
        return resp

    def off(self, ports):
        resp = []
        for port in ports:
            req = 'outlet?{0}=OFF'.format(port)
            out = self.execute_request(req)
            out[1] = 'Success!' if not out[0] else out[1]
            resp.append(out)
        return resp

    def status(self, ports):
        req = 'index.htm'
        val = None
        resp = self.execute_request(req)[1].split(' ')
        for token in resp:
            if token.find('state=') >= 0:
                val = int(token.split('=')[1], 16)
        if val is not None:
            ret_val = [[((val >> (port - 1)) & 0x01 == 0), "", ""]
                       for port in ports]
        else:
            ret_val = [[1, "", "Could not read state"] for port in ports]

        return ret_val

    @staticmethod
    def set_last_request_time(device_name):
        WPSPowerController.request_timestamp[device_name] = time.time()

    def get_request_timestamp(self, device_name):
        return WPSPowerController.request_timestamp.get(device_name)


class FakePowerController(PowerController):
    request_timestamp = {}

    def __init__(self, address, port, user, passwd, url=None, ports=8):
        self.state = [0] * ports
        return

    def on(self, ports):
        for port in ports:
            self.state[port - 1] = 0
        return [[0, "Fake port!", ""] for port in ports]

    def off(self, ports):
        for port in ports:
            self.state[port - 1] = 1
        return [[0, "Fake port!", ""] for port in ports]

    def status(self, ports):
        return [[(self.state[port - 1] & 0x01 == 0),  "Fake port!", ""] for port in ports]

    @staticmethod
    def set_last_request_time(device_name):
        WPSPowerController.request_timestamp[device_name] = time.time()

    def get_request_timestamp(self, device_name):
        return WPSPowerController.request_timestamp.get(device_name)


def _get_power_controller_dict(nodes):
    power_controllers = {}
    for node in nodes:
        if node in _rpower_aliases:
            power_controller, port = _rpower_aliases[node]
            if power_controller not in power_controllers:
                power_controllers[power_controller] = [[node], [port]]
            else:
                power_controllers[power_controller][0].append(node)
                power_controllers[power_controller][1].append(port)
    return power_controllers


def get_last_request_time(nodes):
    ret_val = {}
    for node in nodes:
        ret_val[node] = None

    power_controllers = _get_power_controller_dict(nodes)
    for power_controller, (pnodes, _ports) in power_controllers.items():
        for node in pnodes:
            last_request = power_controller.get_last_request_time(node)
            ret_val[node] = last_request
    return ret_val


def rpower_on(nodes):
    'Turn device on'
    ret_val = {}
    power_controllers = _get_power_controller_dict(nodes)
    for power_controller, (pnodes, ports) in power_controllers.items():
        ret = power_controller.on(ports)
        for node in pnodes:
            ret_val[node] = ret[pnodes.index(node)]
            power_controller.set_last_request_time(node)
    return ret_val


def rpower_off(nodes):
    'Turn device off'
    ret_val = {}
    power_controllers = _get_power_controller_dict(nodes)
    for power_controller, (pnodes, ports) in power_controllers.items():
        ret = power_controller.off(ports)
        for node in pnodes:
            ret_val[node] = ret[pnodes.index(node)]
            power_controller.set_last_request_time(node)
    return ret_val


def rpower_cycle(nodes, timeout=5):
    'Power cycle the device'
    state_off = rpower_off(nodes)
    if any(state_off[node][0] for node in state_off):
        return state_off
    time.sleep(int(timeout))
    return rpower_on(nodes)


# True means that device is off so TODO: Rewrite rpower lib
def rpower_status(nodes):
    'Get device power status'
    ret_val = {}
    power_controllers = _get_power_controller_dict(nodes)
    for power_controller, (pnodes, ports) in power_controllers.items():
        ret = power_controller.status(ports)
        for node in pnodes:
            ret_val[node] = ret[pnodes.index(node)]
    return ret_val


def rpower_get_all_devices():
    return _rpower_all_device_names


def rpower_get_client_devices():
    return _rpower_client_names


def rpower_get_nodes_devices():
    return _rpower_node_names


def get_config_item(container, key):
    if key not in container:
        raise OpenSyncException('Power control unit not properly configured',
                                f'Check rpower {key} settings in locations config')

    return container[key]


def get_node_names(config):
    node_names = [node.get('name') for node in config.get('Nodes', [])]
    return node_names


def get_client_names(config):
    node_names = [node.get('name') for node in config.get('Clients', [])]
    return node_names


def rpower_init_config(config):
    global _rpower_config
    global _rpower_all_device_names
    global _rpower_node_names
    global _rpower_client_names
    global _rpower_aliases
    _rpower_config = config

    if RPOWER_DEVICE not in _rpower_config:
        return

    _rpower_all_device_names = []

    for rpower in _rpower_config[RPOWER_DEVICE]:
        for node in rpower[RPOWER_DEVICE_ALIAS]:
            try:
                _rpower_all_device_names.append(node[RPOWER_DEVICE_ALIAS_NAME])
            except KeyError:
                raise Exception('Rpower name not set in location config (rpower section)')

    node_names = get_node_names(config)
    _rpower_node_names = [node for node in node_names if node in _rpower_all_device_names]
    client_names = get_client_names(config)
    _rpower_client_names = [client for client in client_names if client in _rpower_all_device_names]

    _rpower_aliases = {}
    for device in _rpower_config[RPOWER_DEVICE]:
        power_controller = None
        ipaddr = get_config_item(device, RPOWER_DEVICE_IPDADDR)
        port = get_config_item(device, RPOWER_DEVICE_PORT)
        user = get_config_item(device, RPOWER_DEVICE_USER)
        passwd = get_config_item(device, RPOWER_DEVICE_PASS)
        dtype = get_config_item(device, RPOWER_DEVICE_TYPE)

        if dtype == RPOWER_DEVICE_ENERGENIE:
            power_controller = EnergeniePowerController(ipaddr, port, user, passwd)
        elif dtype == RPOWER_DEVICE_WPS:
            power_controller = WPSPowerController(ipaddr, port, user, passwd)
        elif dtype == RPOWER_DEVICE_FAKE:
            power_controller = FakePowerController(ipaddr, port, user, passwd)

        for alias in get_config_item(device, RPOWER_DEVICE_ALIAS):
            name = get_config_item(alias, RPOWER_DEVICE_ALIAS_NAME)
            port = int(get_config_item(alias, RPOWER_DEVICE_ALIAS_PORT))

            _rpower_aliases[name] = (power_controller, port)
