import time
import re
import pexpect

from lib_testbed.util.logger import log
from lib_testbed.util.switch import switchlib


class TP_Link(switchlib.Switch):
    def __init__(self, switch_name, user, password, ip, testCase, port=23, set_echo=False):
        # Initialize parent class switchlib.Switch with all default parameters
        super(TP_Link, self).__init__()
        self.switch_prompt = switch_name
        self.user = user
        self.password = password
        self.ip = ip
        self.port = port
        self.ts = testCase
        self.set_echo = set_echo
        self.spawn_cmd = f'telnet {self.ip} {self.port}'
        if self.ts:
            self.login()

    def login(self):
        self._spawn(self.spawn_cmd)
        self.setecho(self.set_echo)
        # self.logfile = sys.stdout
        try:
            self.expect('User:')
        except pexpect.exceptions.EOF as e:
            log.error(f'Probably there is no connection with the switch: {self.ip}:{self.port}. Exiting...')
            raise e
        self.sendline(self.user + '\r')
        self.expect('Password:')
        self.sendline(self.password + '\r')
        self.send_command("")
        if self.after == pexpect.TIMEOUT:
            raise pexpect.TIMEOUT('Can not login to switch. Check switch configuration')

    def no_shutdown_interface(self, ports):
        return self.action_interface(ports, "no shutdown")

    def shutdown_interface(self, ports):
        return self.action_interface(ports, "shutdown")

    def action_interface(self, ports, action):
        if not self.ts:
            self.login()

        self.enable_admin_mode()
        self.send_command("configure")
        retval = []
        for port in ports:
            out = self.send_command("interface gigabitEthernet 1/0/{}".format(port))

            if "Invalid port number".lower() in out.lower():
                retval.append([1, "", "Invalid port number {}".format(port)])
                continue

            # (no)shutdown
            self.send_command(action)
            time.sleep(1)
            self.send_command("")
            out = self.send_command("show interface configuration")

            port_num = "Gi1/0/{}".format(port)
            for line in out.splitlines():
                if port_num not in line:
                    continue
                parts = line.split()
                state = parts[1].strip()
                speed = parts[2].strip()
                if 'shutdown' in action:
                    if (action == 'no shutdown' and state == 'Enable')\
                            or (action == 'shutdown' and state == 'Disable'):
                        retval.append([0, "{} {}".format(port_num, state), ""])
                    else:
                        retval.append([1, "", f"Could not change status to {action.upper()} on port {port}"])
                elif 'speed' in action:
                    exp_speed = action.split()[1].strip()
                    if exp_speed.upper() == "AUTO":
                        exp_speed = "Auto"
                    else:
                        exp_speed = "{}M".format(exp_speed)
                    if exp_speed == speed:
                        retval.append([0, "{} {}".format(port_num, speed), ""])
                    else:
                        retval.append([1, "", "Could not change speed to {exp_speed} on port {port}"])
                else:
                    retval.append([0, "", ""])
                break

            self.send_command("exit")
        self.send_command("exit")
        self.send_command("exit")
        if not self.ts:
            self.logout()

        return retval

    def interface_status(self, ports):
        if not self.ts:
            self.login()

        self.enable_admin_mode()
        cmd = 'show interface configuration'
        out = self.send_command(cmd)

        status = {}
        for line in out.splitlines():
            m = re.search(r'([0-9A-Za-z]{3}/[0-9]{1}/([0-9]+) \s* [A-Za-z]{6,7}).*',
                          line.strip())
            if m:
                port_status = m.group(1)
                port_num = m.group(2)
                status[port_num] = port_status

        retval = []
        for port in ports:
            if str(port) in status:
                retval.append([0, status[str(port)], ""])
            else:
                retval.append([1, "", "Could not find status for port {}".format(port)])

        if not self.ts:
            self.logout()

        return retval

    def interface_status_parser(self, status):
        '''
        Parse return value of interface_status() method.
        @param status: Return value of interface_status() method. Example: [0, 'Gi1/0/12  Enable', '']
        @return: {
            'name': <Interface name (Gi1/0/12)>,
            'id': <Interface id (1/0/12)>,
            'type': <Interface type (gigabitEthernet)>,
            'state': <Interface state (Enable)>
        }
        '''
        parsed = {'name': None, 'id': None, 'type': None, 'state': None}
        if status[0] == 0:
            stat = status[1].split()
            parsed['name'] = stat[0]
            if stat[0].startswith('Gi'):
                parsed['type'] = 'gigabitEthernet'
                parsed['id'] = stat[0][2:]
            parsed['state'] = stat[1]
        return parsed

    def interface_info(self, ports):
        if not self.ts:
            self.login()

        self.enable_admin_mode()
        retval = []
        for port in ports:
            cmd = "show interface switchport gigabitEthernet 1/0/{}".format(port)
            out = self.send_command(cmd)

            if "Invalid port number".lower() in out.lower():
                retval.append([1, "", "Invalid port number {}".format(port)])
                continue

            port_status = ""
            for line in out.splitlines():
                if line.strip().startswith("PVID:"):
                    pvid = line.strip().split(' ')[1]
                    port_status += "{:^8} {:^6}".format("PVID", pvid)

                m = re.search(r'([0-9]{1,4}) \s* ([0-9A-Za-z].*) \s* ([A-Za-z].*)', line.strip())
                if m:
                    vlan = m.group(1)
                    description = m.group(2)
                    type = m.group(3)
                    port_status += "\n{:^8} {:^6} {:^8}".format(type, vlan, description)

            if len(port_status) == 0:
                retval.append([1, "", "Problems to find info for port {}".format(port)])
            else:
                retval.append([0, port_status, ""])

        self.send_command("exit")
        if not self.ts:
            self.logout()

        return retval

    def show_ports_isolation_interface(self, ports):
        if not self.ts:
            self.login()

        self.enable_admin_mode()
        retval = []
        for port in ports:
            cmd = f'show port isolation interface gigabitEthernet 1/0/{port}'
            out = self.send_command(cmd)

            if "Invalid port number".lower() in out.lower():
                retval.append([1, "", "Invalid port number {}".format(port)])
                continue

            ports_isolation = None
            for line in out.splitlines():
                line = line.strip()
                if line.startswith("Gi"):
                    line = line.split()
                    ports_isolation = line[2].lstrip('Gi')
                    break

            if not ports_isolation:
                retval.append([1, "", "Problems to find port isolation info for port {}".format(port)])
            else:
                retval.append([0, ports_isolation, ""])

        if not self.ts:
            self.logout()

        return retval

    def interface_info_parser(self, info):
        '''
        Parse return value of interface_info() method.
        @param info: Return value of interface_info() method.
                     Example: [0, '  PVID    712  \n Tagged    4    Management        \nUntagged  712   BlackHole_712     ', '']
        @return: {
            'pvid': <PVID (712)>,
            'tagged': [<Tagged VLAN ID (4)>],
            'untagged': [<Untagged VLAN ID (712)>],
            'vlans': {<VLAN ID (4)>: <VLAN name (Management)>}
        }
        '''
        parsed = {'pvid': None, 'tagged': [], 'untagged': [], 'vlans': {}}
        if info[0] == 0:
            for line in info[1].splitlines():
                s = line.split()
                if 'PVID' in line:
                    parsed['pvid'] = s[1]
                elif 'Tagged' in line:
                    parsed['tagged'].append(s[1])
                    parsed['vlans'][s[1]] = s[2]
                elif 'Untagged' in line:
                    parsed['untagged'].append(s[1])
                    parsed['vlans'][s[1]] = s[2]
        return parsed

    def list_vlan(self):
        if not self.ts:
            self.login()

        self.enable_admin_mode()
        self.send_command("configure")
        out = self.send_command("show vlan brief")
        table = out.lstrip('show vlan brief').strip()

        if not self.ts:
            self.logout()

        return table

    def list_vlan_parser(self, list_vlan):
        '''
        Parse return value of list_vlan() method.
        @param list_vlan: Return value of list_vlan() method.
                          Example: 'VLAN  Name                 Status    Ports\r\n
                                    ----- -------------------- --------- ----------------------------------------\r\n
                                    1     System-VLAN          active    Gi1/0/46, Gi1/0/47, Gi1/0/48, Gi1/0/49,\r\n'
        @return: {
            <VLAN ID (1)>: {
                'name': <VLAN name (System-VLAN)>,
                'status': <VLAN status (active)>,
                'ports': [<Interface name (Gi1/0/46)>]
            }
        }
        '''
        parsed = {}
        vlan = None
        for line in list_vlan.splitlines():
            s = [x.rstrip(",") for x in line.strip().split()]
            if len(s) == 0:
                continue
            if s[0] == "VLAN" or all(x == "-" for x in s[0]):
                continue
            if s[0].isdigit():
                vlan = s[0]
                parsed[vlan] = {}
                parsed[vlan]['name'] = s[1]
                parsed[vlan]['status'] = s[2]
                parsed[vlan]['ports'] = s[3:]
            else:
                parsed[vlan]['ports'].extend(s)
        return parsed

    def list_pvid(self):
        if not self.ts:
            self.login()

        self.enable_admin_mode()
        self.send_command("configure")
        out = self.send_command("show interface switchport")
        table = out.lstrip('show interface switchport').strip()

        if not self.ts:
            self.logout()

        return table

    def list_pvid_parser(self, list_pvid):
        '''
        Parse return value of list_pvid() method.
        @param list_pvid: Return value of list_pvid() method.
                          Example: 'Port      LAG       Type          PVID      Acceptable frame type  Ingress Checking
                                    -------   ---       ----          ----      ---------------------  ----------------
                                    Gi1/0/1   N/A       General       200       All                    Enable\r\n '
        @return: {
            <Interface name (Gi1/0/1)>: {
                'lag': <LAG participation (N/A)>,
                'type': <PVID type (General)>,
                'pvid': <PVID ID (200)>,
                'acceptable_frame_type': <Acceptable frame type (All)>,
                'ingress_checking': <Ingress Checking (Enable)>
            }
        }
        '''
        parsed = {}
        for line in list_pvid.splitlines():
            s = line.strip().split()
            if len(s) == 0:
                continue
            if s[0] == "Port" or all(x == "-" for x in s[0]):
                continue
            parsed[s[0]] = {'lag': s[1], 'type': s[2], 'pvid': s[3], 'acceptable_frame_type': s[4],
                            'ingress_checking': s[5]}
        return parsed

    def delete_vlan(self, ports, vlan):
        if not self.ts:
            self.login()

        self.enable_admin_mode()
        self.send_command("configure")
        retval = []
        for port in ports:
            out = self.send_command("show interface switchport gigabitEthernet 1/0/{}".format(port))

            if "Invalid port number".lower() in out.lower():
                retval.append([1, "", "Invalid port number {}".format(port)])
                continue

            vlans = self._get_vlan_status(out)
            if vlan in vlans:
                self.send_command("interface gigabitEthernet 1/0/{}".format(port))
                out = self.send_command("no switchport general allowed vlan {}".format(vlan))

                vlans2 = self._get_vlan_status(out)
                if vlan not in vlans2:
                    retval.append([0, "Gi1/0/{}  VLAN {}  Deleted".format(port, vlan), ""])
                else:
                    retval.append([1, "", "Problems to delete VLAN {} - "
                                          "Gi1/0/{} ".format(vlan, port)])

            else:
                retval.append([1, "", "Problems to find vlan {} for interface "
                                      "gigabitEthernet 1/0/{}".format(vlan, port)])
        if not self.ts:
            self.logout()

        return retval

    def set_vlan(self, ports, vlan, vlan_type):
        if vlan_type not in ["tagged", "untagged"]:
            return [[1, "", "Vlan type '{}' is not allowed. You can use: "
                            "['tagged', 'untagged']".format(vlan_type)]]

        if not self.ts:
            self.login()
        self.enable_admin_mode()
        self.send_command("configure")

        if not self._check_vlan_exists(vlan):
            return [[1, "", "Vlan '{}' does not exist. Please create it manually "
                            "on the switch ({})!".format(vlan, self.ip)]]

        retval = []
        for port in ports:
            out = self.send_command("interface gigabitEthernet 1/0/{}".format(port))

            if "Invalid port number".lower() in out.lower():
                retval.append([1, "", "Invalid port number {}".format(port)])
                continue

            if vlan_type == "untagged":
                # set PVID
                self.send_command("switchport pvid {}".format(vlan))

            # set VLAN
            self.send_command("switchport general allowed vlan {} {}".format(vlan, vlan_type))
            out = self.send_command("show interface switchport gigabitEthernet 1/0/{}".format(port))
            found = False
            for line in out.splitlines():
                m = re.search(r'([0-9]{1,4}) \s* [0-9A-Za-z].* \s* ([A-Za-z].*)', line.strip())
                if m:
                    vl = m.group(1)
                    v_type = m.group(2)

                    if str(vl) == str(vlan) and str(vlan_type) == str(v_type).lower():
                        retval.append([0, "Gi1/0/{} - VLAN '{}' Type: '{}' Set Successfully"
                                      .format(port, vlan, vlan_type), ""])
                        found = True
                        # delete olf VLAN
                        self._delete_existing_vlan(port, whitelist=[vlan])
                        break

            if not found:
                retval.append([1, "", "Gi1/0/{} - VLAN '{}' Type: '{}' "
                                      "NOT Set".format(port, vlan, vlan_type)])
        if not self.ts:
            self.logout()

        return retval

    def logout(self):
        self.pid = None
        while self.after and isinstance(self.after, bytes) and not self.after.endswith(b'>'):
            self.send_command('exit')
        self.sendline('logout')
        self.close()

    def send_command(self, cmd, prompt=None, timeout=10):
        if not cmd.endswith("\r"):
            cmd += "\r"
        if prompt is None:
            prompt = "{}.*".format(self.switch_prompt)
        more = "Press any key to continue.*"
        pattern_list = [more, pexpect.TIMEOUT, pexpect.EOF, prompt]
        self.sendline(cmd)
        out = ""
        stop_time = time.time() + timeout
        while time.time() < stop_time:
            time.sleep(0.2)
            index = self.expect(pattern=pattern_list, timeout=timeout)
            out += str(self.before, 'utf-8')
            if index == 0:
                # Not finished yet, send space to continue
                self.sendline(" ")
            else:
                break
        return out

    def enable_admin_mode(self):
        self.buffer = bytes(0)
        if 'EOF' in str(self.after):
            self.logout()
            self.login()
        while self.after and isinstance(self.after, bytes) and not self.after.endswith(b'>'):
            self.send_command('exit')

        while self.after and isinstance(self.after, bytes) and not self.after.endswith(b'#'):
            self.send_command('enable')

    def _get_vlan_status(self, before):
        vlans = {}
        for line in before.splitlines():
            m = re.search(r'([0-9]{1,4}) \s* [0-9A-Za-z].* \s* ([A-Za-z].*)', line.strip())
            if m:
                vl = m.group(1)
                type = m.group(2)
                vlans[vl] = type
        return vlans

    def _check_vlan_exists(self, vlan):
        out = self.send_command("show vlan brief")
        vlans = []
        for line in out.splitlines():
            m = re.search(r'([0-9]{1,4}) \s* [0-9A-Za-z].* \s* [A-Za-z].*', line.strip())
            if m:
                vlans.append(m.group(1))
        return (vlan in vlans)

    def _delete_existing_vlan(self, port, whitelist=()):
        out = self.send_command("show interface switchport gigabitEthernet 1/0/{}".format(port))
        vlans = {}
        for line in out.splitlines():
            m = re.search(r'([0-9]{1,4}) \s* [0-9A-Za-z].* \s* ([A-Za-z].*)', line.strip())
            if m:
                vlan = m.group(1)
                vlan_type = m.group(2)
                vlans[vlan] = vlan_type

        for vl_key in vlans:
            if "untagged" == vlans[vl_key].lower() and vl_key not in whitelist:
                self.send_command("no switchport general allowed vlan {}".format(vl_key))
