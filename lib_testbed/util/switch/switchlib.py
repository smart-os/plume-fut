import time
from pexpect import spawn
from lib_testbed.util.opensyncexception import OpenSyncException

SWITCH_KEY = 'Switch'
SWITCH_KEY_IPDADDR = 'ipaddr'
SWITCH_KEY_HOSTNAME = 'hostname'
SWITCH_KEY_PORT = 'port'
SWITCH_KEY_USER = 'user'
SWITCH_KEY_PASS = 'pass'
SWITCH_KEY_TYPE = 'type'
SWITCH_KEY_NAME = 'name'
SWITCH_KEY_ALIAS = 'alias'
SWITCH_KEY_ALIAS_NAME = 'name'
SWITCH_KEY_ALIAS_PORT = 'port'
SWITCH_KEY_ALIAS_BLACKHAUL = 'backhaul'
TBCFG_NODES = 'Nodes'

SWITCH_DEVICE_TP_LINK = 'tplink'
SWITCH_DEVICE_HP = 'hp'

_rpower_aliases = {}
__rpower_config = None
_rpower_all_device_names = []


# Telnet Switch
class Switch(spawn):
    def __init__(self, command=None, timeout=30, maxread=2000, searchwindowsize=None,
                 logfile=None, cwd=None, env=None):
        spawn.__init__(self, command, timeout=timeout, maxread=maxread,
                       searchwindowsize=searchwindowsize, logfile=logfile,
                       cwd=cwd, env=env)

    def login(self):
        raise NotImplementedError()

    def shutdown_interface(self, ports):
        raise NotImplementedError()

    def no_shutdown_interface(self, ports):
        raise NotImplementedError()

    def interface_status(self, ports):
        raise NotImplementedError()

    def interface_status_parser(self, status):
        raise NotImplementedError()

    def interface_info(self, ports):
        raise NotImplementedError()

    def interface_info_parser(self, info):
        raise NotImplementedError()

    def delete_vlan(self, ports, vlan):
        raise NotImplementedError()

    def set_vlan(self, ports, vlan, type):
        raise NotImplementedError()

    def list_vlan(self):
        raise NotImplementedError()

    def list_vlan_parser(self, list_vlan):
        raise NotImplementedError()

    def list_pvid(self):
        raise NotImplementedError()

    def list_pvid_parser(self, list_pvid):
        raise NotImplementedError()

    def logout(self):
        raise NotImplementedError()


def _get_switch_controller_dict(nodes):
    switch_controllers = {}
    for node in nodes:
        if node in _switch_aliases:
            switch_controller, port = _switch_aliases[node]
            if switch_controller not in switch_controllers:
                switch_controllers[switch_controller] = [[node], [port]]
            else:
                switch_controllers[switch_controller][0].append(node)
                switch_controllers[switch_controller][1].append(port)
    return switch_controllers


def switch_list():
    'List all ports configured'
    retval = []
    for node_port in _switch_node_ports:
        retval.append(node_port)
    return (0, retval, None)


def interface_up(nodes):
    'Turn interface up'
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.no_shutdown_interface(ports)
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def interface_down(nodes):
    'Turn interface down'
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.shutdown_interface(ports)
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def switch_status(nodes):
    'Get interface status (enable/disable)'
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.interface_status(ports)
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def get_forward_port_isolation(nodes):
    'Get forward ports isolation of interface'
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.show_ports_isolation_interface(ports)
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def set_forward_port_isolation(nodes, forward_ports):
    'Set port isolation'
    action = f'port isolation gi-forward-list {forward_ports}'
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.action_interface(ports, action)
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def disable_port_isolation(nodes):
    'Disable port isolation'
    action = 'no port isolation'
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.action_interface(ports, action)
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def set_link_speed(nodes, speed):
    'Change link speed'
    cmd = 'speed {}'.format(speed)
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.action_interface(ports, cmd)
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def switch_status_parser(status):
    '''
    Parse return value of switch_status() method.
    @param status: Return value of switch_status() method.
                 Example: {'pod1_eth0': [0, 'Gi1/0/1   Enable', '']}
    @return: {
        <Port alias (pod1_eth0)>: {
            'name': <Interface name (Gi1/0/1)>,
            'id': <Interface id (1/0/1)>,
            'type': <Interface type (gigabitEthernet)>,
            'state': <Interface state (Enable)>
        }
    }
    '''
    parsed = {}
    for port_alias in status:
        switch_controller = _switch_aliases[port_alias][0]
        parsed[port_alias] = switch_controller.interface_status_parser(status[port_alias])
    return parsed


def switch_info(nodes):
    'Get info about VLANs'
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.interface_info(ports)
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def switch_info_parser(info):
    '''
    Parse return value of switch_info() method.
    @param info: Return value of switch_info() method.
                 Example: {'pod1_eth0': [0, '  PVID    200  \n Tagged    4    Management        \nUntagged  200   FakeInternet      ', '']}
    @return: {
        <Port alias (pod1_eth0)>: {
            'pvid': <PVID (200)>,
            'tagged': [<Tagged VLAN ID (4)>],
            'untagged': [<Untagged VLAN ID (200)>],
            'vlans': {<VLAN ID (4)>: <VLAN name (Management)>}
        }
    }
    '''
    parsed = {}
    for port_alias in info:
        switch_controller = _switch_aliases[port_alias][0]
        parsed[port_alias] = switch_controller.interface_info_parser(info[port_alias])
    return parsed


def vlan_remove(nodes, vlan):
    'Delete VLAN for specific interface'
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.delete_vlan(ports, str(vlan))
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def vlan_set(nodes, vlan, vlan_type):
    'Set vlan untagged/tagged'
    ret_val = {}
    switch_controllers = _get_switch_controller_dict(nodes)
    for switch_controller, (snodes, ports) in switch_controllers.items():
        ret = switch_controller.set_vlan(ports, str(vlan), vlan_type)
        ret = validate_response(ret, snodes)
        for node in snodes:
            ret_val[node] = ret[snodes.index(node)]
    return ret_val


def set_connection_ip_type(nodes, ip_type):
    """Set connection ip type. Supported IP types <ipv4, ipv6_stateful, ipv6_stateless, ipv6_slaac>"""
    vlan_map = {
        'ipv4': 200,
        'ipv6_stateful': 201,
        'ipv6_stateless': 202,
        'ipv6_slaac': 203
    }
    target_vlan = vlan_map.get(ip_type)
    if not target_vlan:
        raise Exception(f'Incorrect ip type: {ip_type} option. Supported ip types options: {list(vlan_map.keys())}')
    interface_down(nodes)
    vlan_set(nodes, target_vlan, 'untagged')
    time.sleep(10)
    interface_up(nodes)
    ports_info = switch_info(nodes)
    for eth_port in nodes:
        eth_port_info = ports_info[eth_port]
        if str(target_vlan) not in eth_port_info[1]:
            eth_port_info[2] = f'Change vlan to {target_vlan} vlan for {ip_type} ip type finished unsuccessfully'
            eth_port_info[0] = 1
    return ports_info


def vlan_list():
    'List of all VLANs on the switch(es)'
    switches = []
    for node in _switch_aliases:
        if _switch_aliases[node][0] not in switches:
            switches.append(_switch_aliases[node][0])

    tables = ""
    for switch_controller in switches:
        tables += "ip: {}\n--------------\n".format(switch_controller.ip)
        tables += switch_controller.list_vlan() + "\n\n"
    tables += "-" * 70

    return (0, tables, "")


def vlan_parser(vlan_list):
    '''
    Parse return value of vlan_list() method.
    @param vlan_list: Return value of vlan_list() method.
                      Example: (0,
                                'ip: 192.168.178.201\n
                                 --------------\n
                                 VLAN  Name                 Status    Ports\r\n
                                 ----- -------------------- --------- ----------------------------------------\r\n
                                 1     System-VLAN          active    Gi1/0/46, Gi1/0/47, Gi1/0/48, Gi1/0/49,\r\n
                                                                      Gi1/0/50, Gi1/0/51, Gi1/0/52\r\n',
                                '')
    @return: {
        <Port alias (pod3_eth0)>: {
            <VLAN ID (1)>: {
                'name': <VLAN name (System-VLAN)>,
                'status': <VLAN status (active)>,
                'ports': [<Interface name (Gi1/0/46)>]
            }
        }
    }
    '''
    parsed = {}
    if vlan_list[0] == 0:
        tables = vlan_list[1].split("-" * 70)
        for table in tables:
            switch_controller = None
            header = None
            port_alias = None
            for node in _switch_aliases:
                text = "ip: {}\n--------------\n".format(_switch_aliases[node][0].ip)
                if table.startswith(text):
                    switch_controller = _switch_aliases[node][0]
                    header = text
                    port_alias = node
                    break
            if header is not None:
                vlan_parsed = switch_controller.list_vlan_parser(table[len(header):])
                parsed[port_alias] = vlan_parsed
    return parsed


def pvid_list():
    'List of all PVIDs on the switch(es)'
    switches = []
    for node in _switch_aliases:
        if _switch_aliases[node][0] not in switches:
            switches.append(_switch_aliases[node][0])

    tables = ""
    for switch_controller in switches:
        tables += "ip: {}\n--------------\n".format(switch_controller.ip)
        tables += switch_controller.list_pvid() + "\n\n"
    tables += "-" * 40

    return (0, tables, "")


def pvid_parser(pvid_list):
    '''
    Parse return value of pvid_list() method.
    @param pvid_list: Return value of pvid_list() method.
                      Example: (0,
                                'ip: 192.168.178.201\n
                                 --------------\n
                                 Port      LAG       Type          PVID      Acceptable frame type  Ingress Checking\r\n
                                 -------   ---       ----          ----      ---------------------  ----------------\r\n
                                 Gi1/0/1   N/A       General       200       All                    Enable\r\n ',
                                '')
    @return: {
        <Port alias (pod3_eth0)>: {
            <Interface name (Gi1/0/1)>: {
                'lag': <LAG participation (N/A)>,
                'type': <PVID type (General)>,
                'pvid': <PVID ID (200)>,
                'acceptable_frame_type': <Acceptable frame type (All)>,
                'ingress_checking': <Ingress Checking (Enable)>
            }
        }
    }
    '''
    parsed = {}
    if pvid_list[0] == 0:
        tables = pvid_list[1].split("-" * 40)
        for table in tables:
            switch_controller = None
            header = None
            port_alias = None
            for node in _switch_aliases:
                text = "ip: {}\n--------------\n".format(_switch_aliases[node][0].ip)
                if table.startswith(text):
                    switch_controller = _switch_aliases[node][0]
                    header = text
                    port_alias = node
                    break
            if header is not None:
                pvid_parsed = switch_controller.list_pvid_parser(table[len(header):])
                parsed[port_alias] = pvid_parsed
    return parsed


def connect_eth_client_tool(pod_name, client_name, connect_port=''):
    """Connect ethernet client <pod_name>=str, <client_name>=str, <connect_port>=str if empty get random port"""
    # TODO: Rewrite this library to class, and merge these two libs into one class. So we'll avoid importing SwitchApi
    from lib_testbed.switch.switchlib import SwitchLib as SwitchApi
    switch_api = SwitchApi(config=_switch_config)
    target_port = switch_api.connect_eth_client(pod_name[0], client_name, connect_port)
    return switch_info([target_port])


def disconnect_eth_client(pod_name, client_name):
    """
    Disconnect ethernet client <pod_name>=str, <client_name>=str
    """
    # TODO: Rewrite this library to class, and merge these two libs into one class. So we'll avoid importing SwitchApi
    from lib_testbed.switch.switchlib import SwitchLib as SwitchApi
    switch_api = SwitchApi(config=_switch_config)
    target_port = switch_api.disconnect_eth_client(pod_name[0], client_name)
    return switch_info([target_port])


def recovery_switch_cfg(pod_name):
    """
    Set default configuration on switch for the provided <pod_names>=str.
    """
    # TODO: Rewrite this library to class, and merge these two libs into one class. So we'll avoid importing SwitchApi
    from lib_testbed.switch.switchlib import SwitchLib as SwitchApi
    switch_api = SwitchApi(config=_switch_config)
    target_ports = switch_api.recovery_switch_configuration(pod_name)
    return switch_info(target_ports)


def switch_get_node_ports():
    return _switch_node_ports


def get_config_item(container, key):
    if key not in container:
        raise OpenSyncException(f'Switch unit not properly configured',
                                f'Check switch "{key}" settings in locations config')
    return container[key]


def get_ip_address(container):
    ip_address = container.get(SWITCH_KEY_IPDADDR) if container.get(SWITCH_KEY_IPDADDR) \
        else container.get(SWITCH_KEY_HOSTNAME)
    if not ip_address:
        raise OpenSyncException('Switch unit not properly configured',
                                f'Please provide {SWITCH_KEY_IPDADDR} or {SWITCH_KEY_HOSTNAME} to switch config')
    return ip_address


def logout():
    switches = []
    for node in _switch_aliases:
        if _switch_aliases[node][0] not in switches:
            switches.append(_switch_aliases[node][0])
    for switch_controller in switches:
        switch_controller.logout()


def validate_response(switch_responses, ports):
    if len(switch_responses) != len(ports):
        for port in ports:
            try:
                switch_responses[ports.index(port)]
            except IndexError:
                switch_responses.insert(ports.index(port), [5, '', f'Did not get any response from switch for {port}'])
    return switch_responses


def switch_init_config(config, testCase=False):
    global _switch_config
    global _switch_node_ports
    global _switch_aliases
    global _switch_information
    _switch_config = config

    if TBCFG_NODES not in _switch_config:
        raise OpenSyncException("Nodes not set in configuration",
                                "Define nodes in locations config file")

    _switch_node_ports = []
    for node in _switch_config[TBCFG_NODES]:
        if 'switch' in node:
            for pod_port in node['switch']:
                _switch_node_ports.append(pod_port)

    _switch_aliases = {}
    _switch_information = {}

    if SWITCH_KEY not in _switch_config:
        return

    for device in _switch_config[SWITCH_KEY]:
        switch_controller = None
        ipaddr = get_ip_address(device)
        port = get_config_item(device, SWITCH_KEY_PORT)
        user = get_config_item(device, SWITCH_KEY_USER)
        passwd = get_config_item(device, SWITCH_KEY_PASS)
        dtype = get_config_item(device, SWITCH_KEY_TYPE)
        name = get_config_item(device, SWITCH_KEY_NAME)

        if dtype == SWITCH_DEVICE_TP_LINK:
            from lib_testbed.util.switch import tplink
            switch_controller = tplink.TP_Link(name, user, passwd, ipaddr, testCase, port)

        for alias in get_config_item(device, SWITCH_KEY_ALIAS):
            name = get_config_item(alias, SWITCH_KEY_ALIAS_NAME)
            port = int(get_config_item(alias, SWITCH_KEY_ALIAS_PORT))
            backhaul = int(get_config_item(alias, SWITCH_KEY_ALIAS_BLACKHAUL))

            _switch_aliases[name] = (switch_controller, port)
            _switch_information[name] = (name, port, backhaul)
