import os
import json
import re
import yaml
from lib_testbed.util.common import DeviceCommon
from lib_testbed.util.opensyncexception import OpenSyncException
from lib_testbed.util.logger import log


CONFIG_DIR = 'config'
LOCATIONS_DIR = 'locations'
DEPLOYMENTS_DIR = 'deployments'
MISCS_DIR = 'miscs'
ENV_OPENSYNC_TESTBED = 'OPENSYNC_TESTBED'
TBCFG_PROFILE = 'profile'
TBCFG_NODES = 'Nodes'


def load_file(file_name):
    extension = os.path.splitext(file_name)[1]
    with open(file_name, 'r') as stream:
        if extension == ".yaml":
            base_config_name = 'generic_global'
            ret_dict = yaml.load(stream, YamlLoader)
            if base_config_name in ret_dict:
                base_dict = ret_dict[base_config_name]
                base_dict = dict_deep_update(base_dict, ret_dict)
                base_dict.pop(base_config_name)
                ret_dict = base_dict
        elif extension == ".json":
            base_config_name = 'base_config'
            ret_dict = json.loads(stream.read())
            if base_config_name in ret_dict:
                base_config = ret_dict[base_config_name]
                base_config_path = os.path.join(os.path.dirname(file_name), base_config)
                if not base_config_path.endswith('.json'):
                    base_config_path += '.json'
                new_ret_dict = load_file(base_config_path)
                new_ret_dict.update(ret_dict)
                ret_dict = new_ret_dict.copy()
        else:
            raise OpenSyncException(f'Invalid file extension: {file_name}', 'Please use json files!')
        return ret_dict


def merge(source, destination):
    for key, value in source.items():
        if isinstance(value, dict):
            node = destination.setdefault(key, {})
            merge(value, node)
        else:
            destination[key] = value
    return destination


def find_base_dir():
    return os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))


def find_file(dir, file_name):
    list_dir = os.listdir(dir)
    if not file_name.endswith(".yaml"):
        file_name += ".yaml"
    for file in list_dir:
        if re.compile(file_name).match(file):
            file = dir + '/' + file
            return file
    raise OpenSyncException('Cannot find config file {0}'.format(file_name),
                            f'Expecting config file: {file_name} in {dir}')


def get_config_dir():
    return os.path.join(find_base_dir(), CONFIG_DIR)


def find_location_file(name):
    return find_file(os.path.join(get_config_dir(), LOCATIONS_DIR), name)


def find_deployment_file(name):
    return find_file(os.path.join(get_config_dir(), DEPLOYMENTS_DIR), name)


def find_default_deployment_file(config):
    if os.path.basename(config.get('location_file')).startswith('test-tb'):
        # For unit test use example deployment
        return find_file(os.path.join(get_config_dir(), DEPLOYMENTS_DIR), 'example')
    names = ['dogfood', '(?!example).*']  # 'opensync',
    for name in names:
        try:
            dpl_file = find_file(os.path.join(get_config_dir(), DEPLOYMENTS_DIR), name)
            if 'dogfood' not in os.path.basename(dpl_file):
                log.info(f"Default config: {dpl_file}")
            break
        except OpenSyncException:
            continue
    else:
        raise OpenSyncException('Cannot find default config file', 'Check if config file is not missing')
    return dpl_file


def get_deployment(config, default_deployment=None):
    """
    Get current deployment for a location
    Args:
        config: (dict) location config
        default_deployment: (str) default deployment for a location (in case cannot be discovered from inventory)

    Returns: (str) deployment name

    """
    deployment_name = config.get(TBCFG_PROFILE)
    if not deployment_name:
        raise Exception('Missing profile setting in config file')
    if deployment_name == 'auto':
        try:
            from lib.cloud.api import inventory
        except ModuleNotFoundError:
            raise OpenSyncException("Inventory API does not exist",
                                    "Change auto profile in location config to deployment name")
        conf = load_file(find_default_deployment_file(config))
        inv = inventory.Inventory.fromurl(conf['inventory_url'], conf['inv_user'], conf['inv_pwd'])
        serial = config['Nodes'][0]["id"]
        deployment_name = inv.get_node(serial)["deployment"]
        if not deployment_name:
            if default_deployment:
                deployment_name = default_deployment
            else:
                raise OpenSyncException("Deployment name is set to auto and cannot be discovered from Inventory")
        if deployment_name == 'dog1':
            deployment_name = 'dogfood'
    return deployment_name


def find_misc_file(name):
    try:
        return find_file(os.path.join(get_config_dir(), MISCS_DIR), name)
    except FileNotFoundError:
        return None


def load_tb_config(location_file=None, deployment_file=None, skip_deployment=False):  # noqa:C901
    """
    Load test bed configuration file
    Args:
        location_file: (str) test bed name
        deployment_file: (str) deployment name

    Returns: (dict) loaded configuration

    """
    if location_file and not os.path.isabs(location_file):
        location_file = find_location_file(location_file)
    elif not location_file and not deployment_file:
        location_file = os.environ.get(ENV_OPENSYNC_TESTBED)
        if not location_file:
            raise OpenSyncException(f'{ENV_OPENSYNC_TESTBED} environment variable not set',
                                    'Run pset tool to configure testbed')
        location_file = find_location_file(location_file)

    if location_file:
        config = load_file(location_file)
        if 'include_file' in config:
            location_file = config['include_file']
        for node in config.get(TBCFG_NODES, []):
            # capabilities can be also overwritten in the location config
            capab = get_model_capabilities(node['model'])
            node['capabilities'] = dict_deep_update(capab, node.get('capabilities', {}))
    else:
        config = {}
    if 'include_file' not in config:
        config['location_file'] = location_file
    else:
        config['location_file'] = config['include_file']

    if not skip_deployment:
        if deployment_file and not os.path.isabs(deployment_file) and location_file:
            if config.get(TBCFG_PROFILE) == 'auto':
                loc_deployment = get_deployment(config, default_deployment=deployment_file)
                if loc_deployment != deployment_file:
                    try:
                        from lib.util.cloudtoollib import CloudToolLib
                    except ModuleNotFoundError:
                        raise OpenSyncException("Required CloudToolLib is missing", "Add cloud module")
                    path_loc_deployment = find_deployment_file(loc_deployment)
                    deployment_cfg = load_file(path_loc_deployment)
                    merge_config = merge(config, deployment_cfg)
                    log.info(f'Move location from: {loc_deployment} to: {deployment_file} deployment')
                    kwargs = {"config": merge_config}
                    cloudlib = CloudToolLib(**kwargs)
                    if cloudlib.move_to_deployment(deployment_file):
                        log.info(f'Location has been moved successfully to {deployment_file}')
                        deployment_file = find_deployment_file(deployment_file)
                    else:
                        raise Exception(f'Location has not been moved to {deployment_file}')
                else:
                    deployment_file = find_deployment_file(deployment_file)
            else:
                log.info('Can not move location to another deployment because '
                         'profile field in config is not set to auto')
                deployment_file = None
        elif deployment_file and not os.path.isabs(deployment_file) and not location_file:
            deployment_file = find_deployment_file(deployment_file)

        if not deployment_file:
            deployment_name = get_deployment(config)
            deployment_file = find_deployment_file(deployment_name)

        config['deployment_file'] = deployment_file
        config[TBCFG_PROFILE] = os.path.basename(deployment_file).split(".")[0]

        include = load_file(deployment_file)
        if include:
            config = merge(config, include)

    artifactory_file = find_misc_file('artifactory')
    if artifactory_file:
        include = load_file(artifactory_file)
        if include:
            config = merge(config, include)

    if not config.get("email"):
        config['email'] = config.get("default_email")
        config['password'] = config.get("default_password")
        if 'user_name' not in config:
            config['user_name'] = config.get("default_user_name", '')

    return config


def get_location_name(config):
    return os.path.basename(config.get('location_file', '')).split('.')[0]


def get_deployment_name(config):
    return os.path.basename(config.get('deployment_file', '')).split('.')[0]


def get_model_capabilities(model):
    model_path = DeviceCommon.get_model_path(model, parent_model=None, dev_type='pod')
    cap_file = DeviceCommon.resolve_model_path_file(model_path, 'capabilities.yaml')
    with open(cap_file) as cap:
        capab = yaml.load(cap, YamlLoader)
    # hierarchy: global -> family -> model
    out_capab = capab.pop('generic_global', {})
    dict_deep_update(out_capab, capab.get('generic_family', {}).pop('generic_global', {}))
    dict_deep_update(out_capab, capab.pop('generic_family', {}))
    dict_deep_update(out_capab, capab)
    return out_capab


def dict_deep_update(d, u):
    import collections.abc
    for k, v in u.items():
        # supported channels cannot be merged, due to 5G, 5GL, 5GU differences
        if k == 'supported_channels':
            d[k] = v
            continue
        if isinstance(v, collections.abc.Mapping):
            d[k] = dict_deep_update(d.get(k, {}), v)
        else:
            d[k] = v
    return d


def update_config_with_admin_creds(config):
    if not config:
        return
    dpl = config.get('deployment_id')
    if dpl and dpl in ['beta', 'chi', 'chi-staging', 'delta', 'gamma', 'kappa', 'sigma', 'tau-int', 'tau-prod',
                       'theta']:
        # first option is to search for the /etc/smokecreds/smoke<deployment>.json file
        log.info(f'Using elevated credentials for {dpl}')
        cred_file = f'/etc/smokecreds/smoke{dpl}.json'
        try:
            with open(cred_file) as cred:
                info = json.load(cred)
                config.update(info)
                return
        except IOError:
            pass

        # the other option is to get them directly from Jenkins environments
        admin_user = os.environ.get('CLOUD_SMOKE_USER', None)
        admin_pwd = os.environ.get('CLOUD_SMOKE_PASS', None)
        log.info(f'Admin: {admin_pwd}')
        if admin_user and admin_pwd:
            config['admin_user'] = admin_user
            config['admin_pwd'] = admin_pwd
            return
        log.error(f"Admin credentials not updated!")


class YamlLoader(yaml.SafeLoader):

    def __init__(self, stream):
        self._root = os.path.split(stream.name)[0]
        super(YamlLoader, self).__init__(stream)

    def include(self, node):
        file_path = os.path.join(self._root, self.construct_scalar(node))
        with open(file_path, 'r') as f:
            out = yaml.load(f, YamlLoader)
            if type(out) is dict:
                out['include_file'] = os.path.abspath(file_path)
            return out

    def join(self, node):
        seq = self.construct_sequence(node)
        return ''.join([str(i) for i in seq])


YamlLoader.add_constructor('!include', YamlLoader.include)
YamlLoader.add_constructor('!join', YamlLoader.join)
