#!/usr/bin/env python3
import fnmatch
import os
import re
from lib_testbed.util.sanity import ovsdb
from lib_testbed.util.common import DeviceCommon
from lib_testbed.util.logger import log


class Sanity(object):
    def __init__(self, outfile=None, outstyle='full'):
        self.location = None
        self.outfile = outfile
        self.outstyle = outstyle

    def print_line(self, line):
        print(line)
        if self.outfile:
            print(line, file=self.outfile)

    def run_sanity_on_file(self, filename, gw=None, logs_dir=None):
        sanity = SanityFactory(outfile=self.outfile, outstyle=self.outstyle)
        sanity.load_tables(filename)
        if 'AWLAN_Node table' not in sanity.tables:
            self.print_line('AWLAN_Node table not found in ovsdb dump, skipping')
            return None
        model = DeviceCommon.convert_model_name(sanity.get_device_model(gw))
        obj = sanity.get_sanity_model(model, gw, logs_dir)
        if not obj:
            self.print_line('Unknown device: {}'.format(model))
            return None
        obj.ovsdb_sanity_check()
        return obj

    def sanity_single(self, filename):
        """
            Run sanity given an input file containing dump of OVSDB Tables
        """
        try:
            obj = self.run_sanity_on_file(filename)
            if not obj:
                return 1
            if self.outstyle == 'lib':
                stripped = list()
                for line in obj.output:
                    regex = re.compile("\033\\[[0-9;]+m")
                    line[2] = regex.sub('', line[2])
                    stripped.append(line)
                return stripped
            else:
                obj.sanity_message()
                if 'ERROR' in [line[1] for line in obj.output]:
                    return 1
                return 0
        except Exception as e:
            log.warning(e)
            return 1

    def sanity_location(self, logs_dir):  # noqa C901
        """
            Run sanity in the logs_dir directory
        """
        ret_status = {
            'gw_pod': None,
            'serial': [],
            'ret': True,
            'out': [],
            'wifi_stats': []
        }
        # collect all directory
        listdir = os.listdir(logs_dir)
        logs_list = []
        for item in listdir:
            if os.path.isdir(os.path.join(logs_dir, item)):
                logs_list.append(os.path.join(logs_dir, item))
        if not logs_list:
            # Single directory from one pod
            logs_list = [logs_dir]
        # find out what location is tested and sort logs to start from GW
        root_pod = ''
        stop = False
        for path in logs_list:
            if stop:
                break
            for filename in fnmatch.filter(os.listdir(path), 'ovsdb-client_-f_json_dump*'):
                with open(os.path.join(path, filename)) as json_dump:
                    tables = ovsdb.ovsdb_decode(json_dump)
                    if 'AWLAN_Node table' not in tables:
                        self.print_line('AWLAN_Node table not found in ovsdb dump, skipping')
                        continue
                    gw_node = True
                    eth_uuid = []
                    for iface in tables['Port table']:
                        if iface['name'] in ['eth0', 'eth1', 'eth0.835', 'eth1.835']:
                            eth_uuid.append(iface['_uuid'])
                    if eth_uuid:
                        for bridge in tables['Bridge table']:
                            if bridge['name'] != 'br-wan':
                                continue
                            if type(bridge['ports']) is not list:
                                bridge['ports'] = [bridge['ports']]
                            gw_node = any((x in eth_uuid for x in bridge['ports']))
                    if gw_node:
                        root_pod = tables['AWLAN_Node table'][0]['model']
                        logs_list.insert(0, logs_list.pop(logs_list.index(path)))
                        stop = True
                        break
        if root_pod:
            ret_status['gw_pod'] = root_pod
            if self.outstyle not in ['lib', 'none']:
                self.print_line('GW pod: {}'.format(root_pod))
            gw = {'model': root_pod, 'gw_tables': None}
        else:
            gw = None
        for path in logs_list:
            for filename in fnmatch.filter(os.listdir(path), 'ovsdb-client_-f_json_dump*'):
                file_path = os.path.join(path, filename)
                with open(file_path) as json_dump:
                    obj = self.run_sanity_on_file(json_dump, gw=gw, logs_dir=path)
                    if not obj:
                        continue
                    if gw and not gw['gw_tables']:
                        # print 'Storing GW pod tables for leafs'
                        gw['gw_tables'] = obj.tables
                    if os.path.isfile(obj.logs_dir + 'dmesg'):
                        obj.sys_log_sanity_check()
                        ret_status['wifi_stats'].append(obj.get_wireless_stats())
                    if self.outstyle not in ['lib', 'none']:
                        obj.sanity_message()
                    ret_status['out'].append(obj.output)
                    ret_status['serial'].append(obj._id)
                    if 'ERROR' in [line[1] for line in obj.output]:
                        ret_status['ret'] = False
        return ret_status


class SanityFactory(object):
    sanity_factory = None

    def __init__(self, outfile=None, outstyle='full'):
        self.device_type = None
        self.tables = None
        self.outfile = outfile
        self.outstyle = outstyle

    def load_tables(self, json_dump):
        """
            Parse Ovsdb to to load tables
        """
        self.tables = ovsdb.ovsdb_decode(json_dump)

    def get_device_model(self, gw):
        """
            Parse Ovsdb to determine mode and device type
        """
        model = self.tables['AWLAN_Node table'][0]['model']
        gw_model = gw['model'] if gw else None
        model = DeviceCommon.convert_model_name(model)
        model_path = DeviceCommon.get_model_path(model, gw_model)
        sanity_lib_class = DeviceCommon.resolve_model_path_class(model_path, 'sanity_lib.py')
        return sanity_lib_class.get_model(self.tables['AWLAN_Node table'])

    def get_sanity_model(self, model, gw, logs_dir=None):
        gw_model = gw['model'] if gw else None
        gw_tables = gw['gw_tables'] if gw else None
        model_path = DeviceCommon.get_model_path(model, gw_model)
        if "lib_testbed/pod/opensync/models/generic" in model_path:
            log.warning(f"Cannot find path to sanity_lib for {model}")
            return None
        sanity_lib_class = DeviceCommon.resolve_model_path_class(model_path, 'sanity_lib.py')
        return sanity_lib_class(self.tables, gw_tables, logs_dir, self.outfile, self.outstyle)
