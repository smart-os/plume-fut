import time

from lib_testbed.util.logger import log
from lib_testbed.util.msg.journal_msg import JournalMsg
from lib_testbed.util.msg.logread_msg import LogreadMsg


class Msg:
    def __init__(self, pod):
        self.pod = pod
        self.logger_name = self.get_logger_name()
        if self.logger_name == "journalctl":
            self.logger = JournalMsg(self.pod)
        else:
            self.logger = LogreadMsg(self.pod)

    def msg_cmd_wait(self, timeout, *args, **kwargs):
        start_time = time.time()
        while time.time() - start_time < timeout:
            try:
                resp = self._cmd(*args, **kwargs)
                if resp:
                    break
            except Exception as e:
                if "Return value: 1" not in str(e):
                    raise
                continue
            time.sleep(0.5)
        else:
            raise Exception("Timeout while waiting for journal message after {}sec".format(timeout))
        return resp

    def _cmd(self, cmd=None, service=None, last_lines=1000, cursor=None, timeout=30):
        messages = self.logger.cmd(cmd=cmd, service=service, last_lines=last_lines, cursor=cursor, timeout=timeout)
        for message in messages:
            assert message.get('date_timestamp')
            assert message.get('timestamp')
            assert message.get('value')
            assert message.get('cursor')
        return messages

    def get_cursor(self, last_log=None):
        if not last_log:
            try:
                last_log = self._cmd(last_lines=1)[-1]
            except KeyError:
                return 0
        return last_log['cursor']

    def get_logger_name(self):
        if self.is_systemd():
            return "journalctl"
        else:
            return "logread"

    def is_systemd(self):
        try:
            logread_version = self.pod.run_command('logread -V')
            if logread_version[0] == 0:
                log.info(f'Pod logger for {self.pod.name}: {logread_version[1].strip()}')
                return False
        except Exception:
            pass

        log.info(f'Pod logger for {self.pod.name}: journalctl')
        return True

    def get_service_log(self, service, process, cmd, cursor, timeout, module=None):
        return self.logger.cmd_wait(service=service,
                                    process=process,
                                    cmd=cmd,
                                    cursor=cursor,
                                    timeout=timeout,
                                    module=module)

    def get_logs_to_print(self, service, process, cmd, cursor, timeout, module=None):
        all_logs = self.logger.cmd_wait(service=service,
                                        process=process,
                                        cmd=cmd,
                                        cursor=cursor,
                                        timeout=timeout,
                                        module=module)
        all_logs = [f'{log_row["date_timestamp"]} <{process}> <{log_row["severity"]}> <{log_row["module"]}> '
                    f'{log_row["value"]}' for log_row in all_logs]
        return '\n'.join(all_logs)

    def get_logs_before_reboot(self):
        logs = ''
        path = self.pod.get_stdout(self.pod.get_opensync_path())
        if self.pod.run_command('ls /sys/fs/pstore')[1]:
            logs = self.pod.run_command('cat /sys/fs/pstore/pmsg-ramoops-0')[1]
        elif self.pod.run_command(f'ls {path}/log_archive/syslog')[1]:
            logs = self.pod.run_command(f'zcat {path}/log_archive/syslog/*')[1]
        else:
            log.info('No found any logs before reboot')
        return logs
