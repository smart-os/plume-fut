import datetime
import json
import time
from lib_testbed.util.logger import log


class LogreadMsg:
    def __init__(self, pod):
        self.pod = pod

    def cmd_wait(self, timeout=30, *args, **kwargs):
        start_time = time.time()
        while time.time() - start_time < timeout:
            try:
                resp = self.cmd(timeout=timeout, *args, **kwargs)
                if resp:
                    break
            except Exception as e:
                if "Return value: 1" not in str(e):
                    raise
                continue
            time.sleep(0.5)
        else:
            raise Exception("Timeout while waiting for logread message after {}sec".format(timeout))
        return resp

    def cmd(self, cmd=None, service=None, process=None, last_lines=1000, cursor=None, timeout=30, module=None):
        cmd_str = ""
        service_str = ""
        if service:
            service_str = " -p {}".format(process)
        if module:
            if isinstance(module, list):
                for mod in module:
                    service_str += f" -m {mod}"
            else:
                service_str += f" -m {module}"
        output_str = " -j"  # json
        if cmd:
            cmd_str = cmd
        logread_cmd = "logread {}{}".format(
            service_str, output_str)
        logread_cmd = "sh --login -c '{}'{}".format(logread_cmd, cmd_str)
        out = self.pod.run_command(logread_cmd, timeout=timeout)
        return self.parse_logread_resp(out[1].splitlines(), last_lines, cursor)

    def convert_timestamp(self, realtime_timestamp):
        epoch = int(realtime_timestamp) / 1000 / 1000
        return datetime.datetime.utcfromtimestamp(epoch).strftime("%Y-%m-%d %H:%M:%S")

    def convert_epoch(self, date_time):
        # Date example 'Nov 16 12:43:31'
        # Add missing year to date_time
        date_time = "{} {}".format(datetime.datetime.now().year, date_time)
        date_time = datetime.datetime.strptime(date_time, '%Y %b %d %H:%M:%S')
        return (date_time - datetime.datetime(1970, 1, 1)).total_seconds()

    def parse_logread_resp(self, resp, last_lines, from_cursor):
        messages = []
        for line in resp:
            try:
                line_info = json.loads(line)
            except ValueError:
                # Logread might return not expected lines at the beginning
                continue
            messages.append(line_info)
        messages_new = []
        last_timestamp = 0
        cursor_id = 0
        for message in messages:
            # "Nov 16 12:41:41 SM[1276]: <DEBUG>     MAIN: "
            try:
                # value = "{} {}[{}]: <{}>{:9}: {}".format(
                #     message['message_tm'], message['process'], message.get('pid', ''),
                #     message.get('severity', ''), message.get('module', ''), message['message'])
                value = message['message']
            except KeyError as e:
                log.error("{} missing in: {}".format(e, message))
                continue
            timestamp = self.convert_epoch(message['message_tm']) * 1000000  # usec
            if timestamp == last_timestamp:
                cursor_id += 1
            else:
                cursor_id = 0
                last_timestamp = timestamp
            cursor = timestamp + cursor_id / 100.0
            if from_cursor:
                if cursor <= from_cursor:
                    continue
            message_info = {'date_timestamp': message['message_tm'],
                            'timestamp': timestamp,
                            'value': value,
                            'cursor': cursor,
                            'severity': message.get('severity', 'Unknown'),
                            'module': message.get('module', 'Unknown')
                            }
            messages_new.append(message_info)
        if last_lines and last_lines < len(messages_new):
            messages_new = messages_new[-last_lines:]
        return messages_new
