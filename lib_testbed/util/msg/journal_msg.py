import datetime
import json
import time


class JournalMsg:
    def __init__(self, pod):
        self.pod = pod

    def cmd_wait(self, timeout, *args, **kwargs):
        start_time = time.time()
        while time.time() - start_time < timeout:
            try:
                resp = self.cmd(*args, **kwargs)
                if resp:
                    break
            except Exception as e:
                if "Return value: 1" not in str(e):
                    raise
                continue
            time.sleep(0.5)
        else:
            raise Exception("Timeout while waiting for journal message after {}sec".format(timeout))
        return resp

    def cmd(self, cmd=None, service=None, process=None, last_lines=1000, cursor=None, **kwargs):
        cmd_str = ""
        service_str = ""
        last_lines_str = ""
        since_str = ""
        from_cursor_str = ""

        if service:
            service_str = " -u {}".format(service)
        if last_lines:
            last_lines_str = " -n {}".format(last_lines)
        since = None
        if since:
            since_str = " --since \"{}\"".format(since)
        if cursor:
            from_cursor_str = " --after-cursor \"{}\"".format(cursor)
        output = "json"
        if output:
            output_str = " -o {}".format(output)
        if cmd:
            cmd_str = cmd
        journal_cmd = "journalctl {}{}{}{}{}{}".format(
            last_lines_str, service_str, since_str, from_cursor_str, output_str, cmd_str)
        out = self.pod.run_command(journal_cmd)
        return self.parse_journal_resp(out[1].splitlines(), process)

    def convert_journal_timestamp(self, realtime_timestamp):
        epoch = int(realtime_timestamp) / 1000 / 1000
        return datetime.datetime.utcfromtimestamp(epoch).strftime("%Y-%m-%d %H:%M:%S")

    def parse_journal_resp(self, resp, process):
        messages = []
        for line in resp:
            line_info = json.loads(line)
            messages.append(line_info)
        messages_new = []
        for message in messages:
            if process and process != message['SYSLOG_IDENTIFIER']:
                continue
            message_info = {'date_timestamp': self.convert_journal_timestamp(message['__REALTIME_TIMESTAMP']),
                            'timestamp': int(message['__REALTIME_TIMESTAMP']),
                            'value': message['MESSAGE'],
                            'cursor': message['__CURSOR']
                            }
            messages_new.append(message_info)
        return messages_new
