from lib_testbed.util.ssh.ssh_execute import SshCmd


class BaseLib(SshCmd):
    """Common methods for client and pod"""

    def shell_list(self, **_kwargs):
        """List all device(s) configured"""
        return [0, f'{self.name}', ""]


class Iface:
    def __init__(self, lib):
        self.lib = lib

    def get_iface_ip(self, iface):
        response = self.lib.run_command(f'ip -f inet addr show {iface}')
        if response[0] != 0:
            raise Exception(f'Failed to execute cmd for iface: {iface}')
        iface_info = self.lib.get_stdout(response)
        if not iface_info or 'not exist' in iface_info:
            raise ValueError(f'No ip address assigned to iface: {iface}')
        iface_addr = iface_info.splitlines()[1].split()[1].split('/')[0]
        return iface_addr

    def get_iface_ipv6(self, iface, scope='link'):
        response = self.lib.run_command(f'ip -f inet6 addr show {iface}')
        if response[0] != 0:
            raise Exception(f'Failed to execute cmd for iface: {iface}')
        iface_info = self.lib.get_stdout(response)
        if not iface_info or 'not exist' in iface_info or scope not in iface_info:
            raise ValueError(f'No ipv6 address assigned to iface: {iface}')
        iface_addr = None
        for line in iface_info.splitlines():
            if scope not in line:
                continue
            iface_addr = line.split()[1].split('/')[0]
        return iface_addr

    def get_iface_by_mac(self, mac):
        response = self.lib.run_command(
            f"grep -ri {mac} /sys/class/net/**")
        if response[0] != 0:
            raise ValueError(f'Interface with mac: {mac} not found')
        return response[1].split('/sys/class/net/')[1].split('/')[0]

    def get_subnet_mask(self, iface):
        response = self.lib.run_command(f'ip -f inet addr show {iface}')
        if response[0] != 0:
            raise Exception(f'Failed to execute cmd for iface: {iface}')
        iface_info = self.lib.get_stdout(response)
        if not iface_info:
            raise ValueError(f'No ip address assigned to iface: {iface}')
        subnet_mask = iface_info.splitlines()[1].split()[1].split('/')[1]
        return subnet_mask

    def get_iface_by_mask(self, ip, prefix=24):
        assert prefix == 24  # TODO: handle other prefixes
        pattern = ip[0:ip.rfind('.')]
        sudo = 'sudo' if 'Clients' == self.lib.device_type else ''
        response = self.lib.run_command(f'{sudo} ifconfig | grep {pattern} -B 1 | head -n1')
        if response[0] != 0 or not response[1]:
            raise Exception(f'Failed to resolve iface for ip: {pattern}')
        ifname = self.lib.get_stdout(response).split()[0].rstrip(':')
        return ifname

    def get_mac(self, ifname):
        result = self.lib.run_command(f"cat /sys/class/net/{ifname}/address")
        result = self.lib.strip_stdout_result(result)
        return self.lib.get_stdout(result)
