import datetime
import os
import re
import shutil
import subprocess
import time
from multiprocessing import Lock
from lib_testbed.util.logger import log, LogCatcher

SSH_GATEWAY = 'ssh_gateway'
DEBUG = False

lock = Lock()


class DeviceLogCatcher(LogCatcher):
    def __init__(self, obj, **kwargs):
        super().__init__(**kwargs)
        self.obj = obj

    def get_name(self):
        return self.obj.get_name()

    def add(self, command, new_commands, result, device, start_time):
        start = datetime.datetime.now() - datetime.timedelta(seconds=int(time.time() - start_time))
        start = f"{start.hour:02d}:{start.minute:02d}:{start.second:02d}"
        name = device.name
        self.add_to_logs(f'{start} [{name}] --> {command} [duration: {time.time() - start_time:.2f}s]\n')
        if DEBUG:
            self.add_to_logs(f"Remote command: {new_commands[name]}")
            log.info(f'Response for cmd: {command}\n{result[name][1]}')
        if not result[name][0]:
            info = result[name][1]
            error = ''
        else:
            info = result[name][2]
            error = f'[ERROR] ret:{result[name][0]}, stderr: '
            if DEBUG:
                log.error(error)
        # assert info is not None
        if isinstance(info, bytes):
            info = str(info)
        if self.obj.config.get('log_to_console'):
            log.info(f"[SSH] {name}: {command}", show_file=False)
        self.add_to_logs("{}{}".format(error, info))

    def add_mock(self, command, result, dev_name):
        def get_variable_name(cmd, suffix):
            # Remove all non-word characters (everything except numbers and letters)
            name = re.sub(r"[^\w\s]", '_', cmd)
            # Replace multiple '_' by single '_'
            pattern = '_' + '{1,}'
            name = re.sub(pattern, '_', name)

            # Replace multiple occurrences of a character by a single character
            name = re.sub(r"\s+", '_', name)
            name = name.lower()
            name = name.lstrip('_')
            name = name.rstrip('_')
            short_name = ''
            for word in reversed(name.split('_')):
                if not word or word.isdigit():
                    continue
                if short_name:
                    short_name = '_' + short_name
                short_name = (word if len(word) < 10 else word[:10]) + short_name
                if 'end_sign' in short_name:
                    short_name = ''
                if len(short_name) > 10:
                    break
            short_name = short_name.rstrip('_')
            short_name = short_name.lstrip('_')
            return f'{short_name}_{suffix}'

        mock_logger_name = f"mock_{self.obj.device_type.lower()}_{self.obj.get_name()}"
        buffer = self.get_log_buffer(name=mock_logger_name)
        init_resp = ', init=False' if buffer else ', init=True'
        names_resp = ', self.names' if self.obj.multi_devices else ''
        ret_value = result[dev_name][0]
        stdout = result[dev_name][1]
        stderr = result[dev_name][2]
        if isinstance(stdout, bytes):
            stdout = str(stdout)
        if isinstance(stderr, bytes):
            stderr = str(stderr)
        stdout = stdout.replace('\"', '\\"').replace('\'', '\\').replace('\n', '\\n')
        stderr = stderr.replace('\"', '\\"').replace('\'', '\\').replace('\n', '\\n')
        variable_stdout = 'resp_stdout'
        set_variable_stdout = f"{variable_stdout} = '{stdout}'"
        if len(set_variable_stdout) > 120 - 8:
            set_variable_stdout += '  # noqa E501'
        if stderr:
            variable_stderr = 'resp_stderr'
            set_variable_stderr = f"{variable_stderr} = '{stderr}'"
            if len(set_variable_stderr) > 120 - 8:
                set_variable_stderr += '  # noqa E501'
            set_variable_stderr += '\n'
        else:
            set_variable_stderr = ''
            variable_stderr = "''"
        cmd_str = command.replace("\r", "\\r").replace("\n", "\\n")
        variable_cmd = 'cmd'
        set_variable_cmd = f'{variable_cmd} = \'{cmd_str}\''
        if len(set_variable_cmd) > 120 - 8:
            set_variable_cmd += '  # noqa E501'
        set_response = f'self.mock_ssh_resp.set_response({ret_value}, {variable_stdout}, {variable_stderr}' \
                       f'{names_resp}{init_resp}, {variable_cmd}={variable_cmd})'
        if len(set_response) > 120 - 8:
            set_response += '  # noqa E501'
        mock_resp = f'\n\n{set_variable_cmd}\n' \
                    f'{set_variable_stdout}\n' \
                    f'{set_variable_stderr}' \
                    f'{set_response}\n\n'
        if DEBUG:
            log.debug(mock_resp)
        self.add_to_logs(mock_resp, name=mock_logger_name)

    def collect(self, test_data):  # noqa: C901
        if not test_data.get('failed') or test_data.get('skip_collect'):
            return
        # in case of optional mgmt access device is None and we are not able to get anything
        if self.obj.device is None:
            return
        dev_name = self.obj.get_name()
        dir_name = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        log_pull_dir = f'/tmp/automation/log_pull/{test_data.get("name")}/{dir_name}/{dev_name}'
        with lock:
            try:
                os.makedirs(log_pull_dir)
            except FileExistsError:
                return
        log.debug(f'Initiate log_pull: {log_pull_dir}')
        device_type = 'client' if self.obj.device_type == 'Clients' else 'pod'
        cwd_path = os.getcwd()
        if device_type == 'pod':
            # sometimes error occurs while pod is rebooting, so wait till device is available
            self.obj.wait_available(60)
            pod_date_time = self.obj.get_datetime(skip_exception=True)
            if pod_date_time and pod_date_time.year < 1980:
                log.warning(f"Pod date: {pod_date_time}, zip cannot work on dates < 1980, trying to fix")
                self.obj.set_datetime(datetime.datetime.now(), skip_exception=True)
                time.sleep(1)
                log.info(f'Pod time now: {self.obj.get_datetime(skip_exception=True)}')
            log_pull_gzip = self.obj.get_stdout(self.obj.get_logs(log_pull_dir, timeout=120, skip_log=True),
                                                skip_exception=True)
            if not log_pull_gzip:
                log.error(f"[{self.obj.get_nickname()}] Failed to collect Pod log-pull")
                return
            # Re-compress archive to zip as allure currently doesn't support gzip
            log_pull_file_name = os.path.basename(log_pull_gzip).split(".")[0]
            extract_dir = os.path.join(os.path.dirname(log_pull_gzip), log_pull_file_name)
            with lock:
                # Method unpack_archive() uses unsafe os.chdir()
                # If a thread attempts to hold a lock that’s already held by some other thread,
                # execution of this thread is halted until the lock is released.
                try:
                    shutil.unpack_archive(log_pull_gzip, extract_dir=extract_dir)
                finally:
                    try:
                        tmp_cwd_path = os.getcwd()
                    except FileNotFoundError:
                        tmp_cwd_path = ""
                    if tmp_cwd_path != cwd_path:
                        # unpack_archive might change directory
                        log.error("Root directory has changed, restore it.")
                        os.chdir(cwd_path)
            root_dir = extract_dir
            os.remove(log_pull_gzip)
        else:  # client
            root_dir = os.path.join(log_pull_dir, 'tmp')
            try:
                os.makedirs(root_dir)
            except FileExistsError:
                pass
            dmesg = self.obj.get_stdout(self.obj.run_command('dmesg -T', skip_log=True, timeout=60),
                                        skip_exception=True)
            if dmesg:
                with open(os.path.join(root_dir, 'dmesg'), "w+") as f:
                    f.write(dmesg)
            iwconfig = self.obj.get_stdout(self.obj.run_command('iwconfig', skip_log=True), skip_exception=True)
            if iwconfig:
                with open(os.path.join(root_dir, 'iwconfig'), "w+") as f:
                    f.write(iwconfig)
            ip_a = self.obj.get_stdout(self.obj.run_command('ip a', skip_log=True), skip_exception=True)
            if ip_a:
                with open(os.path.join(root_dir, 'ip_a'), "w+") as f:
                    f.write(ip_a)
            wpa_supplicant_path = self.obj.get_wpa_supplicant_base_path(skip_log=True)  # This should never fail
            if wpa_supplicant_path:
                wpa_supplicant_path += ".log"
                wpa_supplicant_log = self.obj.get_wpa_supplicant_log(lines=10000, timeout=60, skip_log=True)
                if wpa_supplicant_log:
                    with open(os.path.join(root_dir, 'wpa_supplicant_log'), "w+") as f:
                        f.write(wpa_supplicant_log)

            log_pull_file_name = 'log-pull'

        log_pull_zip = os.path.join(log_pull_dir, log_pull_file_name)

        with lock:
            # Method make_archive() uses unsafe os.chdir()
            # If a thread attempts to hold a lock that’s already held by some other thread,
            # execution of this thread is halted until the lock is released.
            try:
                log_pull_zip = shutil.make_archive(log_pull_zip, 'zip', root_dir)
            except ValueError as e:
                log.error(f'Cannot create zip: {e}')
                if "ZIP does not support timestamps before 1980" in str(e):
                    log.info(f'Trying to fix incorrect data in elements')
                    self.fix_files_timestamp(root_dir)
                    log_pull_zip = shutil.make_archive(log_pull_zip, 'zip', root_dir)
            finally:
                try:
                    tmp_cwd_path = os.getcwd()
                except FileNotFoundError:
                    tmp_cwd_path = ""
                if tmp_cwd_path != cwd_path:
                    log.error("Root directory has changed, restore it.")
                    os.chdir(cwd_path)
        shutil.rmtree(root_dir)
        self.initial_logger(name=f'log_pull_{device_type}_{dev_name}',
                            mime_type="application/gzip", source=log_pull_zip)

    @staticmethod
    def fix_files_timestamp(path):
        stamp = datetime.datetime(1980, 1, 1, 0, 0, 0).timestamp()
        for r, d, f in os.walk(path):
            for name in f:
                try:
                    if os.stat(os.path.join(r, name)).st_mtime < stamp:
                        log.info(f"{name} older than 1980")
                        os.utime(os.path.join(r, name))
                except (FileNotFoundError, OSError):
                    continue
