import sys
import logging
import time
from lib_testbed.util.ssh.sshexception import SshException
from lib_testbed.util.ssh import parallelssh
from lib_testbed.util.ssh.device_discovery import DeviceDiscovery
from lib_testbed.util.ssh.device_log_catcher import DeviceLogCatcher
from lib_testbed.util.logger import log


MOCK_RESPONSES_LOGS = True


class SshExecute:
    def __init__(self, device_type, config, **kwargs):
        self.config = config
        self.multi_devices = kwargs.get('multi_obj')
        if self.multi_devices is not None:
            del kwargs['multi_obj']
        self.device_type = device_type
        self.ext_path = None
        dev = kwargs.get('dev')
        if dev:
            del kwargs['dev']
        self.name = dev.name
        self.device = dev.device
        self.last_cmd = {}
        self.log_catcher = DeviceLogCatcher(
            default_name=f"log_{self.get_type_name()}_{self.get_name()}", obj=self)

    def get_nickname(self):
        return self.name

    def get_name(self):
        prefix = 'multi_' if self.multi_devices else ''
        return prefix + self.name

    def get_type_name(self):
        if self.device_type == "Clients":
            return 'client'
        else:
            return 'pod'

    def free_device(self):
        for device_config in self.config[self.device_type]:
            if self.get_nickname() != device_config['name']:
                continue
            if device_config.get('busy'):
                device_config['busy'] = False
            if device_config.get('multi_busy'):
                device_config['multi_busy'] = False

    def is_device_busy(self):
        busy = False
        for device_config in self.config[self.device_type]:
            if self.get_nickname() != device_config['name']:
                continue
            if not self.multi_devices and device_config.get('busy'):
                busy = True
                break
            elif device_config.get('multi_busy'):
                busy = True
                break
        return busy

    def run_command(self, command, device=None, *args, **kwargs):
        """Run ssh command on device
        :return: list of [ret_value, stdout, stderr]"""
        # return self.run_commands(command, device, *args, **kwargs)
        skip_remote = kwargs.get('skip_remote')
        if self.ext_path and not skip_remote:
            command = f"PATH=$PATH:{self.ext_path}; {command}"
        return self.execute(command, device, *args, **kwargs)

    @staticmethod
    def merge_result(old_result, new_result):
        merged_result = [0, "", ""]
        # Merge ret value
        merged_result[0] = old_result[0] + new_result[0]
        # Merge stdout
        merged_result[1] = old_result[1]
        if merged_result[1] and new_result[1]:
            merged_result[1] += "\n"
        merged_result[1] += new_result[1]
        # Merge stderr
        merged_result[2] = old_result[2]
        if merged_result[2] and new_result[2]:
            merged_result[2] += "\n"
        merged_result[2] += new_result[2]
        return merged_result

    @staticmethod
    def _get_stdout(result, cmd, name, skip_exception):
        assert len(result) == 3
        ret = result[0]
        stdout = result[1]
        stderr = result[2]
        try:
            ret_value = int(ret)
        except TypeError:
            ret_value = None
        if not isinstance(stdout, (dict, str, list)):
            stdout = ""
        if not isinstance(stderr, str):
            stderr = ""
        if ret_value != 0 and not skip_exception or 'mux_client_request_session: session request failed' in stderr:
            raise SshException(cmd=cmd,
                               name=name,
                               ret=ret_value,
                               stdout=stdout,
                               stderr=stderr)
        return stdout

    def get_stdout(self, result, skip_exception=False):
        command = self.last_cmd['command']
        name = self.last_cmd['name']
        return self._get_stdout(result, command, name, skip_exception)

    def strip_stdout_result(self, result):
        stdout = result[1]
        if stdout:
            if isinstance(stdout, bytes):
                stdout = stdout.decode("utf-8", 'backslashreplace')
            stdout = stdout.strip('\n').strip()
            result[1] = stdout
        return result

    def ssh(self, params="", **_kwargs):
        """SSH to client"""
        device = self.device
        cmd = device.get_remote_cmd(params)
        # Add parameter for interactive session
        ssh_prefix = "ssh "
        pos = cmd.find(ssh_prefix)
        if pos != -1:
            cmd = "{}{}-tt {}".format(cmd[0:pos], ssh_prefix, cmd[pos + len(ssh_prefix):])
        if 'netns exec' in cmd:
            # Execute bash command
            netns_name = cmd.split('netns exec')[1].split()[0]
            netns_name_end = cmd.rfind(netns_name) + len(netns_name)
            cmd = "{} bash {}".format(cmd[0:netns_name_end], cmd[netns_name_end:])

        if 'sshpass' in device.config and 'sshpass' not in cmd:
            cmd = f'sshpass -p {device.config["sshpass"]} ' + cmd

        p = parallelssh.Popen(cmd,
                              stdin=sys.stdin,
                              stdout=sys.stdout,
                              stderr=sys.stdout,
                              close_fds=True,
                              shell=True)
        sys.stdout, sys.stderr = p.communicate()
        return {device.name: (p.returncode, "", "")}

    @staticmethod
    def execute_cmd(commands, timeout=30):
        return parallelssh.execute_commands(commands, timeout)

    def execute(self, command, device, *args, **kwargs):
        """Run different commands on many devices
        :return: list of [ret_value, stdout, stderr]"""
        assert command
        if not isinstance(command, str):
            raise Exception("Unexpected command type: {}, expecting: string".format(type(command)))
        if not device:
            device = self.device
        if not device:
            raise SshException(cmd=command,
                               name='unknown',
                               ret=1,
                               stdout='',
                               stderr='Device not created')
        self.last_cmd = {'command': command, 'name': device.name}
        skip_remote = kwargs.get("skip_remote")
        if skip_remote:
            del kwargs["skip_remote"]
        skip_log = kwargs.get('skip_log')
        if skip_log:
            del kwargs['skip_log']
        new_kwargs = kwargs.copy()
        skip_exception = kwargs.get("skip_exception")
        if skip_exception is not None:
            del new_kwargs["skip_exception"]
        remote_command_dict = {}
        if args:
            command = "{} {}".format(command, " ".join(args))
        if skip_remote:
            remote_command = command
        else:
            remote_command = device.get_remote_cmd(command=command)
        remote_command_dict.update({device.name: remote_command})

        start_time = time.time()
        result_dict = self.execute_cmd(remote_command_dict, **new_kwargs)
        if not skip_log:
            self.log_catcher.add(command, remote_command_dict, result_dict, device, start_time)
            if log.isEnabledFor(logging.DEBUG):
                self.log_catcher.add_mock(command, result_dict, device.name)
        return result_dict[device.name]

    def result_ok(self, result):
        if result[0]:
            return False
        return True


class SshCmd(SshExecute):
    """Common ssh commands methods for client and pod"""

    def scp(self, *args, **kwargs):
        """SCP: "{DEST}" replaced with root@device"""
        command = self.device.scp_cmd(*args)
        return self.run_command(command, **kwargs, timeout=5 * 60, skip_remote=True)

    def wait_available(self, timeout=5, **kwargs):
        """Wait for device(s) to become available"""
        command = self.device.get_last_hop_cmd('nc -z -w {0} {1} {2}'.format(
            timeout, self.device.get_ip().replace('[', '').replace(']', ''), self.device.get_port()))
        return self.run_command(command, timeout=timeout, **kwargs, skip_remote=True)

    def put_file(self, file_name, location, timeout=10 * 60, **kwargs):
        """Copy a file onto device(s)"""
        command = self.device.scp_cmd(file_name, "{{DEST}}:{}".format(location))
        return self.run_command(command, **kwargs, timeout=timeout, skip_remote=True)
