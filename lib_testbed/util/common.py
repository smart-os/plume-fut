import os
import importlib
import traceback
from pathlib import Path
from lib_testbed.util.logger import log

BASE_DIR = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", '..'))


def quote_cmd_string(cmd_string):
    return cmd_string \
        .replace('\\', '\\\\') \
        .replace('"', '\\"') \
        .replace('$', '\\$') \
        .replace('`', '\\`')


class DeviceCommon:
    @staticmethod
    def convert_model_name(model):
        if not model:
            return model
        return model.lower().replace(" ", "_").replace(".", "_").replace("-", "_")

    @staticmethod
    def get_model_paths(model, dev_type='pod', default_family='opensync'):
        if not model:
            model = 'generic'
        model = DeviceCommon.convert_model_name(model)
        base_dir = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                                 '..', '..'))
        pod_path = os.path.join(base_dir, 'lib_testbed', dev_type)
        model_paths = []

        # Search for model directory
        for file_path in Path(pod_path).glob(f'**/models/{model}'):
            model_paths.append(os.fsdecode(file_path))
        if not model_paths:
            # generic model path
            if not default_family:
                default_family = ''
            model_paths.append(os.path.join(pod_path, default_family, 'models', 'generic'))
        for path in model_paths:
            if not os.path.exists(path):
                raise Exception(f'Model path: {path} does\'t exist\n'
                                f'model: {model}, dev_type: {dev_type}, default_family: {default_family}')
        return model_paths

    @staticmethod
    def get_model_path(model, parent_model, dev_type='pod', default_family='opensync'):
        parent_path = None
        model_path = None
        if parent_model:
            parent_paths = DeviceCommon.get_model_paths(parent_model, dev_type, default_family)
            if len(parent_paths) > 1:
                for parent_path in parent_paths:
                    if DeviceCommon.get_family(parent_path) == 'opensync':
                        break
                else:
                    parent_path = parent_paths[0]
        paths = DeviceCommon.get_model_paths(model, dev_type, default_family)
        if len(paths) > 1:
            for path in paths:
                if parent_path:
                    if DeviceCommon.get_family(path) == DeviceCommon.get_family(parent_path):
                        model_path = path
                        break
                elif DeviceCommon.get_family(path) == 'opensync':
                    model_path = path
                    break
        if not model_path:
            model_path = paths[0]
        return model_path

    @staticmethod
    def get_family(model_path):
        assert model_path
        return os.path.basename(os.path.dirname(os.path.dirname(model_path)))

    @staticmethod
    def resolve_model_path_file(model_path, file_name, default_family='opensync'):
        assert model_path
        assert file_name

        path = None
        # Search for lib directory
        for file_path in Path(model_path).glob(f'**/{file_name}'):
            path = os.fsdecode(file_path)
            break
        if not path:
            # find generic model for the same family
            model_path = os.path.dirname(model_path)
            for file_path in Path(model_path).glob(f'**/generic/**/{file_name}'):
                path = os.fsdecode(file_path)
                break
        if not path:
            # find generic model for default family
            root_path = os.path.dirname(os.path.dirname(model_path))
            if not default_family:
                default_family = ''
            for file_path in Path(root_path).glob(f'**/{default_family}/models/generic/**/{file_name}'):
                path = os.fsdecode(file_path)
                break
        if not path:
            raise KeyError(f'Could not resolve path for file: {file_name} in {model_path}')
        return path

    @staticmethod
    def resolve_model_path_class(model_path, file_name, default_family='opensync'):
        lib_path = DeviceCommon.resolve_model_path_file(model_path, file_name, default_family)
        base_dir = os.path.realpath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..'))
        path_relative = lib_path.replace(base_dir + '/', '')
        class_name = ''.join(x.capitalize() or '_' for x in file_name.split('.')[0].split('_'))
        module_name = path_relative.replace('/', '.').rstrip('.py')
        lib_module = importlib.import_module(module_name)
        return getattr(lib_module, class_name)

    @staticmethod
    def resolve_model_class(file_name, model, gw_model, dev_type='pod', default_family='opensync'):
        model_path = DeviceCommon.get_model_path(model, gw_model, dev_type, default_family)
        if not model_path:
            raise KeyError(f'Could not resolve path for model: {model}')
        return DeviceCommon.resolve_model_path_class(model_path, file_name, default_family)


class Results:
    @staticmethod
    def get_sorted_results(results, devices, skip_exception=False):
        sorted_results = []
        names = [device.get_name() for device in devices]
        for name in names:
            if name not in results:
                error = f"Missing '{name}' in results: {results.keys()}"
                if skip_exception:
                    log.error(error)
                    results[name] = ''
                else:
                    raise Exception(error)
            # if skip_exception and Results.is_ssh_exception(results[name]):
            #     log.info(f"Skipping SshException for '{name}'")
            #     results[name] = ''
            sorted_results.append(results[name])
        return sorted_results

    @staticmethod
    def call_method(attr, self_obj, not_callable, device, results_dict, *args, **kwargs):
        if not_callable:
            result = attr
        else:
            try:
                result = attr(*args, **kwargs)
            except Exception as e:
                except_str = traceback.format_exc()
                e.args += (except_str,)
                result = e

        # prevent wrapped_class from becoming unwrapped
        if result == device:
            result = self_obj
        results_dict.update({device.get_name(): result})
