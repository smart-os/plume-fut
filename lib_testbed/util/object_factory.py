import os
import json
import pytest
from lib_testbed.util.logger import log
from lib_testbed.util.opensyncexception import OpenSyncException
from lib_testbed.util.config import load_tb_config


class ObjectFactory(object):
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def create_obj(self, module_name, **kwargs):
        name = kwargs.get("name")
        if name:
            del kwargs["name"]
        super_admin = kwargs.get("super_admin")
        if super_admin:
            self.update_config_with_admin_creds(kwargs.get('config'))
        self.update_config_with_motion_cred(kwargs.get('config'))
        if not name:
            name = kwargs.get("role")
            if not name:
                if kwargs.get('config'):
                    # skip printing massive config info
                    del kwargs['config']
                raise Exception("Missing 'name' or 'rule' in opensync_ mark: {}".format(kwargs))
        if hasattr(self, name):
            raise Exception("Name conflict, object self.{} already exists".format(name))
        obj = self.resolve_obj(**kwargs)
        setattr(self, name, obj)
        obj.obj_name = {'name': name, 'module_name': module_name}
        return obj

    @staticmethod
    def update_config_with_admin_creds(config):
        if not config:
            return
        dpl = config.get('deployment_id')
        if dpl and dpl in ['beta', 'chi', 'chi-staging', 'delta', 'gamma', 'kappa', 'sigma', 'tau-int', 'tau-prod',
                           'theta']:
            # first option is to search for the /etc/smokecreds/smoke<deployment>.json file
            cred_file = f'/etc/smokecreds/smoke{dpl}.json'
            try:
                with open(cred_file) as cred:
                    info = json.load(cred)
                    config.update(info)
                    return
            except IOError:
                pass

            # the other option is to get them directly from Jenkins environments
            admin_user = os.environ.get('CLOUD_SMOKE_USER', None)
            admin_pwd = os.environ.get('CLOUD_SMOKE_PASS', None)
            if admin_user and admin_pwd:
                config['admin_user'] = admin_user
                config['admin_pwd'] = admin_pwd
                return

    @staticmethod
    def update_config_with_motion_cred(config):
        if not config:
            return
        dpl = config.get('deployment_id')
        if dpl and dpl in ['beta', 'chi', 'chi-staging', 'delta', 'gamma', 'kappa', 'sigma', 'tau-int', 'tau-prod',
                           'theta', 'dog1']:
            cred_file = f'{os.environ["HOME"]}/automation/resources/creds/motion{dpl}.json'
            if not os.path.exists(cred_file):
                return
            try:
                with open(cred_file) as cred:
                    info = json.load(cred)
                    config.update(info)
                    return
            except IOError:
                pass


class PyMoveToDeploymentPlugin:
    @pytest.fixture(scope="session", autouse=True)
    def move_to_deployment_fixture(self, request):
        # Unit tests intentionally leave tb_config unset, because they are not allowed
        # to touch the actual testbed. So also skip migrating the testbed to a different
        # deployment when running unit tests.
        if getattr(request.config.option, "tb_config", None) is None:
            return
        # move and reload whole config
        cfg = self.move_to_deployment(request.config.option.tb_config, request.config.option.config_name)
        if not cfg:
            return
        request.config.option.tb_config = cfg
        for item in request.session.items:
            item.cls.tb_config = cfg
            item.cls.tb_config_orig = cfg

    def __init__(self, deployment):
        self.deployment = deployment

    def move_to_deployment(self, tb_config, config_name):
        loc_deployment = tb_config['profile']
        if loc_deployment != self.deployment:
            try:
                from lib.util.cloudtoollib import CloudToolLib
            except ModuleNotFoundError:
                raise OpenSyncException("Required CloudToolLib is missing", "Add cloud module")
            log.info(f'Moving location from "{loc_deployment}" to "{self.deployment}" deployment')
            kwargs = {"config": tb_config}
            cloudlib = CloudToolLib(**kwargs)
            if cloudlib.move_to_deployment(self.deployment):
                log.info(f'Location has been moved successfully to {self.deployment}')
            else:
                raise Exception(f'Location has not been moved to {self.deployment}')
            return load_tb_config(config_name)
