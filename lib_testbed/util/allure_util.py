import os
import json
import configparser
import logging
import pytest
from filelock import FileLock
from allure_pytest.listener import AllureListener
from lib_testbed.util.logger import log


DEFAULT_SECTION = 'Global'

# Disable info logs for filelock module
logging.getLogger("filelock").setLevel(logging.ERROR)


class AllureUtil:
    def __init__(self, config):
        self.config = config
        if 'config_name' in config.option and config.option.config_name:
            self.section = config.option.config_name
        else:
            self.section = DEFAULT_SECTION

    def _get_results_dir(self):
        return self.config.option.allure_report_dir

    def _get_properties_path(self):
        allure_dir = self._get_results_dir()
        if not allure_dir:
            raise Exception("Missing pytest option --alluredir")
        return os.path.join(allure_dir, 'environment.properties')

    def _get_lock_file(self):
        return f'{self._get_properties_path()}.lock'

    def _read_config(self, config_parser):
        properties_file = self._get_properties_path()
        config_parser.read(properties_file)

    def _write_config(self, config_parser):
        with open(self._get_properties_path(), 'w') as configfile:
            config_parser.write(configfile)

    def _init(self):
        config_parser = configparser.ConfigParser()
        config_parser.clear()
        config_parser.add_section(self.section)
        self._write_config(config_parser)

    def init_categories(self):
        categories_path = os.path.join(self._get_results_dir(), 'categories.json')
        if not os.path.exists(categories_path):
            data = [{
                "name": "Jira issues",
                "messageRegex": ".*Detected issue.*",
                "matchedStatuses": [
                    "skipped"
                ]}]
            with open(categories_path, 'w') as fh:
                fh.write(json.dumps(data))

    def init(self):
        with FileLock(self._get_lock_file()):
            self._init()
            self.init_categories()

    def _set_config(self, name, value):
        config_parser = self._init_and_read_config()
        try:
            config_parser.set(self.section, name, value)
        except configparser.NoSectionError as e:
            log.warning(f'File was overwritten: {e}, skipping')
            return
        self._write_config(config_parser)

    def _init_and_read_config(self):
        config_parser = configparser.ConfigParser()
        self._read_config(config_parser)
        try:
            config_parser.items(self.section)
        except Exception:
            log.warning('environment.properties not initialized')
            self._init()
        return config_parser

    def _get_environment(self, name):
        config_parser = self._init_and_read_config()
        try:
            value = config_parser.get(self.section, name)
        except configparser.NoOptionError:
            value = None
        except configparser.NoSectionError:
            value = None
        return value

    def _get_environments(self):
        config_parser = self._init_and_read_config()
        try:
            return list(config_parser.items(self.section))
        except configparser.NoSectionError as e:
            log.warning(f'File was overwritten: {e}, skipping')
            return []

    def get_environment(self, name):
        with FileLock(self._get_lock_file()):
            config_parser = self._init_and_read_config()
            try:
                value = config_parser.get(self.section, name)
            except configparser.NoOptionError:
                value = None
            except configparser.NoSectionError:
                value = None
            return value

    def get_environments(self):
        with FileLock(self._get_lock_file()):
            config_parser = self._init_and_read_config()
            try:
                return list(config_parser.items(self.section))
            except configparser.NoSectionError as e:
                log.warning(f'File was overwritten: {e}, skipping')
                return []

    def add_environment(self, name, value, optional_suffix=None):
        if value in [None, '']:
            return
        with FileLock(self._get_lock_file()):
            prev_value = self._get_environment(name)
            # in case of config just add config files names. It's needed for CRF, FRV runs with multiple test beds
            if name in ['config'] and prev_value:
                value = f"{prev_value}, {value}" if value not in prev_value else prev_value
            elif optional_suffix and prev_value and prev_value != value:
                name = f'{name}_{optional_suffix}'
                prev_value = self._get_environment(name)
            if prev_value and prev_value != value:
                log.warning(f'Overriding: {name}, previous value: {prev_value}')
            self._set_config(name, value)

    def remove_environment(self, name):
        with FileLock(self._get_lock_file()):
            config_parser = self._init_and_read_config()
            try:
                config_parser.remove_option(self.section, name)
            except configparser.NoSectionError:
                log.warning(f"No {self.section} section in allure environment")
            self._write_config(config_parser)


class MyAllureListener(AllureListener):
    def __init__(self, config):
        self.last_error = None
        super().__init__(config)

    @staticmethod
    def get_test_class_path(node_id):
        return "::".join(node_id.split("::")[:-1])

    @staticmethod
    def is_xfailed(test_result):
        if test_result.status == 'skipped' and 'XFailed' in test_result.statusDetails.message:
            return True
        return False

    @pytest.hookimpl(hookwrapper=True)
    def pytest_runtest_teardown(self, item):
        uuid = self._cache.get(item.nodeid)
        test_result = self.allure_logger.get_test(uuid)

        skip_item = False
        if test_result.status == 'skipped' and not self.is_xfailed(test_result):
            skip_item = True
        if skip_item:
            yield
            uuid = self._cache.get(item.nodeid)
            self._cache.pop(item.nodeid)
            self.allure_logger.drop_test(uuid)
            return
        else:
            yield from super().pytest_runtest_teardown(item)

    @pytest.hookimpl(hookwrapper=True)
    def pytest_runtest_makereport(self, item, call):
        uuid = self._cache.get(item.nodeid)
        if not self.allure_logger.get_test(uuid):
            # self._cache.set(item.nodeid)
            yield
            return
        yield from super().pytest_runtest_makereport(item, call)
