"""
Logging module
"""
import allure
import logging
import os
import sys
import time
import shutil
import threading
import inspect

# NOTICE: Please keep in sync autotest-testrunner and qa-lib-testbed for this file.

LOGGER_NAME = "automation"

loggers = {}


def add_stream_handler_to_logger(my_logger):
    my_logger.setLevel(logging.INFO)
    # create console handler
    stream_hdlr = logging.StreamHandler()
    stream_hdlr.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s.%(msecs)03d [%(levelname).4s] %(message)s', '%H:%M:%S')
    stream_hdlr.setFormatter(formatter)
    my_logger.addHandler(stream_hdlr)


def get_logger(name):
    global loggers

    if loggers.get(name):
        return loggers.get(name)
    else:
        my_logger = logging.getLogger(LOGGER_NAME)
        if "pytest" not in sys.modules:
            add_stream_handler_to_logger(my_logger)
        loggers[name] = my_logger
        return my_logger


def update_logger_with_stream_handler(logger):
    if not [handler for handler in logger.handlers if isinstance(handler, logging.StreamHandler)]:
        add_stream_handler_to_logger(logger)


logger = get_logger(LOGGER_NAME)


class MyLogger(object):
    """Create logger class to customize log with additional information
    """
    ERROR = logging.ERROR
    WARNING = logging.WARNING
    INFO = logging.INFO
    DEBUG = logging.DEBUG

    def __init__(self):
        self.new_call = True
        self.last_attr = None
        self.new_line = True

    def __getattr__(self, attr):
        orig_attr = self.get_orig_attr(attr)
        if callable(orig_attr):
            def hooked(*args, **kwargs):
                args, kwargs = self.modify_args_kwargs(attr, *args, **kwargs)
                result = orig_attr(*args, **kwargs)
                # prevent wrapped_class from becoming unwrapped
                if result == logger:
                    return self
                return result
            return hooked
        else:
            return orig_attr

    def get_orig_attr(self, attr):
        if self.last_attr != attr:
            self.new_call = True
        else:
            self.new_call = False
        if attr == 'console':
            orig_attr = self.log_console
        else:
            if self.last_attr == 'console' and not self.new_line:
                # Log has changed from console to e.g. info. Start from the new line to not continue
                # in the same line as console.
                self.log_console('')
            global logger
            orig_attr = logger.__getattribute__(attr)
        self.last_attr = attr
        return orig_attr

    def get_extended_keyword(self, key, **kwargs):
        value = kwargs.get(key)
        if value != None:
            # Remove from kwargs
            kwargs.pop(key)
        else:
            value = True
        return value, kwargs

    def show_file(self, attr, **kwargs):
        value, kwargs = self.get_extended_keyword('show_file', **kwargs)
        if value and (attr != 'console' or
                        (attr == 'console' and (self.new_call or self.new_line))):
            return True, kwargs
        else:
            return False, kwargs

    def check_indentation(self, **kwargs):
        value, kwargs = self.get_extended_keyword('indent', **kwargs)
        if value is not True:
            return value, kwargs
        else:
            return 0, kwargs

    def modify_args_kwargs(self, attr, *args, **kwargs):
        show_file, kwargs = self.show_file(attr, **kwargs)
        indent, kwargs = self.check_indentation(**kwargs)
        if indent:
            if isinstance(args[0], str):
                # Add whitespaces
                prefix = " " * 2 * indent
                indent_str = prefix + "\n{}".format(prefix).join(args[0].split("\n"))
                args = (indent_str,) + args[1:]
        if show_file:
            if args and isinstance(args[0], str):
                # Add file and line number to message
                try:
                    stack = inspect.stack()
                    file_name = os.path.basename(stack[2][1])
                    file_name = os.path.splitext(file_name)[0]
                    if len(file_name) > 18:
                        file_name = file_name[:12] + ".." + file_name[-4:]
                    line_no = stack[2][2]
                except IndexError:
                    # In case file content has changed
                    file_name = 'file has changed'
                    line_no = ''
                prefix = "[%-18s:%-4s]  " % (file_name, line_no)
                args = (prefix + args[0],) + args[1:]
        return args, kwargs

    def log_console(self, *args, **kwargs):
        end = kwargs.get('end')
        if end is None or end.endswith("\n"):
            self.new_line = True
        else:
            self.new_line = False
        return _log_console(*args, **kwargs)


log = MyLogger()


def _log_console(*args, **kwargs):
    # Add delay thus print call doesn't interrupt the logging queue
    # TODO: Fix printing to stdout without EOL
    time.sleep(0.01)
    print(*args, **kwargs)
    sys.stdout.flush()
    time.sleep(0.01)


class LogCatcher:
    def __init__(self, default_name=None):
        self.loggers = []
        self.default_name = default_name

    def add(self, **_kwargs):
        # implement add() in a derived class
        raise NotImplementedError

    def add_screenshot(self, **_kwargs):
        # implement add_screenshot() in a derived class
        raise NotImplementedError

    def get_logger(self, name=None):
        if not name:
            name = self.default_name
        for logger in self.loggers:
            if logger.get('name') == name:
                return logger
        return None

    def get_log_buffer(self, name=None):
        logger = self.get_logger(name)
        if not logger:
            return []
        return logger['body']

    def initial_logger(self, name, mime_type=allure.attachment_type.TEXT, source=None, extension=None):
        logger = self.get_logger(name)
        if not logger:
            self.loggers.append({'name': name, 'body': [], 'source': source, 'mime_type': mime_type,
                                 'extension': extension})

    def add_to_logs(self, log, name=None):
        logger = self.get_logger(name)
        if not logger:
            if not name:
                name = self.default_name
            self.initial_logger(name)
            logger = self.get_logger(name)
            assert logger
        logger['body'].append(log)

    def collect(self, test_data):
        pass

    def get_all(self, test_data):
        self.collect(test_data)
        return self.loggers

    @classmethod
    def attach_logs(cls, opensync_obj, tb_config, failed=True):

        from lib_testbed.util.common import Results

        for obj in opensync_obj:
            if not hasattr(obj, "log_catcher"):
                obj.log_catcher = LogCatcher.DummyLogCatcher()
            if not hasattr(obj, "get_name"):
                obj.get_name = LogCatcher.ModuleName(obj).get_name

        jobs = []
        results_dict = {}
        collecting_logs = False
        name = tb_config.get('location_file', tb_config.get('deployment_file'))
        if name:
            name = os.path.basename(name).split('.')[0]
        else:
            name = 'unknown'
        test_data = {'name': name, 'failed': failed, 'skip_collect': False}
        for obj in opensync_obj:
            if hasattr(obj, 'obj_list'):
                # Devices Api, should only happen for client/pod tools
                return
            new_test_data = test_data.copy()
            name = obj.get_name()
            if name.startswith('multi_') and name[len('multi_'):] in [tmp_obj.get_name() for tmp_obj in opensync_obj]:
                new_test_data.update({'skip_collect': True})
            elif test_data['failed'] and not collecting_logs:
                log.info("Collecting logs...")
                collecting_logs = True
            # obj.log_catcher.get_all(*args)
            thread = threading.Thread(target=Results.call_method,
                                      args=(obj.log_catcher.get_all, cls, False, obj, results_dict, new_test_data,),
                                      daemon=True)
            thread.start()
            jobs.append(thread)

        max_time = 120
        start_time = time.time()
        for job in jobs:
            exec_time = time.time() - start_time
            timeout = max_time - exec_time if exec_time < max_time else 1
            job.join(timeout=int(timeout))

        resp_loggers = Results.get_sorted_results(results_dict, opensync_obj, skip_exception=True)
        for loggers in resp_loggers:
            if type(loggers) != list:
                error = repr(loggers).replace('\\n', '\n')
                log.error(f"[log_catcher] error occurred\n{error}")
        obj_loggers = [obj.log_catcher.loggers for obj in opensync_obj]
        LogCatcher.attach_to_allure(obj_loggers)

    @staticmethod
    def attach_to_allure(loggers_list):
        attachments = []
        dir_paths = []
        for loggers in loggers_list:
            for logger in loggers:
                if not logger:
                    continue
                if logger['name'] in attachments:  # and logger['name'].startswith('log_pull'):
                    continue
                if logger.get('body'):
                    allure.attach("\n".join(logger['body']), name=logger['name'],
                                  attachment_type=logger['mime_type'])
                elif logger.get('source'):
                    src_file = logger['source']
                    if not os.path.exists(src_file):
                        log.error(f"File {src_file} not found")
                        continue
                    allure.attach.file(source=src_file, name=logger['name'],
                                       attachment_type=logger['mime_type'], extension=logger['extension'])
                    os.remove(src_file)
                    dir_paths.append(os.path.dirname(src_file))
                else:
                    continue
                attachments.append(logger['name'])
        for dir_path in dir_paths:
            shutil.rmtree(dir_path, ignore_errors=True)

    class DummyLogCatcher:
        loggers = []

        def get_all(*_args, **_kwargs):
            return []

    class ModuleName:
        def __init__(self, self_obj):
            self.obj = self_obj

        def get_name(self):
            my_name = self.obj.__class__.__name__
            if my_name == 'module':
                my_name = self.obj.__name__.split('.')[-1]
            # log.info(f"[Log catcher] Consider to implement get_name() in: {my_name}")
            return my_name
