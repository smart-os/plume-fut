
import os
import re
import json
import time
import random
import allure
from datetime import datetime, timedelta
from enum import Enum
from uuid import uuid1

from lib_testbed.client.models.generic.client_api import ClientApi
from lib_testbed.util.logger import log


class IperfMode(Enum):
    SERVER = 's'
    CLIENT = 'c'


class Iperf:
    def __init__(self, server: ClientApi, client: ClientApi, port=None, interval=1, duration=300, bitrate=0,
                 parallel=5, omit=2, mss=None, reverse=False, connect_timeout_ms=30000, bytes_to_send=False):
        self.server = server
        self.client = client
        self.unique_id = uuid1()
        self.iperf_output_file = f'/tmp/iperf-{client.get_nickname()}-{self.unique_id}.out'
        self.port = self._find_unused_port() if port is None else port
        self.interval = interval
        self.duration = duration
        self.bitrate = bitrate
        self.parallel = parallel
        self.omit = omit
        self.mss = mss
        self.reverse = reverse
        self.connect_timeout_ms = connect_timeout_ms
        self.bytes_to_send = bytes_to_send

        self.client.json_result = None
        self.client.csv_result = None
        self.server_pid = None
        self.client_pid = None
        self.measurement_start_time = None
        self.plot_file = f'/tmp/iperf-plot-{client.get_nickname()}-{self.unique_id}.png'
        self.csv_file = f'/tmp/iperf-output-{client.get_nickname()}-{self.unique_id}.csv'

    def reinit_iperf_client(self):
        """
        Re init iperf client configuration in order to start another iperf process on the same client
        Returns:

        """
        self.client.json_result = None
        self.client.csv_result = None
        self.server_pid = None
        self.client_pid = None
        self.measurement_start_time = None
        self.unique_id = uuid1()
        self.iperf_output_file = f'/tmp/iperf-{self.client.get_nickname()}-{self.unique_id}.out'
        self.plot_file = f'/tmp/iperf-plot-{self.client.get_nickname()}-{self.unique_id}.png'
        self.csv_file = f'/tmp/iperf-output-{self.client.get_nickname()}-{self.unique_id}.csv'

    def start_server(self, bind_host=''):
        if bind_host:
            bind_host = f'-B {bind_host}'
        log.info('Starting iperf3 server...')
        cmd = f'iperf3 -s ' \
              f'-p {self.port} ' \
              f'-J ' \
              f'-D ' \
              f'{bind_host}'
        res = self.server.lib.run_command(cmd)
        time.sleep(0.2)
        if res[0] == 0:
            self.server_pid = self._get_iperf_pid(self.server, cmd)
        else:
            assert False, 'Iperf3 server started unsuccessfully!'

    def start_client(self, server_ip=None, duration=None, reverse=False, extra_param='', port=None):
        if duration:
            self.duration = duration
        log.info('Starting iperf3 client...')
        duration_arg = f'-n {self.bytes_to_send}' if self.bytes_to_send else f'-t {self.duration} '
        if server_ip is None:
            server_ip = self.server.get_client_ips(self.server.get_eth_iface().split("\n")[0])["ipv4"]
        mss = f'-M {self.mss} ' if self.mss is not None else ''
        con_tt = f"--connect-timeout {self.connect_timeout_ms} " if 'Clients' == self.client.lib.device_type else ''
        rev = '-R ' if reverse else ''
        bitrate_arg = f'-b {self.bitrate} ' if self.bitrate else ''
        omit_arg = f'-O {self.omit} ' if self.omit else ''
        port = port if port else self.port
        cmd = f'iperf3 -c {server_ip} ' \
              f'-p {port} ' \
              f'-J ' \
              f'{bitrate_arg}' \
              f'-P {self.parallel} ' \
              f'{omit_arg}' \
              f'-i {self.interval} ' \
              f'{duration_arg} ' \
              f'{mss}' \
              f'{con_tt}' \
              f'{rev}' \
              f'{extra_param} ' \
              f'&> {self.iperf_output_file} ' \
              f'&'
        res = self.client.lib.run_command(cmd)
        self.measurement_start_time = datetime.now()

        if res[0] == 0:
            self.client_pid = self._get_iperf_pid(self.client, cmd)

        if res[0] != 0 or not self.client_pid:
            iperf_results = self.client.lib.run_command(f'cat {self.iperf_output_file}')[1]
            log.error(f'Iperf3 client did not start\n: {iperf_results}')
            assert False, f'Iperf3 client did not start'

    def get_result_from_client(self, timeout=None, skip_exception=False):
        log.info(f'Waiting for iperf results(ETA '
                 f'@{self.measurement_start_time + timedelta(seconds=self.duration)})')
        wc = 0
        # protection against infinite loop
        timeout = time.time() + timeout if timeout else time.time() + self.duration + 120
        while wc <= 0 and timeout > time.time():
            try:
                wc = int(self.client.lib.run_command(f'wc -l {self.iperf_output_file}')[1].strip().split(' ')[0])
            except ValueError:
                log.warning(f"Cannot get wc value")
            time.sleep(5)

        if skip_exception and wc == 0:
            return {'error': 'No iperf results'}
        assert wc != 0, f'No iperf result after wait {self.duration + 120} seconds'
        log.info('Successfully got results')

        # fix nan values
        iperf_results = self.client.lib.run_command(f'cat {self.iperf_output_file}')[1]
        iperf_res = ''
        for line in iperf_results.splitlines():
            if 'nan' in line:
                line = line.replace('nan', '0')
            iperf_res += f'{line}\n'
        # Make sure we want to load only iperf output without std err
        iperf_res = iperf_res[iperf_res.find('{'):]
        try:
            res = json.loads(iperf_res)
            # some iperf3 version return sum some sum_received
            if "end" in res and "sum_received" not in res['end'] and res['end'].get('sum'):
                res['end']['sum_received'] = res['end']['sum']
        except json.decoder.JSONDecodeError:
            log.error(f"Cannot parse iperf_res: {iperf_res}")
            assert False, 'Cannot parse iperf results'
        return res

    def export_result_to_csv(self, filename=None, attach_to_allure=True):
        if self.client.json_result is None:
            self.client.json_result = self.get_result_from_client()

        iperf_error = self.client.json_result.get('error')
        if iperf_error and 'interrupt' not in iperf_error:
            raise Exception(f'Iperf ended with error {iperf_error}')

        filename = filename if filename is not None else self.csv_file
        file = open(filename, 'w')

        csv_header = 'Time elapsed in seconds; Speed in bits per second\n'
        self.client.csv_result = [csv_header]
        file.write(csv_header)
        for item in self.client.json_result['intervals']:
            item = item['sum']
            csv_line = f'{int(round(item["end"]))}; {item["bits_per_second"] / 1024 / 1024}'
            self.client.csv_result.append(csv_line)
            file.write(f'{csv_line}\n')

        file.flush()
        file.close()

        if attach_to_allure:
            allure.attach.file(filename, name=filename.split('/')[-1], attachment_type=allure.attachment_type.CSV)

    def export_json_results_to_allure(self, file_name, path_to_file):
        if self.client.json_result is None:
            self.client.json_result = self.get_result_from_client()

        with open(path_to_file, 'w+') as json_file:
            json.dump(self.client.json_result, json_file, indent=2)

        allure.attach.file(path_to_file, name=file_name, attachment_type=allure.attachment_type.JSON)

    def generate_plot(self, filename=None, attach_to_allure=True):
        import matplotlib
        from lib_testbed.util.plotter import Plotter, PlotterSerie
        matplotlib.use('Agg')

        if self.client.json_result is None:
            self.client.json_result = self.get_result_from_client()

        iperf_error = self.client.json_result.get('error')
        if iperf_error and 'interrupt' not in iperf_error:
            raise Exception(f'Iperf ended with error {iperf_error}')

        x_values = list()
        y_values = list()

        max_val = 0
        sum = 0
        for item in self.client.json_result['intervals']:
            item = item['sum']
            x_values.append(int(round(item['end'])))
            y_values.append(item['bits_per_second'] / 1024 / 1024)
            sum += item['bits_per_second']
            if item['bits_per_second'] / 1024 / 1024 > max_val:
                max_val = item['bits_per_second'] / 1024 / 1024

        avg_speed = sum / len(x_values) / 1024 / 1024

        plt = Plotter('Time [s]', 'Speed [Mb/s]')
        plt.add_series(PlotterSerie(x_values, y_values))
        plt.add_series(PlotterSerie(x_values, [avg_speed] * len(x_values)))

        filename = plt.save_to_file(filename if filename is not None else self.plot_file)

        if attach_to_allure:
            allure.attach.file(filename, name=filename.split('/')[-1], attachment_type=allure.attachment_type.PNG)

    def dispose(self):
        log.info('Disposing iperf and temporary files...')

        try:
            os.remove(self.csv_file)
        except FileNotFoundError:
            pass
        try:
            os.remove(self.plot_file)
        except FileNotFoundError:
            pass

        # pods do not have sudo
        if self.server_pid is not None:
            sudo = 'sudo' if 'Clients' == self.server.lib.device_type else ''
            self.server.lib.run_command(f'{sudo} kill -9 {self.server_pid}')
        if self.client_pid is not None:
            sudo = 'sudo' if 'Clients' == self.client.lib.device_type else ''
            self.client.lib.run_command(f'{sudo} kill -9 {self.client_pid}')
        self.client.lib.run_command(f'rm {self.iperf_output_file}', skip_exception=True)

    def _find_unused_port(self, min_port=5000, max_port=6000):
        netstat_out = self.server.lib.run_command('/bin/netstat -taun')[1]
        used_ports = set()
        for item in netstat_out.split('\n'):
            port = re.match(r'^(?:tcp|udp)[6]?\s+\d+\s+\d+\s\S+:(\d{1,5})\s+.+$', item)
            if port is not None:
                used_ports.add(port.groups()[0])

        while True:
            rnd_port = random.randint(min_port, max_port)
            if rnd_port not in used_ports:
                return rnd_port

    def _get_pid_from_file(self, pid_file):
        # not used now since iperf3 on pods do not supports -I switch
        return int(self.server.lib.run_command(f'cat {pid_file}')[1].strip('\x00').strip())

    def _get_iperf_pid(self, device, cmd):
        ps_cmd = 'ps aux' if 'Clients' == device.lib.device_type else 'ps'
        ps_res = device.lib.run_command(f'{ps_cmd} | grep -F "{cmd.split(" -J")[0]}"')[1]
        # remove grep process from the output
        ps_res = [line for line in ps_res.split('\n') if 'grep' not in line]
        if not ps_res[0]:
            # in case of small traffic the iperf process is quickly closed:
            if self.bytes_to_send and 'Clients' == device.lib.device_type:
                return True
            return None
        try:
            iperf_pid = [int(pid) for pid in ps_res[0].split() if pid.isdigit()][0]
        except IndexError:
            return None
        mode = 'server' if 'iperf3 -s' in cmd else 'client'
        log.info(f'Iperf3 {mode} started successfully on {device.get_nickname()} with PID {iperf_pid}')
        return iperf_pid
