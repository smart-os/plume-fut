from lib_testbed.util.object_factory import ObjectFactory
from lib_testbed.client.models.generic.client_api import ClientApi as LinuxClientApi
from lib_testbed.util.ssh.device_api import DevicesApi
from lib_testbed.util.ssh.device_discovery import DeviceDiscovery
from lib_testbed.util.common import DeviceCommon
from lib_testbed.util.logger import log


class Client(ObjectFactory):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def resolve_obj(self, **kwargs):
        kwargs['device_type'] = "Clients"
        dev_discovered = ClientResolver().get_device(**kwargs)
        api_class = ClientResolver().resolve_client_api_class(dev_discovered)
        kwargs['dev'] = dev_discovered
        return api_class(**kwargs)

    def __resolve_pycharm(self):
        # Dead code, workaround for resolving references by pycharm
        if hasattr(self, "INVALID_STATEMENT"):
            self.linux = LinuxClientApi()
            self.mac = LinuxClientApi()
            self.rpi = LinuxClientApi()
            self.all = LinuxClientApi()


class Clients(ObjectFactory):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def resolve_obj(self, **kwargs):
        obj_list = []
        device_type = "Clients"
        if not kwargs['config'].get(device_type):
            raise Exception(f"Missing '{device_type}' config")
        devices = kwargs['config'][device_type]
        kwargs['device_type'] = device_type

        names = kwargs.get('nicknames', [])
        if names:
            del kwargs['nicknames']

        use_host = False
        if 'host' in names:
            use_host = True
            host = [client for client in kwargs['config']['Clients'] if client['name'] == 'host']
            if not host:
                kwargs['config']['Clients'].append(
                    {"name": "host",
                     "hostname": kwargs['config'].get('ssh_gateway'),
                     'type': 'rpi'})

        for device in devices:
            name = device['name']
            if names and name not in names:
                continue
            if name == 'host' and not use_host:
                continue

            kwargs['nickname'] = device['name']
            try:
                dev_discovered = ClientResolver().get_device(**kwargs)
            except KeyError as e:
                if 'No device found matching criteria' not in str(e):
                    raise
                continue
            api_class = ClientResolver().resolve_client_api_class(dev_discovered)
            kwargs['dev'] = dev_discovered
            try:
                class_obj = api_class(**kwargs)
            except Exception as e:
                log.warning(repr(e))
                continue
            obj_list.append(class_obj)
        if not obj_list:
            raise Exception(f"No client available for: {kwargs}")
        return DevicesApi(obj_list, **kwargs)

    def __resolve_pycharm(self):
        # Dead code, workaround for resolving references by pycharm
        if hasattr(self, "INVALID_STATEMENT"):
            self.linux = LinuxClientApi()
            self.mac = LinuxClientApi()
            self.rpi = LinuxClientApi()
            self.all = LinuxClientApi()


class ClientResolver:
    @staticmethod
    def get_device(**kwargs):
        multi_devices = kwargs.get('multi_obj')
        if multi_devices is not None:
            del kwargs['multi_obj']
        return DeviceDiscovery(multi_devices=multi_devices, **kwargs)

    @staticmethod
    def resolve_client_api_class(dev_discovered):
        model = None
        gw_model = None
        file_name = 'client_api.py'
        if dev_discovered.device:
            model = dev_discovered.device.config['type']
        return DeviceCommon.resolve_model_class(file_name, model, gw_model, dev_type='client', default_family=None)
