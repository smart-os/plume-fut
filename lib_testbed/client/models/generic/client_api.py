import re
import time
from lib_testbed.util import rpowerlib
from lib_testbed.util.logger import log
from lib_testbed.util.allure_util import AllureUtil
from distutils.version import StrictVersion
from lib_testbed.util.ssh.device_api import DeviceApi


class ClientApi(DeviceApi):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.lib = self.initialize_device_lib(**kwargs)
        self.iface = self.lib.iface
        self.log_catcher = self.lib.log_catcher
        self.min_required_version = '1.0.1'

    def setup_class_handler(self):
        if self.session_config.option.skip_init or not self.lib.device:
            return

        name = self.get_nickname()
        uptime_result = self.lib.get_stdout(self.lib.uptime(timeout=20), skip_exception=True)
        if not uptime_result:
            assert self.lib.config.get('rpower'), f'{name} has no management access and rpower section ' \
                f'is missing in config'
            # get all rpower aliases for getting client which are connected to rpower
            rpower_aliases = [client['alias'] for client in self.lib.config['rpower']]
            rpower_clients = list()
            for rpower_alias in rpower_aliases:
                for rpower in rpower_alias:
                    rpower_clients.append(rpower.get('name'))

            assert name in rpower_clients, f'{name} without management access has no configured rpower'
            rpowerlib.rpower_init_config(self.lib.config)

            log.info(f'{name} has no management access')
            log.info(f'Check last operation on rpower for: {name}')
            last_request_time = rpowerlib.get_last_request_time([name])
            rpower_status = rpowerlib.rpower_status([name])[name]
            if last_request_time[name] and last_request_time[name] < 60 and rpower_status[0] is not True:
                log.warning(f'Skip changing rpower status due to that last operation has been done'
                            f' {last_request_time[name]} seconds ago')
            else:
                log.info(f'Power cycling not accessible client: {name}')
                rpowerlib.rpower_off([name])
                time.sleep(5)
                rpowerlib.rpower_on(rpowerlib.rpower_get_all_devices())
                timeout = time.time() + (60 * 3)
                while time.time() < timeout:
                    uptime_result = self.lib.get_stdout(self.lib.uptime(timeout=3), skip_exception=True)
                    if uptime_result:
                        break
                    time.sleep(5)
                else:
                    assert False, f'Client name: {name} has no management access. ' \
                        f'Can\'t recover {name} client by power cycle'
                log.info(f'{name} successfully recovered')

        # save FW versions
        version = self.version()
        allure_util = AllureUtil(self.session_config)
        allure_util.add_environment(f'client_{self.lib.name}_version', version, '_error')
        # example ver: "plume_rpi_client_image-1.4-66 [Wed Aug  7 17:30:52 UTC 2019]"
        # example ver: "plume_rpi_client__v1.6-86 [Thu Jan  9 13:36:01 UTC 2020]"
        # example ver: "plume_rpi_server__v1.6-85 [Thu Jan  9 12:54:38 UTC 2020]"
        try:
            ver = re.search(r"(\d+\.\d+.\d+)", version).group().replace('-', '.')
        except AttributeError:
            log.warning(f"Cannot get version from: '{version}'")
            ver = "100.0.0"
        assert ver >= StrictVersion(self.min_required_version), f"FW on the client {self.lib.name} is older than" \
            f" {self.min_required_version}, please upgrade to the latest"

        # save HW info
        hw_info = self.hw_info()
        allure_util.add_environment(f'client_{self.lib.name}_hw_info', hw_info, '_error')

        if self.get_wifi_power_management() == 'on':
            self.set_wifi_power_management('off')

    def get_stdout(self, result, skip_exception=False, **_kwargs):
        return self.lib.get_stdout(result, skip_exception=skip_exception)

    def ping(self, host, v6=False, **kwargs):
        result = self.lib.ping(host, v6, **kwargs)
        return self.get_stdout(result, skip_exception=True)

    def uptime(self, out_format='user', **kwargs):
        result = self.lib.uptime(out_format, **kwargs)
        return self.get_stdout(result, **kwargs)

    def version(self, **kwargs):
        result = self.lib.version(**kwargs)
        ver = self.get_stdout(result, skip_exception=True)
        return ver if ver else '100.0.0 [UNKNOWN]'

    def get_wifi_power_management(self, **kwargs):
        """Get wifi client power save state"""
        result = self.lib.get_wifi_power_management(**kwargs)
        return self.get_stdout(result, skip_exception=True)

    def set_wifi_power_management(self, state, **kwargs):
        """Set wifi client power save state"""
        result = self.lib.set_wifi_power_management(state, **kwargs)
        return self.get_stdout(result, **kwargs)

    def hw_info(self, **kwargs):
        result = self.lib.hw_info(**kwargs)
        ver = self.get_stdout(result, skip_exception=True)
        return ver if ver else 'model_name: UNKNOWN'

    def get_file(self, remote_file, location, create_dir=True, **kwargs):
        """Copy a file from client(s)"""
        result = self.lib.get_file(remote_file, location, create_dir, **kwargs)
        return self.get_stdout(result, **kwargs)

    def put_dir(self, directory, location, **kwargs):
        """Copy dir into client(s)"""
        result = self.lib.put_dir(directory, location, **kwargs)
        return self.get_stdout(result, **kwargs)

    def put_file(self, file_name, location, **kwargs):
        """Copy file into client(s)"""
        result = self.lib.put_file(file_name, location, **kwargs)
        return self.get_stdout(result, **kwargs)

    def info(self, **kwargs):
        """Display client(s) information"""
        result = self.lib.info(**kwargs)
        return self.get_stdout(result, **kwargs)

    # TODO: Add support for json output
    def wifi_winfo(self, ifname="", **kwargs):
        """Display client(s) wireless information, [ifname] if not provided in tbvars.conf"""
        result = self.lib.wifi_winfo(ifname, **kwargs)
        return self.get_stdout(result, **kwargs)

    def ping_check(self, ipaddr='', count=1, **kwargs):
        """Check client(s) wireless connectivity (ICMP), [ipaddr], [ifname] if not provided in tbvars.conf"""
        result = self.lib.ping_check(ipaddr, count, **kwargs)
        return self.get_stdout(result, skip_exception=True)

    def wifi_monitor(self, channel, ht="HT20", ifname="", **kwargs):
        """Set interface WIFI_MON_IF in tbvars.conf file to monitor mode, [ifname] if not provided in tbvars.conf"""
        result = self.lib.wifi_monitor(channel, ht, ifname, **kwargs)
        return self.get_stdout(result, **kwargs)

    def wifi_station(self, ifname="", **kwargs):
        """Set interface WIFI_MON_IF in tbvars.conf file to station mode, [ifname] if not provided in tbvars.conf"""
        result = self.lib.wifi_station(ifname, **kwargs)
        return self.get_stdout(result, **kwargs)

    def ping_v4_v6_arp(self, destination, version, wlan_test, count='8', **kwargs):
        result = self.lib.ping_v4_v6_arp(destination, version, wlan_test, count, **kwargs)
        return self.get_stdout(result, skip_exception=True)

    def ping_ndisc6(self, destination, ifname=None, **kwargs):
        result = self.lib.ping_ndisc6(destination, ifname, **kwargs)
        return self.get_stdout(result)

    def check(self, **kwargs):
        """Check if home-ap interfaces are up"""
        result = self.lib.check(**kwargs)
        return self.get_stdout(result, **kwargs)

    def client_type(self, **kwargs):
        """Display type of client(s)"""
        result = self.lib.client_type(**kwargs)
        return self.get_stdout(result, **kwargs)

    def get_5g_int(self, **kwargs):
        result = self.lib.get_5g_int(**kwargs)
        return self.get_stdout(result, **kwargs)

    def connect(self, ssid=None, psk=None, ifname=None, bssid=None, key_mgmt='WPA-PSK', timeout=60,
                dhclient=True, e_gl_param='', e_net_param='', country=None, ipv4=True, ipv6=False, ipv6_stateless=False,
                wps=False, **kwargs):
        """Connect client(s) to network with wpa_supplicant"""

        region = self.lib.config.get('loc_region', 'US') if not country else country
        # there is no country code like EU, so we need to switch do DE
        region = 'DE' if region == 'EU' else region
        result = self.lib.connect(ssid, psk, ifname=ifname, bssid=bssid, key_mgmt=key_mgmt, timeout=timeout,
                                  dhclient=dhclient, e_gl_param=e_gl_param, e_net_param=e_net_param, country=region,
                                  ipv4=ipv4, ipv6=ipv6, ipv6_stateless=ipv6_stateless, wps=wps, **kwargs)
        return self.get_stdout(result, **kwargs)

    def disconnect(self, ifname='', **kwargs):
        """Connect client(s) to network with wpa_supplicant"""
        result = self.lib.disconnect(ifname, **kwargs)
        return self.get_stdout(result, **kwargs)

    def start_dhcp_client(self, ifname='', cf=None, ipv4=True, ipv6=False, ipv6_stateless=False, **kwargs):
        """Starts dhcp client on wifi iface"""
        result = self.lib.start_dhcp_client(ifname, cf, ipv4=ipv4, ipv6=ipv6, ipv6_stateless=ipv6_stateless, **kwargs)
        return self.get_stdout(result, **kwargs)

    def stop_dhcp_client(self, ifname='', ipv4=True, ipv6=False, ipv6_stateless=False, **kwargs):
        result = self.lib.stop_dhcp_client(ifname, ipv4=ipv4, ipv6=ipv6, ipv6_stateless=ipv6_stateless, **kwargs)
        return self.get_stdout(result, **kwargs)

    def scan(self, ifname="", params='', match_string=None, **kwargs):
        """Trigger flush scan on the client"""
        result = self.get_stdout(self.lib.scan(ifname, params, **kwargs))
        if match_string:
            result_list = []
            for line in result.splitlines():
                # if re.compile(match_string).match(line):
                if "SSID: " not in line:
                    continue
                if match_string == line.replace('SSID: ', '').strip():
                    result_list.append(line)
            result = "\n".join(result_list)
        return result

    def pt_init(self, **kwargs):
        """Performance Tent Init"""
        result = self.lib.pt_init(**kwargs)
        return self.get_stdout(result, **kwargs)

    def join_ifaces(self, **kwargs):
        return self.lib.join_ifaces(**kwargs)

    def get_wlan_iface(self, **kwargs):
        return self.lib.get_wlan_iface(**kwargs)

    def get_eth_iface(self, **kwargs):
        return self.lib.get_eth_iface(**kwargs)

    def get_client_ips(self, interface, ipv6_prefix=None):
        return self.lib.get_client_ips(interface, ipv6_prefix=ipv6_prefix)

    def restart_networking(self, **kwargs):
        return self.lib.restart_networking(**kwargs)

    def start_continuous_ping(self, interface, file_path="/tmp/ping.log", wait='1', target='8.8.8.8', **kwargs):
        return self.lib.start_continuous_ping(interface, file_path, wait, target, **kwargs)

    def stop_continuous_ping(self, file_path="/tmp/ping.log", target='8.8.8.8', threshold=2, **kwargs):
        return self.lib.stop_continuous_ping(file_path, target, threshold, **kwargs)

    def get_mac(self, ifname="", **kwargs):
        """Get wifi mac address"""
        response = self.lib.get_mac(ifname, **kwargs)
        return self.get_stdout(response, **kwargs)

    def restart_eth_namespace(self, **kwargs):
        return self.lib.restart_eth_namespace(**kwargs)

    def reboot(self, **kwargs):
        """Reboot client(s)"""
        response = self.lib.reboot(**kwargs)
        return self.get_stdout(response, **kwargs)

    def set_hostname(self, new):
        response = self.lib.set_hostname(new)
        return self.get_stdout(response, skip_exception=True)

    def get_architecture(self, **kwargs):
        response = self.lib.get_architecture(**kwargs)
        return self.get_stdout(response, **kwargs)

    def check_chariot(self, **kwargs):
        response = self.lib.check_chariot(**kwargs)
        return self.get_stdout(response, **kwargs)

    def get_eth_info(self, timeout=10, **kwargs):
        response = self.lib.get_eth_info(timeout=timeout, **kwargs)
        return self.get_stdout(response, **kwargs)

    def get_wlan_information(self, **kwargs):
        response = self.lib.get_wlan_information(**kwargs)
        return self.get_stdout(response, **kwargs)

    def get_bt_info(self, **kwargs):
        response = self.lib.get_bt_info(**kwargs)
        return self.get_stdout(response, **kwargs)

    def set_tx_power(self, ifname, tx_power_db, **kwargs):
        response = self.lib.set_tx_power(ifname, tx_power_db, **kwargs)
        return self.get_stdout(response, **kwargs)

    def get_frequency(self, ifname='', **kwargs):
        response = self.lib.get_frequency(ifname, **kwargs)
        return self.get_stdout(response, **kwargs)

    def create_ap(self, channel, hw_mode=None, ifname='', ssid='test', extra_param='', **kwargs):
        response = self.lib.create_ap(channel, hw_mode, ifname, ssid, extra_param, **kwargs)
        return self.get_stdout(response, **kwargs)

    def disable_ap(self, ifname='', **kwargs):
        response = self.lib.disable_ap(ifname, **kwargs)
        return self.get_stdout(response, **kwargs)

    def refresh_ip_address(self, iface=None, ipv4=True, ipv6=False, ipv6_stateless=False, timeout=10, reuse=False,
                           **kwargs):
        response = self.lib.refresh_ip_address(iface, ipv4, ipv6, ipv6_stateless, timeout, reuse, **kwargs)
        self.get_stdout(response, **kwargs)
        return False if response[0] else True

    def set_mac(self, interface, new_mac, **kwargs):
        response = self.lib.set_mac(interface, new_mac, **kwargs)
        return self.get_stdout(response, **kwargs)

    def set_ip_address(self, ip, netmask='255.255.255.0', iface=None, **kwargs):
        response = self.lib.set_ip_address(ip, netmask, iface, **kwargs)
        return self.get_stdout(response, **kwargs)

    def start_mqtt_broker(self, **kwargs):
        response = self.lib.start_mqtt_broker(**kwargs)
        return self.get_stdout(response, **kwargs)

    def stop_mqtt_broker(self, **kwargs):
        response = self.lib.stop_mqtt_broker(**kwargs)
        return self.get_stdout(response, **kwargs)
