from lib_testbed.client.client_config import TBCFG_CLIENT_DEPLOY


class ClientTool:
    def __init__(self, lib):
        self.lib = lib
        # Dead code, workaround for resolving references by pycharm
        if hasattr(self, "INVALID_STATEMENT"):
            from lib_testbed.client.models.generic.client_lib import ClientLib
            self.lib = ClientLib()

    def get_name(self):
        return self.lib.get_name()

    def list(self, **kwargs):
        """List all configured clients"""
        return self.lib.shell_list(**kwargs)

    def run(self, command, *args, **kwargs):
        """Run a command

        :return: list of stdout"""
        result = self.lib.run_command(command, *args, **kwargs)
        return self.lib.strip_stdout_result(result)

    def ssh(self, params="", **kwargs):
        """Start interactive ssh session"""
        return self.lib.ssh(params, **kwargs)

    def info(self):
        """Preety client(s) information string"""
        response = self.lib.info()
        info = response[1]
        if not info:
            return [response[0], '', 'No output from tb script']

        info_txt = 'architecture: {}\n'.format(info['arch'])
        info_txt += 'os: {}\n'.format(info['os'])
        if 'chariot' in info:
            info_txt += 'chariot: {}\n'.format(info['chariot'])
        if 'eth' in info:
            for eth, einfo in info['eth'].items():
                info_txt += 'eth: {}\n'.format(eth)
                if 'ip' in einfo:
                    info_txt += ' {} ip: {}\n'.format(eth, einfo['ip'])
                info_txt += ' {} mac: {}\n'.format(eth, einfo['mac'])
        if 'wlan' in info:
            for wlan, winfo in info['wlan'].items():
                info_txt += 'wifi: {}\n'.format(wlan)
                if 'driver' in winfo:
                    info_txt += ' {} driver: {}\n'.format(wlan, winfo['driver'])
                info_txt += ' {} mac: {}\n'.format(wlan, winfo['mac'])
                # if 'channels' in winfo:
                #     info_txt += ' {} channels: {}\n'.format(wlan, winfo['channels'])
        if 'bt' in info:
            for bt, info in info['bt'].items():
                info_txt += 'bt: {}\n'.format(bt)
                if 'name' in info:
                    info_txt += ' {} name: {}\n'.format(bt, info['name'])
                info_txt += ' {} addr: {}\n'.format(bt, info['addr'])
        return [response[0], info_txt, response[2]]

    def reboot(self, **kwargs):
        """Reboot client(s)"""
        return self.lib.reboot(**kwargs)

    def ping(self, host=None, **kwargs):
        """Ping"""
        return self.lib.ping(host, **kwargs)

    def uptime(self, **kwargs):
        """Uptime"""
        command = "uptime"
        return self.lib.run_command(command, **kwargs)

    def version(self, short=False, **kwargs):
        """Display FW ver on the client"""
        return self.lib.version(short, **kwargs)

    def deploy(self, **kwargs):
        """Deploy files to client(s)"""
        return self.lib.deploy(**kwargs)

    def ep(self, command, **kwargs):
        """<stop|start|restart> Control IxChariot endpoint on client(s)"""
        param_to = self.lib.config[TBCFG_CLIENT_DEPLOY]
        if command not in ('stop', 'start', 'restart'):
            raise ValueError('Wrong parameter value')
        return self.lib.run_command('{0}/wifi endpoint {1}'.format(param_to, command), **kwargs)

    def wifi_winfo(self, ifname="", **kwargs):
        """Display client(s) wireless information"""
        return self.lib.wifi_winfo(ifname, **kwargs)

    def ping_check(self, ipaddr='', **kwargs):
        """Check client(s) wireless connectivity (ICMP)"""
        return self.lib.ping_check(ipaddr, **kwargs)

    def wifi_monitor(self, channel, ht="HT20", ifname="", **kwargs):
        """Set interface into monitor mode"""
        return self.lib.wifi_monitor(channel, ht, ifname, **kwargs)

    def wifi_station(self, ifname="", **kwargs):
        """Set interface into station mode"""
        return self.lib.wifi_station(ifname, **kwargs)

    def pt_init(self, **kwargs):
        """Performance Tent Init"""
        tool_path = self.lib.get_tool_path()
        cmd = "{0}/pt-init".format(tool_path)
        return self.lib.run_command(cmd, **kwargs)

    def get_ifaces(self, **kwargs):
        """List all client interfaces"""
        interface = self.lib.join_ifaces(**kwargs).split(',')[0]
        if not interface:
            response = [1, "", "No interface found"]
        else:
            response = [0, interface, ""]
        return response

    def scp(self, *args, **kwargs):
        """SCP: "{DEST}" replaced with provided client"""
        return self.lib.scp(*args, **kwargs)

    def put_file(self, file_name, location, **kwargs):
        """Copy a file to client(s)"""
        return self.lib.put_file(file_name, location, **kwargs)

    def get_file(self, remote_file, location, **kwargs):
        """Copy a file from client(s)"""
        return self.lib.get_file(remote_file, location, **kwargs)

    def put_dir(self, directory, location, **kwargs):
        """Copy dir into client(s)"""
        return self.lib.put_dir(directory, location, **kwargs)

    def connect(self, ssid=None, psk=None, ifname=None, bssid=None, key_mgmt='WPA-PSK', timeout=30, dhclient=True,
                country='US', ipv4=True, ipv6=False, ipv6_stateless=False, wps=False, **kwargs):
        """Connect Wifi client(s) to the network"""
        if type(ipv4) is not bool:
            ipv4 = eval(ipv4)
        if type(ipv6) is not bool:
            ipv6 = eval(ipv6)
        if type(ipv6_stateless) is not bool:
            ipv6 = eval(ipv6_stateless)
        if type(wps) is not bool:
            wps = eval(wps)
        return self.lib.connect(ssid, psk, ifname=ifname, bssid=bssid, key_mgmt=key_mgmt, timeout=timeout,
                                dhclient=dhclient, country=country, ipv4=ipv4, ipv6=ipv6, ipv6_stateless=ipv6_stateless,
                                wps=wps, **kwargs)

    def disconnect(self, ifname=None, **kwargs):
        """Connect client(s) to network with wpa_supplicant"""
        return self.lib.disconnect(ifname, **kwargs)

    def scan(self, ifname="", params='', **kwargs):
        """Trigger scan on the client"""
        return self.lib.scan(ifname, params, **kwargs)

    def start_dhcp_client(self, ifname, cf=None, ipv4=True, ipv6=False, ipv6_stateless=False, **kwargs):
        """Starts dhcp client on wifi iface"""
        if type(ipv4) is not bool:
            ipv4 = eval(ipv4)
        if type(ipv6) is not bool:
            ipv6 = eval(ipv6)
        if type(ipv6_stateless) is not bool:
            ipv6 = eval(ipv6_stateless)
        return self.lib.start_dhcp_client(ifname, cf=cf, ipv4=ipv4, ipv6=ipv6, ipv6_stateless=ipv6_stateless, **kwargs)

    def get_mac(self, ifname="", **kwargs):
        """Get wifi mac address"""
        return self.lib.get_mac(ifname, **kwargs)

    def pod_to_client(self, **kwargs):
        """
        Change pod to client
        """
        return self.lib.pod_to_client(**kwargs)

    def client_to_pod(self, **kwargs):
        """
        Change client to pod
        """
        return self.lib.client_to_pod(**kwargs)

    def upgrade(self, fw_path=None, restore_cfg=True, force=False, **kwargs):
        """
        Upgrade rpi client to target firmware if fw_path=None download latest build version from artifactory
        fw_path: (str) path to image
        restore_cfg: (bool) Restore a client configuration (hostname, dhcpd.conf)
        force: (bool) Flash image even though the same firmware is already on the device
        """
        results = self.lib.upgrade(fw_path, restore_cfg, force, **kwargs)
        self.lib.run_command('rm -R /tmp/automation')
        return results
