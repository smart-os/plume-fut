import os
import re
import time
import random
import pprint
from collections import ChainMap
from datetime import datetime

from lib_testbed.util.logger import log
from lib_testbed.util.common import quote_cmd_string
from lib_testbed.client.client_base import ClientBase
from lib_testbed.client.models.generic.client_tool import ClientTool
from lib_testbed.client.client_config import TBCFG_CLIENT_DEPLOY
from lib_testbed.util.opensyncexception import OpenSyncException
from lib_testbed.util.base_lib import Iface


class ClientLib(ClientBase):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.iface = ClientIface(lib=self)
        self.tool = ClientTool(lib=self)

    def ping(self, host=None, v6=False, **kwargs):
        ping = 'ping6' if v6 else 'ping'
        if host:
            cmd = f"{ping} -c 1 -w 1 {host}"
            result = self.run_command(cmd, self.device, **kwargs)
        else:
            cmd = self.device.get_last_hop_cmd(f"{ping} -c1 -w1 {self.device.get_ip()}")
            result = self.run_command(cmd, self.device, skip_remote=True, **kwargs)
        return self.strip_stdout_result(result)

    def uptime(self, out_format='user', **kwargs):
        """Display uptime of client(s)"""
        if out_format == 'user':
            result = self.run_command("uptime", **kwargs)
        elif out_format == 'timestamp':
            result = self.run_command("cat /proc/uptime", **kwargs)
            if result[0] == 0:
                result[1] = result[1].split()[0].strip()
        else:
            return [1, '', f"Unsupported format {out_format}"]
        return self.strip_stdout_result(result)

    def version(self, short=False, **kwargs):
        """Get client image version"""
        result = self.run_command("cat /.version", **kwargs)
        if short and result[0] == 0:
            version = re.search(r"(\d+\.\d+\-\d+)", result[1]).group()
            result[1] = version
        return self.strip_stdout_result(result)

    def deploy(self, **kwargs):
        if TBCFG_CLIENT_DEPLOY not in self.config:
            raise OpenSyncException("client_deploy_to: configuration not set",
                                    "Define client_deploy_to in locations config file")

        param_to = self.config[TBCFG_CLIENT_DEPLOY]
        base_dir = os.path.dirname(os.path.realpath(__file__))
        deploy_from = os.path.join(base_dir, 'deploy')
        command = "sudo mkdir -p {0}; sudo chown -R plume:plume {0}".format(param_to)
        response = self.run_command(command, **kwargs)
        assert self.result_ok(response)
        return self.put_file(file_name="{}/*".format(deploy_from), location=param_to, **kwargs)

    def hw_info(self, **kwargs):
        """Get client hardware info"""
        result = self.run_command("cat /proc/cpuinfo", **kwargs)
        gen_info = self.strip_stdout_result(result)
        ret = ''
        for info in ['Hardware', 'Revision', 'Model', 'model name', 'machine']:
            for line in gen_info[1].split('\n'):
                row = line.split(':')
                if info not in row[0].strip():
                    continue
                ret += f"{info.replace(' ', '_')}: {row[1].strip()}; "
                break
        # skip last "; "
        return [0, ret[:-2], '']

    def get_wifi_power_management(self, **kwargs):
        """Get wifi client power save state"""
        mode = None
        ifname = self.get_wlan_iface(**kwargs)
        if not ifname:
            return [1, mode, 'No wlan interface']
        result = self.run_command(f'sudo iwconfig {ifname} | grep "Power Management"')
        if self.result_ok(result):
            mode = self.strip_stdout_result(result)[1].replace('Power Management:', '')
        return [0, mode, '']

    def set_wifi_power_management(self, state, **kwargs):
        """Get wifi client power save state"""
        log.info(f"Set wifi power save: {state}")
        ifname = self.get_wlan_iface(**kwargs)
        if not ifname:
            return [1, '', 'No wlan interface']
        return self.run_command(f'sudo iwconfig {ifname} power {state}')

    def reboot(self, **kwargs):
        """Reboot client(s)"""
        result = self.run_command("sudo reboot", **kwargs)
        # Change ret val from 255 to 0 due to lost connection after reboot.
        if result[0] == 255:
            result[0] = 0
        return result

    def get_file(self, remote_file, location, create_dir=True, **kwargs):
        if create_dir:
            location = "{0}/{1}".format(location, self.get_name())
            # Check if location exists
            if not os.path.isdir(location):
                try:
                    os.makedirs(location)
                except Exception as e:
                    return 'Could not create dir {}: {}.'.format(location, str(e))
        command = self.device.scp_cmd("{{DEST}}:{}".format(remote_file), location)
        return self.run_command(command, skip_remote=True, **kwargs)

    def put_dir(self, directory, location, timeout=5 * 60, **kwargs):
        command = f"cd {directory}; tar -cf - *  |" + self.device.get_remote_cmd(
            f"sudo mkdir -p {location}; cd {location}; sudo tar -xof -") + " 2>/dev/null"
        return self.run_command(command, **kwargs, timeout=timeout, skip_remote=True)

    def info(self, **kwargs):
        """
        Display all client(s) information

        Returns: (list) [[(int) ret, (dict) stdout, (str) stderr]]

        """
        arch_info = self.get_stdout(self.get_architecture(**kwargs))
        chariot_info = self.get_stdout(self.check_chariot(**kwargs))
        eth_info = self.get_stdout(self.get_eth_info(**kwargs), skip_exception=True)
        wlan_info = self.get_stdout(self.get_wlan_information(**kwargs), skip_exception=True)
        bt_info = self.get_stdout(self.get_bt_info(**kwargs), skip_exception=True)
        name = self.get_stdout(self.strip_stdout_result(self.run_command('hostname')), skip_exception=True)
        os_info = {'os': 'Linux'}
        hostname = {'hostname': name}
        all_info = dict(ChainMap(arch_info, chariot_info, eth_info, wlan_info, bt_info, os_info, hostname.copy()))
        return [0, all_info.copy(), '']

    def get_architecture(self, **kwargs):
        """
        Get type of architecture on client
        Args:
            **kwargs:

        Returns: (list) [[(int) ret, (dict) stdout, (str) stderr]]
        (dict) stdout: {'arch': (str) architecture'}

        """
        result = self.strip_stdout_result(self.run_command('uname -a', **kwargs))
        arch = self.get_stdout(result, skip_exception=True)
        architecture = dict()
        if 'x86' in arch:
            architecture['arch'] = 'x86'
        elif 'arm' in arch:
            architecture['arch'] = 'arm'
        else:
            architecture['arch'] = 'unknown'
        return [0, architecture.copy(), '']

    def check_chariot(self, **kwargs):
        """
        Get chariot status on client
        Returns:  (list) [[(int) ret, (dict) stdout, (str) stderr]]
        (dict) stdout: {'chariot': (bool) State}

        """
        result = self.strip_stdout_result(self.run_command('ps -ax | grep endpoin', **kwargs))
        status = self.get_stdout(result, skip_exception=True)
        chariot = dict()
        chariot['chariot'] = True if 'endpoint' in status else False
        return [0, chariot.copy(), '']

    def get_eth_info(self, timeout=10, **kwargs):
        """
        Get information from eth interface
        Args:
            timeout: (int) timeout to wait for an dhcp
            **kwargs:

        Returns: (list) [[(int) ret, (dict) stdout, (str) stderr]]
        (dict) stdout: {'eth': {'eth_iface': {'mac': (str) mac_address, 'ip': (str) ip_address}}}

        """
        ifaces = self.get_stdout(self.strip_stdout_result(self.run_command('ls /sys/class/net | grep -e et -e en',
                                                                           **kwargs)), skip_exception=True)
        eth_info = {'eth': {}}
        if not ifaces:
            return [0, eth_info, '']
        ifaces = ifaces.strip('\nlo')
        for iface in ifaces.split('\n'):
            # our eth clients uses only tagged ifaces, so analyze only those with . in name, otherwise we will
            # restart dhcp clients for mgmt iface
            if '.' not in iface:
                continue
            eth_info['eth'][iface] = {'eth': 'true'}
            # get mac address
            eth_info['eth'][iface]['mac'] = \
                self.get_stdout(self.strip_stdout_result(self.run_command(f'cat /sys/class/net/{iface}/address',
                                                                          **kwargs)))
            # refresh IP if iface is up and get ip address
            up = self.get_stdout(self.strip_stdout_result(self.run_command(f'ip add show dev {iface}', **kwargs)))
            if 'state UP' in up:
                self.run_command(f'sudo dhclient -r {iface}', **kwargs)
                self.run_command(f'sudo ifconfig {iface} 0.0.0.0', **kwargs)
                self.run_command(f'sudo kill $(cat /var/run/dhclient.{iface}.pid)', **kwargs)
                self.run_command(f'timeout {timeout} sudo dhclient -v -pf /var/run/dhclient.{iface}.pid {iface}',
                                 **kwargs)

            ip = self.get_stdout(self.strip_stdout_result(self.run_command(
                f'ip -4 add show dev {iface} | grep "inet "')), skip_exception=True)

            if ip:
                eth_info['eth'][iface]['ip'] = ip[ip.rfind('inet') + 5: ip.rfind('/')]

        return [0, eth_info.copy(), '']

    @staticmethod
    def get_iw_supported_commands(iw_info):
        # get supported commands:
        iw_cmd = iw_info.split("Supported commands:")[1]
        supported_cmd = []
        for line in iw_cmd.splitlines():
            line = line.strip()
            if not line:
                continue
            if not line.startswith("*"):
                break
            supported_cmd.append(line[2:])
        return supported_cmd

    @staticmethod
    def get_iw_supported_modes(iw_info):
        iw_out = iw_info.split("Supported interface modes:")[1]
        supported_modes = []
        for line in iw_out.splitlines():
            line = line.strip()
            if not line:
                continue
            if not line.startswith("*"):
                break
            supported_modes.append(line[2:])
        return supported_modes

    def get_wlan_information(self, **kwargs):
        """
        Get information from wlan interface
        Args:
            **kwargs:

        Returns: (list) [[(int) ret, (dict) stdout, (str) stderr]]
        (dict) stdout {'wlan': {'ifname': {'driver': (str) driver_name, 'mac': (str) (mac_addr), 'phy': (str) phy
        , 'FT': (bool) state, 'channels': (list)(int),'ip': (str) ip_address}}}

        """
        wlan_info = {'wlan': {}}
        output = [0, wlan_info, '']
        # dev_ifaces = self.get_stdout(self.strip_stdout_result(self.run_command('ls /sys/class/net', **kwargs)))
        # iface = [dev_iface for dev_iface in dev_ifaces.split() if dev_iface != 'lo']
        iface = self.get_wlan_iface()
        if self.run_command(f'test -d /sys/class/net/{iface}/phy80211')[0] != 0:
            return output
        wlan_info['wlan'][iface] = {'wifi': 'true'}
        # get wifi driver
        driver = self.get_stdout(self.strip_stdout_result(self.run_command(
            f'ls -ll  /sys/class/net/{iface}/device/driver')))
        wlan_info['wlan'][iface]['driver'] = driver.split('/')[-1]
        # get mac address
        wlan_info['wlan'][iface]['mac'] = self.get_stdout(self.strip_stdout_result(self.run_command(
            f'cat /sys/class/net/{iface}/address')))
        # get phy index
        phy = self.get_stdout(self.strip_stdout_result(self.run_command(
            f'cat /sys/class/net/{iface}/phy80211/index')))
        wlan_info['wlan'][iface]['phy'] = f'phy{phy}'
        iw_info = self.get_stdout(self.strip_stdout_result(self.run_command(f'sudo iw phy{phy} info', **kwargs)))
        supported_cmd = self.get_iw_supported_commands(iw_info)
        ft_cmd = ['authenticate', 'associate', 'deauthenticate', 'disassociate']
        wlan_info['wlan'][iface]['FT'] = set(ft_cmd).issubset(set(supported_cmd))
        # Get Supported interface modes
        supported_modes = self.get_iw_supported_modes(iw_info)
        if 'monitor' in supported_modes:
            wlan_info['wlan'][iface]['monitor'] = True
        # get wifi channels
        bands = iw_info.split('Frequencies:')[1:]
        channels = []
        for band in bands:
            for line in band.split('\n'):
                if 'DFS' in line:
                    continue
                if 'MHz' in line and 'disabled' not in line:
                    try:
                        channels.append(int(line[line.find('[') + 1:line.find(']')]))
                    except ValueError:
                        pass
                if line and '*' not in line:
                    break
        wlan_info['wlan'][iface]['channels'] = channels
        # check if ac
        if 'VHT' in iw_info:
            wlan_info['wlan'][iface]['802.11ac'] = True
        # get ip address
        ip = self.get_stdout(self.strip_stdout_result(self.run_command('hostname -I')))
        if ip:
            wlan_info['wlan'][iface]['ip'] = ip
        output = [0, wlan_info.copy(), '']
        return output

    def get_bt_info(self, **kwargs):
        """
        Get bluetooth information
        Args:
            **kwargs:

        Returns:

        """
        bt_info = {'bt': {}}
        response = self.get_stdout(self.strip_stdout_result(self.run_command('hciconfig -a', **kwargs)),
                                   skip_exception=True)
        if 'hci' in response:
            for hci in response.split('hci'):
                if 'BD Address:' not in hci:
                    continue
                hci_name = 'hci' + hci[:hci.find(':')]
                bt_info['bt'][hci_name] = {'bt': 'true',
                                           'addr': hci[hci.find('BD Address:') + 12:
                                                       hci.find('BD Address:') + 29].lower()}
                if 'Name' in hci:
                    bt_info['bt'][hci_name]['name'] = hci[hci.find('Name:') + 7: hci.rfind("'")]
        output = [0, bt_info.copy(), '']
        return output

    def get_wlan_iface(self, **kwargs):
        """
        Get all WLAN interfaces
        Returns: (list) interfaces

        """
        iface = self.get_stdout(self.strip_stdout_result(self.run_command('ls /sys/class/net | grep wl',
                                                                          **kwargs)), skip_exception=True)
        return iface

    def get_eth_iface(self, **kwargs):
        """
        Get all eth interfaces
        Returns: (list) interfaces

        """
        iface = self.get_stdout(self.strip_stdout_result(self.run_command('ls /sys/class/net | grep "et\|en"',
                                                                          **kwargs)), skip_exception=True)  # noqa
        return iface

    def join_ifaces(self, **kwargs):
        """
        Get all interfaces from clients
        Returns: (list)

        """
        wlan_iterface = self.get_wlan_iface(**kwargs)
        eth_interface = self.get_eth_iface(**kwargs)
        interface = ''
        if wlan_iterface and eth_interface:
            interface = ','.join([wlan_iterface, eth_interface])
        elif wlan_iterface and not eth_interface:
            interface = wlan_iterface
        elif eth_interface and not wlan_iterface:
            interface = eth_interface
        return interface

    def wifi_winfo(self, ifname, **kwargs):
        ifname = ifname if ifname else self.get_wlan_iface()
        command = f"sh -c 'sudo iw dev {ifname} link; sudo iw dev {ifname} info'"
        if not ifname:
            return [1, '', 'Missing wlan interface']
        return self.run_command(command, **kwargs)

    def ping_check(self, ipaddr='', count=1, **kwargs):
        if not ipaddr:
            ipaddr = '8.8.8.8'
        command = f"ping -c {count} -t 200 -W 5 {ipaddr}"
        result = self.run_command(command, **kwargs)
        # Check result
        if result[0] != 0:
            result[1] = ''
        elif result[1]:
            result[1] = 'Ping check successful'
        return result

    def wifi_monitor(self, channel, ht, ifname, **kwargs):
        ifname = ifname if ifname else self.get_wlan_iface()

        command = f"sh -c 'sudo ip link set {ifname} down; sudo iw {ifname} set type monitor; " \
                  f"sudo ip link set {ifname} up; sudo iw {ifname} set channel {channel} {ht}; " \
                  f"iw {ifname} info | grep monitor'"

        if not ifname:
            return [1, '', 'Missing wlan interface']
        result = self.strip_stdout_result(self.run_command(command, **kwargs))
        if not result[1] and not result[2]:
            if not result[0]:
                result[0] = 1
            result[2] = 'Can not change interface state to monitor mode'
        return result

    def wifi_station(self, ifname, **kwargs):
        ifname = ifname if ifname else self.get_wlan_iface()

        command = f"sh -c 'sudo ip link set {ifname} down; sudo iw {ifname} set type managed; " \
                  f"iw {ifname} info | grep managed'"

        # Run command only for wifi clients
        if not ifname:
            return [1, '', "Missing wlan interface"]

        result = self.strip_stdout_result(self.run_command(command, **kwargs))
        if not result[1] and not result[2]:
            if not result[0]:
                result[0] = 1
            result[2] = 'Can not change interface state to station mode'
        return result

    def get_mac(self, ifname="", **kwargs):
        """Get wifi mac address"""
        ifname = ifname if ifname else self.join_ifaces()  # TODO: replace self.join_ifaces()
        command = f"cat /sys/class/net/{ifname}/address"
        return self.strip_stdout_result(self.run_command(command, **kwargs))

    def get_wpa_supplicant_base_path(self, ifname=None, **kwargs):
        if not ifname:
            ifname = self.get_wlan_iface(**kwargs)
        if not ifname:
            return None
        return f'/tmp/wpa_supplicant_{ifname}'

    def get_wpa_supplicant_log(self, ifname=None, lines=10000, convert_timestamps=True, **kwargs):
        path = self.get_wpa_supplicant_base_path(ifname=ifname, **kwargs)
        out = self.run_command(f"sudo tail -n{lines} {path}.log", **kwargs)
        if not convert_timestamps:
            return out[1]

        stdout = ''
        for line in out[1].splitlines(keepends=True):
            try:
                sline = line.split(':', maxsplit=1)
                stdout += f'[{datetime.utcfromtimestamp(float(sline[0])).ctime()}]:{sline[1]}'
            except Exception:
                stdout += line

        return stdout

    def connect(self, ssid=None, psk=None, ifname=None, bssid=None, key_mgmt='WPA-PSK', timeout=60,
                dhclient=True, e_gl_param='', e_net_param='', country='US', ipv4=True, ipv6=False, ipv6_stateless=False,
                wps=False, **kwargs):
        """
        Connect client(s) to network starting own wpa_supplicant
        Args:
            ssid: ssid
            psk: password
            ifname: interface name
            bssid: bssid if needed
            key_mgmt: WPA-PSK or FT-PSK
            timeout: timeout for connecting to network
            dhclient: start dhcp client after association
            e_gl_param: extra wpa_supplicant global parameters separated with ','
            e_net_param: extra wpa_supplicant network parameters separated with ','
            country: force regulatory domain for desired country; default country is US
            ipv4 (bool) get IPv4 address
            ipv6 (bool) get IPv6 address
            ipv6_stateless (bool) IPv6 stateless mode
            wps (bool) connect using WPS-PBC
            **kwargs:

        Returns: (list) merged clients response

        """
        new_kwargs = kwargs.copy()
        if new_kwargs.get('skip_exception'):
            del new_kwargs['skip_exception']

        if ssid is None:
            name = self.get_network_name()
            ssid, _ = self.get_network(name)

        if psk is None:
            name = self.get_network_name()
            _, psk = self.get_network(name)

        ifname = ifname if ifname else self.get_wlan_iface()
        if not ifname:
            return [1, '', 'Missing wlan interface']
        log.info('Connect clients {} iface: {} to ssid: {}, bssid: {}'.format(self.device.name, ifname, ssid, bssid))

        if not wps:
            # create wpa_supp conf
            bssid_info = f'bssid={bssid}\n' if bssid else ''
            extra_param = '\n'.join(e_gl_param.split(','))
            extra_net_param = '\n    '.join(e_net_param.split(','))
            ssid_hex = ssid.encode('utf-8').hex()
            ssid_quote = quote_cmd_string(ssid)
            psk_quote = quote_cmd_string(psk)
            resp = self.get_stdout(self.run_command(
                f'wpa_passphrase "{ssid_quote}" "{psk_quote}"', **kwargs))
            assert ssid == resp.split('ssid=')[1].splitlines()[0][1:-1]
            assert psk == resp.split('psk=')[1].splitlines()[0][1:-1]
            psk_hex = resp.split('psk=')[2].splitlines()[0]
            wpa_supp_conf = f"""ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country={country}
{extra_param}

network={{
    ssid={ssid_hex}
    psk={psk_hex}
    proto=RSN
    key_mgmt={key_mgmt}
    scan_ssid=1
    priority=1
    {bssid_info}
    {extra_net_param}
}}"""
        else:
            wpa_supp_conf = f"""ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
ctrl_interface_group=0
update_config=1
country={country}"""

        # first check if old supplicant works and remove old wpa_supplicant files
        base_path = self.get_wpa_supplicant_base_path(ifname)
        self.disconnect(ifname, clean_inteface=dhclient, **kwargs)
        log.info(f'base_path: {base_path}')

        # need to rm with sudo, since wpa_supplicant is started as root
        command = f"sudo rm {base_path}*"
        # Run command only for wifi clients
        self.run_command(command, **kwargs)

        # save wpa_supplicant.conf on client
        command = f"echo '{wpa_supp_conf}' > {base_path}.conf"
        self.run_command(command, **kwargs)

        # start wpa_supplicant in background with logs redirected to /tmp/wpa_supplicant_<ifname>.log
        command = f"sudo wpa_supplicant -D nl80211 -i {ifname} -c {base_path}.conf -P {base_path}.pid " \
            f"-f {base_path}.log -t -B -d"
        result = self.run_command(command, **kwargs)
        # in case of failure print wpa_supplicant.log and exit
        if result[0]:
            log.error('Unable to start wpa_supplicant: ')
            print(self.get_wpa_supplicant_log(ifname=ifname), **new_kwargs)
            self.last_cmd['command'] = command  # Update the command cache for returned result
            return result

        # wait for an association
        timeout = time.time() + timeout
        result = []
        completed = False
        if wps:
            command = f'sudo wpa_cli -i {ifname} wps_pbc'
            result = self.run_command(command, **kwargs)

        while not completed and time.time() < timeout:
            command = f"sudo wpa_cli -i {ifname} status | grep wpa_state=COMPLETED"
            result = self.run_command(command, **kwargs)
            if not result[0]:
                completed = True
            else:
                time.sleep(5)

        # in case of failure print wpa_supplicant.log and exit
        if result[0]:
            print(self.get_wpa_supplicant_log(ifname=ifname), **new_kwargs)
            dmesg_output = self.run_command('dmesg -T | tail -n 100', skip_exception=True)
            log.info(f'\n\nDMESG OUTPUT: \n\n:{pprint.pformat(dmesg_output)}')
            self.last_cmd['command'] = command  # Update the command cache for returned result
            return result
        if dhclient:
            dhcp_result = self.start_dhcp_client(ifname, ipv4=ipv4, ipv6=ipv6, ipv6_stateless=ipv6_stateless, **kwargs)
            result = self.merge_result(result, dhcp_result)
        return result

    def start_dhcp_client(self, ifname, cf=None, ipv4=True, ipv6=False, ipv6_stateless=False, **kwargs):
        """
        Starts dhcp client on wifi iface
        Args:
            ifname: (str) wlan iface name
            cf: (str) custom config file for dhclient
            ipv4 (bool) get IPv4 address
            ipv6 (bool) get IPv6 address
            ipv6_stateless (bool) IPv6 stateful/stateless mode

        Returns: (list) clients response
        """
        if ipv6 is False and ipv6_stateless:
            return [1, '', 'Inappropriate IPv6 configuration, enable ipv6 to use ipv6_stateless mode']
        ifname = ifname if ifname else self.get_wlan_iface()
        if not ifname:
            return [1, '', 'Missing wlan interface']
        command = f"sudo ps aux | grep /var/run/dhclient.{ifname}.pid | grep -v grep | awk '{{print $2}}' | " \
                  f"xargs kill"
        self.run_command(command, **kwargs)
        time.sleep(0.2)

        ip = ''
        ip += '-4 ' if ipv4 else ''
        ip += '-6 ' if ipv6 else ''
        ip += '-S ' if ipv6_stateless else ''

        if '-6 ' in ip:
            self.run_command(f'rdisc6 {ifname}')

        if cf:
            command = "echo '{}' > /tmp/dhclient_{}.conf".format(cf, ifname)
            self.run_command(command, **kwargs)
            command = "sudo /sbin/dhclient {0} -pf /var/run/dhclient.{1}.pid -cf /tmp/dhclient_{1}.conf {1}"\
                .format(ip, ifname)
        else:
            command = "sudo /sbin/dhclient {0} -pf /var/run/dhclient.{1}.pid {1}".format(ip, ifname)
        log.info(f'Start dhclient for {ifname}')
        return self.run_command(command, **kwargs)

    def stop_dhcp_client(self, ifname, ipv4=True, ipv6=False, ipv6_stateless=False, **kwargs):
        ip = ''
        ip += '-4 ' if ipv4 else ''
        ip += '-6 ' if ipv6 else ''
        ip += '-S ' if ipv6_stateless else ''

        return self.run_command(f'sudo dhclient {ip} -r {ifname}', **kwargs)

    def disconnect(self, ifname, clean_inteface=True, **kwargs):
        """
        Kills the wpa_supplicant and dhclient (if exists) based on the pid file in name
        Args:
            ifname: wlan iface name

        Returns: (list) merged clients response
        """
        ifname = ifname if ifname else self.get_wlan_iface()
        if not ifname:
            return [1, '', 'Missing wlan interface']

        wpa_path_ifname = f'/tmp/wpa_supplicant_{ifname}.pid'
        command = f"sudo ps aux | grep -P {wpa_path_ifname} | grep -v grep"
        response = self.run_command(command, **kwargs)
        if not response[1]:
            return [0, f'Wpa supplicant not running for ifname: {ifname}', '']

        log.info(f"Disconnect client: {self.get_name()}, ifname: {ifname}")

        # Stop dhclient
        command = f"sudo ps aux | grep /var/run/dhclient.{ifname}.pid | grep -v grep | awk '{{print $2}}' | " \
                  f"xargs sudo kill"
        response = self.run_command(command, **kwargs)
        if response[0] and "Usage:" not in response[2]:
            log.warning(f'Unable to kill dhclient for {self.device.name}, error: {response[2]}')

        if clean_inteface:
            command = f"sudo ifconfig {ifname} 0.0.0.0"
            self.run_command(command, **kwargs)

        # Stop wpa
        command = f"sudo ps aux | grep -P {wpa_path_ifname} | grep -v grep | awk '{{print $2}}' | " \
                  f"xargs sudo kill"
        response = self.run_command(command, **kwargs)
        if response[0] and "Usage:" not in response[2]:
            log.warning(f'Unable to kill wpa_supplicant for {self.device.name}, error: {response[2]}')

        # Remove wpa supplicant log file
        if self.result_ok(self.run_command(f'ls {wpa_path_ifname}', **kwargs)):
            command = f"sudo rm {wpa_path_ifname}"
            self.run_command(command, **kwargs)
        return [0, '', '']

    def scan(self, ifname="", params='', **kwargs):
        """
        Trigger flush scan on the client
        Args:
            ifname: (str) interface name
            params: (str) arguments for 'iw scan' command

        Returns: (list) clients response scan response
        """
        ifname = ifname if ifname else self.get_wlan_iface()
        if not ifname:
            return [1, '', 'Missing wlan interface']
        # make sure interface is up
        command = f"sudo ifconfig {ifname} up"
        self.run_command(command, **kwargs)

        command = f"sudo iw {ifname} scan flush {params}"

        # Sometimes if device is connected to network we getting 'Device or resource busy (-16)'
        timeout = time.time() + 60
        output = ''
        while timeout > time.time():
            output = self.run_command(command, **kwargs)
            if output[0] == 0:
                break
            time.sleep(5)
        return output

    def check(self, **kwargs):
        """Check if home-ap interfaces are up"""
        tool_path = self.get_tool_path()
        return self.run_command('{}/client-check'.format(tool_path), **kwargs)

    def pt_init(self, **kwargs):
        """Performance Tent Init"""
        tool_path = self.get_tool_path()
        return self.run_command("{0}/pt-init".format(tool_path), **kwargs)

    def client_type(self, **kwargs):
        """Display type of client(s)"""
        result = self.run_command("cat /proc/cmdline | grep -o board=[^[:space:]]* | cut -d'=' -f2 | head -1", **kwargs)
        return self.strip_stdout_result(result)

    def get_5g_int(self, **kwargs):
        """Display 5Ghz capable wifi interfaces on the client"""
        # TODO: change for get_wlan_iface
        cmd_wlans = 'sudo iwconfig 2>&1 | grep 802.11 | cut -d " " -f 1'
        kwargs['skip_exception'] = True
        wlan_interface = self.get_stdout(self.run_command(cmd_wlans, **kwargs))
        if not wlan_interface:
            return [1, '', 'No WLAN interface']
        cmd = 'sudo iwlist {} freq | grep "5\..*GHz"'.format(wlan_interface.strip())  # noqa: check if valid

        frequencies_5g = self.get_stdout(self.run_command(cmd, **kwargs))
        wlan = wlan_interface.strip()
        if frequencies_5g:
            response = [0, wlan, '']
        else:
            response = [1, '', 'Wlan: {} not supporting 5GHz'.format(wlan)]
        return response

    def get_client_ips(self, interface, ipv6_prefix=None, **kwargs):
        interface = interface if interface else self.get_wlan_iface()
        ip_adds = {'ipv4': False,
                   'ipv6': False}
        result = self.get_stdout(self.run_command("/sbin/ifconfig {}".format(interface), **kwargs))
        if result:
            if "inet " in result.strip() or "inet6 " in result.strip():
                for line in result.splitlines():
                    if "inet addr:" in line:
                        ip_adds['ipv4'] = line.strip().split(" ")[1].split(":")[1].strip()
                    elif "inet " in line:
                        ip_adds['ipv4'] = line.strip().split(" ")[1]
                    if ipv6_prefix and ipv6_prefix not in line:
                        continue
                    if "inet6 addr:" in line:
                        ip_adds['ipv6'] = line.strip().split(" ")[2].split("/")[0].strip()
                    elif "inet6 " in line:
                        ip_adds['ipv6'] = line.strip().split(" ")[1]
        return ip_adds

    # TODO: This method always works only for static namespace (old framework bug)
    def restart_eth_namespace(self, **kwargs):
        command = 'sudo /etc/network/eth1_101 down', 'sudo /etc/network/eth1_101 up'
        response = False
        # Try 3 times before exit
        i = 0
        while not response or i < 2:
            cmd = "'%s' > /dev/null 2>&1" % command
            result = self.run_command(cmd, **kwargs)
            response = False
            if result[0] == 0:
                response = True
            time.sleep(5)
            i += 1
        return response

    def ping_v4_v6_arp(self, destination, version, wlan_test, count='8', **kwargs):
        ping_cmd = ""
        if version == 'v4':
            ping_cmd = f"sudo /bin/ping -I {wlan_test} -c {count} {destination}"

        elif version == 'v6':
            ping_cmd = f"sudo /bin/ping6 -c {count} -I {wlan_test} {destination}"

        elif version == 'arp':
            ping_cmd = f"sudo /usr/sbin/arping -I {wlan_test} -c {count} {destination}"
        result = self.run_command(ping_cmd, **kwargs)
        return self.strip_stdout_result(result)

    def ping_ndisc6(self, destination, ifname=None, **kwargs):
        ifname = ifname if ifname else self.get_wlan_iface()
        result = self.run_command(f"sudo /usr/bin/ndisc6 {destination} {ifname}", **kwargs)
        return self.strip_stdout_result(result)

    def restart_networking(self, **kwargs):
        result = self.run_command("sudo /etc/init.d/networking restart &", **kwargs)
        if not self.result_ok(result):
            return False
        return True

    def start_continuous_ping(self, ifname, file_path="/tmp/ping.log", wait='1', target='8.8.8.8', **kwargs):
        self.run_command("sudo killall ping", **kwargs)
        cmd_output = self.run_command("/bin/ping -I {} -W {} {} > {} &".format(ifname, wait, target, file_path))
        if not cmd_output[0] == 0:
            return False
        return True

    def stop_continuous_ping(self, file_path="/tmp/ping.log", target='8.8.8.8', threshold=2, **kwargs):
        response = {'result': False, 'all_pings': None, 'success_ping': None, 'missed_ping': None, 'max_time': None}
        self.run_command('killall ping', **kwargs)
        cmd_output = self.run_command("cat {}".format(file_path), **kwargs)
        self.run_command("rm /tmp/ping.log", **kwargs)
        if cmd_output[0] != 0:
            return response
        ping_res = cmd_output[1].strip()
        if len(ping_res) == 0:
            return response
        ping_list = ping_res.split('\n')[1:-1]
        all_pings = int(ping_list[-1].split('=')[1].split()[0])

        successful_ping_list = []
        for ping in ping_list:
            if target in ping:
                successful_ping_list.append(ping)

        successful_ping = len(successful_ping_list)
        missed_pings = all_pings - successful_ping

        new_time_list = []
        for row in successful_ping_list:
            time_list = row.split()[-2:]
            time_ping = "{}{}".format(time_list[0].split('=')[1], time_list[1])
            new_time_list.append(time_ping)
        max_time = max(new_time_list)

        if missed_pings < int(threshold):
            response['result'] = True

        response['all_pings'] = all_pings
        response['success_ping'] = successful_ping
        response['missed_ping'] = missed_pings
        response['max_time'] = max_time
        return response

    def set_hostname(self, new):
        """
        Change client hostname
        Args:
            new: (str) New client hostname
        Returns:

        """
        old_name = self.get_stdout(self.strip_stdout_result(self.run_command('hostname')))
        return self.run_command(f'sudo hostname {new}; '
                                f'sudo -h localhost sed -i "s/{old_name}/{new}/g" /etc/hosts; '
                                f'sudo -h localhost sed -i "s/{old_name}/{new}/g" /etc/hostname')

    def set_tx_power(self, ifname, tx_power_db, **kwargs):
        """
        Set client tx power for iface

        Args:
            ifname: (str) client interface name
            tx_power_db: (int) tx power value to set (dBm)

        Returns:

        """
        txpower = tx_power_db * 100
        command = f'sudo /sbin/iw {ifname} set txpower fixed {txpower}'
        return self.run_command(command, **kwargs)

    def get_frequency(self, ifname, **kwargs):
        """
        Provide operating frequency for client iface when connected to AP
        Args:
            ifname: (str) name of wlan interface

        Returns: (int) frequency in MHz

        """
        ifname = ifname if ifname else self.get_wlan_iface()
        freq = None
        wifi_info = self.get_stdout(self.run_command(f'sudo iw {ifname} link', **kwargs)).splitlines()
        for line in wifi_info:
            if 'freq:' in line:
                freq = line.split(':')[1].strip()
                break

        return [0, freq, ''] if freq else [1, '', f'Frequency on {ifname} not found']

    def pod_to_client(self, **kwargs):
        """
        Change pod to client
        Returns:

        """
        raise NotImplementedError('This method is implemented for specific client only')

    def client_to_pod(self, **kwargs):
        """
        Change client to pod
        Returns:

        """
        raise NotImplementedError('This method is implemented for specific client only')

    def create_ap(self, channel, hw_mode=None, ifname='', ssid='test', extra_param='', **kwargs):
        """
        Create access point on the client, by the use hostap
        Args:
            channel: (int) access point channel
            hw_mode: (str) hw mode 'g' for 2.4G 'a' for 5G
            ifname: (str) name of interface
            ssid: (str) network SSID
            extra_param: whatever you ave in mind to include in a config

        Returns:

        """
        ifname = ifname if ifname else self.get_wlan_iface()
        hw_mode = 'g' if 1 <= channel <= 13 else 'a'
        extra_param = extra_param if extra_param else ''
        hostapd_cfg = f"""ctrl_interface=/var/run/hostapd
interface={ifname}
ssid={ssid}

hw_mode={hw_mode}
channel={channel}
{extra_param}"""

        # make sure that hostap is not running
        self.disable_ap(ifname, **kwargs)

        self.run_command(f'sudo rm /tmp/hostapd_{ifname}.conf')
        # save hostap.conf on the client
        command = f"echo '{hostapd_cfg}' > /tmp/hostapd_{ifname}.conf"
        self.run_command(command, **kwargs)

        command = f'hostapd -f /tmp/hostapd_{ifname}.log -t -B -P /tmp/hostapd_{ifname}.pid /tmp/hostapd_{ifname}.conf'
        response = self.run_command(command, **kwargs)
        return response

    def disable_ap(self, ifname='', **kwargs):
        ifname = ifname if ifname else self.get_wlan_iface()
        hostapd_path = f'/tmp/hostapd_{ifname}.pid'
        command = f"sudo ps aux | grep -P {hostapd_path} | grep -v grep"
        response = self.run_command(command, **kwargs)
        if not response[1]:
            return [0, f'Hostapd not running for ifname: {ifname}', '']

        command = f"sudo ps aux | grep -P {hostapd_path} | grep -v grep | awk '{{print $2}}' | " \
                  f"xargs sudo kill"

        response = self.run_command(command, **kwargs)
        if response[0] and "Usage:" not in response[2]:
            log.warning(f'Unable to kill hostapd for {self.device.name}, error: {response[2]}')

        # Remove hostapd log file
        if self.result_ok(self.run_command(f'ls {hostapd_path}', **kwargs)):
            command = f"sudo rm {hostapd_path}"
            self.run_command(command, **kwargs)
        return [0, '', '']

    def refresh_ip_address(self, iface=None, ipv4=True, ipv6=False, ipv6_stateless=False, timeout=10, reuse=False,
                           **kwargs):
        """
        Refresh ip_address.
        Args:
            iface: (str) name of interface
            timeout: (int) timeout for dhclient
            ipv4 (bool) get IPv4 address
            ipv6 (bool) get IPv6 address
            ipv6_stateless (bool) IPv6 stateful/stateless mode
            reuse (bool) restarts dhclient with existing dhclient arguments
            **kwargs:

        Returns: (ret_val, std_out, str_err)

        """
        iface = iface if iface else self.get_wlan_iface(skip_exception=True)
        iface = iface if iface else self.get_eth_iface(skip_exception=True)
        assert iface, f'No found interface name'

        ip = ''
        ip += '-4 ' if ipv4 else ''
        ip += '-6 ' if ipv6 else ''
        ip += '-S ' if ipv6_stateless else ''

        reuse_only = kwargs.pop('reuse_only', False)
        dhclient = f'dhclient -v {ip}-pf /var/run/dhclient.{iface}.pid {iface}'
        if reuse:
            cur_dhclient = self.run_command(f"sudo ps aux | grep $(cat /var/run/dhclient.{iface}.pid) | grep -v grep",
                                            **kwargs)
            if cur_dhclient[0]:
                if reuse_only:
                    return [1, '', 'No dhclient running, nothing to restart']
                log.warning('Cannot get running dhclient. Starting with default parameters')
            else:
                dhclient = cur_dhclient[1][cur_dhclient[1].find('dhclient'):]

        rdisk6_result = ''
        if '-6 ' in dhclient:
            rdisk6_result = self.run_command(f'rdisc6 {iface}')

        self.run_command(f'sudo kill $(cat /var/run/dhclient.{iface}.pid)', **kwargs)
        self.run_command(f'sudo dhclient -r {iface}; sudo dhclient -r -6 {iface}', **kwargs)
        self.run_command(f'sudo ifconfig {iface} 0.0.0.0', **kwargs)

        out = self.run_command(f'timeout {timeout} sudo {dhclient}', timeout=timeout + 20)
        if rdisk6_result:
            out = self.merge_result(out, rdisk6_result)
        return out

    def set_mac(self, interface, new_mac, **kwargs):
        self.run_command(f'sudo ifconfig {interface} down', **kwargs)
        return self.run_command(f'sudo ifconfig {interface} hw ether {new_mac}', **kwargs)

    def set_ip_address(self, ip, netmask='255.255.255.0', iface=None, **kwargs):
        iface = iface if iface else self.get_wlan_iface(skip_exception=True)
        iface = iface if iface else self.get_eth_iface(skip_exception=True)
        assert iface, f'No interface found'
        netmask = '' if ip == '0.0.0.0' else f" netmask {netmask}"
        return self.run_command(f'sudo ifconfig {iface} {ip}{netmask}', **kwargs)

    def upgrade(self, fw_path=None, restore_cfg=True, force=False, **kwargs):
        def wait_for_upgrade_completed():
            log.info(f"Client {self.get_nickname()} has upgrade in progress, waiting")
            timeout_min = 20
            timeout = time.time() + timeout_min * 60
            while time.time() < timeout:
                if self.run_command("ls /tmp/upgrade_in_progress")[0]:
                    log.info(f"Client {self.get_nickname()} upgrade finished")
                    return self.version()
                time.sleep(5)
            return [3, '', f'Upgrade not finished within {timeout_min} minutes']

        def get_latest_fw_ver():
            base_dir = os.path.dirname(os.path.realpath(__file__))
            client_install = os.path.join(base_dir, 'deploy', 'client_install.sh')
            with open(client_install) as cl_inst:
                for line in cl_inst.readlines():
                    if 'version=' not in line:
                        continue
                    ver = line.replace("version=", '').replace('"', '').replace('\n', '')
                    return ver
            assert False, f'Cannot get latest version from {client_install}'

        log.info('Upgrading linux client')
        latest_fw = get_latest_fw_ver()
        if self.get_stdout(self.version()) == latest_fw:
            if not force:
                return [0, f"{self.get_nickname()} has already {latest_fw}, skipping upgrade", '']
            log.info(f"{self.get_nickname()} has already {latest_fw}, but upgrade is forced")

        # skip client, which do not have Internet
        if self.refresh_ip_address(reuse=True, reuse_only=True)[0]:
            log.info(f"{self.get_nickname()} dhclient failed, upgrade not possible")
            return wait_for_upgrade_completed()

        # first ping sometimes fails in case dead device was removed
        self.ping_check(count=3)

        if self.ping_check()[0]:
            log.info(f"{self.get_nickname()} has not internet access, upgrade not possible")
            return wait_for_upgrade_completed()

        # short sleep to spread threads in time
        time.sleep(random.randint(1, 20) / 10)

        if not self.run_command("ls /tmp/upgrade_in_progress")[0]:
            return wait_for_upgrade_completed()
        self.run_command("touch /tmp/upgrade_in_progress")
        # copy client_install.sh
        res = self.deploy(**kwargs)
        if res[0]:
            self.run_command("rm /tmp/upgrade_in_progress")
            return res

        # execute client_install.sh
        out = self.run_command(f"{self.config[TBCFG_CLIENT_DEPLOY]}/client_install.sh")
        res = self.merge_result(res, out)
        if res[0]:
            self.run_command("rm /tmp/upgrade_in_progress")
            return res

        # update authorized_keys from rpi-server
        from lib_testbed.client.client import Clients
        kwargs['config'] = self.config
        kwargs['multi_obj'] = False
        kwargs['nicknames'] = ['host']
        kwargs['type'] = 'rpi'
        clients_obj = Clients(**kwargs)
        clients_api = clients_obj.resolve_obj(**kwargs)
        out = clients_api.run_raw(f"scp -o stricthostkeychecking=no -o userknownhostsfile=/dev/null "
                                  f"/home/plume/.ssh/authorized_keys "
                                  f"{self.device.config['host']['user']}@{self.device.config['host']['name']}:"
                                  f"/home/plume/.ssh/authorized_keys")

        res = self.merge_result(res, out[0])
        self.reboot()
        self.wait_available(5 * 60)
        self.run_command("rm /tmp/upgrade_in_progress")
        return res

    def start_mqtt_broker(self, **kwargs):
        # It is designed for rpi server only
        raise NotImplementedError("Rpi-server only")

    def stop_mqtt_broker(self, **kwargs):
        # It is designed for rpi server only
        raise NotImplementedError("Rpi-server only")


class ClientIface(Iface):
    pass
