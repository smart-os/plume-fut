import importlib

from lib_testbed.util.logger import log
from lib_testbed.util.object_factory import ObjectFactory


class Switch(ObjectFactory):
    def resolve_obj(self, **kwargs):
        config = kwargs.get("config")
        if not config:
            raise Exception("Missing config in kwargs: {}".format(kwargs))
        module_name = "switchlib"
        class_name = "SwitchLib"
        kwargs = {"config": config}
        module_path = ".".join(["lib_testbed", "switch", module_name])
        try:
            module = importlib.import_module(module_path)
            _class = getattr(module, class_name)
            log.debug("Calling {}.{}".format(module_path, class_name))
            return _class(**kwargs)
        except ImportError:
            raise Exception("Class: {} not implemented, expected path: {}.py".format(
                class_name, module_path))
