import re
import time
import random
from lib_testbed.util.switch import switchlib
from lib_testbed.util.logger import log


class SwitchLib:
    def __init__(self, **kwargs):
        self.config = kwargs.get("config")
        switchlib.switch_init_config(self.config, testCase=False)

    @staticmethod
    def get_switch_alias(pod_name, all_ports=False):
        """
        Method for get switch configuration from tb config
        Args:
            pod_name: (str) pod name
            all_ports: (bool) Return information for all ports

        Returns: (tuple) (vlan_name, vlan_port, vlan_blackhaul) if all_ports is False
        otherwise (list) [(tuple) (vlan_name, vlan_port, vlan_blackhaul))

        """
        port_names = [port_name for port_name in switchlib._switch_node_ports if pod_name in port_name]
        if port_names:
            if all_ports:
                return [switchlib._switch_information[port_name] for port_name in port_names]
            else:
                return switchlib._switch_information[random.choice(port_names)]
        return None, None, None

    @staticmethod
    def change_untagged_vlan(port_name, target_vlan):
        """
        Method for change untagged vlan on switch
        Args:
            port_name: (str) port_name from tb config
            target_vlan:   (int) vlan number which will set

        Returns: (bool)

        """
        switchlib.interface_down([port_name])
        switchlib.vlan_set([port_name], target_vlan, 'untagged')
        time.sleep(10)
        switchlib.interface_up([port_name])
        port_info = SwitchLib.switch_info(port_name)

        if str(target_vlan) not in port_info:
            log.error(f"Current port info: {port_name}, target vlan: {target_vlan}")
            log.error('Wan port action finished unsuccessfully, trying again')
            switchlib.interface_down([port_name])
            switchlib.vlan_set([port_name], target_vlan, 'untagged')
            time.sleep(10)
            switchlib.interface_up([port_name])
            port_info = SwitchLib.switch_info(port_name)
            if str(target_vlan) not in port_info:
                log.error(f"Current port info: {port_name}, target vlan: {target_vlan}")
                raise Exception('Wan port action finished unsuccessfully')

        log.info(f'Change blackhaul action finished successfully on {port_name} to {target_vlan}')
        log.debug(port_info)
        return True

    @staticmethod
    def switch_info(vlan_port_names):
        """
        Methods for get actual configuration on Switch
        Args:
            vlan_port_names: For one port: (str) vlan port name
                             For multiple port (list) [(str)]

        Returns:

        """
        if isinstance(vlan_port_names, str):
            return switchlib.switch_info([vlan_port_names])[vlan_port_names][1]
        return switchlib.switch_info(vlan_port_names)

    @staticmethod
    def logout():
        """
        Method for logout from Switch
        Returns: None

        """
        return switchlib.logout()

    @staticmethod
    def disable_port(port_name):
        """
        Disable port on switch
        Args:
            port_name: (str) port name from tb config

        Returns:

        """
        return switchlib.interface_down([port_name])

    @staticmethod
    def enable_port(port_name):
        """
        Enable port on switch
        Args:
            port_name: (str) port name from tb config

        Returns:

        """
        return switchlib.interface_up([port_name])

    @staticmethod
    def get_interface_state(port_name):
        """
        Get interface status (enable/disable)
        Args:
            port_name: (str) port name from tb config

        Returns: (bool) If enable return True else False

        """
        state = switchlib.switch_status([port_name])[port_name]
        state = True if 'Enable' in state[1] else False
        return state

    @staticmethod
    def set_link_speed(port_name, link_speed):
        """
        Set link speed on port
        Args:
            port_name: (str) port name from tb config
            link_speed: (int) link speed (10, 100, 1000) or (str) 'auto'

        Returns: list(list((str))) current port information about link speed

        """
        results = switchlib.set_link_speed([port_name], link_speed)
        return results

    def switch_wan_port(self, pod_name, target_port='', disable_no_used_ports=True):
        """
        Switch wan port between two eth ports
        Args:
            pod_name: (str) pod name where we want to change port
            target_port: (str) Optional target port name eth0 or eth1
            disable_no_used_ports: (bool) Disable no used ports if True

        Returns: (str) current wan port information

        """
        port_aliases = self.get_switch_alias(pod_name, all_ports=True)
        assert port_aliases, f'Not found port information for {pod_name} device'
        ports_name = [port_name[0] for port_name in port_aliases]
        assert len(ports_name) == 2, f'This method is valid only for devices with two eth ports'
        ports_info = self.switch_info(ports_name)
        old_wan_port_name = ''
        wan_vlan = ''
        for port_name in ports_info:
            port_current_information = ports_info[port_name][1]
            wan_vlan = self.get_wan_vlan_number(port_current_information)
            if wan_vlan:
                old_wan_port_name = port_name
                break

        assert old_wan_port_name and wan_vlan, f'No found wan vlan\n{ports_info}'
        ports_name.remove(old_wan_port_name)
        new_wan_port_name = ports_name[0]

        if target_port not in new_wan_port_name:
            if disable_no_used_ports:
                self.disable_no_used_ports(device_aliases=port_aliases)
            log.info(f'Changing switch port is not needed. Current wan port: {old_wan_port_name} '
                     f'target wan port {target_port}')
            return False

        # Get default blackhole from test bed config for old wan port
        default_old_wan_blackhole = [port_alias[2] for port_alias in port_aliases
                                     if old_wan_port_name == port_alias[0]][0]

        # disabling gateway port
        self.disable_port(old_wan_port_name)

        # set target port to vlan 200
        self.change_untagged_vlan(new_wan_port_name, int(wan_vlan))

        # set blackhole to old target blackhole on old gateway port
        self.change_untagged_vlan(old_wan_port_name, default_old_wan_blackhole)

        if disable_no_used_ports:
            # disable no used ports
            self.disable_no_used_ports(device_aliases=port_aliases)
        return self.switch_info(new_wan_port_name)

    def connect_eth_client(self, pod_name, client_name, connect_port=''):
        """
        Connect dynamic ethernet client by changing vlan settings
        Args:
            pod_name: (str) Pod name where we want to connect eth client
            client_name: (str) Client name which we want to connect to pod
            connect_port: (str) Connection target port eth0 or eth1. If empty get random port.
        Returns: (str) Target port where is connected eth client

        """
        port_info = self.get_switch_alias(pod_name, all_ports=True)
        assert port_info, f'Not found port information for {pod_name} device'

        # get eth client blackhole
        client_blackhole = [client.get('vlan') for client in self.config.get('Clients')
                            if client_name == client.get('name')][0]
        assert client_blackhole, f'Client blackhole has not been found for {client_name}'

        # get non wan port
        target_port = None
        for port in port_info:
            # port: (name, port_id, def_vlan)
            # skip port if is not this, which was requested
            if connect_port and connect_port not in port[0]:
                continue
            port_status = self.switch_info(port[0])
            log.debug(port_status)
            # skip internet port
            if self.get_wan_vlan_number(port_status):
                continue
            # we want only port which has default/blackhole vlan set currently
            if str(port[2]) not in port_status:
                continue
            target_port = port[0]
            break
        assert target_port, f'Target port has not been found. Probably you want to connect eth client to wan port or ' \
            f'client is already connected to another pod'

        # connect client to target port
        self.change_untagged_vlan(target_port, client_blackhole)
        # sleep for cloud to put iface to bridge
        time.sleep(15)
        return target_port

    def disconnect_eth_client(self, pod_name, client_name):
        """
        Disconnect dynamic ethernet client by changing vlan settings
        Args:
            pod_name: (str) Pod name where we want to connect eth client
            client_name: (str) Client name which we want to connect to pod

        Returns: (str) Target port where was connected eth client

        """
        # get port information
        port_info = self.get_switch_alias(pod_name, all_ports=True)
        assert port_info, f'Not found port information for {pod_name} device'
        ports_name = [port_name[0] for port_name in port_info]

        # get eth client blackhole
        client_blackhole = [client.get('vlan') for client in self.config.get('Clients')
                            if client_name == client.get('name')][0]
        assert client_blackhole, f'Client blackhole has not been found for {client_name}'

        # get port where is connected eth client
        client_port = [port_name for port_name in ports_name if str(client_blackhole) in self.switch_info(port_name)]
        if not client_port:
            log.info('Ethernet client is disconnected')
            return
        client_port = client_port[0]
        assert client_port, f'Port where is connected eth client has not been found'

        # get default port blackhole from test bed config
        default_old_wan_blackhole = [port_alias[2] for port_alias in port_info if client_port == port_alias[0]][0]

        # disconnect eth client from port
        self.change_untagged_vlan(client_port, default_old_wan_blackhole)

        # disable no used ports
        self.disable_no_used_ports(device_aliases=port_info)
        return client_port

    def recovery_switch_configuration(self, pod_names=None, force=False, set_default_wan=False):
        """
        Set default configuration on switch from test bed config.
        Args:
            pod_names: (list) [(str)] or (str) for single device
            force: (bool) By default False, if True recover also fake internet backhauls
            set_default_wan: (bool) Set default wan port if True

        Returns: (list) recovered switch vlans

        """
        if isinstance(pod_names, str):
            pod_names = [pod_names]

        ports_info = [self.get_switch_alias(pod_name, all_ports=True) for pod_name in pod_names]
        # get ports without None values
        ports_info = [port for port in ports_info if port[0]]
        # Applies only when any port from cfg is marked as uplink
        if set_default_wan:
            self.set_default_wan_port(ports_info, disable_no_used_ports=False)
        vlan_name_list = list()
        wan_port = 0
        for port_info in ports_info:
            for port_alias in port_info:
                vlan_name = port_alias[0]
                vlan_name_list.append(vlan_name)
                default_blackhole = port_alias[2]
                switch_info = self.switch_info(vlan_name)
                if not force and self.get_wan_vlan_number(switch_info) and wan_port == 0:
                    wan_port += 1
                    continue
                if str(default_blackhole) not in switch_info:
                    log.info(f'Setting default configuration on {vlan_name}')
                    self.change_untagged_vlan(vlan_name, default_blackhole)
            self.disable_no_used_ports(device_aliases=port_info)
        return vlan_name_list

    def set_default_wan_port(self, ports_info, disable_no_used_ports=True):
        port_uplink = None
        port_names = self.get_port_names_from_aliases(ports_info)
        for port_name in port_names:
            port_role = self.get_role_of_port(port_name)
            if 'uplink' in port_role:
                port_uplink = port_name
                break
        if not port_uplink:
            return
        port_uplink = port_uplink.split('_')
        device_name = port_uplink[0]
        target_port = port_uplink[1]
        log.info(f'Setting default wan port for {device_name} to {target_port}')
        self.switch_wan_port(device_name, target_port, disable_no_used_ports)

    @staticmethod
    def get_port_names_from_aliases(ports_aliases):
        port_names = []
        for pod_port_info in ports_aliases:
            for port_alias in pod_port_info:
                port_names.append(port_alias[0])
        return port_names

    def disable_no_used_ports(self, dev_name=None, device_aliases=None):
        use_port = None
        switch_aliases = self.get_switch_alias(dev_name, all_ports=True) if dev_name else device_aliases
        if None not in switch_aliases and len(switch_aliases) >= 1:
            port_names = [switch_alias[0] for switch_alias in switch_aliases]
            use_port = [port_name for port_name in port_names if not use_port
                        and self.get_wan_vlan_number(self.switch_info(port_name))]
            if use_port:
                use_port = use_port[0]
                # Make sure that use port is enable
                if not self.get_interface_state(use_port):
                    self.enable_port(use_port)
                log.info(f'Used port: {use_port}')
                port_names.remove(use_port)
            for port_name in port_names:
                port_role = self.get_role_of_port(port_name)
                port_state = self.get_interface_state(port_name)
                if 'mn' in port_role:
                    log.info(f'Management used port: {port_name}')
                    if not port_state:
                        self.enable_port(port_name)
                    continue
                log.info(f'No used port: {port_name}')
                if port_state:
                    log.info(f'Disabling no used port {port_name}')
                    self.disable_port(port_name)

    def get_role_of_port(self, vlan_port_name):
        """
        Get role of vlan port which is described in switch configuration (mn, lan)
        Args:
            vlan_port_name: (str) Name of vlan port

        Returns: (list)

        """
        vlan_roles = [vlan_role['switch'][vlan_port_name] for vlan_role in self.config['Nodes']
                      if vlan_port_name in vlan_role.get('switch', [])]
        vlan_roles = vlan_roles[0] if vlan_roles else vlan_roles
        vlan_roles = [vlan_role.lower() for vlan_role in vlan_roles] if vlan_roles else vlan_roles
        return vlan_roles

    def set_connection_ip_type(self, pod_name, ip_type):
        """
        Set connection ip type
        Args:
            pod_name: (str) Name of device
            ip_type: (str) One of the ip type from [ipv4, ipv6_stateful, ipv6_stateless, ipv6_slaac] list

        Returns: None

        """
        ports_aliases = self.get_switch_alias(pod_name, all_ports=True)
        ports_name = [port_alias[0] for port_alias in ports_aliases]
        target_port = [port_name for port_name in ports_name if self.get_wan_vlan_number(self.switch_info(port_name))]
        target_port = target_port if target_port else [ports_name[0]]
        result = switchlib.set_connection_ip_type(target_port, ip_type)[target_port[0]]
        assert result[0] == 0, f'{result[2]}'

    def get_connection_ip_type(self, pod_name):
        """
        Get connection ip type
        Args:
            pod_name: (str) Name of device

        Returns: (str) Used ip type from [ipv4, ipv6_stateful, ipv6_stateless, ipv6_slaac]. Empty string in case none
         above.
        """
        ports_aliases = self.get_switch_alias(pod_name, all_ports=True)
        ports_name = [port_alias[0] for port_alias in ports_aliases]
        vlan = ''
        for port_name in ports_name:
            vlan = self.get_wan_vlan_number(self.switch_info(port_name))
            if vlan:
                break
        vlan_map = {
            '200': 'ipv4',
            '201': 'ipv6_stateful',
            '202': 'ipv6_stateless',
            '203': 'ipv6_slaac'
        }
        return vlan_map.get(vlan, '')

    def get_wan_port(self, pod_name):
        """
        Get wan port from target device
        Args:
            pod_name: (str) name of device

        Returns:

        """
        ports_aliases = self.get_switch_alias(pod_name, all_ports=True)
        ports_name = [port_alias[0] for port_alias in ports_aliases]
        ports_info = self.switch_info(ports_name)
        wan_port = ''
        for port_name in ports_name:
            port_info = ports_info[port_name][1]
            if self.get_wan_vlan_number(port_info):
                wan_port = port_name
                break
        assert wan_port, f'Wan port no found for the following device: {pod_name}.\n{ports_info}'
        log.info(f'Wan port for {pod_name}: {wan_port}')
        return wan_port

    @staticmethod
    def get_forward_ports_isolation(vlan_port_names):
        """
        Methods for get forward ports isolation from interface
        Args:
            vlan_port_names: For one port: (str) vlan port name
                             For multiple port (list) [(str)]

        Returns:

        """
        if isinstance(vlan_port_names, str):
            return switchlib.get_forward_port_isolation([vlan_port_names])[vlan_port_names][1]
        return switchlib.get_forward_port_isolation(vlan_port_names)

    @staticmethod
    def set_forward_ports_isolation(vlan_port_names, ports_isolation):
        """
        Set forward ports isolation on target port
        Args:
            vlan_port_names: For one port: (str) vlan port name
                             For multiple port (list) [(str)]
            ports_isolation: (list) list of ports isolation for example (int(),int()) or (str)
            for example '1/0/1-2,1/0/4'

        Returns:

        """
        if isinstance(ports_isolation, list):
            ports_isolation = [f'1/0/{port_isolation}' for port_isolation in ports_isolation]
            ports_isolation = ','.join(ports_isolation)
        if isinstance(vlan_port_names, str):
            return switchlib.set_forward_port_isolation([vlan_port_names], ports_isolation)[vlan_port_names][1]
        return switchlib.set_forward_port_isolation(vlan_port_names, ports_isolation)

    @staticmethod
    def disable_ports_isolation(vlan_port_names):
        """
        Disable ports isolation on target port
        Args:
            vlan_port_names: For one port: (str) vlan port name
                             For multiple port (list) [(str)]
        Returns: None

        """
        if isinstance(vlan_port_names, str):
            return switchlib.disable_port_isolation([vlan_port_names])[vlan_port_names][1]
        return switchlib.disable_port_isolation(vlan_port_names)

    @staticmethod
    def get_wan_vlan_number(port_info):
        result = re.findall('2[0-1][0-9]', port_info)
        return result[0] if result else ''
