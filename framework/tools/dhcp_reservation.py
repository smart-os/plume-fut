#!/usr/bin/env python3

from copy import deepcopy
import argparse
import os
import sys
_basedir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(_basedir)
from config.testbed.config import testbed_cfg  # noqa: E402
from framework.lib.config import Config  # noqa: E402
from framework.lib.dhcpsetuplib import DhcpSetup  # noqa: E402
from framework.lib.switchlib import NetworkSwitch  # noqa: E402
import framework.tools.logger as logger  # noqa: E402

log = logger.FutLoggerAdapter(extra={logger.LOGR_ATTR: '[DHCP_UTIL]'})

if __name__ == '__main__':
    tool_description = """DHCP server configuration tool"""
    dhcpd_conf_path = '/etc/dhcp'
    dhcpd_conf_file = 'dhcpd.conf'
    dhcpd_conf_filepath = f'{dhcpd_conf_path}/{dhcpd_conf_file}'
    help_msg = 'Creating dhcpd reservations on RPI server, based on testbed configuration.\n' \
               'Any device connected to network switch will get specified reserved IPs'
    def_device_list = [
        'dut',
        'ref',
        'ref2',
        'client',
        'client2',
    ]

    # Configuration class instance
    testbed = Config(testbed_cfg)
    def_host = testbed.get('network_switch.host')
    def_username = testbed.get('network_switch.username')
    def_password = testbed.get('network_switch.password')
    def_switch_name = testbed.get('network_switch.name')

    parser = argparse.ArgumentParser(
        description=tool_description,
        formatter_class=argparse.RawTextHelpFormatter
    )

    parser.add_argument(
        '--hostname', '--ip', '-i',
        required=False,
        default=def_host,
        type=str,
        help='Name/ip address of the network switch to configure\n'
    )
    parser.add_argument(
        '--username', '-u',
        required=False,
        default=def_username,
        type=str,
        help='Username for network switch access\n'
    )
    parser.add_argument(
        '--password', '-p',
        required=False,
        default=def_password,
        type=str,
        help='Password for network switch access\n'
    )
    parser.add_argument(
        '--devices', '-d',
        required=False,
        default=def_device_list,
        choices=def_device_list,
        nargs='+',
        type=str,
        help=f'List of devices to configure: {def_device_list}\n'
    )
    parser.add_argument(
        '--dhcp_reservation', '-r',
        action='store_true',
        help=help_msg
    )

    # Retrieve input arguments
    opts = parser.parse_args()

    # EXECUTE OPERATIONS
    if opts.dhcp_reservation:
        # Enforce root privileges when editing dhcpd configuration
        if os.geteuid() != 0:
            log.critical(msg='Script does not have root privilege, exiting. Run with sudo.')
            sys.exit(-1)
        for msg in help_msg.split(sep='\n'):
            log.info(msg=msg)
        # Network switch management class
        network_switch = NetworkSwitch(
            host=opts.hostname,
            username=opts.username,
            password=opts.password,
            switch_name=def_switch_name
        )
        # Returns mac table as dict, per port if specified
        interface_cfg = testbed.get('network_switch.interface')
        dhcp_device_config = {}
        for device in opts.devices:
            # Ensure selected devices are valid
            if device not in def_device_list:
                raise Exception(f'Device {device} not in allowed device list {def_device_list}')
            # Get network switch port and management vlan for each device
            device_cfg = testbed.get(device)
            dhcp_device_config[device] = deepcopy(device_cfg)
            nswitch_port_cfg = [c for c in interface_cfg if c['name'] == device_cfg['tb_role']][0]
            port = nswitch_port_cfg.get('port')
            mgmt_vlan = device_cfg['mgmt_vlan']
            # Ask network switch for mac address for each port
            device_mac = network_switch.get_port_mac(port=port, vlan=mgmt_vlan)
            dhcp_device_config[device]['mac_addr'] = device_mac
            if device_mac is not None:
                log.info(msg=f'{device} on port:{port} with mgmt_vlan:{mgmt_vlan} and MAC {device_mac}')
            else:
                log.info(msg=f'{device} not connected to port:{port}')

        log.warning(msg='If devices are connected, but no MAC address was found, '
                        'try to reboot them to generate traffic on the port.')
        # Create dhcp reservations on RPI server
        dhcp_setup = DhcpSetup(dhcpd_conf_filepath)
        dhcp_setup.dhcp_reservation(device_config=dhcp_device_config)
        log.info(msg='DHCP reservation setup finished without errors.')
        sys.exit(0)

    # No arguments provided
    parser.print_help(sys.stderr)
