#!/usr/bin/env python3

from copy import deepcopy
import argparse
import difflib
import glob
import importlib.util
import json
import os
import re
import sys
sys.path.append(os.path.realpath(__file__).split('fut-base')[0])


object_open_regex = re.compile(r'(\")(.*?)(\")(\s*):(\s*){')
object_key_regex = re.compile(r'(\")(.*?)(\")(\s*):(\s*)')
list_key_regex = re.compile(r'(\")(.*?)(\")(\s*):(\s*)\[')

indent_spaces = 4


def copy_rename(old_file_path, new_file_path):
    try:
        backup_cfg_file = open(new_file_path, 'w')
        with open(old_file_path, 'r') as org_cfg_file:
            backup_cfg_file.write(org_cfg_file.read())
            backup_cfg_file.close()
        print(f'File created in: {new_file_path}')
    except Exception as e:
        print(f'Could not create file: {new_file_path}')
        print(f'Old file: {old_file_path}')
        print(e)
        sys.exit(1)


def insert_trailing_comma(sorted_cfg_lines):
    """ Insert trailing comma to dicts, lists.
    Doing this manually, one would:
      find:     (^(?![\}])((?![\[\{,]).)*$)
      replace:  $1,
    Known limitation: does not support adding trailing comma to empty dict
    """  # noqa
    if isinstance(sorted_cfg_lines, str):
        sorted_cfg_lines_with_comma = re.sub(
            r'(^(?!\})((?![\[\{,]).)*$)',
            r'\1,',
            sorted_cfg_lines
        )
    elif isinstance(sorted_cfg_lines, list):
        sorted_cfg_lines_with_comma = []
        for line in sorted_cfg_lines:
            sorted_cfg_lines_with_comma.append(insert_trailing_comma(line))
    else:
        raise ValueError
    return sorted_cfg_lines_with_comma


def map_comments_to_lines(cfg_lines, var_name):
    cfg_lines_count = len(cfg_lines)
    mapped_cfg_lines = []
    comment_map = []
    skip_n_lines = 0
    comment_id = 1000
    for cfg_line_index in range(cfg_lines_count):
        if skip_n_lines > 1:
            skip_n_lines -= 1
            continue

        cfg_line_index = cfg_line_index
        current_cfg_line = cfg_lines[cfg_line_index]

        if current_cfg_line == '\n':
            continue

        first_current_character = list(current_cfg_line)[len(current_cfg_line) - len(current_cfg_line.lstrip())]

        if cfg_line_index == 0 and first_current_character == "#":
            comment_block, skip_n_lines = get_comment_block(cfg_lines=cfg_lines, start_from=cfg_line_index)
            comment_map.append({
                'comment_id': 'before_test_cfg',
                "comment_block": comment_block,
            })
        elif first_current_character == "#":
            comment_block, skip_n_lines = get_comment_block(cfg_lines=cfg_lines, start_from=cfg_line_index)

            comment_location, comment_index = get_comment_location(
                cfg_lines=cfg_lines,
                comment_index=cfg_line_index,
                var_name=var_name
            )

            comment_id_string = f'comment_identifier_{comment_id}'

            if cfg_line_index + skip_n_lines >= cfg_lines_count:
                # comments are post test_cfg
                comment_map.append({
                    'comment_id': 'after_test_cfg',
                    "comment_block": comment_block,
                })
                continue

            if comment_location == 'object':
                comment_id_string = f'comment_object_identifier_{comment_id}'
                identifier_key_line = f'"zzz_comment_object_identifier":"{comment_id_string}",\n'
                previous_line = mapped_cfg_lines[len(mapped_cfg_lines) - 1]
                if previous_line:
                    if previous_line.strip()[-1] != ',':
                        identifier_key_line = f',"zzz_comment_object_identifier":"{comment_id_string}",\n'

                if cfg_lines[cfg_line_index + skip_n_lines]:
                    next_key_value = re.search(r'(\")(.*?)(\")(\s*)(\:)', cfg_lines[cfg_line_index + skip_n_lines])
                    if next_key_value:
                        coi_str = next_key_value.group().replace('\"', '').replace(':', '')[:-1]
                        identifier_key_line = f'"{coi_str}_comment_object_identifier":"{comment_id_string}",\n'

            elif comment_location == 'array':
                identifier_key_line = f'"{comment_id_string}",\n'
                previous_line = mapped_cfg_lines[len(mapped_cfg_lines) - 1]
                if previous_line:
                    last = previous_line.strip()[-1]
                    if last != ',' and last not in ['[', '{']:
                        identifier_key_line = f',"{comment_id_string}",\n'

            elif 'after_test_cfg':
                identifier_key_line = ""

            mapped_cfg_lines.append(identifier_key_line)
            comment_map.append({
                'comment_id': comment_id_string,
                "comment_block": comment_block
            })
            comment_id += 1
        else:
            mapped_cfg_lines.append(current_cfg_line)

    return mapped_cfg_lines, comment_map


def get_comment_block(cfg_lines, start_from):
    cfg_lines_count = len(cfg_lines)
    comment_block = []
    comment_lines_count = 0
    for cfg_line_index in range(start_from, cfg_lines_count):
        current_cfg_line = cfg_lines[cfg_line_index]

        if current_cfg_line == '\n':
            continue

        first_current_character = list(current_cfg_line)[len(current_cfg_line) - len(current_cfg_line.lstrip())]

        if first_current_character == "#":
            comment_block.append(current_cfg_line)
            comment_lines_count += 1
        else:
            break

    return comment_block, comment_lines_count


def get_comment_location(cfg_lines, comment_index, var_name):
    lists_opened = 0
    lists_closed = 0
    objects_opened = 0
    objects_closed = 0
    cfg_line_index = 0

    for cfg_line_index in reversed(range(0, comment_index)):
        current_cfg_line = cfg_lines[cfg_line_index]
        if current_cfg_line == '\n':
            continue

        first_current_character = list(current_cfg_line)[len(current_cfg_line) - len(current_cfg_line.lstrip())]
        if current_cfg_line.strip() in ["}", "},"] and first_current_character != "#":
            objects_closed += 1
        elif first_current_character != "#" and (
            re.search(object_open_regex, current_cfg_line) or current_cfg_line.strip() == "{"
        ):
            objects_opened += 1
        elif current_cfg_line.strip() in ["]", "],"]:
            lists_closed += 1
        elif first_current_character != "#" and (
            re.search(list_key_regex, current_cfg_line) or current_cfg_line.strip() == "["
        ):
            lists_opened += 1
        if lists_opened > lists_closed:
            return 'array', cfg_line_index
        elif objects_opened > objects_closed or (
            first_current_character == "{" and objects_closed == 0
        ) or var_name + " = {" in current_cfg_line and first_current_character != "#":
            return 'object', cfg_line_index

    return "after_test_cfg", cfg_line_index


def write_comments_back(sorted_cfg_lines, comment_map, var_name):
    commented_cfg_lines = []
    sorted_cfg_lines_count = len(sorted_cfg_lines)
    for line_index in range(sorted_cfg_lines_count):
        sorted_line = sorted_cfg_lines[line_index]
        skip_line_insert = False
        if "comment_identifier_" in sorted_line:
            comment_block_to_insert = get_comment_from_map(
                line_with_comment=sorted_line,
                comment_map=comment_map
            )
            for comment_line in comment_block_to_insert:
                commented_cfg_lines.append(comment_line)
            continue
        elif "zzz_comment_object_identifier" in sorted_line:
            comment_block_to_insert = get_comment_from_map(
                line_with_comment=sorted_line,
                comment_map=comment_map,
                object_comment=True
            )
            for comment_line in comment_block_to_insert:
                commented_cfg_lines.append(comment_line)
            skip_line_insert = True
        elif "_comment_object_identifier" in sorted_line:
            comment_block_to_insert = get_comment_from_map(
                line_with_comment=sorted_line,
                comment_map=comment_map,
                object_comment=True
            )
            last_line_index = len(commented_cfg_lines) - 1

            if commented_cfg_lines[last_line_index].strip() in ['],', ']']:
                commented_cfg_lines = commented_cfg_lines + comment_block_to_insert
            else:
                comment_line_number = 1
                for comment_line in comment_block_to_insert:
                    commented_cfg_lines.insert(last_line_index + comment_line_number, comment_line)
                    comment_line_number += 1
            skip_line_insert = True
        if not skip_line_insert:
            commented_cfg_lines.append(sorted_line + "\n")

    pre_test_cfg_comments = get_comment_from_map(
        line_with_comment='before_test_cfg',
        comment_map=comment_map
    )
    comment_line_number = 0

    if pre_test_cfg_comments:
        for line in pre_test_cfg_comments:
            commented_cfg_lines.insert(0 + comment_line_number, line)
            comment_line_number += 1

    pre_test_cfg_comments = get_comment_from_map(
        line_with_comment='after_test_cfg',
        comment_map=comment_map
    )
    for line in pre_test_cfg_comments:
        commented_cfg_lines.append(line)

    return commented_cfg_lines, comment_line_number


def get_comment_from_map(line_with_comment, comment_map, object_comment=False):
    if line_with_comment in ['before_test_cfg', 'after_test_cfg']:
        comment_id = line_with_comment
    elif object_comment:
        comment_id = re.search(r'(comment_object_identifier_).*(?=")', line_with_comment).group()
    else:
        comment_id = re.search(r'(comment_identifier_).*(?=")', line_with_comment).group()

    for comment_info in comment_map:
        if comment_info['comment_id'] == comment_id:
            return comment_info['comment_block']
    return ""


def sort_configuration(args, override_path=None):
    cfg_file_path = args.cfg if override_path is None else override_path
    cfg_file_path_split = cfg_file_path.split('/')
    cfg_module_name = cfg_file_path_split[-1].replace('.py', '')

    if args.backup:
        # Create backup file of original configuration file
        new_cfg_file_path = f'{"/".join(cfg_file_path_split[:-1])}/{cfg_module_name}.py.orig'
        copy_rename(old_file_path=cfg_file_path, new_file_path=new_cfg_file_path)

    with open(cfg_file_path) as f:
        cfg_lines_initial = f.readlines()

    cfg_lines_mapped, comment_map = map_comments_to_lines(cfg_lines=deepcopy(cfg_lines_initial), var_name=args.variable)

    mapped_cfg_filename = f'{"/".join(cfg_file_path_split[:-1])}/{cfg_module_name}_mapped.py'
    with open(mapped_cfg_filename, "w") as mapped_cfg_file:
        for line in cfg_lines_mapped:
            mapped_cfg_file.write(line)

    spec = importlib.util.spec_from_file_location(cfg_module_name, cfg_file_path)
    print(cfg_module_name)
    module = importlib.util.module_from_spec(spec)
    sys.modules[cfg_module_name] = module
    spec.loader.exec_module(module)  # module contents come into namespace
    cfg = eval(f'module.{args.variable}')
    sorted_cfg_file_lines = json.dumps(cfg, sort_keys=True, indent=indent_spaces).split("\n")
    sorted_cfg_file_lines = insert_trailing_comma(sorted_cfg_lines=sorted_cfg_file_lines)

    sorted_cfg_file_with_comments_lines, test_cfg_start = write_comments_back(
        sorted_cfg_lines=sorted_cfg_file_lines,
        comment_map=comment_map,
        var_name=args.variable
    )

    try:
        test_cfg_file = open(f'{"/".join(cfg_file_path_split[:-1])}/{cfg_module_name}.py', "w")

        if len(sorted_cfg_file_with_comments_lines) == 1:
            sorted_cfg_file_with_comments_lines[test_cfg_start] = args.variable + " = { }\n"
        else:
            sorted_cfg_file_with_comments_lines[test_cfg_start] = args.variable + " = {\n"

        for line in sorted_cfg_file_with_comments_lines:
            if '\"true\"' not in line:
                line = line.replace('true', 'True')
            if '\"false\"' not in line:
                line = line.replace('false', 'False')
            if '\"null\"' not in line:
                line = line.replace('null', 'None')

            test_cfg_file.write(line)
        test_cfg_file.close()

        if args.list_diff:
            diff_cfg_file = open(
                f'{"/".join(cfg_file_path_split[:-1])}/{cfg_module_name}.diff.txt', "w")

            with open(cfg_file_path) as sorted_cfg_file:
                sorted_cfg_file_lines = sorted_cfg_file.readlines()

            diff = difflib.unified_diff(
                cfg_lines_initial,
                sorted_cfg_file_lines
            )

            for line in diff:
                diff_cfg_file.write(line)
                print(line)

            diff_cfg_file.close()

        print('Configuration successfully sorted')
        print(f'{cfg_module_name}::{cfg_file_path}')

        os.remove(mapped_cfg_file.name)
    except Exception as error:
        print('Errors encountered')
        print('Rolling back to original file')
        copy_rename(
            old_file_path=f'{"/".join(cfg_file_path_split[:-1])}/{cfg_module_name}.py.orig',
            new_file_path=f'{"/".join(cfg_file_path_split[:-1])}/{cfg_module_name}.py'
        )
        print(error)


if __name__ == '__main__':
    tool_description = 'Sort FUT testcase configuration file\n'
    '\n'
    'Before sorting the config file, make sure the following are requirements are met:\n'
    '  file contains variable "test_cfg"\n'
    '  if the variable name that contains content intended for sorting is different, see optional arguments\n'
    '\n'
    'Example of usage:\n'
    'python3 cfg_sort.py --cfg ~/config.py -var custom_variable_name',

    # Instantiate the parser
    parser = argparse.ArgumentParser(
        description=tool_description,
        formatter_class=argparse.RawTextHelpFormatter
    )

    # Define options
    parser.add_argument(
        '--cfg',
        required=True,
        type=str,
        help='Full/absolute path to configuration file'
    )
    parser.add_argument(
        '--list_diff',
        required=False,
        default=False,
        action="store_true",
        help='Print and save difference pre-post sort file'
    )
    parser.add_argument(
        '--backup',
        required=False,
        default=False,
        action="store_true",
        help='Create backup of original config file in <cfgname>.orig'
    )
    parser.add_argument(
        '--variable',
        required=False,
        type=str,
        default='test_cfg',
        help='Name of variable value to sort'
    )
    parser.add_argument(
        '--recursive',
        required=False,
        type=str,
        default=False,
        help='Recursive sort all files within given directory, matching filename\n'
             'Example: python3 cfg_sort.py --cfg config/model/ --recursive _config.py'
    )

    configs_to_sort = []
    # Parse the arguments
    args = parser.parse_args()

    if not args.recursive and not os.path.isfile(args.cfg):
        print(f'Configuration file does not exist at given path:\n{args.cfg}')
        sys.exit(1)
    elif os.path.isdir(args.cfg):
        for absolute_path in glob.iglob(args.cfg + '/**/*' + args.recursive, recursive=True):
            if not os.path.isfile(absolute_path):
                print(f'Error: {absolute_path} is not file, skipping.')
                continue
            configs_to_sort.append(absolute_path)

        if not configs_to_sort:
            print(f'No files gathered in {args.cfg} folder matching following patten {args.cfg}/**/*{args.recursive}')
            sys.exit(1)

    if not args.recursive:
        sort_configuration(args=args)
    else:
        for absolute_path in configs_to_sort:
            sort_configuration(args=args, override_path=absolute_path)

    sys.exit(0)
