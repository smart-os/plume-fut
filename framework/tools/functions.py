import framework.tools.logger
import random
import string
import base64

global log
log = framework.tools.logger.get_logger()


def deep_dictionary_merge(source, destination):
    for key, value in source.items():
        if isinstance(value, dict):
            # get node or create one
            node = destination.setdefault(key, {})
            deep_dictionary_merge(value, node)
        else:
            destination[key] = value

    return destination


# Could use quote() from pipes but suspecting at test execution instability
# so just escape args that contain spaces
def sanitize_arg(arg):
    try:
        if (arg[0] == '"' and arg[-1] == '"') or (arg[0] == "'" and arg[-1] == "'") or (arg[0] == '-'):
            arg = arg
        elif " " in arg:
            if '"' in arg:
                arg = arg.replace('"', '\"')
            arg = '"{}"'.format(arg)
        else:
            arg = arg
    except Exception as e:
        log.warning(msg=e)
        arg = arg
    return arg


def get_command_arguments(*args):
    command = ""
    for arg in args:
        command = str(command) + ' ' + str(sanitize_arg(arg))

    return command


def get_by_key(array_of_dict, look_key, look_value):
    for check_dict in array_of_dict:
        if not isinstance(check_dict, dict):
            return {}
        for key, val in check_dict.items():
            if key == look_key and (check_dict[key] == look_value or check_dict[key] is look_value):
                return check_dict
    return {}


def generate_image_key():
    letters_and_digits = string.ascii_lowercase + string.digits + string.ascii_uppercase
    image_key_pure = ''.join(random.choice(letters_and_digits) for i in range(32))

    return str(base64.b64encode(image_key_pure.encode('ascii'))).replace('"b\'', '').replace('\'', '')
