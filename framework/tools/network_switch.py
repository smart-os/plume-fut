#!/usr/bin/env python3

import argparse
import os
import sys
_basedir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
sys.path.append(_basedir)
from config.testbed.config import testbed_cfg  # noqa: E402
from framework.lib.config import Config  # noqa: E402
from framework.lib.switchlib import NetworkSwitch  # noqa: E402
from framework.lib.switchlib import SUPPORTED_CONFIG  # noqa: E402


if __name__ == '__main__':
    tool_description = """Network switch configuration tool"""

    TP_LINK_PORTS = 8  # 8 or 16 depending on switch model
    available_ports = range(1, TP_LINK_PORTS + 1)
    mac_table_types = ['dynamic', 'filtering', 'static']

    # Configuration class instance
    testbed = Config(testbed_cfg)
    def_host = testbed.get('network_switch.host')
    def_username = testbed.get('network_switch.username')
    def_password = testbed.get('network_switch.password')
    def_switch_name = testbed.get('network_switch.name')

    parser = argparse.ArgumentParser(
        description=tool_description,
        formatter_class=argparse.RawTextHelpFormatter
    )

    parser.add_argument(
        '--hostname', '-host',
        required=False,
        default=def_host,
        type=str,
        help='Name/ip address of the network switch to configure\n'
    )
    parser.add_argument(
        '--username', '-user',
        required=False,
        default=def_username,
        type=str,
        help='Username for network switch access\n'
    )
    parser.add_argument(
        '--password', '-pass',
        required=False,
        default=def_password,
        type=str,
        help='Password for network switch access\n'
    )
    parser.add_argument(
        '--gw',
        required=False,
        default=False,
        choices=available_ports,
        type=int,
        help='Set port role to gw preset.\n'
    )
    parser.add_argument(
        '--leaf',
        required=False,
        default=False,
        choices=available_ports,
        type=int,
        help='Select port role to leaf preset.\n'
    )
    parser.add_argument(
        '--show_vlan', '-V',
        action='store_true',
        default=False,
        help='Show network switch vlan info\n'
    )
    parser.add_argument(
        '--show_pvid', '-P',
        action='store_true',
        default=False,
        help='Show network switch pvid info\n'
    )
    parser.add_argument(
        '--show_configuration', '-c',
        required=False,
        default=False,
        choices=SUPPORTED_CONFIG,
        type=str,
        help='Print plain text switch startup/running configuration.\n'
    )
    parser.add_argument(
        '--show_mac_address_table', '-m',
        required=False,
        default=None,
        nargs='*',
        choices=available_ports,
        type=int,
        help='Print mac address table for one or more port (default: all).\n',
    )
    parser.add_argument(
        '--clear_mac_address_table', '-C',
        required=False,
        const=mac_table_types[0],
        default=None,
        nargs='?',
        choices=mac_table_types,
        type=str,
        help='Clear mac address table\n'
    )
    parser.add_argument(
        '--get_port_mac', '-M',
        required=False,
        default=None,
        choices=available_ports,
        nargs='+',
        type=int,
        help='Show mac address of device connected to port.\n',
    )

    # Retrieve input arguments
    options = parser.parse_args()

    # Class instance to manage the network switch
    network_switch = NetworkSwitch(
        host=options.hostname,
        username=options.username,
        password=options.password,
        switch_name=def_switch_name,
    )

    if options.show_vlan:
        print('Showing network switch vlan table')
        network_switch.show_vlan()
        sys.exit(0)

    if options.show_pvid:
        print('Showing network switch pvid table')
        network_switch.show_pvid()
        sys.exit(0)

    if options.show_configuration:
        cfg_type = options.show_configuration
        print(f'Showing network switch {cfg_type} configuration')
        network_switch.show_config(cfg_type)
        sys.exit(0)

    switch_ports = options.show_mac_address_table
    if switch_ports is not None:
        if len(switch_ports) == 0:
            print(f'Showing mac address table')
            network_switch.mac_address_table(action='show')
            sys.exit(0)
        for port in switch_ports:
            print(f'Showing mac address table for port {port}')
            network_switch.mac_address_table(action='show', port=port)
        sys.exit(0)

    table_type = options.clear_mac_address_table
    if table_type:
        print(f'Clearing mac address table for type {table_type}')
        network_switch.mac_address_table(action='clear', table_type=table_type)
        sys.exit(0)

    port_list = options.get_port_mac
    if port_list is not None:
        for port in port_list:
            print(f'Showing MAC address of device on port {port}')
            mac = network_switch.get_port_mac(port=port)
            print(mac)
        sys.exit(0)

    if options.gw:
        port = options.gw
        print(f'Setting port {port} to gw preset')
        network_switch.set_port_node_role(port=port, role='gw')
        sys.exit(0)
    elif options.leaf:
        port = options.leaf
        print(f'Setting port {port} to leaf preset')
        network_switch.set_port_node_role(port=port, role='leaf')
        sys.exit(0)

    # No arguments provided, print help
    parser.print_help(sys.stderr)
