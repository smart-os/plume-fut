from copy import deepcopy
from config.testbed.config import testbed_cfg
from .lib.config import Config
from .lib.fut_exception import FutException
from .lib.test_handler import FutTestHandler
from .lib.recipe import Recipe
from .lib.switchlib import NetworkSwitch
from .tools.logger import FutFatalError
from lib_testbed.pod.pod_ext import PodExt
from pathlib import Path
from threading import Thread
import framework.tools.logger
import importlib.util
import os
import pytest
import subprocess
import sys
import time

ExpectedShellResult = 0
TEST_SCRIPT_TIMEOUT = 180
global log
log = framework.tools.logger.get_logger()


class ThreadWithReturnValue(Thread):
    def __init__(
        self,
        group=None,
        target=None,
        name=None,
        args=(),
        kwargs={}
    ):
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)

    def join(self, *args):
        Thread.join(self, *args)
        return self._return


class FutMainClass:
    def __init__(self):
        # workspace: top of fut source code directory tree on RPI server
        self.workspace = str(Path(os.path.dirname(os.path.realpath(__file__))).parent)
        self.testbed_cfg = Config(self.handle_testbed_config())
        self.testbed_cfg.set("workspace", str(self.workspace))
        self.devices_cfg = ["dut", "ref"]
        self.recipe = Recipe()
        self.fut_exception = FutException()
        self.dut = None
        self.ref = None
        self.dut_cfg_folder = None
        self.ref_cfg_folder = None
        # fut_topdir: top of fut source code directory tree on remote device
        self.fut_dir = self.testbed_cfg.get('default.fut_dir', '/tmp/fut-base/')
        self.rpi_pass = self.testbed_cfg.get('server.password')
        self.use_docker = True if os.getenv('USE_DOCKER') == 'true' else False

        # Setting symlink to /tmp/fut-base of working directory fut-base so curl transfer works from default RPI host
        # dir_real_path = os.path.realpath(self.testbed_cfg.get('workspace'))
        # if dir_real_path != '/tmp/fut-base' or dir_real_path != '/tmp/fut-base/':
        #     if os.path.islink('/tmp/fut-base'):
        #         self.execute('unlink /tmp/fut-base', as_sudo=True)
        #     if os.path.isdir('/tmp/fut-base') or os.path.isfile('/tmp/fut-base'):
        #         self.execute('rm -rf /tmp/fut-base', as_sudo=True)
        #     self.execute('ln -sf {} {}'.format(dir_real_path, '/tmp/fut-base'))

        # LOAD DEVICES CONFIGURATIONS #

        device_cfg_folders = {}
        for device_name in self.devices_cfg:
            device_folder = self.testbed_cfg.get('{}.CFG_FOLDER'.format(device_name), None)
            if device_folder:
                device_cfg_folders[device_name] = device_folder
            else:
                log.info(msg=f'No {device_name} config folder for {device_folder}, skipping')
                continue

            try:
                device_config_path = f'{self.workspace}/config/model/{device_folder}/device/config.py'
                device_module_name = "device_configuration"
                spec = importlib.util.spec_from_file_location(device_module_name, device_config_path)
                module = importlib.util.module_from_spec(spec)
                sys.modules[device_module_name] = module
                spec.loader.exec_module(module)  # module contents come into namespace
                framework_cfg = module.framework_cfg
                shell_cfg = module.shell_cfg
                device_cfg = module.device_cfg

                if device_name == 'dut':
                    self.__setattr__('dut_cfg_folder', device_folder)
                if device_name == 'ref':
                    self.__setattr__('ref_cfg_folder', device_folder)

                self.__setattr__(
                    device_name, {
                        "test_cfg": False if device_name != 'dut' else self.get_test_config(),
                        "shell_cfg": Config(deepcopy(shell_cfg)),
                        "device_cfg": Config(deepcopy(device_cfg)),
                        "framework_cfg": Config(deepcopy(framework_cfg)),
                        "name": device_name
                    }
                )
            except Exception as e:
                raise type(e)("Error importing {} config\n{}".format(device_name, e))

        self.framework_cfg = deepcopy(self.dut['framework_cfg'])
        self.rpi_pass = self.testbed_cfg.get('server.password')
        self.network_switch = NetworkSwitch(
            host=self.testbed_cfg.get('network_switch.host'),
            username=self.testbed_cfg.get('network_switch.username'),
            password=self.testbed_cfg.get('network_switch.password'),
        )

        erase_log_dump_folder_cmd = "rm -rf {}".format(self.framework_cfg.get("log_messages_path"))
        os.system(erase_log_dump_folder_cmd)

    def get_test_configurations(self, list_test_only=False):
        testcase_config_full = self.dut["test_cfg"].cfg
        testcase_names = list(testcase_config_full.keys())
        if list_test_only:
            return testcase_names

        enhanced_testcase_config = []
        for testcase_name in testcase_names:
            config_content = testcase_config_full[testcase_name]
            skip = False if "skip" not in config_content else config_content["skip"]
            skip_msg = "" if "skip_msg" not in config_content else config_content["skip_msg"]
            if 'test_script_timeout' in config_content:
                test_script_timeout = config_content['test_script_timeout']
            else:
                test_script_timeout = self.dut['framework_cfg'].get('test_script_timeout', TEST_SCRIPT_TIMEOUT)
            extra_testcase_config = {
                "name": testcase_name,
                "skip": skip,
                "skip_msg": skip_msg,
                "test_script_timeout": test_script_timeout
            }
            enhanced_testcase_config.append(deepcopy(extra_testcase_config))

            if isinstance(config_content, list):
                name_index = 0
                for scenario in config_content:
                    extra_testcase_config = {
                        "name": "{}[config_{}]".format(testcase_name, name_index),
                        "skip": True if "skip" in scenario and scenario["skip"]
                                        is True else False,
                        "skip_msg": "" if "skip_msg" not in scenario else scenario["skip_msg"],
                        "test_script_timeout":
                            self.dut['framework_cfg'].get('test_script_timeout', TEST_SCRIPT_TIMEOUT) if
                                'test_script_timeout' not in scenario else scenario['test_script_timeout']
                    }
                    enhanced_testcase_config.append(deepcopy(extra_testcase_config))
                    name_index = name_index + 1
            enhanced_testcase_config.append(deepcopy(extra_testcase_config))
        return enhanced_testcase_config

    def get_test_config(self, cfg_file_prefix=None, device="dut"):
        """ Import python dictionary from configuration file

        The function looks for any filename "*_config.py" and loads it.
        If "cfg_file_prefix" is specified, import from single file, else
        find all files that match the expression.
        """

        test_cfg = {}
        if device == "ref":
            cfg_folder = self.ref_cfg_folder
        elif device == "dut":
            cfg_folder = self.dut_cfg_folder
        else:
            cfg_folder = self.dut_cfg_folder
        testcase_dir = f'config/model/{cfg_folder}/testcase'
        testcase_mod = testcase_dir.replace('/', '.')
        if cfg_file_prefix is not None:
            # If prefix is specified, import from single file
            config_files = [f'{cfg_file_prefix}_config', ]
        else:
            # No prefix specified, import all correctly named files from folder
            tc_dir_contents = os.listdir(f'{self.workspace}/{testcase_dir}')
            config_files = [f.replace('.py', '') for f in tc_dir_contents if '_config.py' in f]
        for cfg_file in config_files:
            spec = importlib.util.find_spec(f'{testcase_mod}.{cfg_file}')
            if spec is not None:
                log.info(msg=f'Importing {spec.name}')
                module = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(module)
                # Shallow-merge new module over existing dictionary
                test_cfg = {**test_cfg, **module.test_cfg}
        # Create Config class instance from dictionary and return
        return Config(test_cfg)

    @staticmethod
    def handle_testbed_config():
        fut_dut_test_cfg_name = os.getenv("DUT_CFG_FOLDER")
        fut_ref_test_cfg_name = os.getenv("REF_CFG_FOLDER")
        if not fut_dut_test_cfg_name:
            fut_dut_test_cfg_name = testbed_cfg["dut"]["CFG_FOLDER"]
        if not fut_ref_test_cfg_name:
            fut_ref_test_cfg_name = testbed_cfg["ref"]["CFG_FOLDER"]

        if not fut_dut_test_cfg_name:
            pytest.exit("DUT_CFG_FOLDER is not defined, quiting!")
        if not fut_ref_test_cfg_name:
            pytest.exit("REF_CFG_FOLDER is not defined, quiting!")

        testbed_cfg["dut"]["CFG_FOLDER"] = fut_dut_test_cfg_name
        testbed_cfg["ref"]["CFG_FOLDER"] = fut_ref_test_cfg_name

        return testbed_cfg

    def testbed_validation(self, connection_names_to_validate):
        """
        connection_names_to_validate: input parameter, relates to active testbed devices
        Testbed validation does its own exception handling
        """
        testbed_valid = True
        testbed_validation_messages = []
        # Validate connections for all declared testbed devices
        for conn_name in connection_names_to_validate:
            # Check if device responds to ping
            log.info(msg=f"Checking if {conn_name} is online and responsive")
            conn_host = self.testbed_cfg.get(f"{conn_name}.host")
            conn_ping_cmd = f'ping -c 2 {conn_host} > /dev/null 2>&1'
            conn_ping_result = os.system(conn_ping_cmd)
            if conn_ping_result != 0:
                testbed_validation_messages.append(f'Host {conn_name} on {conn_host} is not online!')
                testbed_valid = False
                continue

            # Check management ssh access to device
            log.info(msg=f"Checking management SSH access to {conn_name} {conn_host}")
            conn_ssh = SSHConnection(
                hostname=conn_host,
                username=self.framework_cfg.get('username'),
                password=self.framework_cfg.get('password'),
                fut_dir=self.testbed_cfg.get(f'{conn_name}.fut_dir', self.testbed_cfg.get('default.fut_dir')),
                rsa_key_path=self.testbed_cfg.get('server.rsa.key_path'),
                rsa_key_pass=self.testbed_cfg.get('server.rsa.key_pass'),
            )
            if not conn_ssh.connection_established:
                testbed_validation_messages.append(
                    f"SSH connection could not be established on {conn_name} {conn_host}")
                testbed_valid = False
                continue

            # Check file transfer to device
            test_file_path = "/tmp/test_file.txt"
            log.info(msg=f"Checking file {test_file_path} transfer on {conn_name} {conn_host}")
            os.system('touch {}'.format(test_file_path))
            if conn_ssh.simple_auth:
                test_file_transfer_cmd = "sshpass -p {} scp {} {}@{}:/tmp".format(
                    conn_ssh.password,
                    test_file_path,
                    conn_ssh.username,
                    conn_ssh.hostname,
                )
            else:
                test_file_transfer_cmd = "scp -q {} {}:/tmp".format(
                    test_file_path,
                    conn_ssh.hostname,
                )
            log.info(msg=test_file_transfer_cmd)
            file_transfer_result = os.system(test_file_transfer_cmd)
            if file_transfer_result != 0:
                testbed_validation_messages.append(f"File transfer failed for {conn_name} {conn_host}")
                testbed_valid = False
                continue

            # Clean up and close test ssh connection
            conn_ssh.execute(f'rm {test_file_path}')
            conn_ssh.close()

        # testbed_validation_messages_string = "\n".join(testbed_validation_messages)
        if not testbed_valid:
            log.critical(msg=testbed_validation_messages)
            raise FutFatalError()

    def initialize_fut_environment(self, connection_names, config=None):
        pod_apis = self.get_pod_apis(connection_names)

        # Commented out for test case verifying system time accuracy
        # where we don't want time to be pushed to DUT. DUT must obtain time on its own.
        # self.sync_time(ssh_connections)

        fasttest = config.get("fasttest") is True
        handlers = self.get_test_handlers(pod_apis, fasttest)

        if config.get("transferonly") is True:
            for handler in handlers:
                handler.transfer(full=True)
                fut_set_env_path = handler.create_fut_set_env()
                handler.transfer(file=fut_set_env_path, to='/tmp/')
            return True

        for handler in handlers:
            handler.transfer()
            fut_set_env_path = handler.create_fut_set_env()
            handler.transfer(file=fut_set_env_path, to='/tmp/')
            if not fasttest:
                handler.set_allowed_channels()

        # if not fasttest:
        #     self.bulk_log_clear(pod_apis)

        if len(handlers) == 1:
            return handlers[0]
        else:
            return tuple(handlers)

    def get_pod_apis(self, pod_names):
        pod_apis = []
        for pod_name in pod_names:
            model_cfg = getattr(self, pod_name)
            pod = PodExt(
                ssh_gateway=f'{self.testbed_cfg.get("server.username")}@{self.testbed_cfg.get("server.host")}',
                gw_id='',
                model=model_cfg['framework_cfg'].get('pod_api_model'),
                host={
                    "user": model_cfg['framework_cfg'].get('username'),
                    "name": self.testbed_cfg.get(f'{pod_name}.host'),
                    "pass": model_cfg['framework_cfg'].get('password'),
                    "port": model_cfg['framework_cfg'].get('ssh_port', '22'),
                },
                host_recover={
                    "screen_app": "platform_3f980000_usb_usb_0_1_3_1_0",
                    "screen_acc": "platform_3f980000_usb_usb_0_1_1_2_1_0",
                }
            )
            pod_api = pod.get_pod_api()
            pod_api.__setattr__('name', pod_name)
            pod_apis.append(pod_api)

        return pod_apis

    def bulk_log_clear(self, ssh_connections):
        threads = []

        os.system("rm -rf {}".format(self.framework_cfg.get("log_dump_path")))
        os.system("rm -rf {}".format(self.framework_cfg.get("log_messages_path")))

        for ssh_connection in ssh_connections:
            thread = Thread(target=ssh_connection.do_log_pull_clear)
            thread.start()
            threads.append(thread)
        for thread in threads:
            thread.join()

    def get_test_handlers(self, pod_apis, fasttest=False):
        test_handlers = []

        for pod_api in pod_apis:
            model_configs = getattr(self, pod_api.name)
            test_handlers.append(
                FutTestHandler(
                    pod_api=pod_api,
                    workspace=self.workspace,
                    shell_cfg=model_configs["shell_cfg"],
                    device_cfg=model_configs["device_cfg"],
                    framework_cfg=self.framework_cfg,
                    testbed_cfg=self.testbed_cfg,
                    fasttest=fasttest,
                    recipe=self.recipe
                )
            )
        if len(test_handlers) == 1:
            return [test_handlers[0]]
        else:
            return tuple(test_handlers)

    @staticmethod
    def set_current_time(ssh):
        time_now = time.gmtime()
        current_time = time.strftime("%Y-%m-%d %H:%M:%S", time_now)
        current_time_shell = time.strftime("%Y%m%d%H%M.%S", time_now)
        log.info(msg="Setting time {} on {}".format(current_time, ssh.name))
        ssh.execute('date -s {}'.format(current_time_shell))

    def sync_time(self, ssh_connections):
        threads = []
        if not isinstance(ssh_connections, tuple):
            ssh_connections = [ssh_connections]

        log.info(msg="Synchronizing time on ssh connections")
        for ssh_connection in ssh_connections:
            thread = Thread(target=self.set_current_time, args=[ssh_connection])
            thread.start()
            threads.append(thread)

        for thread in threads:
            thread.join()

    def async_execution(self, functions):
        threads = []
        results = []

        for function in functions:
            thread = ThreadWithReturnValue(target=function)
            thread.start()
            threads.append(thread)

        for thread in threads:
            results.append(thread.join())

        return results

    def execute(self, cmd, print_std=False, as_sudo=False, timeout=340):
        log.info(msg='Executing {} on RPI Server'.format(cmd))
        cmd = cmd.split()
        try:
            cmd_start_time = time.strftime('%H:%M:%S')
            if not as_sudo:
                cmd_process = subprocess.Popen(
                    cmd,
                    shell=False,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    universal_newlines=True
                )
            else:
                cmd_pass = subprocess.Popen(
                    ['echo', self.rpi_pass],
                    shell=False,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    universal_newlines=True
                )
                cmd_process = subprocess.Popen(
                    ['sudo', '-n', '-S'] + cmd,
                    shell=False,
                    stdin=cmd_pass.stdout,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    universal_newlines=True
                )
            tmp_std_out = ''
            tmp_std_err = ''
            last = time.time()
            drop_time = last + timeout
            while cmd_process.poll() is None:
                if last > drop_time:
                    log.error(tmp_std_out)
                    log.error(tmp_std_err)
                    log.error(cmd_process.returncode)
                    raise TimeoutError('Command execution took too long')
                if time.time() - last > 5:
                    last = time.time()
                if cmd_process.stdout:
                    tmp_stdout_read = cmd_process.stdout.read(1)
                    if tmp_stdout_read:
                        tmp_std_out += tmp_stdout_read
                if cmd_process.stderr:
                    tmp_stderr_read = cmd_process.stderr.read(1)
                    if tmp_stderr_read:
                        tmp_std_err += tmp_stderr_read
            ret = cmd_process.poll(), tmp_std_out + cmd_process.stdout.read(), tmp_std_err
            if cmd_process.stdout:
                cmd_process.stdout.close()
            if cmd_process.stderr:
                cmd_process.stderr.close()
            cmd_end_time = time.strftime('%H:%M:%S')
            self.recipe.add(
                device='RPI_SERVER',
                cmd=' '.join(cmd),
                start_t=cmd_start_time,
                end_t=cmd_end_time,
                ec=cmd_process.returncode
            )
            if print_std or cmd_process.returncode != 0:
                for line in ret:
                    print(str(line))
        except Exception as e:
            log.exception(msg=f'Exception caught during command execution\n{e}')
            return False

        return cmd_process.returncode

    def run(self, path, args='', **kwargs):
        exit_code = self.execute(
            '{}/shell/{}.sh {}'.format(
                self.workspace,
                path,
                args,
            ), print_std=True, **kwargs
        )
        return exit_code

    def set_switch_configuration(self, cfg):
        # Pending implementation
        raise NotImplementedError()

    def get_unit_test_config(self, folder=''):
        ut_config = []
        unit_test_path = "{}/resource/ut/{}".format(self.workspace, folder)

        for path, sub_dirs, files in os.walk(unit_test_path):
            if files and len(files) == 1 and files[0] == "unit":
                test_path = "{}/unit".format(
                    str(path)
                    .replace(str(self.workspace), "")
                    .replace('shell/', "")
                )
                ut_cfg_def = {
                    "path": test_path,
                    "name": test_path.split('/')[-2]
                }

                ut_cfg_add = self.dut["test_cfg"].get(ut_cfg_def["name"], {})
                ut_cfg = {**ut_cfg_def, **ut_cfg_add}

                ut_config.append(deepcopy(ut_cfg))

        return ut_config
