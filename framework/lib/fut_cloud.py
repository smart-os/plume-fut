from pathlib import Path
import framework.tools.logger
import os

global log
log = framework.tools.logger.get_logger()


class FutCloud:
    def __init__(
            self
    ):
        fut_home = str(Path(os.path.dirname(os.path.realpath(__file__))).parent.parent)
        self.hap_path = '{}/shell/tools/rpi/files/haproxy.cfg'.format(fut_home)
        self.cloud_script = '{}/shell/tools/rpi/start_cloud_simulation.sh'.format(fut_home)
        if not os.path.isfile(self.hap_path):
            log.exception(f'Missing file {self.hap_path}')

        if not os.path.isfile(self.cloud_script):
            log.exception(f'Missing file {self.cloud_script}')

    def start_cloud(self):
        log.info('Starting FUT Cloud simulation')
        if os.system('{}'.format(self.cloud_script)) != 0:
            log.error('Couldn\'t start FUT cloud simulation')
            return False
        log.info('FUT Cloud simulation started')
        return True

    def restart_cloud(self):
        log.info('Restarting FUT Cloud simulation')
        if os.system('{} -r'.format(self.cloud_script)) != 0:
            log.error('Could not restart FUT cloud simulation')
            return False
        log.info('FUT Cloud simulation restarted')
        return True

    def stop_cloud(self):
        log.info('Stopping FUT Cloud simulation')
        if os.system('{} -s'.format(self.cloud_script)) != 0:
            log.error('Could not stop FUT cloud simulation')
            return False
        log.info('FUT Cloud simulation stopped')
        return True

    # tls_ver one of [10, 11, 12, '1.0', '1.1', '1.2']
    def change_tls_ver(self, tls_ver):
        log.info('Changing FUT Cloud TLS version to {}'.format(tls_ver))
        tls_ver = str(tls_ver).replace('.', '')
        tls_ver_exp = '.'.join(tls_ver)
        r_1 = 'ssl-default-bind-options force-tlsv.* ssl-max-ver TLSv.* ssl-min-ver TLSv.*'
        r_2 = 'ssl-default-server-options force-tlsv.* ssl-max-ver TLSv.* ssl-min-ver TLSv.*'

        s_0 = f'force-tlsv{tls_ver} ssl-max-ver TLSv{tls_ver_exp} ssl-min-ver TLSv{tls_ver_exp}'
        s_1 = f'ssl-default-bind-options {s_0}'
        s_2 = f'ssl-default-server-options {s_0}'

        r_1 = os.system(f"sed -i 's/{r_1}/{s_1}/g' {self.hap_path} && grep -q '{s_1}' {self.hap_path}")
        r_2 = os.system(f"sed -i 's/{r_2}/{s_2}/g' {self.hap_path} && grep -q '{s_2}' {self.hap_path}")

        if r_1 != 0 or r_2 != 0:
            log.error('Failed to change FUT Cloud TLS version')
            return False
        log.info('FUT Cloud TLS version changed')
        return True
