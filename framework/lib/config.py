class Config:
    def __init__(self, config):
        self.cfg = config

    def get(self, cfg_value_path, fallback=''):
        value_path_split = cfg_value_path.split('.')
        if len(value_path_split) == 1 and value_path_split[0] in self.cfg:
            value = self.cfg[value_path_split[0]]
        else:
            value = self._get_recursive(value_path_split, self.cfg)

        if not value:
            return fallback
        else:
            return value

    def _get_recursive(self, value_path_split, cfg_dict):
        key = value_path_split[0]

        if key not in cfg_dict:
            return False
        elif cfg_dict[key] and isinstance(cfg_dict[key], type({})) and cfg_dict:
            value_path_split.pop(0)
            if value_path_split:
                return self._get_recursive(value_path_split, cfg_dict[key])
            else:
                return cfg_dict[key]
        else:
            return cfg_dict[key]

    # method set() works only on one level of dict
    def set(self, key, value):
        self.cfg[key] = value
        return True

    def check(self, value_path_split, if_equal='NotCheckingForEqual'):
        value = self.get(value_path_split)

        if value and if_equal == 'NotCheckingForEqual':
            return True
        elif value and if_equal != 'NotCheckingForEqual' and value == if_equal:
            return True
        else:
            return False
