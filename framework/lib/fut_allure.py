from pathlib import Path
import framework.tools.logger
import configparser
import os

global log
log = framework.tools.logger.get_logger()

class FutAllureClass:
    default_section = 'Global'
    config_parser = configparser.ConfigParser()
    clear = True
    envlist_filepath = Path(__file__).resolve().parents[2].joinpath("docker/env.list")
    set_allure_environment_values = list(filter(None, open(envlist_filepath).read().split('\n')))

    def __init__(self, allure_dir):
        self.allure_dir = allure_dir
        self.allure_environment = {
            "allure_report_dir": self.allure_dir
        }

    def get_properties_path(self):
        return os.path.join(self.allure_dir, 'environment.properties')

    def write_config(self):
        with open(self.get_properties_path(), 'w') as configfile:
            self.config_parser.write(configfile)

    def init_config(self):
        self.config_parser.clear()
        self.config_parser.add_section(self.default_section)
        self.write_config()

    def get_environment(self, name):
        if self.clear:
            self.init_config()
            self.clear = False
        properties_file = self.get_properties_path()
        self.config_parser.read(properties_file)
        try:
            value = self.config_parser.get(self.default_section, name)
        except configparser.NoOptionError:
            value = None
        return value

    def set_config(self, name, value):
        self.config_parser.set(self.default_section, name, value)
        self.write_config()

    def add_environment(self, name, value, optional_suffix=None):
        if value in [None, ''] or name in [None, '']:
            return
        prev_value = self.get_environment(name)
        if optional_suffix and prev_value and prev_value != value:
            name = f'{name}_{optional_suffix}'
            prev_value = self.get_environment(name)
        if prev_value and prev_value != value:
            log.info(msg=f'Overriding: {name}, previous value: {prev_value}, value: {value}')
        self.set_config(name, value)

    def set_device_versions(self, test_handlers):
        for handler in test_handlers:
            try:
                name = handler.name
                version = handler.get_version()
                self.add_environment(
                    'fut_{}_model'.format(name),
                    handler.shell_cfg.get('MODEL')
                )
                self.add_environment(
                    'fut_{}_version'.format(name),
                    version
                )
            except Exception as e:
                log.warning(msg='Could not get model version')
                log.warning(msg=e)

    def set_global_environment(self):
        for env_var in self.set_allure_environment_values:
            env_value = os.getenv(env_var)
            if env_value:
                self.add_environment(env_var, env_value)
