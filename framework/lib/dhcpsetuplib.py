import difflib
import framework.tools.logger as logger
import os
import re
import sys
import time

SEP = '**********'
log = logger.FutLoggerAdapter(extra={logger.LOGR_ATTR: '[DHCP_SETUP]'})


class DhcpSetup:
    def __init__(self, dhcpd_conf_filepath):
        self.dhcpd_conf_filepath = dhcpd_conf_filepath

    def check_line(self, line, check_for_key):
        for device in self.device_configuration:
            cfg_dict = self.device_configuration[device]
            if str(cfg_dict[check_for_key]) in line:
                return device
        return False

    def cfg_search(self, pattern, cfg_lines, start_index, reverse=False):
        if reverse:
            search_range = reversed(range(0, start_index))
        else:
            search_range = range(start_index, len(cfg_lines))

        for line_index in search_range:
            check_line = cfg_lines[line_index]
            if not check_line.strip():
                continue
            first_current_character = list(check_line)[len(check_line) - len(check_line.lstrip())]
            if first_current_character == '#':
                if re.search('#.*}', check_line):
                    return False, False
            if re.search(pattern, check_line):
                return check_line, line_index
        return False, False

    def confirm_action(self, question):
        reply = str(input(question + ' (Y/n): ')).strip()
        log.info(msg=reply)
        if reply.lower() in ['y', 'yes', '']:
            log.info(msg='\n')
            return True
        else:
            log.info(msg='Exiting script')
            sys.exit(1)

    def comment_block(self, cfg_lines, from_index):
        if not from_index:
            return cfg_lines

        to_end_line, to_index = self.cfg_search('.*}.*', cfg_lines, from_index)
        for comment_at_index in range(from_index, to_index):
            current_cfg_line = cfg_lines[comment_at_index]
            if not current_cfg_line.strip():
                continue
            first_current_character = list(current_cfg_line)[len(current_cfg_line) - len(current_cfg_line.lstrip())]
            if first_current_character == '#':
                continue
            cfg_lines[comment_at_index] = f'# {cfg_lines[comment_at_index]}'

        cfg_lines[to_index] = f'# {cfg_lines[to_index]}'
        return cfg_lines

    def copy_rename(self, old_file_path, new_file_path):
        try:
            backup_cfg_file = open(new_file_path, 'w')
            with open(old_file_path, 'r') as org_cfg_file:
                backup_cfg_file.write(org_cfg_file.read())
                backup_cfg_file.close()
            log.info(msg=f'File created in: {new_file_path}')
        except Exception as e:
            log.warning(msg=f'Could not create file: {new_file_path}')
            log.warning(msg=f'Old file: {old_file_path}')
            log.warning(msg=e)
            sys.exit(1)

    def backup_dhcpd_file(self):
        time_string = time.strftime('%Y%m%d-%H%M%S')
        new_conf_path = f'{self.dhcpd_conf_filepath}.{time_string}.old'
        self.copy_rename(self.dhcpd_conf_filepath, new_conf_path)
        return new_conf_path

    def comment_duplicates(self, dhcp_conf_file_lines, device_cfg, host_index):
        to_end_line, to_index = self.cfg_search('.*}.*', dhcp_conf_file_lines, host_index)

        device_name = device_cfg['tb_role']
        device_mac = device_cfg['mac_addr']
        device_mgmt_ip = device_cfg['mgmt_ip']
        device_wan_ip = device_cfg['wan_ip']

        duplicate_host_declaration, duplicate_host_index = self.cfg_search(
            f'.*host.*{device_name}.*',
            dhcp_conf_file_lines,
            host_index,
            True
        )
        duplicate_mac_declaration, duplicate_mac_index = self.cfg_search(
            f'.*{device_mac}.*',
            dhcp_conf_file_lines,
            host_index,
            True
        )
        duplicate_ip_declaration, duplicate_ip_index = self.cfg_search(
            f'.*{device_mgmt_ip}.*',
            dhcp_conf_file_lines,
            host_index,
            True
        )

        if not duplicate_mac_declaration:
            duplicate_mac_declaration, duplicate_mac_index = self.cfg_search(
                f'.*{device_mac}.*',
                dhcp_conf_file_lines,
                to_index
            )
        if not duplicate_ip_declaration:
            duplicate_ip_declaration, duplicate_ip_index = self.cfg_search(
                f'.*{device_mgmt_ip}.*',
                dhcp_conf_file_lines,
                to_index
            )
        if not duplicate_host_declaration:
            duplicate_host_declaration, duplicate_host_index = self.cfg_search(
                f'.*host.*{device_name}.*',
                dhcp_conf_file_lines,
                to_index
            )

        if duplicate_mac_declaration:
            log.info(msg=f'Duplicate MAC address {device_mac} for {device_name} found, will comment out block')
            host_line, host_index = self.cfg_search('.*host.*{', dhcp_conf_file_lines, duplicate_mac_index, True)
            dhcp_conf_file_lines = self.comment_block(dhcp_conf_file_lines, host_index)

        if duplicate_ip_declaration:
            log.info(msg=f'Duplicate management ip {device_mgmt_ip} for {device_name} found, will comment out block')
            host_line, host_index = self.cfg_search('.*host.*{', dhcp_conf_file_lines, duplicate_ip_index, True)
            dhcp_conf_file_lines = self.comment_block(dhcp_conf_file_lines, host_index)

        if 'wan_ip' in device_cfg:
            duplicate_ip_declaration, duplicate_ip_index = self.cfg_search(
                f'.*{device_wan_ip}.*',
                dhcp_conf_file_lines,
                to_index
            )
            if duplicate_ip_declaration:
                log.info(msg=f'Duplicate WAN address {device_wan_ip} for {device_name} found, will comment out block')
                host_line, host_index = self.cfg_search('.*host.*{', dhcp_conf_file_lines, duplicate_ip_index, True)
                dhcp_conf_file_lines = self.comment_block(dhcp_conf_file_lines, host_index)

        if duplicate_host_declaration:
            log.info(msg=f'Duplicate host name for {device_name} found, will comment out block')
            dhcp_conf_file_lines = self.comment_block(dhcp_conf_file_lines, duplicate_host_index)

        return dhcp_conf_file_lines

    def handle_new_dhcpd_lines(self):
        # Read existing config file
        with open(self.dhcpd_conf_filepath) as cfgfile:
            dhcp_conf_file_lines = cfgfile.readlines()

        # Parse existing config file
        for line_index in range(len(dhcp_conf_file_lines)):
            current_cfg_line = dhcp_conf_file_lines[line_index]
            # Ignore empty lines
            if not current_cfg_line.strip():
                continue
            # Ignore comments
            first_current_character = list(current_cfg_line)[len(
                current_cfg_line) - len(current_cfg_line.lstrip())]
            if first_current_character == '#':
                continue

            # Process existing device configuration in file content
            device_found_key = self.check_line(current_cfg_line, 'mac_addr')
            if device_found_key:
                device_cfg = self.device_configuration[device_found_key]
                device_cfg['found'] = True
                host_line, host_index = self.cfg_search(
                    '.*host.*{', dhcp_conf_file_lines, line_index, True)
                if not host_line:
                    device_mac = device_cfg['mac_addr']
                    log.info(msg=f'DHCP filepath {self.dhcpd_conf_filepath} is not valid, issue at line {line_index}.')
                    log.info(msg=f'No host definition for MAC address {device_mac}')
                    sys.exit(1)

                host_name = re.search(r'(?<=host)(.*)(?={)', str(host_line)).group().strip()
                if device_cfg['tb_role'] != host_name:
                    host_name_line = 'host ' + str(device_cfg['tb_role']) + ' {\n'
                    dhcp_conf_file_lines[host_index] = host_name_line
                else:
                    device_cfg['valid_host_index'] = host_index

                # Create address line
                ip_address, ip_index = self.cfg_search('.*fixed-address.*', dhcp_conf_file_lines, host_index)
                cfg_address = ', '.join(s for s in [device_cfg['mgmt_ip'], device_cfg['wan_ip']] if s is not None)
                if any(ip not in str(ip_address) for ip in cfg_address.split(',')):
                    dhcp_conf_file_lines[ip_index] = f'    fixed-address {cfg_address};\n'

                # Comment duplicate device configurations
                dhcp_conf_file_lines = self.comment_duplicates(dhcp_conf_file_lines, device_cfg, host_index - 1)

        # Create new device configuration
        for device_key in self.device_configuration:
            device_cfg = self.device_configuration[device_key]
            try:
                found = device_cfg['found']
            except Exception as e:
                log.warning(msg=e)
                found = False
            if not found and device_cfg['mac_addr']:
                fixed_address = ', '.join(s for s in [device_cfg['mgmt_ip'], device_cfg['wan_ip']] if s is not None)
                self.device_configuration[device_key]['found'] = True
                device_name = device_cfg['tb_role']
                device_mac = device_cfg['mac_addr']
                configuration_to_append = [
                    '\n',
                    f'host {device_name} ' + '{\n',
                    f'\thardware ethernet {device_mac};\n',
                    f'\tfixed-address {fixed_address};\n',
                    '}\n'
                ]
                dhcp_conf_file_lines = dhcp_conf_file_lines + configuration_to_append
                device_name = device_cfg['tb_role']
                host_declaration, host_index = self.cfg_search(
                    f'.*host.*{device_name}.*',
                    dhcp_conf_file_lines,
                    len(dhcp_conf_file_lines),
                    True
                )
                dhcp_conf_file_lines = self.comment_duplicates(dhcp_conf_file_lines, device_cfg, host_index)

        return dhcp_conf_file_lines

    def dhcp_reservation(self, device_config):
        """ Create DHCP IP reservations for specified devices with provided config.
            Key information for each device is the name, MAC, list of IPs """
        self.device_configuration = device_config
        if not os.path.isfile(self.dhcpd_conf_filepath):
            log.info(msg=f'{self.dhcpd_conf_filepath} does not exist')
            sys.exit(1)

        if all(self.device_configuration[device]['mac_addr'] is None for device in self.device_configuration):
            log.info(msg='No active devices detected by the network switch, retry the script.')
            log.info(msg='The network switch needs to see traffic to return device MAC addresses')
            sys.exit(1)

        backup_cfg_path = self.backup_dhcpd_file()
        revert_cmd = f'cp {self.dhcpd_conf_filepath} {backup_cfg_path}'
        new_dhcpd_lines = self.handle_new_dhcpd_lines()
        log.info(msg='Editing done. Please manually validate resulting changes.')

        for line in new_dhcpd_lines:
            print(line, end='')  # Do not use log here

        with open(self.dhcpd_conf_filepath) as original_dhcpd_file:
            original_dhcpd_lines = original_dhcpd_file.readlines()

        diff = ''.join(difflib.unified_diff(
            original_dhcpd_lines,
            new_dhcpd_lines,
            fromfile='Original',
            tofile='New',
            lineterm='')
        )
        if diff:
            log.info(msg=f'{SEP} Difference {SEP}')
            print(diff, end='')  # Do not use log here
        else:
            log.info(msg=f'{SEP} No changes made to dhcpd.conf file {SEP}')
            sys.exit(0)

        self.confirm_action('Do you wish to apply changes?')

        try:
            dhcpd_cfg_file = open(self.dhcpd_conf_filepath, 'w')
            for line in new_dhcpd_lines:
                dhcpd_cfg_file.write(line)
            dhcpd_cfg_file.close()
            time.sleep(1)
            log.info(msg='DHCPD Configuration file created')
        except Exception as e:
            log.warning(msg='Error encountered, reversing changes to dhcpd.conf to backup')
            log.warning(msg=e)
            os.system(revert_cmd)
            sys.exit(1)

        log.info(msg='Restarting service isc-dhcp-server')
        restart_code = os.system('service isc-dhcp-server restart')
        if restart_code != 0:
            log.info(msg='Service restart exit code is not 0, reverting to backup and restarting')
            os.system(revert_cmd)
            time.sleep(1)
            os.system('service isc-dhcp-server restart')
            sys.exit(1)

        log.info(msg='DHCPD configuration updated')
        return True
