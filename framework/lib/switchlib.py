import pexpect
import time
import sys
import os
from pathlib import Path
topdir_path = str(Path(os.path.realpath(__file__)).parents[2])
sys.path.append(topdir_path)
from config.testbed.config import testbed_cfg  # noqa: E402
from framework.lib.config import Config  # noqa: E402
import framework.tools.logger  # noqa: E402

SUPPORTED_CONFIG = ['running', 'startup']
global log
log = framework.tools.logger.get_logger()

# Telnet Switch
class NetworkSwitch(pexpect.spawn):
    def __init__(
        self,
        host,
        username,
        password,
        switch_name='',
        port=23,
        set_echo=False,
        command=None,
        timeout=30,
        maxread=2000,
        searchwindowsize=None,
        logfile=None,
        cwd=None,
        env=None
    ):
        pexpect.spawn.__init__(
            self,
            command,
            timeout=timeout,
            maxread=maxread,
            searchwindowsize=searchwindowsize,
            logfile=logfile,
            cwd=cwd,
            env=env
        )
        self.mac_filter = ['MAC', 'mac', '-', 'continue']
        self.mac_keys = ['mac', 'vlan', 'port', 'type', 'aging']

        self.host = host
        self.username = username
        self.password = password
        self.port = port
        self.set_echo = set_echo
        self.cmd_endline = '\r'
        self.spawn_timeout = timeout
        self.morestr = "Press any key to continue.*"
        self.cmd_timeout = 10
        self.cmd_sleep = 0.1
        self.spawn_cmd = f'telnet {self.host} {self.port}'
        self.supported_config = SUPPORTED_CONFIG

        testbed = Config(testbed_cfg)
        if switch_name is not None:
            self.switch_prompt = switch_name
        else:
            self.switch_prompt = testbed.get('network_switch.name')
        self.promptchar = testbed.get('network_switch.promptchar')
        self.adminpromptchar = testbed.get('network_switch.adminpromptchar')
        self.switch_type = testbed.get('network_switch.type')

    def _send_command(self, cmd, prompt=None, timeout=None):
        if timeout is None:
            timeout = self.cmd_timeout
        if not cmd.endswith(self.cmd_endline):
            cmd += self.cmd_endline
        if prompt is None:
            # This regex only matches prompt
            prompt = f'{self.switch_prompt}[{self.promptchar}{self.adminpromptchar}]'
            # Regex with wildcard matches possible config file content: switch_name
            # prompt = "{}.*".format(self.switch_prompt)
        pattern_list = [self.morestr, pexpect.TIMEOUT, pexpect.EOF, prompt]
        self.sendline(cmd)
        out = ""
        stop_time = time.time() + timeout
        while time.time() < stop_time:
            time.sleep(self.cmd_sleep)
            index = self.expect(pattern=pattern_list, timeout=timeout)
            out += str(self.before, 'utf-8')
            if index == 0:
                # Matched "more" string, pressing key to continue
                self.sendline(" ")
            elif index == 3:
                # Matched prompt
                break
            else:
                # On TIMEOUT, EOF, ...
                break
        return out

    def _login(self):
        """Performs telnet login to network switch."""
        self._spawn(self.spawn_cmd)
        self.setecho(self.set_echo)
        self.expect('User:')
        self.sendline(self.username + self.cmd_endline)
        self.expect('Password:')
        self.sendline(self.password + self.cmd_endline)
        self._send_command("")
        if self.after == pexpect.TIMEOUT:
            raise pexpect.TIMEOUT('Can not login to switch, check switch configuration.')

    def _enable_admin_mode(self):
        """Enter privileged configuration mode of network switch."""
        self.buffer = bytes(0)
        if 'EOF' in str(self.after):
            self._logout()
            self._login()
        while self.after and isinstance(self.after, bytes) and not self.after.endswith(self.promptchar.encode()):
            self._send_command('exit')
        while self.after and isinstance(self.after, bytes) and not self.after.endswith(self.adminpromptchar.encode()):
            self._send_command('enable')

    def _logout(self):
        """Performs telnet logout from network switch."""
        self.pid = None
        while self.after and isinstance(self.after, bytes) and not self.after.endswith(self.promptchar.encode()):
            self._send_command('exit')
        self.sendline('logout')
        self.close()

    def mac_address_table(self, action, port=None, table_type='dynamic', return_output=False):
        if action.lower() in 'show':
            option = f'interface gigabitEthernet 1/0/{port}' if port is not None else ''
        elif action.lower() in 'clear':
            option = table_type
        else:
            raise NotImplementedError()
        cmd = f'{action} mac address-table {option}'
        self._login()
        self._enable_admin_mode()
        out = self._send_command(cmd)
        self._logout()
        if return_output is True:
            return out
        else:
            print(out)

    def get_mac_table(self, port):
        """ Returns mac table as dict, per port if specified """
        output = self.mac_address_table(action='show', port=port, return_output=True)
        if isinstance(output, (bytes, bytearray)):
            output = output.decode('utf-8')
        # Split output(str) into list of strings, one per line:
        lines = output.splitlines(keepends=False)
        # Remove lines (str) with unwanted content:
        lines = [item for item in lines if all(s not in item for s in self.mac_filter)]
        # Split individual lines into sub-lists of strings, while removing empty ones
        matrix = list(filter(None, [line.split() for line in lines]))
        # Create a list of dicts with preset keys
        mac_table = [dict(zip(self.mac_keys, row)) for row in matrix]
        return mac_table

    def get_port_mac(self, port, vlan=4):
        """ Returns single mac address per port """
        mac_table = self.get_mac_table(port=port)
        # From list of dicts, create a sub-list with correct vlan
        mgmt_mac_table = [item for item in mac_table if item['vlan'] in str(vlan)]
        if len(mgmt_mac_table) > 1:
            raise Exception('should only have one MAC')
        try:
            if isinstance(mgmt_mac_table, list):
                mgmt_mac_table = mgmt_mac_table[0]
            port_mac = mgmt_mac_table['mac'].lower()
        except Exception as e:
            log.warning(msg=e)
            port_mac = None
        return port_mac

    def acquire_device_mac_new(self, device_configuration):
        # Function is deprecated
        for device in device_configuration:
            port = device_configuration[device]['port']
            try:
                mgmt_vlan = device_configuration[device]['mgmt_vlan']
            except KeyError:
                mgmt_vlan = None
            mac_address = None
            retries = 3
            for i in range(1, retries + 1):
                if mgmt_vlan is not None:
                    mac_address = self.get_port_mac(port=port, vlan=mgmt_vlan)
                else:
                    mac_address = self.get_port_mac(port=port)
                if mac_address:
                    print(f'{device} - {mac_address}')
                    break
                if i is retries:
                    print(f'{device} - No MAC address found')
            # Store mac address
            device_configuration[device]['device_mac'] = mac_address
        return device_configuration

    def show_vlan(self, return_output=False):
        cmd = f'show vlan brief'
        self._login()
        self._enable_admin_mode()
        out = self._send_command(cmd)
        self._logout()
        if return_output is True:
            return out
        else:
            print(out)

    def show_pvid(self, return_output=False):
        cmd = f'show interface switchport'
        self._login()
        self._enable_admin_mode()
        out = self._send_command(cmd)
        self._logout()
        if return_output is True:
            return out
        else:
            print(out)

    def show_config(self, cfg_type, return_output=False):
        if cfg_type in self.supported_config:
            cmd = f'show {cfg_type}-config'
        else:
            raise NotImplementedError()
        self._login()
        self._enable_admin_mode()
        out = self._send_command(cmd)
        self._logout()
        if return_output is True:
            return out
        else:
            print(out)

    """
    configure
      interface gigabitEthernet 1/0/$port
        no switchport general allowed vlan $vlan_number
        switchport general allowed vlan $vlan_number untagged
        switchport general allowed vlan $vlan_number tagged
        switchport pvid $vlan_number
    """
    def configure_switchport_vlan(self, port, vlan, allowed, tagged):
        raise NotImplementedError()
        cfg_cmd = 'configure'
        iface_cmd = f'interface gigabitEthernet 1/0/{port}'
        self._login()
        self._enable_admin_mode()
        out = self._send_command(cfg_cmd)
        print(out)
        out = self._send_command(iface_cmd)
        print(out)
        out = self._configure_switchport_vlan(vlan, allowed=allowed, tagged=tagged, return_output=True)
        print(out)
        self._logout()

    def _configure_switchport_vlan(self, vlan, allowed=True, tagged=True, return_output=False):
        """Configure port vlan, within interface configuration mode."""
        raise NotImplementedError()
        action_cmd = f'switchport general allowed vlan {vlan}'
        if not allowed:
            action_cmd = f'no {action_cmd}'
        elif allowed and tagged:
            action_cmd = f'{action_cmd} tagged'
        elif allowed and not tagged:
            action_cmd = f'{action_cmd} untagged'
        else:
            raise NotImplementedError()
        out = self._send_command(action_cmd)
        if return_output is True:
            return out
        else:
            print(out)

    def configure_switchport_pvid(self, port):
        raise NotImplementedError()

    def set_port_node_role(self, port, role):
        raise NotImplementedError()
