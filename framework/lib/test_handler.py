from config.rules.regulatory import channel_ht_mode_map
from config.rules.regulatory import regulatory_channel_list
from framework.tools.functions import get_command_arguments
import framework.tools.logger
from .fut_exception import FutException
from .recipe import Recipe
from .config import Config
from datetime import datetime
import time
import os
import re


TEST_SCRIPT_TIMEOUT = 180
global log
log = framework.tools.logger.get_logger()

class FutTestHandler:
    def __init__(
        self,
        pod_api=None,
        workspace=None,
        shell_cfg=None,
        device_cfg=None,
        framework_cfg=None,
        testbed_cfg=None,
        fasttest=None,
        fut_exception=None,
        recipe=None
    ):
        self.recipe = recipe
        if not self.recipe:
            self.recipe = Recipe()

        self.fut_exception = fut_exception
        if not self.fut_exception:
            self.fut_exception = FutException()

        self.pod_api = pod_api
        self.device_cfg = device_cfg
        self.shell_cfg = shell_cfg
        self.testbed_cfg = testbed_cfg
        self.framework_cfg = framework_cfg
        self.fasttest = fasttest
        try:
            self.name = self.pod_api.name
        except AttributeError:
            self.name = 'dut'
        self.workspace = f'{workspace}/'

        self.allowed_channels = {}
        self.regulatory_domain = self.device_cfg.get('regulatory_domain', 'US')
        self.fut_dir = self.testbed_cfg.get(
            '{}.fut_dir'.format(self.name),
            self.testbed_cfg.get('default.fut_dir', '/tmp/fut-base/')
        )
        self.remove_base_folder()
        self.shell_timeout_org = self.framework_cfg.get('test_script_timeout', TEST_SCRIPT_TIMEOUT)
        self.test_script_timeout = self.framework_cfg.get('test_script_timeout', TEST_SCRIPT_TIMEOUT)
        self.opensync_rootdir = self.shell_cfg.get('OPENSYNC_ROOTDIR')
        self.shell_env = Config(self.get_shell_cfg())

        # Retrieve version string
        try:
            self.device_version_str = self.get_version()
        except Exception as e:
            log.warning(f'Could not get fw version\n{e}')
            self.device_version_str = None

    def get_remote_test_command(self, test_path, params="", suffix=".sh", folder='shell/'):
        remote_command = "{}/{}/{}{} {}".format(
            self.fut_dir,
            folder,
            test_path,
            suffix,
            params
        )

        return remote_command

    def get_if_names(self, as_arguments=False):
        phy_radio_keys = [
            'phy_radio_name_24g',
            'phy_radio_name_5g',
            'phy_radio_name_5gl',
            'phy_radio_name_5gu',
        ]
        if_names = [
            self.device_cfg.get(phy_radio_name, None) for phy_radio_name in phy_radio_keys
            if self.device_cfg.get(phy_radio_name, None) is not None
        ]

        return ' '.join(if_names) if as_arguments else if_names

    def run(
        self,
        test_path,
        params="",
        suffix=".sh",
        folder='shell/',
    ):
        remote_command = self.get_remote_test_command(test_path, params, suffix, folder)
        log.info(f'Executing ({self.name}): {remote_command}')
        cmd_start = time.strftime("%H:%M:%S")
        cmd_res = self.pod_api.run_raw(remote_command, timeout=self.test_script_timeout + 30)
        cmd_end = time.strftime("%H:%M:%S")
        if cmd_res[0] != 0:
            log.debug(
                f"device={self.name}\n"
                f"cmd={remote_command}\n"
                f"ec={cmd_res[0]}\n"
                f"std_err={cmd_res[2]}"
            )
        else:
            log.debug(
                f"device={self.name}\n"
                f"cmd={remote_command}\n"
                f"ec={cmd_res[0]}"
            )
        self.recipe.add(
            device=self.name,
            cmd=remote_command,
            start_t=cmd_start,
            end_t=cmd_end,
            ec=cmd_res[0]
        )
        print(cmd_res[1])
        self.fut_exception.fut_raise(cmd_res)
        return cmd_res[0]

    def run_raw(
            self,
            test_path,
            params="",
            suffix=".sh",
            folder='shell/',
    ):
        remote_command = self.get_remote_test_command(test_path, params, suffix, folder)
        log.info(f'Executing ({self.name}): {remote_command}')
        cmd_start = time.strftime("%H:%M:%S")
        cmd_res = self.pod_api.run_raw(remote_command, timeout=self.test_script_timeout + 30)
        cmd_end = time.strftime("%H:%M:%S")
        if cmd_res[0] != 0:
            log.debug(
                f"device={self.name}\n"
                f"cmd={remote_command}\n"
                f"ec={cmd_res[0]}\n"
                f"std_err={cmd_res[2]}"
            )
        else:
            log.debug(
                f"device={self.name}\n"
                f"cmd={remote_command}\n"
                f"ec={cmd_res[0]}"
            )
        self.recipe.add(
            device=self.name,
            cmd=remote_command,
            start_t=cmd_start,
            end_t=cmd_end,
            ec=cmd_res[0]
        )
        print(cmd_res[1])
        self.fut_exception.fut_raise(cmd_res)

        return cmd_res[0], cmd_res[1]

    def set_allowed_channels(self):
        log.info(msg="Acquiring allowed channels for {}".format(self.name))

        phy_radio_names = self.get_if_names(as_arguments=True)
        self.run('tools/device/wm2_setup', phy_radio_names)
        allowed_channels_dict = {}
        for if_name in self.get_if_names(as_arguments=False):
            allowed_ch_res = self.pod_api.run_raw('tools/device/ovsdb/get_ovsdb_value', get_command_arguments(
                'Wifi_Radio_State', 'channels', 'if_name', if_name, 'true'
            ))
            allowed_ch_ec, allowed_ch_str = allowed_ch_res[0], allowed_ch_res[1]
            log.info(allowed_ch_res)
            if allowed_ch_ec == 0 and allowed_ch_str:
                allowed_channels_list_str = re.findall(r"\d+", allowed_ch_str)
                if allowed_channels_list_str:
                    try:
                        allowed_channels_list = list(map(int, allowed_channels_list_str))
                    except Exception as e:
                        log.exception(msg=f'No allowed channels\n{e}')
                        allowed_channels_list = False

                    allowed_channels_dict[if_name] = allowed_channels_list
                else:
                    allowed_channels_dict[if_name] = False
            else:
                log.info(msg="Unable to acquire allowed channels for radio band {} on ".format(
                    if_name,
                    self.name
                ))
                allowed_channels_dict[if_name] = False
        self.__setattr__('allowed_channels', allowed_channels_dict)
        return allowed_channels_dict

    def check_interface_configuration(self, ht_mode, channel, if_name):
        is_in_domain = True
        is_dfs = False

        if channel in regulatory_channel_list[self.regulatory_domain]["non_dfs"]:
            is_dfs = False
        elif channel in regulatory_channel_list[self.regulatory_domain]["dfs"]:
            is_dfs = True
        else:
            is_in_domain = False

        if not is_in_domain:
            log.info(msg="Channel {} is not in regulatory domain of {}, expecting fail!".format(
                channel, self.regulatory_domain)
            )
            return False

        channel_allowed = True

        if if_name in self.allowed_channels \
                and isinstance(self.allowed_channels[if_name], list) \
                and channel not in self.allowed_channels[if_name]:
            log.info(msg="Channel is not in Wifi_Radio_State allowed_channels columns. Test Could fail!")

        if not channel_allowed:
            log.info(msg="Channel {} is not allowed on {}, expecting fail!".format(channel, self.name))
            return False

        if is_dfs:
            log.info(msg=f"Channel {channel} is dfs channel, test could fail due to longer dfs channel range scan")

        if channel not in channel_ht_mode_map[ht_mode]:
            log.info(msg="Channel {} is out of ht range {}, expecting fail!".format(channel, ht_mode))
            return False
        else:
            return True

    def create_radio_vif_interfaces(self, cfgs):
        bring_up_pass = True
        for cfg in cfgs:
            if_name = cfg.get("if_name", "")
            vif_if_name = cfg.get("vif_if_name", "")
            channel = cfg.get("channel", "")
            channel_mode = cfg.get("channel_mode", "")
            ht_mode = cfg.get("ht_mode", "")
            hw_mode = cfg.get("hw_mode", "")
            country = cfg.get("country", "")
            enabled = cfg.get("enabled", "")
            mode = cfg.get("mode", "")
            ssid = cfg.get("ssid", "")
            ssid_broadcast = cfg.get("ssid_broadcast", "")
            vif_radio_idx = cfg.get("vif_radio_idx", "")
            security = self.get_security_settings(cfg)
            parent = cfg.get("parent", "")
            mac_list = cfg.get("mac_list", "")
            mac_list_type = cfg.get("mac_list_type", "")
            tx_chainmask = cfg.get("tx_chainmask", "")
            tx_power = cfg.get("tx_power", "")
            fallback_parents = cfg.get("fallback_parents", "")
            ap_bridge = cfg.get("ap_bridge", "")
            bridge = cfg.get("bridge", "")
            dynamic_beacon = cfg.get("dynamic_beacon", "")
            vlan_id = cfg.get("vlan_id", "")

            interface_configuration_valid = self.check_interface_configuration(ht_mode, channel, if_name)

            if not interface_configuration_valid:
                log.info(msg='Invalid ht_mode / channel configuration, expecting fail on {}'.format(self.name))

            if_name = "-if_name {}".format(if_name) if if_name != "" else ""
            vif_if_name = "-vif_if_name {}".format(vif_if_name) if vif_if_name != "" else ""
            channel = "-channel {}".format(channel) if channel != "" else ""
            channel_mode = "-channel_mode {}".format(channel_mode) if channel_mode != "" else ""
            ht_mode = "-ht_mode {}".format(ht_mode) if ht_mode != "" else ""
            hw_mode = "-hw_mode {}".format(hw_mode) if hw_mode != "" else ""
            country = "-country {}".format(country) if country != "" else ""
            enabled = "-enabled {}".format(enabled) if enabled != "" else ""
            mode = "-mode {}".format(mode) if mode != "" else ""
            ssid = "-ssid {}".format(ssid) if ssid != "" else ""
            ssid_broadcast = "-ssid_broadcast {}".format(ssid_broadcast) if ssid_broadcast != "" else ""
            vif_radio_idx = "-vif_radio_idx {}".format(vif_radio_idx) if vif_radio_idx != "" else ""
            security = "-security {}".format(security) if security != "" else ""
            parent = "-parent {}".format(parent) if parent != "" else ""
            mac_list = "-mac_list {}".format(mac_list) if mac_list != "" else ""
            mac_list_type = "-mac_list_type {}".format(mac_list_type) if mac_list_type != "" else ""
            tx_chainmask = "-tx_chainmask {}".format(tx_chainmask) if tx_chainmask != "" else ""
            tx_power = "-tx_power {}".format(tx_power) if tx_power != "" else ""
            fallback_parents = "-fallback_parents {}".format(fallback_parents) if fallback_parents != "" else ""
            ap_bridge = "-ap_bridge {}".format(ap_bridge) if ap_bridge != "" else ""
            bridge = "-bridge {}".format(bridge) if bridge != "" else ""
            dynamic_beacon = "-dynamic_beacon {}".format(dynamic_beacon) if dynamic_beacon != "" else ""
            vlan_id = "-vlan_id {}".format(vlan_id) if vlan_id != "" else ""

            dut_cmd_params = get_command_arguments(
                if_name,
                vif_if_name,
                vif_radio_idx,
                channel,
                channel_mode,
                ht_mode,
                hw_mode,
                country,
                enabled,
                mode,
                ssid,
                ssid_broadcast,
                security,
                parent,
                mac_list,
                mac_list_type,
                tx_chainmask,
                tx_power,
                fallback_parents,
                ap_bridge,
                bridge,
                dynamic_beacon,
                vlan_id
            )

            exit_code = self.run('tools/device/create_radio_vif_interface', dut_cmd_params)

            if interface_configuration_valid and exit_code != 0 and exit_code != 3:
                bring_up_pass = False
                break
            elif interface_configuration_valid and exit_code == 0:
                bring_up_pass = True
            elif interface_configuration_valid and exit_code == 3:
                return "skip"
            elif not interface_configuration_valid and exit_code != 0:
                bring_up_pass = True
            else:
                bring_up_pass = False
                break

        return bring_up_pass

    def change_name(self, name):
        self.__setattr__('name', name)
        self.pod_api.__setattr__('name', name)

    def create_inet_interfaces(self, cfgs):
        bring_up_pass = True
        for cfg in cfgs:
            if_name = cfg.get("if_name", "")
            enabled = cfg.get("enabled", "")
            network = cfg.get("network", "")
            if_type = cfg.get("if_type", "")
            ip_assign_scheme = cfg.get("ip_assign_scheme", "")
            NAT = cfg.get("NAT", "")
            upnp_mode = cfg.get("upnp_mode", "")
            inet_addr = cfg.get("inet_addr", "")
            netmask = cfg.get("netmask", "")
            broadcast = cfg.get("broadcast", "")
            mtu = cfg.get("mtu", "")
            dhcpd = self.get_dhcpd_settings(cfg.get('dhcpd', False))
            gre_ifname = cfg.get("if_gre", "")
            gre_remote_inet_addr = cfg.get("gre_remote_inet_addr", "")
            gre_local_inet_addr = cfg.get("gre_remote_inet_addr", "")

            if_name = "-if_name {}".format(if_name) if if_name != "" else ""
            enabled = "-enabled {}".format(enabled) if enabled != "" else ""
            network = "-network {}".format(network) if network != "" else ""
            if_type = "-if_type {}".format(if_type) if if_type != "" else ""
            ip_assign_scheme = "-ip_assign_scheme {}".format(ip_assign_scheme) if ip_assign_scheme != "" else ""
            NAT = "-NAT {}".format(NAT) if NAT != "" else ""
            upnp_mode = "-upnp_mode {}".format(upnp_mode) if upnp_mode != "" else ""
            inet_addr = "-inet_addr {}".format(inet_addr) if inet_addr != "" else ""
            netmask = "-netmask {}".format(netmask) if netmask != "" else ""
            broadcast = "-broadcast {}".format(broadcast) if broadcast != "" else ""
            mtu = "-mtu {}".format(mtu) if mtu != "" else ""
            dhcpd = "-dhcpd {}".format(dhcpd) if dhcpd != "" else ""
            gre_ifname = "-gre_ifname {}".format(gre_ifname) if gre_ifname != "" else ""
            gre_remote_inet_addr = f"-gre_remote_inet_addr {gre_remote_inet_addr}" if gre_remote_inet_addr != "" else ""
            gre_local_inet_addr = f"-gre_local_inet_addr {gre_local_inet_addr}" if gre_local_inet_addr != "" else ""

            dut_cmd_params = get_command_arguments(
                if_name,
                enabled,
                network,
                if_type,
                inet_addr,
                netmask,
                broadcast,
                ip_assign_scheme,
                mtu,
                NAT,
                upnp_mode,
                dhcpd,
                gre_ifname,
                gre_remote_inet_addr,
                gre_local_inet_addr
            )

            create_inet_interfaces_exit_code = self.run('tools/device/create_inet_interface', dut_cmd_params)

            if create_inet_interfaces_exit_code == 0:
                bring_up_pass = True
            else:
                bring_up_pass = False
                break

        return bring_up_pass

    @staticmethod
    def get_dhcpd_settings(dhcpd_cfg):
        if not dhcpd_cfg:
            return ""

        dhcpd_cfg = Config(dhcpd_cfg)
        dhcp_option = "" if 'dhcp_option' not in dhcpd_cfg.cfg else '["dhcp_option","{}"]'.format(
            dhcpd_cfg.get('dhcp_option', "\"\"")
        )
        force = "" if 'force' not in dhcpd_cfg.cfg else '["force","{}"]'.format(
            dhcpd_cfg.get('force', "\"\"")
        )
        lease_time = "" if 'lease_time' not in dhcpd_cfg.cfg else '["lease_time","{}"]'.format(
            dhcpd_cfg.get('lease_time', "\"\"")
        )
        start = "" if 'start' not in dhcpd_cfg.cfg else '["start","{}"]'.format(
            dhcpd_cfg.get('start', "\"\"")
        )
        stop = "" if 'stop' not in dhcpd_cfg.cfg else '["stop","{}"]'.format(
            dhcpd_cfg.get('stop', "\"\"")
        )
        dhcpd_string = \
            '\'["map",[{}]]\''.replace(
                ' ', ''
            ).format(
                ','.join(x for x in [
                    dhcp_option,
                    force,
                    lease_time,
                    start,
                    stop
                ] if x)
            )
        return dhcpd_string

    @staticmethod
    def get_security_settings(if_cfg):
        security_cfg = if_cfg.get("security", False)
        if not security_cfg:
            return ""

        security_cfg = Config(security_cfg)
        encryption = "" if 'encryption' not in security_cfg.cfg else '["encryption","{}"]'.format(
            security_cfg.get('encryption', "\"\"")
        )
        key = "" if 'key' not in security_cfg.cfg else '["key","{}"]'.format(
            security_cfg.get('key', "\"\"")
        )
        mode = "" if 'mode' not in security_cfg.cfg else '["mode","{}"]'.format(
            security_cfg.get('mode', "\"\"")
        )
        security_string = \
            '\'["map",[{}]]\''.replace(
                ' ', ''
            ).format(
                ','.join(x for x in [
                    encryption,
                    key,
                    mode
                ] if x)
            )
        return security_string

    @staticmethod
    def bring_up_sta_interface(dut_handler, ref_handler, dut_cfg):
        ref_ssid = dut_cfg["ref_ssid"]

        val_res =  ref_handler.run_raw('tools/device/ovsdb/get_ovsdb_value', get_command_arguments(
            'Wifi_VIF_State', 'mac', 'ssid', ref_ssid
        ))
        ref_mac_ec, ref_mac_str = val_res[0], val_res[1]
        dut_cfg["ssid"] = ref_ssid
        dut_cfg["parent"] = ref_mac_str
        dut_cfg["mode"] = "sta"

        bring_up_sta_result = dut_handler.create_radio_vif_interfaces([dut_cfg])

        return bring_up_sta_result

    def connect_rpi_to_wpa2(
            self,
            interface,
            network_ssid,
            network_bssid,
            network_pass,
            network_key_mgmt,
            enable_dhcp,
    ):

        connect_cmd = get_command_arguments(
            interface,
            network_ssid,
            network_bssid,
            network_pass,
            network_key_mgmt,
            enable_dhcp,
        )

        return self.run("tools/rpi/connect_to_wpa2", connect_cmd)

    def get_log_read(self):
        if not self.shell_env.get('LOGREAD', False):
            return ["LOGREAD not available on this device"]

        log_read_lines = self.pod_api.run_raw(
            '{} | tail -600'.format(self.shell_env.get('LOGREAD'))
        )[1]
        return log_read_lines

    # TRANSFER START #
    def remove_base_folder(self):
        base_path = self.fut_dir
        cmd = "rm -rf {}".format(
            base_path
        )
        exit_code = self.pod_api.run_raw(cmd)[0]

        if exit_code != 0:
            log.info(f'Failed to remove {base_path} on {self.name} - exit code - {exit_code}')
            return False
        return True

    def clear_tests_folder(self):
        tests_path = f'{self.fut_dir}/shell/tests/*'
        cmd = f"rm -rf {tests_path}"
        exit_code = self.pod_api.run_raw(cmd)[0]
        if exit_code != 0:
            log.warning(f'Failed to empty {tests_path} on {self.name}')
        return True

    def transfer(self, *args, **kwargs):
        cfg = Config({
            'method': kwargs.get('method') if kwargs.get('method') else self.framework_cfg.get('transfer_method'),
            'tr_name': kwargs.get('tr_name') if kwargs.get('tr_name') else 'fut_tr.tar',
            'direction': kwargs.get('direction') if kwargs.get('direction') else 'push',
            'to': kwargs.get('to') if kwargs.get('to') else self.fut_dir,
            'full': kwargs.get('full') if kwargs.get('full') else False,
            'manager': kwargs.get('manager') if kwargs.get('manager') else False,
            'file': kwargs.get('file') if kwargs.get('file') else False,
            'folder': kwargs.get('folder') if kwargs.get('folder') else False,
        })
        cfg.set('tr_path', kwargs.get('tr_path') if kwargs.get('tr_path') else f'{self.workspace}/{cfg.get("tr_name")}')

        log.info(f'Initiated transfer to {self.name}')
        log.debug(f'Transfer cfg: {cfg.cfg}')

        self.create_tr_tar(cfg)
        self.pod_api.run_raw(f'mkdir -p {cfg.get("to")}')
        log.debug(f'Transfering {cfg.get("tr_path")} from RPI to {cfg.get("to")} on {self.name}')
        self.pod_api.put_file(file_name=cfg.get("tr_path"), location=cfg.get("to"))
        if True:
            tr_name = cfg.get('tr_name')
            unpack_tar_cmd = f'cd {cfg.get("to")} && tar -C / -xf {tr_name}'
            log.debug(f'Unpacking {tr_name}: {unpack_tar_cmd}')
            unpack_res = self.pod_api.run_raw(unpack_tar_cmd)
            unpack_ec, unpack_std, unpack_std_err = unpack_res[0], unpack_res[1], unpack_res[2]
            if unpack_ec != 0:
                raise Exception(
                    f'Unpack command fail!\n'
                    f'cmd: {unpack_tar_cmd}\n'
                    f'std_out:\n{unpack_std}\n'
                    f'std_err:\n{unpack_std_err}\n'
                    f'ec: {unpack_ec}'
                )

            tar_content = self.pod_api.run_raw(f'cd {cfg.get("to")} && tar -tf {tr_name}')[1]
            tar_reg_paths = re.findall(
                rf'({cfg.get("to", self.fut_dir)[1:-1]}\/.*?\/)|({cfg.get("to", self.fut_dir)[1:-1]}\/.*?.*)',
                '\n'.join(tar_content)
            )
            tar_paths = list(dict.fromkeys(tar_reg_paths))

            paths_for_permissions = []
            for tp in tar_paths:
                paths_for_permissions.append('/' + ''.join(tp))

            if paths_for_permissions:
                paths_for_permissions = ' '.join(paths_for_permissions)
                set_permission_cmd = f"chmod -R 755 {paths_for_permissions}"
                log.debug(f'Setting permissions on {paths_for_permissions}: {set_permission_cmd}')
                permission_ec = self.pod_api.run_raw(set_permission_cmd)[0]
                if permission_ec != 0:
                    raise Exception(f'Permission command fail, set_permission_cmd: {set_permission_cmd}')

            rm_tar_cmd = f'rm {cfg.get("to")}/{tr_name}'
            log.debug(f'Removing {tr_name}: {rm_tar_cmd}')
            rm_ec = self.pod_api.run_raw(rm_tar_cmd)[0]
            if rm_ec != 0:
                raise Exception(f'Remove tar command fail, rm_tar_cmd: {rm_tar_cmd}')
        log.info(f'Transfer finished to {self.name}')
        return True

    def get_tr_cmd(self, cfg):
        tr_method = cfg.get('method')
        tr_direction = cfg.get('direction')
        if any(
            [
                (tr_method == 'scp' and tr_direction == 'push'),
                (tr_method == 'curl' and tr_direction == 'pull')
            ]
        ):
            tr_host = 'rpi'
        else:
            tr_host = 'device'

        to_path = cfg.get('to')
        from_path = cfg.get('tr_path')

        # TODO: How to check from PodApi if sshpass is used?
        simple_auth = self.framework_cfg.get("password", False)
        if tr_host == 'rpi' and simple_auth and tr_method == 'scp':
            tr_cmd = f'sshpass -p {self.framework_cfg.get("password")} scp -q {from_path} {self.framework_cfg.get("username")}@{self.testbed_cfg.get(f"{self.name}.host")}:{to_path}'
        elif tr_host == 'rpi' and not simple_auth and tr_method == 'scp':
            tr_cmd = f'scp -q {from_path} {self.framework_cfg.get("username")}@{self.testbed_cfg.get(f"{self.name}.host")}:{to_path}'
        elif tr_host == 'rpi' and tr_method == 'curl' and tr_direction == 'pull':
            tr_cmd = f'curl --silent --fail http://{self.testbed_cfg.get(f"{self.name}.host")}/{from_path} > {to_path}{cfg.get("tr_name")}'
        elif tr_host == 'device' and tr_method == 'scp':
            tr_pwd = self.testbed_cfg.get("server.password")
            tr_username = self.testbed_cfg.get("server.username")
            tr_hostname = self.testbed_cfg.get("server.host")
            tr_cmd = f'sshpass -p {tr_pwd} scp -q {from_path} {tr_username}@{tr_hostname}:{to_path}'
        elif tr_host == 'device' and tr_method == 'curl':
            self.pod_api.run_raw(f'mkdir -p {to_path}')
            curl_host = self.testbed_cfg.get("server.curl.host")
            tr_cmd = f'curl --silent --fail {curl_host}/{from_path} > {to_path}{cfg.get("tr_name")}'
        else:
            raise Exception(
                'Invalid transfer combination provided:\n'
                f'tr_method: {tr_method}\n'
                f'tr_type: {tr_direction}\n'
                f'tr_host: {tr_host}'
            )

        return tr_cmd, tr_host

    def create_tr_tar(self, cfg):
        tr_path = cfg.get('tr_path')
        log.debug('Creating tar file for transfer')
        real_path = self.workspace
        if cfg.get('full', False):
            tar_cmd = f'tar -cvf {tr_path} {self.workspace}shell/'
        elif cfg.get('manager', False):
            tar_cmd = f'tar -cvf {tr_path} {self.workspace}shell/tests/{cfg.get("manager")}'
        elif cfg.get('file', cfg.get('folder', False)):
            tmp_path_param = cfg.get('file', cfg.get('folder', False))
            real_path = tmp_path_param if os.path.isabs(tmp_path_param) else f'{self.workspace}/{tmp_path_param}'
            tar_cmd = f'tar -cvf {tr_path} {real_path}'
            if os.path.basename(real_path):
                real_path = real_path.replace(os.path.basename(real_path), '')
        else:
            tar_cmd = f'tar -cvf {tr_path} {real_path}shell/lib {real_path}shell/config {real_path}shell/tools'

        tar_home_reg = os.path.relpath(real_path, '/').replace('/', '\\/')
        tar_to_reg = os.path.relpath(cfg.get('to', self.fut_dir), '/').replace('/', '\\/')
        tar_cmd = tar_cmd + f" --transform 's/{tar_home_reg}/{tar_to_reg}/'"
        log.debug(f'Executing on RPI: {tar_cmd}')
        tar_ec = os.system(tar_cmd)
        if tar_ec != 0:
            raise Exception(f'Failed to create tar file for transfer\ncfg: {cfg}\ntar_cmd: {tar_cmd}\ntar_ec: {tar_ec}')

        # TRANSFER END #
        # SHELL ENV START #

    def set_shell_environment_cmd(self):
        log.critical(f"Setting shell environment on {self.name}")
        env_vars_cmd = []
        for env_var_name, env_var_value in self.shell_cfg.cfg.items():
            if self.string_is_int(env_var_value):
                export_env_var_cmd = f'export {env_var_name}={env_var_value}'
            else:
                export_env_var_cmd = f'export {env_var_name}="{env_var_value}"'
            env_vars_cmd.append(export_env_var_cmd)
        env_var_final_cmd = " && ".join(env_vars_cmd)
        self.__setattr__('shell_env_command', env_var_final_cmd)

    @staticmethod
    def string_is_int(string_to_check):
        try:
            int(string_to_check)
            return True
        except ValueError:
            return False

    def get_shell_cfg(self):
        tmp_cfg = {}
        try:
            for key, value in self.shell_cfg.cfg.items():
                if isinstance(value, tuple):
                    tmp_cfg[key] = value[0].format(*[self.__getattribute__(i) for i in value[1:]])
                else:
                    tmp_cfg[key] = value
            tmp_cfg["GLOBAL_FUT_TOP_DIR"] = self.fut_dir
        except Exception as e:
            log.warning(msg=f'could not get shell_cfg\n\t{e}')
            tmp_cfg = {}
        return tmp_cfg

    def update_shell_timeout(self, new_timeout):
        if not new_timeout and self.test_script_timeout == self.shell_timeout_org:
            return True

        if new_timeout:
            if new_timeout == self.test_script_timeout:
                return True
        elif self.test_script_timeout == self.shell_timeout_org:
            return True
        else:
            new_timeout = self.shell_timeout_org

        log.info(f'Changing shell timeout from {self.test_script_timeout} to {new_timeout}')
        self.__setattr__('test_script_timeout', new_timeout)
        fut_set_env_path = self.create_fut_set_env()
        self.transfer(file=fut_set_env_path, to='/tmp/')
        self.recipe.clear()

    def create_fut_set_env(self):
        shell_fut_env_path = f'{self.workspace}shell/config/fut_set_env.sh'
        shell_fut_env_file = open(shell_fut_env_path, "w")
        for key, value in self.get_shell_cfg().items():
            ini_line = "{}=\"{}\"\n".format(key, value)
            shell_fut_env_file.write(ini_line)
        shell_fut_env_file.close()
        return shell_fut_env_path
    # SHELL ENV END #

    # UTILITY START #
    def get_version(self):
        device_version_cmd = self.framework_cfg.get('device_version_cmd', None)
        if not device_version_cmd:
            log.warning('Could not acquire device version - Missing framework_cfg["device_version_cmd"]')
        dev_ver_res = self.pod_api.run_raw(device_version_cmd)
        if dev_ver_res[0] != 0:
            log.warning(f'Could not acquire device version!\n'
                        f'cmd: {device_version_cmd}\n'
                        f'ec: {dev_ver_res[0]}\n'
                        f'std_err: {dev_ver_res[2]}')
            return None
        return dev_ver_res[1]

    def set_current_time(self):
        current_time = datetime.now()
        current_time_shell = time.strftime("%Y%m%d%H%M.%S")
        log.info(msg="Setting time {} on {}".format(
            current_time,
            self.name
        ))
        self.pod_api.run_raw('date -s {}'.format(current_time_shell))

    def do_log_pull_clear(self):
        logpull_cmd = self.device_cfg.get('LOGDUMP', False)
        if not logpull_cmd:
            return True

        log.info(msg='Clearing device {} logs'.format(
            self.name
        ))
        self.pod_api.run("{} > /tmp/clear_log_pull.tar.gz".format(
            logpull_cmd
        ))
    # UTILITY END #
