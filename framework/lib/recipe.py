class Recipe:
    def __init__(self):
        self.recipe = []
        self.recipe_setup = []

    def add(self, device=None, cmd=None, start_t=None, end_t=None, ec=None):
        self.recipe.append({
            'device': device,
            'cmd': cmd,
            'start_t': start_t,
            'end_t': end_t,
            'ec': ec,
        })

    def clear(self):
        self.recipe = []

    def mark_setup(self):
        self.recipe_setup = self.recipe
        self.recipe = []

    def clear_full(self):
        self.recipe = []
        self.recipe_setup = []

    def get_for_allure(self):
        recipe_list = []

        recipes = self.recipe_setup + self.recipe

        for rec in recipes:
            recipe_list.append(
                f'# device {rec["device"]}, start: {rec["start_t"]}, end: {rec["end_t"]}, exit_code: {rec["ec"]}\n'
                f'{rec["cmd"]}\n'
            )

        return recipe_list
