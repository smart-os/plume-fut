from .globals import ExpectedShellResult
from .globals import FutMain
import pytest


@pytest.fixture(autouse=True, scope="module")
def manager_folder_transfer(dut_handler, fasttest):
    assert dut_handler.clear_tests_folder()
    assert dut_handler.transfer(manager='ledm')
    FutMain.recipe.clear_full()

    yield
    if not fasttest:
        dut_handler.do_log_pull_clear()


ledm_config = FutMain.get_test_config(cfg_file_prefix='LEDM')


########################################################################################################################
test_ledm_led_on_off_inputs = ledm_config.get('ledm_led_on_off', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_ledm_led_on_off_inputs)
def test_ledm_led_on_off(test_config, dut_handler):
    assert dut_handler.run('tests/ledm/ledm_setup') == ExpectedShellResult
    assert dut_handler.run('tests/ledm/ledm_led_on_off') == ExpectedShellResult


########################################################################################################################
test_ledm_led_config_inputs = ledm_config.get('ledm_led_config', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_ledm_led_config_inputs)
def test_ledm_led_config(test_config, dut_handler):
    assert dut_handler.run('tests/ledm/ledm_setup') == ExpectedShellResult
    assert dut_handler.run('tests/ledm/ledm_led_config') == ExpectedShellResult
