from datetime import datetime
from config.testbed.config import testbed_cfg
from framework.lib.config import Config
from framework.lib.fut_cloud import FutCloud
from framework.tools.functions import get_command_arguments
from .globals import ExpectedShellResult
from .globals import FutMain
import pytest


@pytest.fixture(autouse=True, scope="module")
def onbrd_initial_setup(dut_handler, fasttest):
    assert dut_handler.clear_tests_folder()
    assert dut_handler.transfer(manager='dm')

    FutMain.recipe.clear_full()
    assert dut_handler.run('tests/dm/onbrd_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
    FutMain.recipe.mark_setup()
    yield
    if not fasttest:
        dut_handler.do_log_pull_clear()


# Read entire testcase configuration
onbrd_config = FutMain.get_test_config(cfg_file_prefix='ONBRD')


########################################################################################################################
test_onbrd_verify_fw_version_awlan_node_inputs = onbrd_config.get('onbrd_verify_fw_version_awlan_node', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_fw_version_awlan_node_inputs)
def test_onbrd_verify_fw_version_awlan_node(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['search_rule'],
    )
    assert dut_handler.run('tests/dm/onbrd_verify_fw_version_awlan_node', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_redirector_address_awlan_node_inputs = onbrd_config.get(
    'onbrd_verify_redirector_address_awlan_node', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_redirector_address_awlan_node_inputs)
def test_onbrd_verify_redirector_address_awlan_node(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['redirector_addr'],
    )
    assert dut_handler.run('tests/dm/onbrd_verify_redirector_address_awlan_node', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_id_awlan_node_inputs = onbrd_config.get('onbrd_verify_id_awlan_node', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_id_awlan_node_inputs)
def test_onbrd_verify_id_awlan_node(test_config, dut_handler):
    assert dut_handler.run('tests/dm/onbrd_verify_id_awlan_node') == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_model_awlan_node_inputs = onbrd_config.get('onbrd_verify_model_awlan_node', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_model_awlan_node_inputs)
def test_onbrd_verify_model_awlan_node(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['model'],
    )
    assert dut_handler.run('tests/dm/onbrd_verify_model_awlan_node', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_client_certificate_files_inputs = onbrd_config.get('onbrd_verify_client_certificate_files', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_client_certificate_files_inputs)
def test_onbrd_verify_client_certificate_files(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['cert_file'],
    )
    assert dut_handler.run('tests/dm/onbrd_verify_client_certificate_files', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_manager_hostname_resolved_inputs = onbrd_config.get('onbrd_verify_manager_hostname_resolved', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_manager_hostname_resolved_inputs)
def test_onbrd_verify_manager_hostname_resolved(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['manager_addr'],
        *test_config['target'] if isinstance(test_config['target'], list) else test_config['target'],
    )
    assert dut_handler.run('tests/dm/onbrd_verify_manager_hostname_resolved', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_wan_ip_address_inputs = onbrd_config.get('onbrd_verify_wan_ip_address', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_wan_ip_address_inputs)
def test_onbrd_verify_wan_ip_address(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['wan_interface'],
        testbed_cfg['dut']['wan_ip'],
    )
    assert dut_handler.run('tests/dm/onbrd_verify_wan_ip_address', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_dhcp_dry_run_success_inputs = onbrd_config.get('onbrd_verify_dhcp_dry_run_success', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_dhcp_dry_run_success_inputs)
def test_onbrd_verify_dhcp_dry_run_success(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['if_name'],
    )
    assert dut_handler.run('tests/dm/onbrd_verify_dhcp_dry_run_success', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_bridge_mode_inputs = onbrd_config.get('onbrd_verify_bridge_mode', [])


@pytest.mark.parametrize(
    'test_config',
    test_onbrd_verify_bridge_mode_inputs
)
def test_onbrd_verify_bridge_mode(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['wan_interface'],
        test_config['wan_ip'],
        test_config['home_interface'],
        test_config['uplink_interface'],
        test_config['patch_w2h'],
        test_config['patch_h2w'],
    )
    res = dut_handler.run('tests/dm/onbrd_verify_bridge_mode', test_args) == ExpectedShellResult

    # Restore connection - run onbrd_setup.sh
    assert dut_handler.run('tests/dm/onbrd_setup') == ExpectedShellResult
    assert res


########################################################################################################################
test_onbrd_verify_number_of_radios_inputs = onbrd_config.get('onbrd_verify_number_of_radios', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_number_of_radios_inputs)
def test_onbrd_verify_number_of_radios(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['num_of_radios'],
    )
    assert dut_handler.run('tests/dm/onbrd_verify_number_of_radios', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_client_tls_connection_inputs = onbrd_config.get('onbrd_verify_client_tls_connection', [])


@pytest.mark.parametrize(
    'test_config',
    test_onbrd_verify_client_tls_connection_inputs
)
def test_onbrd_verify_client_tls_connection(test_config, dut_handler):
    tls_ver = test_config['tls_ver']
    assert FutCloud().change_tls_ver(tls_ver)
    assert FutCloud().restart_cloud()

    res = dut_handler.run('tests/dm/onbrd_verify_client_tls_connection') == ExpectedShellResult

    # Cloud connection must be broken - run onbrd_setup.sh
    assert dut_handler.run('tests/dm/onbrd_setup') == ExpectedShellResult
    assert res


########################################################################################################################
test_onbrd_verify_router_mode_inputs = onbrd_config.get('onbrd_verify_router_mode', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_router_mode_inputs)
def test_onbrd_verify_router_mode(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['wan_interface'],
        test_config['home_interface'],
        test_config['start_pool'],
        test_config['end_pool'],
    )

    res = dut_handler.run('tests/dm/onbrd_verify_router_mode', test_args) == ExpectedShellResult

    # Restore connection - run onbrd_setup.sh
    assert dut_handler.run('tests/dm/onbrd_setup') == ExpectedShellResult
    assert res


########################################################################################################################
test_onbrd_verify_onbrd_vaps_on_radios_inputs = onbrd_config.get('onbrd_verify_onbrd_vaps_on_radios', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_onbrd_vaps_on_radios_inputs)
def test_onbrd_verify_onbrd_vaps_on_radios(test_config, dut_handler):
    test_config = Config(test_config)
    test_args = get_command_arguments(
        test_config.get('interface.vif_if_name'),
    )
    assert dut_handler.run('tools/device/vif_clean', test_args) == ExpectedShellResult
    assert dut_handler.create_radio_vif_interfaces([test_config.get('interface')]) is True
    assert dut_handler.run('tests/dm/onbrd_verify_onbrd_vaps_on_radios', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_home_vaps_on_radios_inputs = onbrd_config.get('onbrd_verify_home_vaps_on_radios', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_home_vaps_on_radios_inputs)
def test_onbrd_verify_home_vaps_on_radios(test_config, dut_handler):
    test_config = Config(test_config)
    test_args = get_command_arguments(
        test_config.get('interface.vif_if_name'),
    )
    assert dut_handler.run('tools/device/vif_clean', test_args) == ExpectedShellResult
    assert dut_handler.create_radio_vif_interfaces([test_config.get('interface')]) is True
    assert dut_handler.run('tests/dm/onbrd_verify_home_vaps_on_radios', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_home_vaps_on_home_bridge_inputs = onbrd_config.get('onbrd_verify_home_vaps_on_home_bridge', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_home_vaps_on_home_bridge_inputs)
def test_onbrd_verify_home_vaps_on_home_bridge(test_config, dut_handler):
    test_config = Config(test_config)
    test_args = get_command_arguments(
        test_config.get('interface.vif_if_name'),
        test_config.get('home_interface.name'),
    )
    assert dut_handler.run('tools/device/vif_clean', test_args) == ExpectedShellResult
    assert dut_handler.create_radio_vif_interfaces([test_config.get('interface')]) is True
    assert dut_handler.run('tests/dm/onbrd_verify_home_vaps_on_home_bridge', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_dut_system_time_accuracy_inputs = onbrd_config.get('onbrd_verify_dut_system_time_accuracy', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_dut_system_time_accuracy_inputs)
def test_onbrd_verify_dut_system_time_accuracy(test_config, dut_handler):
    time_sec_since_epoch = datetime.utcnow().strftime('%s')
    test_args = time_sec_since_epoch + get_command_arguments(test_config['time_accuracy'],)

    assert dut_handler.run('tests/dm/onbrd_verify_dut_system_time_accuracy', test_args) == ExpectedShellResult


########################################################################################################################
test_onbrd_verify_device_mode_awlan_node_inputs = onbrd_config.get('onbrd_verify_device_mode_awlan_node', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_onbrd_verify_device_mode_awlan_node_inputs)
def test_onbrd_verify_device_mode_awlan_node(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['device_mode'],
    )

    assert dut_handler.run('tests/dm/onbrd_verify_device_mode_awlan_node', test_args) == ExpectedShellResult
