from framework.tools.functions import get_command_arguments
from .globals import ExpectedShellResult
from .globals import FutMain
import copy
import pytest


@pytest.fixture(autouse=True, scope="module")
def wm2_initial_setup(dut_handler, ref_handler, fasttest):
    for handler in [dut_handler, ref_handler]:
        if not handler:
            continue
        assert handler.clear_tests_folder()
        assert handler.transfer(manager='wm2')
    FutMain.recipe.clear_full()
    for handler in [dut_handler, ref_handler]:
        if not handler:
            continue
        assert handler.run('tests/wm2/wm2_setup', handler.get_if_names(True)) == ExpectedShellResult
    FutMain.recipe.mark_setup()
    yield
    if not fasttest:
        dut_handler.do_log_pull_clear()


wm_test_config = FutMain.get_test_config(cfg_file_prefix='WM')


########################################################################################################################
test_wm2_ht_mode_and_channel_iteration_first_run = False
test_wm2_ht_mode_and_channel_iteration_inputs = wm_test_config.get("wm2_ht_mode_and_channel_iteration")
test_wm2_ht_mode_and_channel_iteration_scenarios = []
for test_scenario in test_wm2_ht_mode_and_channel_iteration_inputs:
    for test_channel in test_scenario["channels"]:
        for ht_mode in test_scenario["ht_modes"]:
            tmp_cfg = copy.deepcopy(test_scenario)
            tmp_cfg.pop("channels", None)
            tmp_cfg.pop("ht_modes", None)
            tmp_cfg["channel"] = test_channel
            tmp_cfg["ht_mode"] = ht_mode
            if 'test_script_timeout' not in test_scenario:
                tmp_cfg["test_script_timeout"] = False
            else:
                tmp_cfg["test_script_timeout"] = test_scenario['test_script_timeout']
            test_wm2_ht_mode_and_channel_iteration_scenarios.append(tmp_cfg)


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_ht_mode_and_channel_iteration_scenarios)
def test_wm2_ht_mode_and_channel_iteration(test_config, dut_handler):
    global test_wm2_ht_mode_and_channel_iteration_first_run
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['vif_if_name'],
        test_config['vif_radio_idx'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country']
    )
    assert dut_handler.run('tests/wm2/wm2_ht_mode_and_channel_iteration', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_channel_non_dfs_inputs = wm_test_config.get("wm2_set_channel_non_dfs")
test_wm2_set_channel_non_dfs_scenarios = []
for test_scenario in test_wm2_set_channel_non_dfs_inputs:
    for test_channel in test_scenario["channels"]:
        tmp_cfg = copy.deepcopy(test_scenario)
        tmp_cfg.pop("channels", None)
        tmp_cfg["channel"] = test_channel
        if 'test_script_timeout' not in test_scenario:
            tmp_cfg["test_script_timeout"] = False
        else:
            tmp_cfg["test_script_timeout"] = test_scenario['test_script_timeout']
        test_wm2_set_channel_non_dfs_scenarios.append(tmp_cfg)


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_set_channel_non_dfs_scenarios)
def test_wm2_set_channel_non_dfs(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['default_channel'],
        test_config['channel_type']
    )
    assert dut_handler.run('tests/wm2/wm2_set_channel', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_channel_dfs_inputs = wm_test_config.get("wm2_set_channel_dfs")
test_wm2_set_channel_dfs_scenarios = []
for test_scenario in test_wm2_set_channel_dfs_inputs:
    for test_channel in test_scenario["channels"]:
        tmp_cfg = copy.deepcopy(test_scenario)
        tmp_cfg.pop("channels", None)
        tmp_cfg["channel"] = test_channel
        tmp_cfg["test_script_timeout"] = False if 'test_script_timeout' not in test_scenario else test_scenario[
            'test_script_timeout'
        ]
        test_wm2_set_channel_dfs_scenarios.append(tmp_cfg)


@pytest.mark.dut_only()
@pytest.mark.dfs()
@pytest.mark.parametrize('test_config', test_wm2_set_channel_dfs_scenarios)
def test_wm2_set_channel_dfs(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['default_channel'],
        test_config['channel_type']
    )
    assert dut_handler.run('tests/wm2/wm2_set_channel', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_ht_mode_first_run = False
test_wm2_set_ht_mode_inputs = wm_test_config.get("wm2_set_ht_mode")
test_wm2_set_ht_mode_scenarios = []
for test_scenario in test_wm2_set_ht_mode_inputs:
    for test_channel in test_scenario["channels"]:
        for ht_mode in test_scenario["ht_modes"]:
            tmp_cfg = copy.deepcopy(test_scenario)
            tmp_cfg.pop("channels", None)
            tmp_cfg.pop("ht_modes", None)
            tmp_cfg["channel"] = test_channel
            tmp_cfg["ht_mode"] = ht_mode
            tmp_cfg["test_script_timeout"] = False if 'test_script_timeout' not in test_scenario else test_scenario[
                'test_script_timeout'
            ]
            test_wm2_set_ht_mode_scenarios.append(tmp_cfg)


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_set_ht_mode_scenarios)
def test_wm2_set_ht_mode(test_config, dut_handler):
    global test_wm2_set_ht_mode_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name']
    )
    assert dut_handler.run('tests/wm2/wm2_set_ht_mode', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_bcn_int_inputs = wm_test_config.get("wm2_set_bcn_int", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_set_bcn_int_inputs)
def test_wm2_set_bcn_int(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['bcn_int']
    )
    assert dut_handler.run('tests/wm2/wm2_set_bcn_int', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_radio_tx_chainmask_inputs = wm_test_config.get("wm2_set_radio_tx_chainmask", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_set_radio_tx_chainmask_inputs)
def test_wm2_set_radio_tx_chainmask(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['tx_chainmask']
    )

    assert dut_handler.run('tests/wm2/wm2_set_radio_tx_chainmask', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_radio_thermal_tx_chainmask_first_run = False
test_wm2_set_radio_thermal_tx_chainmask_inputs = wm_test_config.get("wm2_set_radio_thermal_tx_chainmask", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_set_radio_thermal_tx_chainmask_inputs)
def test_wm2_set_radio_thermal_tx_chainmask(test_config, dut_handler):
    global test_wm2_set_radio_thermal_tx_chainmask_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['tx_chainmask'],
        test_config['thermal_tx_chainmask']
    )

    if not test_wm2_set_radio_thermal_tx_chainmask_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        test_wm2_set_radio_thermal_tx_chainmask_first_run = True

    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_set_radio_thermal_tx_chainmask', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_radio_enabled_first_run = False
test_wm2_set_radio_enabled_first_run_inputs = wm_test_config.get("wm2_set_radio_enabled", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_set_radio_enabled_first_run_inputs)
def test_wm2_set_radio_enabled(test_config, dut_handler):
    global test_wm2_set_radio_enabled_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['enabled']
    )

    if not test_wm2_set_radio_enabled_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        test_wm2_set_radio_enabled_first_run = True

    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_set_radio_enabled', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_immutable_radio_if_name_first_run = False
test_wm2_immutable_radio_if_name_first_run_inputs = wm_test_config.get("wm2_immutable_radio_if_name", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_immutable_radio_if_name_first_run_inputs)
def test_wm2_immutable_radio_if_name(test_config, dut_handler):
    global test_wm2_immutable_radio_if_name_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['custom_if_name']
    )

    if not test_wm2_immutable_radio_if_name_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        test_wm2_immutable_radio_if_name_first_run = True
    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_immutable_radio_if_name', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_immutable_radio_hw_type_first_run = False
test_wm2_immutable_radio_hw_type_first_run_inputs = wm_test_config.get("wm2_immutable_radio_hw_type", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_immutable_radio_hw_type_first_run_inputs)
def test_wm2_immutable_radio_hw_type(test_config, dut_handler):
    global test_wm2_immutable_radio_hw_type_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['hw_type']
    )

    if not test_wm2_immutable_radio_hw_type_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        test_wm2_immutable_radio_hw_type_first_run = True

    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_immutable_radio_hw_type', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_immutable_radio_hw_mode_first_run = False
test_wm2_immutable_radio_hw_mode_first_run_inputs = wm_test_config.get("wm2_immutable_radio_hw_mode", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_immutable_radio_hw_mode_first_run_inputs)
def test_wm2_immutable_radio_hw_mode(test_config, dut_handler):
    global test_wm2_immutable_radio_hw_mode_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['custom_hw_mode']
    )

    if not test_wm2_immutable_radio_hw_mode_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        test_wm2_immutable_radio_hw_mode_first_run = True

    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_immutable_radio_hw_mode', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_immutable_radio_freq_band_first_run = False
test_wm2_immutable_radio_freq_band_first_run_inputs = wm_test_config.get("wm2_immutable_radio_freq_band", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_immutable_radio_freq_band_first_run_inputs)
def test_wm2_immutable_radio_freq_band(test_config, dut_handler):
    global test_wm2_immutable_radio_freq_band_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['freq_band']
    )
    if not test_wm2_immutable_radio_freq_band_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        test_wm2_immutable_radio_freq_band_first_run = True

    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_immutable_radio_freq_band', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_radio_vif_configs_first_run = False
test_wm2_set_radio_vif_configs_first_run_inputs = wm_test_config.get("wm2_set_radio_vif_configs", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_set_radio_vif_configs_first_run_inputs)
def test_wm2_set_radio_vif_configs(test_config, dut_handler):
    global test_wm2_set_radio_vif_configs_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['custom_channel']
    )

    if not test_wm2_set_radio_vif_configs_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        test_wm2_set_radio_vif_configs_first_run = True

    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_set_radio_vif_configs', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_radio_country_first_run = False
test_wm2_set_radio_country_first_run_inputs = wm_test_config.get("wm2_set_radio_country", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_set_radio_country_first_run_inputs)
def test_wm2_set_radio_country(test_config, dut_handler):
    global test_wm2_set_radio_country_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['country']
    )

    if not test_wm2_set_radio_country_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        test_wm2_set_radio_country_first_run = True

    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_set_radio_country', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_dfs_cac_aborted_first_run = False
test_wm2_dfs_cac_aborted_inputs = wm_test_config.get("wm2_dfs_cac_aborted", [])


@pytest.mark.dut_only()
@pytest.mark.dfs()
@pytest.mark.parametrize('test_config', test_wm2_dfs_cac_aborted_inputs)
def test_wm2_dfs_cac_aborted(test_config, dut_handler):
    global test_wm2_dfs_cac_aborted_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel1'],
        test_config['channel2'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['default_channel'],
        test_config['channel_type']
    )

    if not test_wm2_dfs_cac_aborted_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        test_wm2_dfs_cac_aborted_first_run = True

    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_dfs_cac_aborted', test_cmd) == ExpectedShellResult


########################################################################################################################
wm2_set_radio_fallback_parents_first_run = False
wm2_set_radio_fallback_parents_inputs = wm_test_config.get("wm2_set_radio_fallback_parents", [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', wm2_set_radio_fallback_parents_inputs)
def test_wm2_set_radio_fallback_parents(test_config, dut_handler):
    global wm2_set_radio_fallback_parents_first_run
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['fallback_channel']
    )
    if not wm2_set_radio_fallback_parents_first_run:
        assert dut_handler.run('tests/wm2/wm2_setup', dut_handler.get_if_names(True)) == ExpectedShellResult
        wm2_set_radio_fallback_parents_first_run = True

    assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
    assert dut_handler.run('tests/wm2/wm2_set_radio_fallback_parents', test_cmd) == ExpectedShellResult


########################################################################################################################
test_wm2_set_radio_tx_power_inputs = wm_test_config.get("wm2_set_radio_tx_power", [])
test_wm2_set_radio_tx_power_scenarios = []
for test_scenario in test_wm2_set_radio_tx_power_inputs:
    for test_tx_power in test_scenario["tx_powers"]:
        tmp_cfg = copy.deepcopy(test_scenario)
        tmp_cfg.pop("tx_powers", None)
        tmp_cfg["tx_power"] = test_tx_power
        test_wm2_set_radio_tx_power_scenarios.append(tmp_cfg)


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_wm2_set_radio_tx_power_scenarios)
def test_wm2_set_radio_tx_power(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['vif_radio_idx'],
        test_config['if_name'],
        test_config['ssid'],
        dut_handler.get_security_settings(test_config),
        test_config['channel'],
        test_config['ht_mode'],
        test_config['hw_mode'],
        test_config['mode'],
        dut_handler.regulatory_domain if 'country' not in test_config else test_config['country'],
        test_config['vif_if_name'],
        test_config['tx_power'],
        "\"\"" if "min_tx_power" not in test_config else test_config["min_tx_power"],
        "\"\"" if "max_tx_power" not in test_config else test_config["max_tx_power"]
    )

    assert dut_handler.run('tests/wm2/wm2_set_radio_tx_power', test_cmd) == ExpectedShellResult
