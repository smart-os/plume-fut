from .globals import FutMain
from .globals import ExpectedShellResult
import pytest
import os
import framework.tools.logger

skip_ut_test = False
ut_test_skip_reason = ''
ut_folder_name = ''
global log
log = framework.tools.logger.get_logger()


@pytest.fixture(autouse=True, scope="module")
def unit_test_pre_setup_procedure(dut_handler):
    if skip_ut_test:
        pytest.skip(ut_test_skip_reason)

    assert dut_handler.clear_tests_folder()
    assert dut_handler.transfer(manager='ut')
    assert dut_handler.transfer(folder=ut_res_path, to=FutMain.fut_dir + '/resource/ut/')
    FutMain.recipe.clear_full()
    assert dut_handler.run('tests/ut/ut_setup') == ExpectedShellResult
    FutMain.recipe.mark_setup()


ut_test_config = FutMain.get_test_config('UT')

ut_name = ut_test_config.get('ut.ut_name', '')
ut_res_path = '{}/resource/ut/'.format(FutMain.workspace)

try:
    if ut_name:
        ut_tar_path = '{}{}.tar'.format(ut_res_path, ut_name)
        ut_tar_bz2_path = '{}{}.tar.bz2'.format(ut_res_path, ut_name)
        ut_folder_path = '{}{}'.format(ut_res_path, ut_name)
        if os.path.isdir(ut_folder_path):
            log.info(msg='{} found, using it for UT collection'.format(ut_folder_path))
            ut_folder_name = ut_name
        elif os.path.isfile(ut_tar_path):
            log.info(msg='{} found, extracting it and using it for UT collection'.format(ut_tar_path))
            FutMain.execute('mkdir -p {}'.format(ut_folder_path))
            FutMain.execute('tar -xf {} -C {}'.format(ut_tar_path.replace('.bz2', ''), ut_folder_path))
            ut_folder_name = ut_name
        elif os.path.isfile(ut_tar_bz2_path):
            log.info(msg='{} found, extracting it and using it for UT collection'.format(ut_tar_bz2_path))
            FutMain.execute('mkdir -p {}'.format(ut_folder_path))
            FutMain.execute('bzip2 -d {}'.format(ut_tar_bz2_path))
            FutMain.execute('tar -xf {} -C {}'.format(ut_tar_bz2_path.replace('.bz2', ''), ut_folder_path))
            ut_folder_name = ut_name
        else:
            skip_ut_test = True
            ut_test_skip_reason = 'No folder {} or {} found, skipping!'.format(ut_folder_path, ut_tar_bz2_path)
    else:
        log.info(msg='UT_config ut_name not provided, will use first .tar.bz2 found, if none, will skip UT_test!')
        ut_found = False
        for item in os.listdir(ut_res_path):
            if item.endswith('.tar.bz2'):
                ut_folder_name = item.replace('.tar.bz2', '')
                ut_folder_path = '{}{}'.format(ut_res_path, ut_folder_name)

                if os.path.isdir(ut_folder_path):
                    FutMain.execute('rm -rf {}'.format(ut_folder_path))

                item_path = os.path.join(ut_res_path, item)

                FutMain.execute('mkdir -p {}'.format(ut_folder_path))
                FutMain.execute('bzip2 -kfd {}'.format(item_path))
                FutMain.execute('tar -xf {} -C {}'.format(item_path.replace('.bz2', ''), ut_folder_path))

                ut_found = True
                break
        if not ut_found:
            skip_ut_test = True
            ut_test_skip_reason = 'No *.tar.bz2 found in {}, skipping!'.format(ut_res_path)
except Exception as e:
    skip_ut_test = True
    ut_test_skip_reason = 'Exception caught while collecting UT_test\n{}'.format(e)


ut_config = FutMain.get_unit_test_config(folder=ut_folder_name)

if not ut_config:
    ut_config.append({
        "name": "No_UT_tests"
    })
    skip_ut_test = True
    ut_test_skip_reason = ut_test_skip_reason if ut_test_skip_reason else 'No UT tests collected'


@pytest.mark.skipif(condition=skip_ut_test, reason=ut_test_skip_reason)
@pytest.mark.parametrize(
    'test_config', ut_config,
    ids=[i['name'] for i in ut_config]
)
def test_ut(test_config, dut_handler):
    log.info(msg="Executing unit test {}".format(test_config['name']))
    assert dut_handler.run(test_config['path'], suffix="", folder='') == ExpectedShellResult
