from copy import deepcopy
from framework.lib.config import Config
from framework.tools.functions import get_command_arguments
from .globals import ExpectedShellResult
from .globals import FutMain
import pytest


@pytest.fixture(autouse=True, scope="module")
def sm_initial_setup(dut_handler, ref_handler, fasttest):
    for handler in [dut_handler, ref_handler]:
        if not handler:
            continue
        assert handler.clear_tests_folder()
        assert handler.transfer(manager='sm')
    FutMain.recipe.clear_full()
    for handler in [dut_handler, ref_handler]:
        if not handler:
            continue
        assert handler.run('tests/sm/sm_setup', handler.get_if_names(True)) == ExpectedShellResult
    FutMain.recipe.mark_setup()
    yield
    if not fasttest:
        dut_handler.do_log_pull_clear()


sm_test_config = FutMain.get_test_config(cfg_file_prefix='SM')

########################################################################################################################
test_sm_neighbor_report_inputs = sm_test_config.get("sm_neighbor_report")
test_sm_neighbor_report_scenarios = []
for test_scenario in test_sm_neighbor_report_inputs:
    tmp_scenario = {
        "create_interfaces": True,
        "dut_interfaces": test_scenario["dut_interfaces"],
        "neighbor_interfaces": test_scenario["neighbor_interfaces"],
        "report_configuration": test_scenario["report_configurations"].pop(0),
        "test_script_timeout":
            False if 'test_script_timeout' not in test_scenario else
            test_scenario['test_script_timeout']
    }
    if "skip" in tmp_scenario["report_configuration"]:
        tmp_scenario["skip"] = tmp_scenario["report_configuration"].get("skip")
        if "skip_msg" in tmp_scenario["report_configuration"]:
            tmp_scenario["skip_msg"] = tmp_scenario["report_configuration"].get("skip_msg")
        else:
            tmp_scenario["skip_msg"] = ""
    test_sm_neighbor_report_scenarios.append(deepcopy(tmp_scenario))
    tmp_scenario["dut_interfaces"] = None
    tmp_scenario["neighbor_interfaces"] = None
    for report_configuration in test_scenario["report_configurations"]:
        tmp_scenario = {
            "create_interfaces": False,
            "report_configuration": report_configuration,
            "test_script_timeout":
                False if 'test_script_timeout' not in test_scenario else
                test_scenario['test_script_timeout']
        }
        if "skip" in report_configuration:
            tmp_scenario["skip"] = report_configuration.get("skip")
            if "skip_msg" in report_configuration:
                tmp_scenario["skip_msg"] = report_configuration.get("skip_msg")
            else:
                tmp_scenario["skip_msg"] = ""
        test_sm_neighbor_report_scenarios.append(deepcopy(tmp_scenario))


@pytest.mark.require_ref()
@pytest.mark.parametrize('test_config', test_sm_neighbor_report_scenarios)
def test_sm_neighbor_report(test_config, dut_handler, ref_handler):
    test_config = Config(test_config)
    if test_config.check("create_interfaces", True):
        def dut_setup():
            assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
            return dut_handler.create_radio_vif_interfaces(
                test_config.get("dut_interfaces")
            )

        def neighbor_setup():
            assert ref_handler.run('tools/device/vif_clean') == ExpectedShellResult
            return ref_handler.create_radio_vif_interfaces(test_config.get("neighbor_interfaces"))

        dut_res, ref_res = FutMain.async_execution([dut_setup, neighbor_setup])
        assert dut_res is True
        assert ref_res is True

    report_cfg = test_config.get("report_configuration")

    get_ovsdb_res = ref_handler.run_raw('tools/device/ovsdb/get_ovsdb_value', get_command_arguments(
        'Wifi_VIF_State', 'mac', 'ssid', report_cfg['neighbor_ssid']
    ))
    neigh_mac_ec, neigh_mac_str = get_ovsdb_res[0], get_ovsdb_res[1]
    assert neigh_mac_ec == ExpectedShellResult

    report_inspect_cmd = get_command_arguments(
        report_cfg["radio_type"],
        report_cfg["channel"],
        report_cfg["survey_type"],
        report_cfg["reporting_interval"],
        report_cfg["sampling_interval"],
        report_cfg["report_type"],
        report_cfg["neighbor_ssid"],
        neigh_mac_str
    )
    report_scan_result = dut_handler.run("tests/sm/sm_inspect_neighbor_report", report_inspect_cmd)
    assert report_scan_result == ExpectedShellResult


########################################################################################################################
previous_test_result = True
test_sm_leaf_report_inputs = sm_test_config.get("sm_leaf_report")
test_sm_leaf_report_scenarios = []
for test_scenario in test_sm_leaf_report_inputs:
    tmp_scenario = {
        "create_interfaces": True,
        "dut_interfaces": test_scenario["dut_interfaces"],
        "leaf_interface": test_scenario["leaf_interfaces"].pop(0),
        "test_script_timeout":
            False if 'test_script_timeout' not in test_scenario else
            test_scenario['test_script_timeout']
    }
    if "skip" in tmp_scenario["leaf_interface"]:
        tmp_scenario["skip"] = tmp_scenario["leaf_interface"].get("skip")
        if "skip_msg" in tmp_scenario["leaf_interface"]:
            tmp_scenario["skip_msg"] = tmp_scenario["leaf_interface"].get("skip_msg")
        else:
            tmp_scenario["skip_msg"] = ""
    test_sm_leaf_report_scenarios.append(deepcopy(tmp_scenario))
    tmp_scenario["dut_interfaces"] = None
    tmp_scenario["leaf_interfaces"] = None
    for leaf_configuration in test_scenario["leaf_interfaces"]:
        tmp_scenario = {
            "create_interfaces": False,
            "leaf_interface": leaf_configuration,
            "test_script_timeout":
                False if 'test_script_timeout' not in test_scenario else
                test_scenario['test_script_timeout']
        }
        if "skip" in leaf_configuration:
            tmp_scenario["skip"] = leaf_configuration.get("skip")
            tmp_scenario["skip_msg"] = leaf_configuration.get("skip_msg") if "skip_msg" in leaf_configuration else ""
        test_sm_leaf_report_scenarios.append(tmp_scenario)


@pytest.mark.require_ref()
@pytest.mark.parametrize('test_config', test_sm_leaf_report_scenarios)
def test_sm_leaf_report(test_config, dut_handler, ref_handler):
    test_config = Config(test_config)
    if test_config.check("create_interfaces", True):
        assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
        assert dut_handler.create_radio_vif_interfaces(
            test_config.get("dut_interfaces")
        ) is True
    leaf_cfg = test_config.get("leaf_interface")
    assert ref_handler.run('tools/device/vif_clean') == ExpectedShellResult
    bring_sta_interface_result = ref_handler.bring_up_sta_interface(
        ref_handler,
        dut_handler,
        leaf_cfg
    )
    assert bring_sta_interface_result is True

    get_ovsdb_res = ref_handler.run_raw('tools/device/ovsdb/get_ovsdb_value', get_command_arguments(
        'Wifi_VIF_State', 'mac', 'ssid', leaf_cfg['ref_ssid']
    ))
    leaf_mac_ec, leaf_mac_str = get_ovsdb_res[0], get_ovsdb_res[1]
    assert leaf_mac_ec == ExpectedShellResult

    inspect_leaf_connection_cmd = get_command_arguments(
        leaf_cfg["report_cfg"]["radio_type"],
        leaf_cfg["report_cfg"]["reporting_interval"],
        leaf_cfg["report_cfg"]["sampling_interval"],
        leaf_cfg["report_cfg"]["report_type"],
        leaf_mac_str
    )
    report_scan_result = dut_handler.run('tests/sm/sm_inspect_leaf_report', inspect_leaf_connection_cmd)
    assert report_scan_result == ExpectedShellResult


########################################################################################################################
test_sm_survey_report_inputs = sm_test_config.get("sm_survey_report")
test_sm_survey_report_scenarios = []
for test_scenario in test_sm_survey_report_inputs:
    tmp_scenario = {
        "dut_interfaces": test_scenario["dut_interfaces"],
        "create_interfaces": True,
        "radio_type": test_scenario["reports_cfg"][0]["radio_type"],
        "survey_type": test_scenario["reports_cfg"][0]["survey_type"],
        "reporting_interval": test_scenario["reports_cfg"][0]["reporting_interval"],
        "sampling_interval": test_scenario["reports_cfg"][0]["sampling_interval"],
        "report_type": test_scenario["reports_cfg"][0]["report_type"],
        "channel": test_scenario["reports_cfg"][0]["channels"].pop(0),
        "test_script_timeout":
            False if 'test_script_timeout' not in test_scenario else
            test_scenario['test_script_timeout']
    }
    test_sm_survey_report_scenarios.append(deepcopy(tmp_scenario))
    tmp_scenario["dut_interfaces"] = None
    for report_cfg in test_scenario["reports_cfg"]:
        tmp_scenario["radio_type"] = report_cfg["radio_type"]
        tmp_scenario["survey_type"] = report_cfg["survey_type"]
        tmp_scenario["create_interfaces"] = False
        if "skip" in report_cfg:
            tmp_scenario["skip"] = report_cfg.get("skip")
            tmp_scenario["skip_msg"] = report_cfg.get("skip_msg") if "skip_msg" in report_cfg else ""
        for report_channel in report_cfg["channels"]:
            tmp_scenario["channel"] = report_channel
            test_sm_survey_report_scenarios.append(deepcopy(tmp_scenario))


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_sm_survey_report_scenarios)
def test_sm_survey_report(test_config, dut_handler):
    test_config = Config(test_config)
    if test_config.check("create_interfaces", True):
        assert dut_handler.create_radio_vif_interfaces(
            test_config.get("dut_interfaces")
        ) is True
    inspect_survey_report_cmd = get_command_arguments(
        test_config.get("radio_type"),
        test_config.get("channel"),
        test_config.get("survey_type"),
        test_config.get("reporting_interval"),
        test_config.get("sampling_interval"),
        test_config.get("report_type"),
    )
    report_scan_result = dut_handler.run('tests/sm/sm_inspect_survey_report', inspect_survey_report_cmd)
    assert report_scan_result == ExpectedShellResult
