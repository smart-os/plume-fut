from framework.main import FutMainClass
import string
import unicodedata

ExpectedShellResult = 0

FutMain = FutMainClass()

dut_handler_var = None
ref_handler_var = None
rpi_handler_var = None
gw_handler_var = None

established_connection_handlers_var = []

valid_filename_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
char_limit = 200


def string_to_filename(
    input_string,
    whitelist=valid_filename_chars,
    replace_characters=' ',
    replace_with="_"
):
    for r in replace_characters:
        input_string = input_string.replace(r, replace_with)
    cleaned_filename = unicodedata.normalize('NFKD', input_string).encode('ASCII', 'ignore').decode()
    cleaned_filename = ''.join(c for c in cleaned_filename if c in whitelist)
    return cleaned_filename[:char_limit]
