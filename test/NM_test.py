from copy import deepcopy
from framework.tools.functions import get_command_arguments
from .globals import ExpectedShellResult
from .globals import FutMain
import pytest


@pytest.fixture(autouse=True, scope="module")
def nm2_initial_setup(dut_handler, ref_handler, fasttest):
    for handler in [dut_handler, ref_handler]:
        if not handler:
            continue
        assert handler.clear_tests_folder()
        assert handler.transfer(manager='nm2')

    FutMain.recipe.clear_full()
    for handler in [dut_handler, ref_handler]:
        if not handler:
            continue
        assert handler.run('tests/nm2/nm2_setup', handler.get_if_names(True)) == ExpectedShellResult
    FutMain.recipe.mark_setup()
    yield
    if not fasttest:
        dut_handler.do_log_pull_clear()


nm_test_config = FutMain.get_test_config(cfg_file_prefix='NM')


########################################################################################################################
test_nm2_enable_disable_iface_network_inputs = nm_test_config.get('nm2_enable_disable_iface_network', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_enable_disable_iface_network_inputs)
def test_nm2_enable_disable_iface_network(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
    )
    assert dut_handler.run('tests/nm2/nm2_enable_disable_iface_network', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_rapid_multiple_insert_delete_iface_scenarios = []
test_nm2_rapid_multiple_insert_delete_iface_inputs = nm_test_config.get('nm2_rapid_multiple_insert_delete_iface', [])
for test_nm2_rapid_multiple_insert_delete_iface_configuration in test_nm2_rapid_multiple_insert_delete_iface_inputs:
    for iter_count in range(test_nm2_rapid_multiple_insert_delete_iface_configuration["iteration_count"]):
        tmp_scenario = {
            "if_name": test_nm2_rapid_multiple_insert_delete_iface_configuration["if_name"],
            "if_type": test_nm2_rapid_multiple_insert_delete_iface_configuration["if_type"],
            "vlan_no": test_nm2_rapid_multiple_insert_delete_iface_configuration["vlan_start"] + iter_count,
            "skip": test_nm2_rapid_multiple_insert_delete_iface_configuration.get('skip'),
            "skip_msg": test_nm2_rapid_multiple_insert_delete_iface_configuration.get('skip_msg'),
            "test_script_timeout":
                False if 'test_script_timeout' not in test_nm2_rapid_multiple_insert_delete_iface_configuration else
                test_nm2_rapid_multiple_insert_delete_iface_configuration['test_script_timeout']
        }
        test_nm2_rapid_multiple_insert_delete_iface_scenarios.append(deepcopy(tmp_scenario))


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_rapid_multiple_insert_delete_iface_scenarios)
def test_nm2_rapid_multiple_insert_delete_iface(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['vlan_no'],
        test_config['if_type'],
    )
    assert dut_handler.run('tests/nm2/nm2_rapid_multiple_insert_delete_iface', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_rapid_multiple_insert_delete_ovsdb_row_scenarios = []
test_nm2_rapid_multiple_insert_delete_ovsdb_row_inputs = nm_test_config.get(
    'nm2_rapid_multiple_insert_delete_ovsdb_row', [])

for test_nm2_rapid_multiple_insert_delete_ovsdb_row_config in test_nm2_rapid_multiple_insert_delete_ovsdb_row_inputs:
    for iter_count in range(test_nm2_rapid_multiple_insert_delete_ovsdb_row_config["iteration_count"]):
        tmp_scenario = {
            "if_name": test_nm2_rapid_multiple_insert_delete_ovsdb_row_config["if_name"],
            "if_type": test_nm2_rapid_multiple_insert_delete_ovsdb_row_config["if_type"],
            "vlan_no": test_nm2_rapid_multiple_insert_delete_ovsdb_row_config["vlan_start"] + iter_count,
            "skip": test_nm2_rapid_multiple_insert_delete_ovsdb_row_config.get('skip'),
            "skip_msg": test_nm2_rapid_multiple_insert_delete_ovsdb_row_config.get('skip_msg'),
            "test_script_timeout":
                False if 'test_script_timeout' not in test_nm2_rapid_multiple_insert_delete_ovsdb_row_config else
                test_nm2_rapid_multiple_insert_delete_ovsdb_row_config['test_script_timeout']
        }
        test_nm2_rapid_multiple_insert_delete_ovsdb_row_scenarios.append(deepcopy(tmp_scenario))


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_rapid_multiple_insert_delete_ovsdb_row_scenarios)
def test_nm2_rapid_multiple_insert_delete_ovsdb_row(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['vlan_no'],
        test_config['if_type'],
    )
    assert dut_handler.run('tests/nm2/nm2_rapid_multiple_insert_delete_ovsdb_row', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_gateway_inputs = nm_test_config.get('nm2_set_gateway', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_gateway_inputs)
def test_nm2_set_gateway(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['gateway'],
    )
    if test_config['if_type'] == 'vif':
        assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
        assert dut_handler.create_radio_vif_interfaces([test_config['interface']])

    assert dut_handler.run('tests/nm2/nm2_set_gateway', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_ovsdb_remove_reinsert_iface_inputs = nm_test_config.get('nm2_ovsdb_remove_reinsert_iface', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_ovsdb_remove_reinsert_iface_inputs)
def test_nm2_ovsdb_remove_reinsert_iface(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
    )
    assert dut_handler.run('tests/nm2/nm2_ovsdb_remove_reinsert_iface', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_ovsdb_configure_interface_dhcpd_inputs = nm_test_config.get('nm2_ovsdb_configure_interface_dhcpd', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_ovsdb_configure_interface_dhcpd_inputs)
def test_nm2_ovsdb_configure_interface_dhcpd(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['start_pool'],
        test_config['end_pool'],
    )
    if test_config['if_type'] == 'vif':
        assert dut_handler.run('tools/device/vif_clean') == ExpectedShellResult
        assert dut_handler.create_radio_vif_interfaces([test_config['interface']])
    assert dut_handler.run('tests/nm2/nm2_ovsdb_configure_interface_dhcpd', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_inet_addr_inputs = nm_test_config.get('nm2_set_inet_addr', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_inet_addr_inputs)
def test_nm2_set_inet_addr(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['inet_addr'],
    )
    assert dut_handler.run('tests/nm2/nm2_set_inet_addr', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_mtu_inputs = nm_test_config.get('nm2_set_mtu', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_mtu_inputs)
def test_nm2_set_mtu(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['mtu'],
    )
    assert dut_handler.run('tests/nm2/nm2_set_mtu', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_nat_inputs = nm_test_config.get('nm2_set_nat', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_nat_inputs)
def test_nm2_set_nat(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['NAT'],
    )
    assert dut_handler.run('tests/nm2/nm2_set_nat', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_netmask_inputs = nm_test_config.get('nm2_set_netmask', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_netmask_inputs)
def test_nm2_set_netmask(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['netmask'],
    )
    assert dut_handler.run('tests/nm2/nm2_set_netmask', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_broadcast_inputs = nm_test_config.get('nm2_set_broadcast', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_broadcast_inputs)
def test_nm2_set_broadcast(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['broadcast'],
    )
    assert dut_handler.run('tests/nm2/nm2_set_broadcast', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_ip_assign_scheme_inputs = nm_test_config.get('nm2_set_ip_assign_scheme', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_ip_assign_scheme_inputs)
def test_nm2_set_ip_assign_scheme(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['ip_assign_scheme'],
    )
    assert dut_handler.run('tests/nm2/nm2_set_ip_assign_scheme', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_ovsdb_ip_port_forward_inputs = nm_test_config.get('nm2_ovsdb_ip_port_forward', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_ovsdb_ip_port_forward_inputs)
def test_nm2_ovsdb_ip_port_forward(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['src_ifname'],
        test_config['src_port'],
        test_config['dst_ipaddr'],
        test_config['dst_port'],
        test_config['protocol']
    )
    assert dut_handler.run('tests/nm2/nm2_ovsdb_ip_port_forward', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_dns_inputs = nm_test_config.get('nm2_set_dns', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_dns_inputs)
def test_nm2_set_dns(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['primary_dns'],
        test_config['secondary_dns']
    )
    assert dut_handler.run('tests/nm2/nm2_set_dns', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_upnp_mode_inputs = nm_test_config.get('nm2_set_upnp_mode', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_upnp_mode_inputs)
def test_nm2_set_upnp_mode(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['internal_if'],
        test_config['external_if']
    )
    assert dut_handler.run('tests/nm2/nm2_set_upnp_mode', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_parent_ifname_inputs = nm_test_config.get('nm2_set_parent_ifname', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_parent_ifname_inputs)
def test_nm2_set_parent_ifname(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['parent_ifname'],
        test_config['vlan_id'],
    )
    assert dut_handler.run('tests/nm2/nm2_set_parent_ifname', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_set_vlan_id_inputs = nm_test_config.get('nm2_set_vlan_id', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_set_vlan_id_inputs)
def test_nm2_set_vlan_id(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['parent_ifname'],
        test_config['vlan_id'],
    )
    assert dut_handler.run('tests/nm2/nm2_set_vlan_id', test_cmd) == ExpectedShellResult


########################################################################################################################
test_nm2_configure_nonexistent_iface_inputs = nm_test_config.get('nm2_configure_nonexistent_iface', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_nm2_configure_nonexistent_iface_inputs)
def test_nm2_configure_nonexistent_iface(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name'],
        test_config['if_type'],
        test_config['inet_addr'],
    )
    assert dut_handler.run('tests/nm2/nm2_configure_nonexistent_iface', test_cmd) == ExpectedShellResult
