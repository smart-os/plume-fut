from framework.tools.functions import generate_image_key
from framework.tools.functions import get_command_arguments
from .globals import ExpectedShellResult
from .globals import FutMain
import framework.tools.logger
import os
import pytest

global log
log = framework.tools.logger.get_logger()


@pytest.fixture(autouse=True, scope="module")
def um_initial_setup(dut_handler, ref_handler, fasttest):
    if not FutMain.use_docker:
        pytest.skip('non-docker execution. Test UM only in docker environment')

    um_fw_name = um_test_config.get('um_config.fw_name', '')
    um_fw_path = f'{FutMain.workspace}/resource/um/{um_fw_name}'
    um_fw_md5_path = f'{FutMain.workspace}/resource/um/{um_fw_name}.md5'

    if not os.path.isfile(um_fw_path):
        pytest.skip('UM test FW image is missing in {}!\nSkipping UM test cases'.format(um_fw_path))

    if not os.path.isfile(um_fw_md5_path):
        assert FutMain.run('tools/rpi/um/um_create_md5_file', get_um_fw_path()) == ExpectedShellResult

    assert dut_handler.clear_tests_folder()
    assert dut_handler.transfer(manager='um')

    FutMain.recipe.clear_full()
    um_setup_res = dut_handler.run(
        'tests/um/um_setup',
        dut_handler.device_cfg.get('um_fw_download_path', '/tmp/pfirmware/')
    )
    assert um_setup_res == ExpectedShellResult
    FutMain.recipe.mark_setup()

    yield
    if not fasttest:
        dut_handler.do_log_pull_clear()


um_test_config = FutMain.get_test_config(cfg_file_prefix='UM')


def get_um_fw_url(prefix=''):
    um_fw_name = um_test_config.get('um_config.fw_name', '')
    curl_host = FutMain.testbed_cfg.get('server.curl.host', 'http://192.168.4.1:8000/')
    um_fw_url = f'{curl_host}/fut-base/resource/um/{prefix}{um_fw_name}'
    return um_fw_url


def get_um_fw_path(prefix=''):
    um_fw_name = um_test_config.get('um_config.fw_name', '')
    um_fw_path = f'{FutMain.workspace}/resource/um/{prefix}{um_fw_name}'
    return um_fw_path


def duplicate_image(prefix=''):
    um_fw_name = um_test_config.get('um_config.fw_name', '')
    um_fw_path = f'{FutMain.workspace}/resource/um/{um_fw_name}'
    um_fw_prefix_path = f'{FutMain.workspace}/resource/um/{prefix}{um_fw_name}'
    try:
        res = os.system(f'cp {um_fw_path} {um_fw_prefix_path}')
        if res != 0:
            raise Exception('Error creating duplicated image')
    except Exception as e:
        log.info(msg=e)
        return False

    return True


########################################################################################################################
test_um_set_firmware_url_inputs = um_test_config.get('um_set_firmware_url', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_um_set_firmware_url_inputs)
def test_um_set_firmware_url(test_config, dut_handler):
    um_fw_url = get_um_fw_url()
    test_cmd = get_command_arguments(
        dut_handler.device_cfg.get('um_fw_download_path') if 'fw_path' not in test_config else test_config['fw_path'],
        um_fw_url
    )
    assert dut_handler.run('tests/um/um_set_firmware_url', test_cmd) == ExpectedShellResult


########################################################################################################################
test_um_set_invalid_firmware_url_inputs = um_test_config.get('um_set_invalid_firmware_url', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_um_set_invalid_firmware_url_inputs)
def test_um_set_invalid_firmware_url(test_config, dut_handler):
    test_cmd = get_command_arguments(
        dut_handler.device_cfg.get('um_fw_download_path') if 'fw_path' not in test_config else test_config['fw_path'],
        get_um_fw_url(prefix='non_existing_fw_url_')
    )
    assert dut_handler.run('tests/um/um_set_invalid_firmware_url', test_cmd) == ExpectedShellResult


########################################################################################################################
test_um_corrupt_image_inputs = um_test_config.get('um_corrupt_image', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_um_corrupt_image_inputs)
def test_um_corrupt_image(test_config, dut_handler):
    log.info(msg='Creating corrupted unencrypted image')
    assert FutMain.run('tools/rpi/um/um_create_corrupt_image_file', get_um_fw_path()) == ExpectedShellResult
    assert FutMain.run('tools/rpi/um/um_create_md5_file', get_um_fw_path(prefix='corrupt_')) == ExpectedShellResult
    test_cmd = get_command_arguments(
        dut_handler.device_cfg.get('um_fw_download_path') if 'fw_path' not in test_config else test_config['fw_path'],
        get_um_fw_url(prefix='corrupt_')
    )
    assert dut_handler.run('tests/um/um_corrupt_image', test_cmd) == ExpectedShellResult


########################################################################################################################
test_um_corrupt_md5_sum_inputs = um_test_config.get('um_corrupt_md5_sum', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_um_corrupt_md5_sum_inputs)
def test_um_corrupt_md5_sum(test_config, dut_handler):
    md5_fw_prefix = 'corrupt_md5_sum_'
    log.info(msg='Duplicating image with prefix {}'.format(md5_fw_prefix))
    assert duplicate_image(prefix=md5_fw_prefix)
    log.info(msg='Creating corrupted md5 sum')
    corrupt_md5_res = FutMain.run('tools/rpi/um/um_create_corrupt_md5_file', get_um_fw_path(prefix=md5_fw_prefix))
    assert corrupt_md5_res == ExpectedShellResult
    test_cmd = get_command_arguments(
        dut_handler.device_cfg.get('um_fw_download_path') if 'fw_path' not in test_config else test_config['fw_path'],
        get_um_fw_url(prefix=md5_fw_prefix)
    )
    assert dut_handler.run('tests/um/um_corrupt_md5_sum', test_cmd) == ExpectedShellResult


########################################################################################################################
test_um_missing_md5_sum_inputs = um_test_config.get('um_missing_md5_sum', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_um_missing_md5_sum_inputs)
def test_um_missing_md5_sum(test_config, dut_handler):
    md5_fw_prefix = 'missing_md5_sum_'
    log.info(msg='Duplicating image with prefix {}'.format(md5_fw_prefix))
    assert duplicate_image(prefix=md5_fw_prefix)
    test_cmd = get_command_arguments(
        dut_handler.device_cfg.get('um_fw_download_path') if 'fw_path' not in test_config else test_config['fw_path'],
        get_um_fw_url(prefix=md5_fw_prefix)
    )
    assert dut_handler.run('tests/um/um_missing_md5_sum', test_cmd) == ExpectedShellResult


########################################################################################################################
test_um_set_invalid_firmware_pass_inputs = um_test_config.get('um_set_invalid_firmware_pass', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_um_set_invalid_firmware_pass_inputs)
def test_um_set_invalid_firmware_pass(test_config, dut_handler):
    fw_pass = generate_image_key() if 'fw_pass' not in test_config else test_config['fw_pass']
    test_cmd = get_command_arguments(
        dut_handler.device_cfg.get('um_fw_download_path') if 'fw_path' not in test_config else test_config['fw_path'],
        get_um_fw_url(),
        fw_pass
    )
    assert dut_handler.run('tests/um/um_set_invalid_firmware_pass', test_cmd) == ExpectedShellResult


########################################################################################################################
test_um_set_upgrade_timer_inputs = um_test_config.get('um_set_upgrade_timer', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_um_set_upgrade_timer_inputs)
def test_um_set_upgrade_timer(test_config, dut_handler):
    test_cmd = get_command_arguments(
        dut_handler.device_cfg.get('um_fw_download_path') if 'fw_path' not in test_config else test_config['fw_path'],
        get_um_fw_url(),
        test_config['fw_up_timer'],
        um_test_config.get('um_config.fw_name', '')
    )
    assert dut_handler.run('tests/um/um_set_upgrade_timer', test_cmd) == ExpectedShellResult


########################################################################################################################
test_um_set_upgrade_dl_timer_inputs = um_test_config.get('um_set_upgrade_dl_timer', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_um_set_upgrade_dl_timer_inputs)
def test_um_set_upgrade_dl_timer(test_config, dut_handler):
    test_cmd = get_command_arguments(
        dut_handler.device_cfg.get('um_fw_download_path') if 'fw_path' not in test_config else test_config['fw_path'],
        get_um_fw_url(),
        test_config['fw_dl_timer']
    )
    assert dut_handler.run('tests/um/um_set_upgrade_dl_timer', test_cmd) == ExpectedShellResult
