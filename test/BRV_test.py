from framework.tools.functions import get_command_arguments
from .globals import ExpectedShellResult
from .globals import FutMain
import pytest


@pytest.fixture(autouse=True, scope="module")
def brv_initial_setup(dut_handler):
    assert dut_handler.transfer(manager='dm')
    assert dut_handler.run('tests/dm/brv_setup') == ExpectedShellResult
    yield


# Read entire testcase configuration
brv_config = FutMain.get_test_config(cfg_file_prefix='BRV')


brv_ovs_correct_version_args = brv_config.get('brv_ovs_correct_version', [])
@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', brv_ovs_correct_version_args)
def test_brv_ovs_correct_version(test_config, dut_handler):
    test_args = get_command_arguments(test_config['ovs_ver'])
    assert dut_handler.run('tests/dm/brv_ovs_correct_version', test_args) == ExpectedShellResult


brv_is_tool_on_system_args = brv_config.get('brv_is_tool_on_system', [])
@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', brv_is_tool_on_system_args)
def test_brv_is_tool_on_system(test_config, dut_handler):
    test_args = get_command_arguments(test_config['tool'])
    assert dut_handler.run('tests/dm/brv_is_tool_on_system', test_args) == ExpectedShellResult


brv_busybox_builtins_args = brv_config.get('brv_busybox_builtins', [])
@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', brv_busybox_builtins_args)
def test_brv_busybox_builtins(test_config, dut_handler):
    test_args = get_command_arguments(test_config['tool'])
    assert dut_handler.run('tests/dm/brv_busybox_builtins', test_args) == ExpectedShellResult
