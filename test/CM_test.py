from framework.tools.functions import get_command_arguments
from .globals import ExpectedShellResult
from .globals import FutMain
import pytest
import time


@pytest.fixture(autouse=True, scope="module")
def cm2_initial_setup(dut_handler, ref_handler, fasttest):
    for handler in [dut_handler, ref_handler]:
        if not handler:
            continue
        assert handler.clear_tests_folder()
        assert handler.transfer(manager='cm2')

    FutMain.recipe.clear_full()

    if dut_handler:
        assert dut_handler.run('tests/cm2/cm2_setup', f"{dut_handler.device_cfg.get('interface_name_eth_wan')} true gw") == ExpectedShellResult
    if ref_handler:
        assert ref_handler.run('tests/cm2/cm2_setup', f"{ref_handler.device_cfg.get('interface_name_eth_wan')} false leaf") == ExpectedShellResult

    FutMain.recipe.mark_setup()

    yield
    if not fasttest:
        dut_handler.do_log_pull_clear()


cm_dut_test_config = FutMain.get_test_config(cfg_file_prefix='CM', device="dut")


########################################################################################################################
test_cm2_internet_lost_inputs = cm_dut_test_config.get('cm2_internet_lost', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_cm2_internet_lost_inputs)
def test_cm2_internet_lost(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['unreachable_internet_counter'],
        "check_counter"
    )
    get_ovsdb_res = dut_handler.run_raw('tools/device/ovsdb/get_ovsdb_value', get_command_arguments(
            'Wifi_Master_State', 'inet_addr', 'if_name', 'br-wan'
        )
    )
    dut_adr_ec, dut_adr = get_ovsdb_res[0], get_ovsdb_res[1]

    block_res = FutMain.run('tools/rpi/cm/address_internet_man', get_command_arguments(dut_adr, 'block'), as_sudo=True)
    internet_lost_res = dut_handler.run('tests/cm2/cm2_internet_lost', test_cmd)
    unblock_res = FutMain.run('tools/rpi/cm/address_internet_man', get_command_arguments(dut_adr, 'unblock'), as_sudo=True)

    assert block_res == ExpectedShellResult
    assert unblock_res == ExpectedShellResult
    assert internet_lost_res == ExpectedShellResult

    internet_recovered_res = dut_handler.run('tests/cm2/cm2_internet_lost', "0 internet_recovered")
    assert internet_recovered_res == ExpectedShellResult


########################################################################################################################
test_cm2_link_lost_inputs = cm_dut_test_config.get('cm2_link_lost', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_cm2_link_lost_inputs)
def test_cm2_link_lost(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['uplink_interface'],
        test_config['wan_interface'],
        FutMain.testbed_cfg.get('dut.wan_ip'),
    )
    assert dut_handler.run('tests/cm2/cm2_link_lost', test_cmd) == ExpectedShellResult


########################################################################################################################
test_cm2_ble_status_interface_down_inputs = cm_dut_test_config.get('cm2_ble_status_interface_down', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_cm2_ble_status_interface_down_inputs)
def test_cm2_ble_status_interface_down(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['if_name']
    )
    assert dut_handler.run('tests/cm2/cm2_ble_status_interface_down', test_cmd) == ExpectedShellResult


########################################################################################################################
test_cm2_ble_status_internet_block_inputs = cm_dut_test_config.get('cm2_ble_status_internet_block', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_cm2_ble_status_internet_block_inputs)
def test_cm2_ble_status_internet_block(test_config, dut_handler):
    dut_adr_res = dut_handler.run_raw('tools/device/ovsdb/get_ovsdb_value', get_command_arguments(
            'Wifi_Master_State', 'inet_addr', 'if_name', 'br-wan'
        )
    )
    dut_adr_ec, dut_adr = dut_adr_res[0], dut_adr_res[1]
    # Execute block at RPI
    block_res = FutMain.run('tools/rpi/cm/address_internet_man', get_command_arguments(dut_adr, 'block'), as_sudo=True)
    ble_check_res = dut_handler.run('tests/cm2/cm2_ble_status_internet_block', "internet_blocked")
    # Execute unblock at RPI
    unblock_res = FutMain.run('tools/rpi/cm/address_internet_man', get_command_arguments(dut_adr, 'unblock'), as_sudo=True)

    assert block_res == ExpectedShellResult
    assert unblock_res == ExpectedShellResult
    assert ble_check_res == ExpectedShellResult

    ble_check_recovered_res = dut_handler.run('tests/cm2/cm2_ble_status_internet_block', "internet_recovered")

    assert ble_check_recovered_res == ExpectedShellResult


########################################################################################################################
test_cm2_cloud_down_inputs = cm_dut_test_config.get('cm2_cloud_down', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_cm2_cloud_down_inputs)
def test_cm2_cloud_down(test_config, dut_handler):
    test_cmd = get_command_arguments(
        test_config['unreachable_cloud_counter'],
        "check_counter"
    )

    dut_adr_res = dut_handler.run_raw('tools/device/ovsdb/get_ovsdb_value', get_command_arguments(
            'Wifi_Master_State', 'inet_addr', 'if_name', 'br-wan'
        )
    )
    dut_adr_ec, dut_adr = dut_adr_res[0], dut_adr_res[1]
    # Execute block at RPI
    block_res = FutMain.run('tools/rpi/cm/address_internet_man', get_command_arguments(dut_adr, 'block'), as_sudo=True)
    cloud_down_res = dut_handler.run('tests/cm2/cm2_cloud_down', test_cmd)
    # Execute unblock at RPI
    unblock_res = FutMain.run('tools/rpi/cm/address_internet_man', get_command_arguments(dut_adr, 'unblock'), as_sudo=True)

    assert block_res == ExpectedShellResult
    assert unblock_res == ExpectedShellResult
    assert cloud_down_res == ExpectedShellResult

    internet_recovered_res = dut_handler.run('tests/cm2/cm2_cloud_down', "0 internet_recovered")
    assert internet_recovered_res == ExpectedShellResult


########################################################################################################################
test_cm2_dns_failure_inputs = cm_dut_test_config.get('cm2_dns_failure', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_cm2_dns_failure_inputs)
def test_cm2_dns_failure(test_config, dut_handler):
    get_ovsdb_res = dut_handler.run_raw('tools/device/ovsdb/get_ovsdb_value', get_command_arguments(
            'Wifi_Master_State', 'inet_addr', 'if_name', 'br-wan'
        )
    )
    dut_adr_ec, dut_adr = get_ovsdb_res[0], get_ovsdb_res[1]
    assert dut_adr_ec == ExpectedShellResult

    man_adr = test_config['manager_addr']

    # Execute block at RPI
    block_res = FutMain.run('tools/rpi/cm/address_internet_man', get_command_arguments(dut_adr, 'block'), as_sudo=True)
    dns_block_res = dut_handler.run('tests/cm2/cm2_dns_failure', get_command_arguments('dns_blocked', man_adr))
    # Execute unblock at RPI
    unblock_res = FutMain.run('tools/rpi/cm/address_internet_man', get_command_arguments(dut_adr, 'unblock'), as_sudo=True)

    assert block_res == ExpectedShellResult
    assert unblock_res == ExpectedShellResult
    dns_unblock_res = dut_handler.run('tests/cm2/cm2_dns_failure', get_command_arguments('dns_recovered', man_adr))
    assert dns_block_res == ExpectedShellResult
    assert dns_unblock_res == ExpectedShellResult


########################################################################################################################
test_cm2_ssl_check_inputs = cm_dut_test_config.get('cm2_ssl_check', [])


@pytest.mark.dut_only()
@pytest.mark.parametrize('test_config', test_cm2_ssl_check_inputs)
def test_cm2_ssl_check(test_config, dut_handler):
    assert dut_handler.run('tests/cm2/cm2_ssl_check') == ExpectedShellResult


########################################################################################################################
test_cm2_verify_gre_tunnel_dut_gw_inputs_dut = cm_dut_test_config.get('cm2_verify_gre_tunnel_dut_gw', [])


@pytest.mark.require_ref
@pytest.mark.parametrize('test_config', test_cm2_verify_gre_tunnel_dut_gw_inputs_dut)
def test_cm2_verify_gre_tunnel_dut_gw(test_config, dut_handler, ref_handler):
    # First, prepare REF to run independently. This is needed for radio MAC
    assert ref_handler.run('tools/device/default_setup') == ExpectedShellResult
    # Get all input arguments
    dut_device_cfg = FutMain.dut['device_cfg']
    ref_device_cfg = FutMain.ref['device_cfg']

    # Determine radio bands for both devices
    def get_radio_band_from_channel(channel, radio_channels):
        for band in radio_channels:
            channels = radio_channels[band]
            if channels is not None and channel in channels:
                return band
        return None

    channel = test_config["gw_radio_channel"]
    gw_radio_band = get_radio_band_from_channel(channel, dut_device_cfg.get('radio_channels'))
    assert gw_radio_band is not None
    leaf_radio_band = get_radio_band_from_channel(channel, ref_device_cfg.get('radio_channels'))
    assert leaf_radio_band is not None

    # Determine LEAF radio MAC remotely at runtime, to whitelist on GW
    leaf_radio_if = ref_device_cfg.get(f'phy_radio_name.{leaf_radio_band}')
    mac_if_clause = f'if_name=={leaf_radio_if}'
    get_ovsdb_res = ref_handler.run_raw('tools/device/get_radio_mac_from_ovsdb', get_command_arguments(mac_if_clause))
    mac_res, leaf_radio_mac_raw = get_ovsdb_res[0], get_ovsdb_res[1]
    assert mac_res == ExpectedShellResult
    assert leaf_radio_mac_raw != '' and leaf_radio_mac_raw is not None

    dut_args = get_command_arguments(
        dut_device_cfg.get('interface_name_eth_wan'),
        dut_device_cfg.get(f'backhaul_ap.{gw_radio_band}'),
        test_config['gw_bhaul_vif_radio_idx'],
        dut_device_cfg.get(f'phy_radio_name.{gw_radio_band}'),
        channel,
        test_config['gw_radio_ht_mode'],
        test_config['gw_radio_hw_mode'],
        dut_device_cfg.get('mtu_wan'),
        dut_device_cfg.get('mtu_uplink_gre'),
        dut_device_cfg.get('mtu_backhaul'),
        leaf_radio_mac_raw,
        FutMain.testbed_cfg.get('ref.wan_ip'),
        test_config['bhaul_psk'],
        test_config['bhaul_ssid'],
    )

    ref_args = get_command_arguments(
        ref_device_cfg.get(f'backhaul_sta.{leaf_radio_band}'),
        ref_device_cfg.get('interface_name_wan_bridge'),
        test_config['bhaul_psk'],
        test_config['bhaul_ssid'],
    )

    # Execute the tests
    def run_ref():
        return ref_handler.run('tests/cm2/cm2_verify_gre_tunnel_ref_extender', ref_args)

    def run_dut():
        return dut_handler.run('tests/cm2/cm2_verify_gre_tunnel_dut_gw', dut_args)

    dut_res, ref_res = FutMain.async_execution([run_dut, run_ref])

    assert dut_res == ExpectedShellResult
    assert ref_res == ExpectedShellResult
