from framework.tools.functions import get_command_arguments
from .globals import ExpectedShellResult
from .globals import FutMain
import pytest


@pytest.fixture(autouse=True, scope="module")
def manager_folder_transfer(dut_handler, fasttest):
    assert dut_handler.clear_tests_folder()
    assert dut_handler.transfer(manager='hello_world')
    FutMain.recipe.clear_full()
    assert dut_handler.run('tests/hello_world/hello_world_setup') == ExpectedShellResult
    FutMain.recipe.mark_setup()
    yield


# Read testcase configuration
hello_world_config = FutMain.get_test_config(cfg_file_prefix='hello_world')


########################################################################################################################
test_hello_world_insert_module_key_value_inputs = hello_world_config.get('hello_world_insert_module_key_value', [])


@pytest.mark.parametrize('test_config', test_hello_world_insert_module_key_value_inputs)
@pytest.mark.dut_only()
def test_hello_world_insert_module_key_value(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['kv_key'],
        test_config['kv_value'],
    )
    assert dut_handler.run('tests/hello_world/hello_world_insert_module_key_value', test_args) == ExpectedShellResult


########################################################################################################################
test_hello_world_update_module_key_value_inputs = hello_world_config.get('hello_world_update_module_key_value', [])


@pytest.mark.parametrize('test_config', test_hello_world_update_module_key_value_inputs)
@pytest.mark.dut_only()
def test_hello_world_update_module_key_value(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['kv_key'],
        test_config['kv_value'],
        test_config['kv_changed_value'],
    )
    assert dut_handler.run('tests/hello_world/hello_world_update_module_key_value', test_args) == ExpectedShellResult


########################################################################################################################
test_hello_world_insert_demo_fail_to_update_inputs = hello_world_config.get(
    'hello_world_insert_demo_fail_to_update', [])


@pytest.mark.parametrize('test_config', test_hello_world_insert_demo_fail_to_update_inputs)
@pytest.mark.dut_only()
def test_hello_world_insert_demo_fail_to_update(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['kv_key'],
        test_config['kv_value'],
    )
    try:
        expect_to_pass = test_config['expect_to_pass']
    except KeyError:
        expect_to_pass = True
    test_result = dut_handler.run('tests/hello_world/hello_world_insert_demo_fail_to_update', test_args)
    compare_result = (test_result == ExpectedShellResult)
    assert compare_result == expect_to_pass


########################################################################################################################
test_hello_world_insert_demo_fail_to_write_inputs = hello_world_config.get('hello_world_insert_demo_fail_to_write', [])


@pytest.mark.parametrize('test_config', test_hello_world_insert_demo_fail_to_write_inputs)
@pytest.mark.dut_only()
def test_hello_world_insert_demo_fail_to_write(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['kv_key'],
        test_config['kv_value'],
    )
    try:
        expect_to_pass = test_config['expect_to_pass']
    except KeyError:
        expect_to_pass = True
    test_result = dut_handler.run('tests/hello_world/hello_world_insert_demo_fail_to_write', test_args)
    compare_result = (test_result == ExpectedShellResult)
    assert compare_result == expect_to_pass


########################################################################################################################
test_hello_world_insert_foreign_module_inputs = hello_world_config.get('hello_world_insert_foreign_module', [])


@pytest.mark.parametrize('test_config', test_hello_world_insert_foreign_module_inputs)
@pytest.mark.dut_only()
def test_hello_world_insert_foreign_module(test_config, dut_handler):
    test_args = get_command_arguments(
        test_config['kv_key'],
        test_config['kv_value'],
        test_config['module_name'],
    )
    assert dut_handler.run('tests/hello_world/hello_world_insert_foreign_module', test_args) == ExpectedShellResult
