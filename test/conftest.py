from framework.lib.fut_allure import FutAllureClass
from .globals import FutMain
from .globals import established_connection_handlers_var
from .globals import dut_handler_var
from .globals import ref_handler_var
from .globals import rpi_handler_var
from pathlib import Path
import framework.tools.logger
import allure
import os
import pytest
import re
import time

_devices = ['dut', 'ref', 'rpi_client', 'gw']
_devices_default = ['dut', 'ref']
report_map = {
    'test_name': None,
    'group': {
        'FRM': [],
        'SHELL': [],
        'ERROR': [],
        'DEBUG': [],
        'LOGREAD': [],
        'TRACEBACK': [],
        'OTHER': [],
    }
}


def pytest_addoption(parser):
    # collection only
    group = parser.getgroup(name="collect")
    # pytest-native '--collect-only' option collects tests without execution
    group.addoption('--listtests', '--list-tests', action="store_true",
                    help="collected tests with configuration entry")
    group.addoption('--listconfigs', '--list-configs', action="store_true",
                    help="collected tests, parametrized with all configuration entries")

    # running and selection options
    group = parser.getgroup(name="general")
    group.addoption('--runtest', '--run-test', action="store", default=False, nargs="+",
                    help="run a subset of collected and configured tests")
    group.addoption('--transferonly', '--transfer-only', action="store_true", default=False,
                    help="transfer files to devices without test execution")
    group.addoption('--fasttest', '--fast-test', action="store_true", default=False,
                    help="skip test session setup step")
    group.addoption('--devices', action="store",
                    nargs="+", default=_devices_default, choices=_devices,
                    help="force which devices to use for testing")
    parser.addoption('--dbg', action="store_true",
                     help="Increase logging level to debug and show extra info")


@pytest.hookimpl(tryfirst=True)
def pytest_configure(config):
    global log
    log = framework.tools.logger.get_logger()
    log.debug(msg='Entered conftest.py')

    config.addinivalue_line(
        "markers", "dut_only: mark test that require only dut device"
    )
    config.addinivalue_line(
        "markers", "require_ref: mark test that require ref device"
    )
    config.addinivalue_line(
        "markers", "require_client: mark test that require rpi client device"
    )
    config.addinivalue_line(
        "markers", "dfs: mark test that uses dfs channels"
    )

    if config.option.allure_report_dir:
        log.debug(f'config.option.allure_report_dir:{config.option.allure_report_dir}')
        if not os.path.isabs(config.option.allure_report_dir):
            log.warning(msg='pytest.conftest.pytest_configure: Assumming path is relative to workspace')
            config.option.allure_report_dir = str(Path(FutMain.workspace).joinpath(config.option.allure_report_dir))


def pytest_collection_modifyitems(session, config, items):
    testcase_names = FutMain.get_test_configurations(list_test_only=True)
    tr = config.pluginmanager.get_plugin('terminalreporter')

    if config.getoption('listtests'):
        log.debug(msg=f'List collected testcases for selected configuration')
        tr.write_line('Available test cases:', bold=True)
        for tc in testcase_names:
            tr.write_line(f'\t{tc}')
        pytest.exit("Listed available test cases.")

    if config.getoption('listconfigs'):
        log.debug(msg=f'List available configurations for selected model')
        tr.write_line('Available test configurations:', bold=True)
        testcase_configs = [tc.name for tc in items if tc.name.split('[')[0] in testcase_names]
        for tc in testcase_configs:
            tr.write_line(f'\t{tc}')
        pytest.exit("Listed available test configurations.")

    log.debug(msg='Retrieving test configurations')
    list_of_test_case_configurations = FutMain.get_test_configurations()
    run_only_test = config.getoption("runtest")

    tmp_items = items.copy()
    items.clear()
    for item in tmp_items:
        test_case_configuration = list(
            filter(
                lambda tc_cfg_to_check: tc_cfg_to_check['name'] == item.name,
                list_of_test_case_configurations
            )
        )
        if not test_case_configuration:
            test_case_configuration = list(
                filter(
                    lambda tc_cfg_to_check: tc_cfg_to_check['name'] == item.name.split('[')[0],
                    list_of_test_case_configurations
                )
            )

        if test_case_configuration:
            if run_only_test is not False:  # Do not optimize this line
                if item.name.split('[')[0] in run_only_test or item.name in run_only_test:
                    log.debug(msg=f'appending {item.name} to list of tests for execution')
                    items.append(item)
            else:
                if test_case_configuration[0]["skip"]:
                    skip_marker = pytest.mark.skip(reason=test_case_configuration[0]["skip_msg"])
                    log.debug(msg=f'marking {item.name} as skipped')
                    item.add_marker(skip_marker)
                log.debug(msg=f'appending {item.name} to list of tests for execution')
                items.append(item)


def pytest_itemcollected(item):
    """ Upon test function collection, remove test_ string from name """
    log.debug(msg=f'{item.name}')
    item.name = item.name.replace('test_', '')
    if re.search(r'(\[config)([0-9]+)(])', item.name):
        item.name = item.name.replace('[config', '[config_')


def pytest_report_collectionfinish(config, startdir, items):
    log.debug(msg=f'{len(items)} items')
    tr = config.pluginmanager.get_plugin('terminalreporter')
    tr.write_line(f"Pytest collection finished {len(items)} items", bold=True)


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item, call):
    log.debug(msg=f'{item.name} {call}')
    global report_map
    # yield to other hooks, to obtain _pytest.runner.TestReport object
    report = (yield).get_result()
    log.debug(msg=f'{item.name} report available')
    # There are three reporting phases: "setup", "call", "teardown"
    if report.when != 'call':
        return  # only postprocess on "call"

    if report_map['test_name'] != item.name:
        log.debug(msg=f'Initializing report map for test {item.name}')
        report_map = {
            'test_name': item.name,
            'group': {
                'FRM': [],
                'SHELL': [],
                'ERROR': [],
                'DEBUG': [],
                'LOGREAD': [],
                'OTHER': [],
                'RECIPE': [],
            }
        }

    log_lines = report.caplog.split('\n')
    err_lines = report.capstderr.split('\n')
    std_lines = report.capstdout.split('\n')

    log.debug(msg='Categorizing report logs by group')
    # 1) merge all sources: log, stdout, stderr
    all_lines = log_lines + std_lines + err_lines
    # 2) remove empty strings
    all_lines = list(filter(lambda x: x != '', all_lines))
    # 3) iterate over each line
    for line in all_lines:
        # 4) Decide into which group it belongs
        if '[FRM]' in line:
            report_map['group']['FRM'].append(line)
        elif '[SHELL]' in line:
            report_map['group']['SHELL'].append(line)
        elif '[ERROR]' in line:
            report_map['group']['ERROR'].append(line)
        elif '[DEBUG]' in line:
            report_map['group']['DEBUG'].append(line)
        else:
            report_map['group']['DEBUG'].append(line)

    if report.failed:
        log.debug(msg=f'Test {item.name} failed, generating verbose log groups')
        try:
            report_map['group']['RECIPE'] = FutMain.recipe.get_for_allure()
        except Exception as t_e:
            report_map['group']['RECIPE'] = [f'Could not generate test recipe\n{t_e}']
            log.error(msg=t_e)

        for test_handler in established_connection_handlers_var:
            if test_handler.name != 'dut':
                continue
            try:
                report_map['group']['LOGREAD'] = test_handler.get_log_read().splitlines()
            except Exception as l_e:
                log.error(msg=f'Error while generating log_read dump')
                log.error(msg=l_e)

    log.debug(msg='Attaching reports to allure')
    for group in report_map['group'].keys():
        try:
            body = "\n".join(report_map['group'][group])
            allure.attach(
                name=group,
                body=body
            )
        except Exception as e:
            log.error(msg=f'Exception attaching {item.name} log {group} to allure')
            log.error(msg=e)


@pytest.hookimpl(tryfirst=True)
def pytest_ignore_collect(config):
    active_testbed_devices = config.getoption("devices")
    if config.getoption("transferonly"):
        FutMain.initialize_fut_environment(active_testbed_devices, {"transferonly": True})
        pytest.exit('File transfer to devices complete.', pytest.ExitCode.OK)


@pytest.fixture(scope="module")
def fasttest(pytestconfig):
    return pytestconfig.getoption("fasttest")


@pytest.fixture(scope='module')
def dut_handler():
    return dut_handler_var


@pytest.fixture(scope='module')
def ref_handler():
    return ref_handler_var


@pytest.fixture(scope='module')
def rpi_handler():
    return rpi_handler_var


@pytest.fixture(scope='module')
def established_connection_handlers():
    return established_connection_handlers_var


@pytest.fixture()
def transferonly(pytestconfig):
    return pytestconfig.getoption("transferonly")


@pytest.fixture()
def runtest(pytestconfig):
    return pytestconfig.getoption("runtest")


@pytest.fixture()
def listtests(pytestconfig):
    return pytestconfig.getoption("listtests")


@pytest.fixture()
def devices(pytestconfig):
    return pytestconfig.getoption("devices")


@pytest.fixture
def dbg(pytestconfig):
    return pytestconfig.getoption("dbg")


@pytest.fixture(scope='session', autouse=True)
def session_fixture(pytestconfig):
    log.debug(msg='entered session')
    global dut_handler_var, ref_handler_var, rpi_handler_var, established_connection_handlers_var

    active_testbed_devices = pytestconfig.getoption("devices")

    if pytestconfig.getoption("transferonly"):
        FutMain.initialize_fut_environment(active_testbed_devices, {"transferonly": True})
        pytest.exit('File transfer to devices complete.')

    try:
        fasttest_option = pytestconfig.getoption("fasttest")
        fut_config = {"fasttest": fasttest_option}
        # Testbed validaiton
        if not fasttest_option:
            log.debug(msg='Performing testbed validation')
            # FutMain.testbed_validation(active_testbed_devices)

        if len(active_testbed_devices) == 1:
            if 'dut' in active_testbed_devices:
                dut_handler_var = FutMain.initialize_fut_environment(active_testbed_devices, fut_config)
                established_connection_handlers_var.append(dut_handler_var)
            elif 'ref' in active_testbed_devices:
                ref_handler_var = FutMain.initialize_fut_environment(active_testbed_devices, fut_config)
                established_connection_handlers_var.append(ref_handler_var)
            elif 'rpi_client' in active_testbed_devices:
                rpi_handler_var = FutMain.initialize_fut_environment(active_testbed_devices, fut_config)
                established_connection_handlers_var.append(rpi_handler_var)
        elif len(active_testbed_devices) == 2:
            if 'dut' in active_testbed_devices and 'ref' in active_testbed_devices:
                dut_handler_var, ref_handler_var = FutMain.initialize_fut_environment(
                    active_testbed_devices, fut_config)
                established_connection_handlers_var.append(dut_handler_var)
                established_connection_handlers_var.append(ref_handler_var)
            elif 'dut' in active_testbed_devices and 'rpi_client' in active_testbed_devices:
                dut_handler_var, rpi_handler_var = FutMain.initialize_fut_environment(
                    active_testbed_devices, fut_config)
                established_connection_handlers_var.append(dut_handler_var)
                established_connection_handlers_var.append(rpi_handler_var)
            elif 'ref' in active_testbed_devices and 'rpi_client' in active_testbed_devices:
                dut_handler_var, rpi_handler_var = FutMain.initialize_fut_environment(
                    active_testbed_devices, fut_config)
                established_connection_handlers_var.append(ref_handler_var)
                established_connection_handlers_var.append(rpi_handler_var)
        elif len(active_testbed_devices) == 3:
            dut_handler_var, ref_handler_var, rpi_handler_var = FutMain.initialize_fut_environment(
                active_testbed_devices, fut_config)
            established_connection_handlers_var.append(dut_handler_var)
            established_connection_handlers_var.append(ref_handler_var)
            established_connection_handlers_var.append(rpi_handler_var)
        else:
            msg = 'Invalid configuration of active testbed devices'
            log.critical(msg=msg)
            pytest.exit(msg)

        allure_dir_option = pytestconfig.getoption('allure_report_dir')
        if allure_dir_option:
            f_allure = FutAllureClass(allure_dir_option)
            f_allure.set_global_environment()
            f_allure.set_device_versions(established_connection_handlers_var)
    except Exception as e:
        log.exception(msg=str(e))
        pytest.exit(str(e))
    yield


@pytest.fixture(autouse=True)
def function_fixture(request):
    log.debug(msg='entered function fixture')
    FutMain.recipe.clear()
    if hasattr(request.node, 'callspec'):
        if request.node.callspec.params is not None:
            if 'test_config' in request.node.callspec.params:
                test_configuration = request.node.callspec.params['test_config']

                if "skip" in test_configuration and test_configuration['skip'] is True:
                    skip_msg = 'Test skipped.'
                    if 'skip_msg' in test_configuration:
                        skip_msg = test_configuration['skip_msg']
                    pytest.skip(msg=skip_msg)

                if "xfail" in test_configuration and test_configuration['xfail'] is True:
                    request.node.add_marker(
                        pytest.mark.xfail(
                            reason='Known FW bug, expected to fail' if 'xfail_msg' not in test_configuration else
                            test_configuration['xfail_msg']))

                timeout = False
                try:
                    timeout = int(test_configuration['test_script_timeout'])
                except KeyError:
                    pass
                except Exception as e:
                    log.exception(msg=e)

                py_test_timeout = FutMain.framework_cfg.get("py_test_timeout", 60)
                request.node.add_marker(pytest.mark.timeout(py_test_timeout if not timeout else timeout + 30))

                for test_handler in established_connection_handlers_var:
                    test_handler.update_shell_timeout(timeout)

            else:
                log.warning('Function lacks "test_config" input parameter!')


def pytest_sessionfinish(session, exitstatus):
    framework.tools.logger.root_logger.debug(msg='session finished')
    _print_versions_for_jenkins_job_descriptions(session)


# def pytest_assertrepr_compare(config, op, left, right):  # This declaration is correct according to pytest-4.3.1 doc
def pytest_assertrepr_compare(op, left, right):
    log.debug(msg=f'compare {left} {op} {right}')
    if (isinstance(left, int) and left == 3) or (isinstance(left, str) and left == 'skip'):
        pytest.skip('CHANNEL not available for use')


def _print_versions_for_jenkins_job_descriptions(session):
    framework.tools.logger.root_logger.info(msg='Generating Jenkins HTML job descriptor')
    tr = session.config.pluginmanager.get_plugin('terminalreporter')
    duration = time.strftime('%H:%M:%S', time.gmtime(time.time() - tr._sessionstarttime))
    passed = 0
    if 'passed' in tr.stats:
        passed = len(tr.stats['passed'])
    failed = session.testsfailed
    collected = session.testscollected

    # This string acts as token for setting build description - match with Jenkins config!
    jenkins_descriptor = os.getenv('FUT_JENKINS_DESCRIPTOR')
    if jenkins_descriptor is None:
        jenkins_descriptor = '[FutJenkinsBuildDescription]'

    dut_fw_version = None
    if dut_handler_var:
        dut_fw_version = dut_handler_var.get_version().split('-g')[0]

    dut_cfg_folder = os.getenv('DUT_CFG_FOLDER')

    fut_git = os.getenv('GIT_BRANCH')
    if fut_git is None or 'detached' in fut_git:
        fut_git = os.getenv('GIT_COMMIT')
        if fut_git is not None:
            fut_git = fut_git[:7]
    if isinstance(fut_git, str):
        if fut_git.startswith('origin/'):
            fut_git = fut_git[len('origin/'):]

    html_fail = f'<font color="red">fail:{failed}</font>'
    html_pass = f'<font color="green">pass:{passed}</font>'
    html_all = f'<font color="black">all:{collected}</font>'
    html_fw_ver = f'{dut_fw_version}'
    html_dut_cfg_folder = f'{dut_cfg_folder}'
    html_git_id = f'<font color="magenta">{fut_git}</font>'
    html_time = f'{duration}'

    html_jenkins_description = '{}{} {} {}<br>{} / {}<br>{}<br>{}{}'.format(
        jenkins_descriptor,
        html_fail,
        html_pass,
        html_all,
        html_fw_ver,
        html_dut_cfg_folder,
        html_git_id,
        html_time,
        jenkins_descriptor
    )
    tr.write_sep(sep="#")
    # Contain description string to one line, use print for clean string output
    print(html_jenkins_description.replace('\n', ''))  # Do not use log here
